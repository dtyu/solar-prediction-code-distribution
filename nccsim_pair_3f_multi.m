% This function is trying to find the best high altitude cloud using
%   It starts from startp on TSI3, gets recSize rectangle area as reference area. It will search the
%   whole area to find the best H using nccsumall
%   Previous version test_ncc3d_tsi32.m
%       fomula:     similarity = ncc31_21+ncc31_11+ncc_21_22+ncc_31_32+ncc_11_12
%       
%   INPUT:
%       datevector_obj      --- obj of current and next frame time
%       TSI3StartPt         --- 2x1 double, [starty startx]
%       TSI3RecSize         --- 2x1 double, [recH recW]
%       MVWinSize           --- Int, Search MV in [y-MVWinSize:y+MVWinSize,x-MVWinSize:x+MVWinSize]
%       dataDir             --- String, TSI mat Root directory
%       HSearchRange        --- Nx1 double, H range for searching
%       NCCMiniThres        --- double, Min ncc for single ncc operation.Smaller than it equals 0
%       TSI3BlkMask         --- [UNDISTH,UNDISTW] boolean, TSI3BlkMask

%   OUTPUT:
%       H               --- Most possible height
%       MV              --- [MV_y MV_x]
%       HMV             --- [HMV32_y HMV32_x;HMV31_y HMV31_x]
%       cc_all          --- Dim=[Hcc,Wcc,H_l]
%
%   Version 2.0:        2013/10/07  Use 3 frames (9 images)
%   Version 3.0:        2013/12/18
%   Version 3.1:        2014/03/27  Improve the detection speed
%   Version 3.2:        2014/04/15  Modify the code motion vector extractions
%   Version 3.3:        2014/04/29  
%       Modify the main algorithm based on Shinjae's suggestion: do clustering first to get 2
%       layers motion vectors.
%                           

function nccsim_pair_3f_multi(inputmatf,outputmatf)
%% load input
t=load(inputmatf);
tmdv=t.tmdv;
nextframe_dn=t.nextframe_dn;
TSI3StartPt=t.TSI3StartPt;
TSI3RecSize=t.TSI3RecSize;
MVWinSize=t.MVWinSize;
MVRef=t.MVRef;
dataDir=t.dataDir;
HSearchRange=t.HSearchRange;
NCCMiniThres=t.NCCMiniThres;
TSI3BlkMask=t.TSI3BlkMask;
CLDFRACTHRES=t.CLDFRACTHRES;
configfpath=t.configfpath;


%% Main



%mv_x=0;mv_y=0;H=0;HMV_x=0;HMV_y=0;maxcc=0;cc_all=0;
%mtsi_startup;
run(configfpath);
% CLDFRACTHRES=0.3;
INVALIDPIXELCOUNT=0.3;
MVWARNINGTHRES=0.4;

%[tmdv tmdv_next tmdn tmdn_next]=datevectorparser(datevector_obj);
tmdn=datenum(tmdv);
tmdv_next=datevec(tmdn+nextframe_dn);
tmdv_next2=datevec(tmdn+nextframe_dn*2);


recH=TSI3RecSize(1);
recW=TSI3RecSize(2);
undistp31=TSI3StartPt;
uy31=undistp31(1);
ux31=undistp31(2);


t=load(HeightMatrixValidMatf);
hma32_valid=t.H_valid_32;
HMV32_x=t.HMVx_valid_32;
HMV32_y=t.HMVy_valid_32;
hma31_valid=t.H_valid_31;
HMV31_x=t.HMVx_valid_31;
HMV31_y=t.HMVy_valid_31;
H_range_full=t.H_range_full;
H_l=length(HSearchRange);
full_valid=zeros(H_l,1);
H_range_full=int32(H_range_full);
H_range=int32(HSearchRange);
for tmi=1:H_l
    ind=find(H_range_full==H_range(tmi),1);
    try
        full_valid(tmi)=ind;
    catch err
        err.message
    end
end

hma32_valid=hma32_valid(full_valid);
HMV32_x=HMV32_x(full_valid);
HMV32_y=HMV32_y(full_valid);
hma31_valid=hma31_valid(full_valid);
HMV31_x=HMV31_x(full_valid);
HMV31_y=HMV31_y(full_valid);


uy32=uy31-MVWinSize;
ux32=ux31-MVWinSize;
%SearchWinSize=SearchWinSize_ext*2+1;
uy31_e=uy31+recH-1;
ux31_e=ux31+recW-1;
uy32_e=uy31_e+MVWinSize;
ux32_e=ux31_e+MVWinSize;
recH_search=uy32_e-uy32+1;
recW_search=ux32_e-ux32+1;
maxRecSize_search=max([recH_search recW_search]);
imma21=TO_rgb2lum(tmp_GetImgMatFromDataDir(tmdv,TSI2,dataDir));
imma31=TO_rgb2lum(tmp_GetImgMatFromDataDir(tmdv,TSI3,dataDir));
imma22=TO_rgb2lum(tmp_GetImgMatFromDataDir(tmdv_next,TSI2,dataDir));
imma32=TO_rgb2lum(tmp_GetImgMatFromDataDir(tmdv_next,TSI3,dataDir));
imma23=TO_rgb2lum(tmp_GetImgMatFromDataDir(tmdv_next2,TSI2,dataDir));
imma33=TO_rgb2lum(tmp_GetImgMatFromDataDir(tmdv_next2,TSI3,dataDir));
invalidma21=(imma21~=0);    invalidma22=(imma22~=0);    invalidma23=(imma23~=0);
invalidma31=(imma31~=0);    invalidma32=(imma32~=0);    invalidma33=(imma33~=0);



[~,imma21_cldma,imma31_cldma]=clouddetector(tmdv,'model_rbr.mat',dataDir,configfpath);
blk31=single(TO_GetImgVal_Pad([uy31 ux31],[recH recW],imma31));
blk31_invalid=logical(TO_GetImgVal_Pad([uy31 ux31],[recH recW],invalidma31));
blk31_invalid=~blk31_invalid;
invalidc31=length(find(blk31_invalid))/(recH*recW);
if invalidc31>INVALIDPIXELCOUNT
	dispstr=sprintf('CF on TSI31 is not trusted for reference search since its INVALID points percentage %f is more than %f.',invalidc31,INVALIDPIXELCOUNT);
    disp(dispstr);
    H=[];   MV=[];  HMV=[];
    cc_all=[];maxcc=0;HCredCounts=0;bCloseNeighbor=false;
    save(outputmatf,'H','MV','HMV','cc_all','maxcc','HCredCounts','bCloseNeighbor');
    return;
end

if 0==std(blk31)
    bSTDInvalid_31=true;
else
    bSTDInvalid_31=false;
end
blk31_mask=TO_GetImgVal_Pad([uy31 ux31],[recH recW],TSI3BlkMask);
blk31(~blk31_mask)=0;  % invalid false=0
% blk31_cld=logical(TO_GetImgVal_Pad([uy31 ux31],[recH recW],imma31_cldma));
% cf31=length(find(blk31_cld))/(recH*recW);
%blk32_search=single(TO_GetImgVal_Pad([uy32 ux32],[recH_search recW_search],imma32));


%blk22_search=imma22(uy22:uy22_e,ux22:ux22_e);
% if 0~=std(blk31(:)) && 0~=std(blk32_search(:))
%     cc=normxcorr2_blk(blk31,blk32_search);
%     cc(cc<NCCMiniThres|isnan(cc))=0;
% else
%     cc=zeros(recH+recH_search-1,recW+recW_search-1);
% end

% [Hcc,Wcc]=size(cc);
% cc_all=zeros(Hcc,Wcc,H_l);
% sim_hrange=zeros(H_l,1);


possibleMVX3=-MVWinSize:1:MVWinSize;
possibleMVY3=-MVWinSize:1:MVWinSize;
possibleMVX_N=length(possibleMVX3);
possibleMVY_N=length(possibleMVY3);
mv_ref=true(possibleMVY_N,possibleMVX_N);
refmvx=MVRef(1);    refmvy=MVRef(2);
MVDIST=5;
bMVRef=false;
if refmvx~=0 || refmvy~=0
    bMVRef=true;
    for tmvy=1:possibleMVY_N
        mvy3=possibleMVY3(tmvy);
        for tmvx=1:possibleMVX_N
            mvx3=possibleMVX3(tmvx);
            if sqrt((mvy3-refmvy)^2+(mvx3-refmvx)^2) > MVDIST
                mv_ref(tmvy,tmvx)=false;
            end
        end
    end
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   1. Generate TSI3 motion vector VALID mask
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TODO: High H cloud, do minute motion checking
Credmv_all=zeros(possibleMVY_N,possibleMVX_N);
for tmvy=1:possibleMVY_N
    mvy3=possibleMVY3(tmvy);
    for tmvx=1:possibleMVX_N
        mvx3=possibleMVX3(tmvx);
        if false==mv_ref(tmvy,tmvx)
            continue;
        end
        %         if mvx3==10 && mvy3==-10
        %             tmvy
        %         end
        ux32=ux31+mvx3;
        uy32=uy31+mvy3;
        ux33=ux31+2*mvx3;
        uy33=uy31+2*mvy3;
        
        
        blk32_invalid=logical(TO_GetImgVal_Pad([uy32 ux32],[recH recW],invalidma32));
        blk33_invalid=logical(TO_GetImgVal_Pad([uy33 ux33],[recH recW],invalidma33));
        blk32_invalid=~blk32_invalid;
        blk33_invalid=~blk33_invalid;
        
        % Check invalid pixel counts
        
        invalidc32=length(find(blk32_invalid))/(recH*recW);
        invalidc33=length(find(blk33_invalid))/(recH*recW);
        if (invalidc31>INVALIDPIXELCOUNT) || (invalidc32>INVALIDPIXELCOUNT) || ...
                (invalidc33>INVALIDPIXELCOUNT)
            Credmv_all(tmvy,tmvx)=0; 
        %elseif 0==invalidc31 && 0==invalidc32 && 0==invalidc33
        elseif invalidc31<=INVALIDPIXELCOUNT && invalidc32<=INVALIDPIXELCOUNT && ...
                invalidc33<=INVALIDPIXELCOUNT
            Credmv_all(tmvy,tmvx)=2;
        else
            Credmv_all(tmvy,tmvx)=1;
        end
    end
end

if isempty(find(Credmv_all==2,1))
    disp('Motion vectors on TSI3 are not valid in ALL directions and range');
    H=[];   MV=[];  HMV=[];
    cc_all=[];maxcc=0;HCredCounts=0;bCloseNeighbor=false;
    save(outputmatf,'H','MV','HMV','cc_all','maxcc','HCredCounts','bCloseNeighbor');
    return;
end




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   2. Calculate TSI3 3 consecutive frames similarity
%       If motion vector is not valid then skip this similarity calculation
%       If 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TODO: Should be a function. Call it using larger range when get invalid result

% Calculate the motion vector similarities of multiple frames on TSI3
MVValid=(Credmv_all==2);        % if 3 frames are ALL valid then treat it as valid

cc_mv=zeros(possibleMVY_N,possibleMVX_N);

% calculate possible searching range 
sx32=ux31-MVWinSize;
sy32=uy31-MVWinSize;
ex32=ux31+recW-1+MVWinSize;
ey32=uy31+recH-1+MVWinSize;
sx33=ux31-2*MVWinSize;
sy33=uy31-2*MVWinSize;
ex33=ux31+recW-1+2*MVWinSize;
ey33=uy31+recH-1+2*MVWinSize;

blk32_search=single(TO_GetImgVal_Pad([sy32 sx32],[ey32-sy32+1 ex32-sx32+1],imma32));
blk33_search=single(TO_GetImgVal_Pad([sy33 sx33],[ey33-sy33+1 ex33-sx33+1],imma33));

cc_31_32=normxcorr2_blk(blk31,blk32_search);
cc_31_33=normxcorr2_blk(blk31,blk33_search);
% coordinate conversion: from cc to motion vectors
cc_mv1=cc_31_32(recH+1:recH+possibleMVY_N,recW+1:recW+possibleMVX_N);
cc_mv2=cc_31_33(recH+1:recH+possibleMVY_N*2,recW+1:recW+possibleMVX_N*2);
cc_mv2_scale=zeros(possibleMVY_N,possibleMVX_N);
for tmvy=1:possibleMVY_N
    for tmvx=1:possibleMVX_N
        cc_mv2_scale(tmvy,tmvx)=cc_mv2(tmvy*2,tmvx*2);
    end
end
cc_mv=cc_mv1+cc_mv2_scale;


% for tmvy=1:possibleMVY_N
%     mvy3=possibleMVY3(tmvy);
%     for tmvx=1:possibleMVX_N
%         mvx3=possibleMVX3(tmvx);
%         if ~MVValid(tmvy,tmvx)
%             continue;
%         end
%         ux32=ux31+mvx3;
%         uy32=uy31+mvy3;
%         ux33=ux31+2*mvx3;
%         uy33=uy31+2*mvy3;
%         
%         
%         % TSI similarities
%         %   (TSI31 TSI32) + (TSI32 TSI33)
%         blk32=single(TO_GetImgVal_Pad([uy32 ux32],[recH recW],imma32));
%         blk33=single(TO_GetImgVal_Pad([uy33 ux33],[recH recW],imma33));        
%         if (0==std(blk32))    bSTDInvalid_32=true;
%         else                bSTDInvalid_32=false;
%         end
%         if (0==std(blk33))    bSTDInvalid_33=true;
%         else                bSTDInvalid_33=false;
%         end
%         
%         if bSTDInvalid_31 || bSTDInvalid_32
%             sim31_32=0;
%         else
%             cc_31_32=normxcorr2_blk(blk31,blk32);
%             sim31_32=cc_31_32(recH,recW);
%             if sim31_32<NCCMiniThres
%                 sim31_32=0;
%             end
%         end
%         
%         if bSTDInvalid_32 || bSTDInvalid_33
%             sim32_33=0;
%         else
%             cc_32_33=normxcorr2_blk(blk32,blk33);
%             sim32_33=cc_32_33(recH,recW);
%             if sim32_33<NCCMiniThres
%                 sim32_33=0;
%             end
%         end
%         
%         sim_tsi3=sim31_32+sim32_33;
%         if sim_tsi3<2*NCCMiniThres
%             %disp('simiarity check got 0 when used this motion vector!');
%             continue;
%         end
%         cc_mv(tmvy,tmvx)=sim_tsi3;
%     end
% end

if isempty(find(cc_mv~=0,1))
    disp('Condt.1: All motion vectors are not matched on 3 frames of TSI3');
    H=[];   MV=[];  HMV=[];
    cc_all=[];maxcc=0;HCredCounts=0;bCloseNeighbor=false;
    save(outputmatf,'H','MV','HMV','cc_all','maxcc','HCredCounts','bCloseNeighbor');
    return;
end
bCloseNeighbor=false;   

% Here we decrease the searching range by choosing only largest 20 MVs
if ~bMVRef
    tmvals=sort(cc_mv(:),'descend');
    tmthres=tmvals(MaxNofDetectionMotionVector); % choose at most N motion vectors for height estimation next round
    MVValid=cc_mv>=tmthres;
else
    MVValid=cc_mv>0;
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   2. Calculate TSI2 similarity and TSI2,3 pair similarity
%       If 0==cc_mv, then skip calculation since motion vector cannot be trused on TSI3
%       If motion vector doesn't match on ALL 3 TSI2 images, then skip and set Cred_all=0,1;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

HCredCounts=zeros(H_l,1);
cc_all=zeros(possibleMVY_N,possibleMVX_N,H_l);
Cred_all=zeros(possibleMVY_N,possibleMVX_N,H_l);

for tmvy=1:possibleMVY_N
    mvy3=possibleMVY3(tmvy);
    for tmvx=1:possibleMVX_N
        mvx3=possibleMVX3(tmvx);
        if ~MVValid(tmvy,tmvx)
            %disp('Skip simiarity check on TSI2 since this motion vector is not valid on TSI3!');
            continue;
        end
        for i=1:H_l
            ux21=round(ux31+HMV32_x(i));
            uy21=round(uy31+HMV32_y(i));
            ux22=ux21+mvx3;
            uy22=uy21+mvy3;
            ux23=ux21+2*mvx3;
            uy23=uy21+2*mvy3;
            
%             if (uy21<1 || uy21>UNDISTH || ux21<1 || ux21>UNDISTW || ...
%                     uy22<1 || uy22>UNDISTH || ux22<1 || ux22>UNDISTW || ...
%                     uy23<1 || uy23>UNDISTH || ux23<1 || ux23>UNDISTW)
%                 %disp('CF TSI2 is on the edge part. Just ignore them');
%                 continue;
%             end

            % Check TSI2 invalid pixel counts
            blk21_invalid=logical(TO_GetImgVal_Pad([uy21 ux21],[recH recW],invalidma21));
            blk22_invalid=logical(TO_GetImgVal_Pad([uy22 ux22],[recH recW],invalidma22));
            blk23_invalid=logical(TO_GetImgVal_Pad([uy23 ux23],[recH recW],invalidma23));
            blk21_invalid=~blk21_invalid;
            blk22_invalid=~blk22_invalid;
            blk23_invalid=~blk23_invalid;
            invalidc21=length(find(blk21_invalid))/(recH*recW);
            invalidc22=length(find(blk22_invalid))/(recH*recW);
            invalidc23=length(find(blk23_invalid))/(recH*recW);
            if (invalidc21>INVALIDPIXELCOUNT) || (invalidc22>INVALIDPIXELCOUNT) || ...
                    (invalidc23>INVALIDPIXELCOUNT)
                Cred_all(tmvy,tmvx,i)=0;
                continue;
            %elseif 0==invalidc21 && 0==invalidc22 && 0==invalidc23
            elseif invalidc21<=INVALIDPIXELCOUNT && invalidc22<=INVALIDPIXELCOUNT && ...
                    invalidc23<=INVALIDPIXELCOUNT 
                Cred_all(tmvy,tmvx,i)=2;
            else
                Cred_all(tmvy,tmvx,i)=1;
                continue;
            end
            
            % TSI2 similarities
            %   (TSI21 TSI22) + (TSI22 TSI23)
            blk21=single(TO_GetImgVal_Pad([uy21 ux21],[recH recW],imma21));
            blk22=single(TO_GetImgVal_Pad([uy22 ux22],[recH recW],imma22));
            blk23=single(TO_GetImgVal_Pad([uy23 ux23],[recH recW],imma23));            
            if 0==std(blk21)    bSTDInvalid_21=true;
            else                bSTDInvalid_21=false;
            end
            if 0==std(blk22)    bSTDInvalid_22=true;
            else                bSTDInvalid_22=false;
            end
            if 0==std(blk23)    bSTDInvalid_23=true;
            else                bSTDInvalid_23=false;
            end

            
            if bSTDInvalid_21 || bSTDInvalid_22
                sim21_22=0;
            else
                cc_21_22=normxcorr2_blk(blk21,blk22);
                sim21_22=cc_21_22(recH,recW);
                if sim21_22<NCCMiniThres
                    sim21_22=0;
                end
            end
            if sim21_22==0
                continue; % based on current settings, if TSI21 TSI22 are not matched, no need to search more
            end
            
            
            if bSTDInvalid_22 || bSTDInvalid_23
                sim22_23=0;
            else
                cc_22_23=normxcorr2_blk(blk22,blk23);
                sim22_23=cc_22_23(recH,recW);
                if sim22_23<NCCMiniThres
                    sim22_23=0;
                end
            end
            if sim22_23==0
                continue; % based on current settings, if TSI22 TSI23 are not matched, no need to search more
            end
            
            sim_tsi2=sim21_22+sim22_23; % No need to check 2*NCCMiniThres since sim21_22 and sim22_23 are checked in previous step
            %if sim_tsi2<2*NCCMiniThres
            %    %disp('simiarity check got 0 when used this motion vector!');
            %    continue;
            %end
            
            % TSI2 and TSI3 similarities
            % (TSI21 TSI31) + (TSI22 TSI32) + (TSI23 TSI33)
            blk32=single(TO_GetImgVal_Pad([uy32 ux32],[recH recW],imma32));
            blk33=single(TO_GetImgVal_Pad([uy33 ux33],[recH recW],imma33));
            if (0==std(blk32))    bSTDInvalid_32=true;
            else                bSTDInvalid_32=false;
            end
            if (0==std(blk33))    bSTDInvalid_33=true;
            else                bSTDInvalid_33=false;
            end
            
            if bSTDInvalid_21 || bSTDInvalid_31
                sim21_31=0;
            else
                cc_21_31=normxcorr2_blk(blk21,blk31);
                sim21_31=cc_21_31(recH,recW);
                if sim21_31<NCCMiniThres
                    sim21_31=0;
                end
            end
            if sim21_31==0
                continue; % based on current settings, if TSI21 TSI31 are not matched, no need to search more
            end
            
            
            if bSTDInvalid_22 || bSTDInvalid_32
                sim22_32=0;
            else
                cc_22_32=normxcorr2_blk(blk22,blk32);
                sim22_32=cc_22_32(recH,recW);
                if sim22_32<NCCMiniThres
                    sim22_32=0;
                end
            end
            if sim22_32==0
                continue; % based on current settings, if TSI22 TSI23 are not matched, no need to search more
            end
            
            if bSTDInvalid_23 || bSTDInvalid_33
                sim23_33=0;
            else
                cc_23_33=normxcorr2_blk(blk23,blk33);
                sim23_33=cc_23_33(recH,recW);
                if sim23_33<NCCMiniThres
                    sim23_33=0;
                end
            end
            if sim23_33==0
                continue; % based on current settings, if TSI23 TSI33 are not matched, no need to search more
            end
            
            sim_tsipair=sim21_31+sim22_32+sim23_33; % no need to check with 3*NCCMiniThres 
            %if sim_tsipair<3*NCCMiniThres
            %    %disp('simiarity check between TSI2 and TSI3 got 0 when used this motion vector!')
            %    continue;
            %end
            cc_all(tmvy,tmvx,i)=cc_mv(tmvy,tmvx)+sim_tsi2+sim_tsipair;
        end
    end 
end

for i=1:H_l
    HCredCounts(i)=length(find(Cred_all(:,:,i)==2));
end

%[maxcc,ind]=max(cc_all(:));
ind=find(cc_all>0); % cc_all>7*NCCMiniThres
if isempty(ind)
    maxcc=0;
else
    maxcc=cc_all(ind);
end

[tmy,tmx,hind]=ind2sub(size(cc_all),ind);
H=H_range(hind)';
HMV_y= [HMV32_y(hind);HMV31_y(hind)];
HMV_x= [HMV32_x(hind);HMV31_x(hind)];
%mv_x=tmx-recW-MVWinSize;
%mv_y=tmy-recH-MVWinSize;    
mv_y=possibleMVY3(tmy)';
mv_x=possibleMVX3(tmx)';
MV=[mv_y mv_x];
HMV=[HMV_y HMV_x];


%% output
save(outputmatf,'H','MV','HMV','cc_all','maxcc','HCredCounts','bCloseNeighbor');
end





% %uy31_arr=zeros(possibleMVY_N*possibleMVX_N*H_l,1);
% N_arr=possibleMVY_N*possibleMVX_N*H_l;
% uy32_arr=zeros(N_arr,1);
% uy33_arr=zeros(N_arr,1);
% %ux31_arr=zeros(N_arr,1);
% ux32_arr=zeros(N_arr,1);
% ux33_arr=zeros(N_arr,1);
% uy21_arr=zeros(N_arr,1);
% uy22_arr=zeros(N_arr,1);
% uy23_arr=zeros(N_arr,1);
% ux21_arr=zeros(N_arr,1);
% ux22_arr=zeros(N_arr,1);
% ux23_arr=zeros(N_arr,1);
% H_arr=zeros(N_arr,1);
% MVx_arr=zeros(N_arr,1);
% MVy_arr=zeros(N_arr,1);
% 
% 
% 
% for tmvy=1:possibleMVY_N
%     mvy3=possibleMVY3(tmvy);
%     for tmvx=1:possibleMVX_N
%         mvx3=possibleMVX3(tmvx);
%         for i=1:H_l
%             index=(tmvy-1)*H_l*possibleMVX_N+(tmvx-1)*H_l+i;
% %             dist1=index-1;
% %             tmvy1=floor(dist1/(H_l*possibleMVX_N))+1;
% %             tmvx1=floor((dist1-(tmvy1-1)*H_l*possibleMVX_N)/H_l)+1;
% %             i1=dist1-(tmvy1-1)*H_l*possibleMVX_N-(tmvx1-1)*H_l+1;
% %             if i1~=i || tmvy1~=tmvy || tmvx1~=tmvx
% %                 i
% %             end
%             
%             
%             
%             H_arr(index)=H_range(i);
%             MVy_arr(index)=mvy3;
%             MVx_arr(index)=mvx3;
%             
%             ux21=round(ux31+HMV32_x(i));
%             uy21=round(uy31+HMV32_y(i));
%             ux22=ux21+mvx3;
%             uy22=uy21+mvy3;
%             ux23=ux21+2*mvx3;
%             uy23=uy21+2*mvy3;
%             ux32=ux31+mvx3;
%             uy32=uy31+mvy3;
%             ux33=ux31+2*mvx3;
%             uy33=uy31+2*mvy3;
%             
%             ux21_arr(index)=ux21;
%             ux22_arr(index)=ux22;
%             ux23_arr(index)=ux23;
%             uy21_arr(index)=uy21;
%             uy22_arr(index)=uy22;
%             uy23_arr(index)=uy23;
%             
%             ux32_arr(index)=ux32;
%             ux33_arr(index)=ux33;
%             uy32_arr(index)=uy32;
%             uy33_arr(index)=uy33;
%         end
%     end
% end


% for index=1:N_arr
%     ux21=    ux21_arr(index);
%     ux22=    ux22_arr(index);
%     ux23=    ux23_arr(index);
%     uy21=    uy21_arr(index);
%     uy22=    uy22_arr(index);
%     uy23=    uy23_arr(index);
%     ux32=    ux32_arr(index);
%     ux33=    ux33_arr(index);
%     uy32=    uy32_arr(index);
%     uy33=    uy33_arr(index);
% 
%     if (uy21<1 || uy21>UNDISTH || ux21<1 || ux21>UNDISTW || ...
%             uy22<1 || uy22>UNDISTH || ux22<1 || ux22>UNDISTW || ...
%             uy23<1 || uy23>UNDISTH || ux23<1 || ux23>UNDISTW)
%         %disp('CF TSI2 is on the edge part. Just ignore them');
%         continue;
%     end
%     
%     
%     % TSI2 similarities
%     %   (TSI21 TSI22) + (TSI22 TSI23)
%     blk21=single(TO_GetImgVal_Pad([uy21 ux21],[recH recW],imma21));
%     blk22=single(TO_GetImgVal_Pad([uy22 ux22],[recH recW],imma22));
%     blk23=single(TO_GetImgVal_Pad([uy23 ux23],[recH recW],imma23));
%     blk21_invalid=logical(TO_GetImgVal_Pad([uy21 ux21],[recH recW],invalidma21));
%     blk22_invalid=logical(TO_GetImgVal_Pad([uy22 ux22],[recH recW],invalidma22));
%     blk23_invalid=logical(TO_GetImgVal_Pad([uy23 ux23],[recH recW],invalidma23));
%     blk21_invalid=~blk21_invalid;
%     blk22_invalid=~blk22_invalid;
%     blk23_invalid=~blk23_invalid;
%     % Check TSI2 invalid pixel counts
%     invalidc21=length(find(blk21_invalid))/(recH*recW);
%     invalidc22=length(find(blk22_invalid))/(recH*recW);
%     invalidc23=length(find(blk23_invalid))/(recH*recW);
%  
%     dist1=index-1;
%     tmvy=floor(dist1/(H_l*possibleMVX_N))+1;
%     tmvx=floor((dist1-(tmvy-1)*H_l*possibleMVX_N)/H_l)+1;
%     i=dist1-(tmvy-1)*H_l*possibleMVX_N-(tmvx-1)*H_l+1;
% 
%     
%     if (invalidc21>INVALIDPIXELCOUNT) || (invalidc22>INVALIDPIXELCOUNT) || ...
%             (invalidc23>INVALIDPIXELCOUNT)
%         Cred_all(tmvy,tmvx,i)=0;
%         continue;
%     elseif 0==invalidc21 && 0==invalidc22 && 0==invalidc23
%         Cred_all(tmvy,tmvx,i)=2;
%     else
%         Cred_all(tmvy,tmvx,i)=1;
%         continue;
%     end
%     
%     
%     if 0==std(blk21)    bSTDInvalid_21=true;
%     else                bSTDInvalid_21=false;
%     end
%     if 0==std(blk22)    bSTDInvalid_22=true;
%     else                bSTDInvalid_22=false;
%     end
%     if 0==std(blk23)    bSTDInvalid_23=true;
%     else                bSTDInvalid_23=false;
%     end
%     
%     if bSTDInvalid_21 || bSTDInvalid_22
%         sim21_22=0;
%     else
%         cc_21_22=normxcorr2_blk(blk21,blk22);
%         sim21_22=cc_21_22(recH,recW);
%         if sim21_22<NCCMiniThres
%             sim21_22=0;
%         end
%     end
%     
%     if bSTDInvalid_22 || bSTDInvalid_23
%         sim22_23=0;
%     else
%         cc_22_23=normxcorr2_blk(blk22,blk23);
%         sim22_23=cc_22_23(recH,recW);
%         if sim22_23<NCCMiniThres
%             sim22_23=0;
%         end
%     end
%     
%     sim_tsi2=sim21_22+sim22_23;
%     if sim_tsi2<2*NCCMiniThres
%         %disp('simiarity check got 0 when used this motion vector!');
%         continue;
%     end
%     
%     % TSI2 and TSI3 similarities
%     % (TSI21 TSI31) + (TSI22 TSI32) + (TSI23 TSI33)
%     blk32=single(TO_GetImgVal_Pad([uy32 ux32],[recH recW],imma32));
%     blk33=single(TO_GetImgVal_Pad([uy33 ux33],[recH recW],imma33));
%     if (0==std(blk32))    bSTDInvalid_32=true;
%     else                bSTDInvalid_32=false;
%     end
%     if (0==std(blk33))    bSTDInvalid_33=true;
%     else                bSTDInvalid_33=false;
%     end
%     
%     if bSTDInvalid_21 || bSTDInvalid_31
%         sim21_31=0;
%     else
%         cc_21_31=normxcorr2_blk(blk21,blk31);
%         sim21_31=cc_21_31(recH,recW);
%         if sim21_31<NCCMiniThres
%             sim21_31=0;
%         end
%     end
%     
%     
%     if bSTDInvalid_22 || bSTDInvalid_32
%         sim22_32=0;
%     else
%         cc_22_32=normxcorr2_blk(blk22,blk32);
%         sim22_32=cc_22_32(recH,recW);
%         if sim22_32<NCCMiniThres
%             sim22_32=0;
%         end
%     end
%     
%     
%     if bSTDInvalid_23 || bSTDInvalid_33
%         sim23_33=0;
%     else
%         cc_23_33=normxcorr2_blk(blk23,blk33);
%         sim23_33=cc_23_33(recH,recW);
%         if sim23_33<NCCMiniThres
%             sim23_33=0;
%         end
%     end
%     
%     sim_tsipair=sim21_31+sim22_32+sim23_33;
%     if sim_tsipair<3*NCCMiniThres
%         %disp('simiarity check between TSI2 and TSI3 got 0 when used this motion vector!')
%         continue;
%     end
%     
%     cc_all(tmvy,tmvx,i)=cc_mv(tmvy,tmvx)+sim_tsi2+sim_tsipair;
% 
% end
