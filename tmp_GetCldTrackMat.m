%   tmp_GetCldTrackMat is to get mat file given tmdv and HRedDir
%   Current version is to get *.cldtrkreg.mat out which is regulated results by module CldTrack_Regulate
%   VERSION 1.0     2013/11/04

function [cldtrkmatoutf_reg,cldtrk_recheckmatf]=tmp_GetCldTrackMat(HRefDir,tmdv,configfpath)
run(configfpath);

tmf=GetImgFromDV_BNL(tmdv,TSI3);
cldtrkmatoutf_reg=sprintf('%s%s.cldtrkreg.mat',HRefDir,tmf(1:end-4));
cldtrkmatoutf_2=sprintf('%s%s.cldtrk2.mat',HRefDir,tmf(1:end-4));
cldtrk_recheckmatf=cldtrkmatoutf_2;


% cldtrkmatoutf_2=sprintf('%s%s.cldtrk2.mat',HRefDir,tmf(1:end-4));
% t=load(cldtrkmatoutf_2);
% H=t.H;
% Conn_sx=t.Conn_sx;    Conn_sy=t.Conn_sy;
% Conn_H=t.Conn_H;      Conn_W=t.Conn_W;
% HMV_x=t.HMV_x; HMV_y=t.HMV_y;
% MAXCC=t.MAXCC;
% nConn=t.nCF;


% t=load(cldtrkmatoutf_reg);
% H_Reg=t.H_Reg;  Conn_Reg=t.Conn_Reg;    CldC_Reg=t.CldC_Reg;
% MV_x_Reg=t.MV_x_Reg;    MV_y_Reg=t.MV_y_Reg;
% HMV_x_Reg=t.HMV_x_Reg;    HMV_y_Reg=t.HMV_y_Reg;
% RegH=t.RegH;    RegHMV_x=t.RegHMV_x;    RegHMV_y=t.RegHMV_y;
% RegMV_x=t.RegMV_x;  RegMV_y=t.RegMV_y;





end