function test_histgrammatch_2r
startup;

% tsiDirs={'./tsi1_undist/','./tsi2_undist/','./tsi3_undist/'};
tsiDirs={'./undist_20130610/tsi1/','./undist_20130610/tsi2/','./undist_20130610/tsi3/'};
otsiDirs={'./tsi1/','./tsi2/','./tsi3/'};
nTSI=3;
tsi1Dir=tsiDirs{1};
tsi2Dir=tsiDirs{2};
tsi3Dir=tsiDirs{3};
otsi1Dir=otsiDirs{1};
otsi2Dir=otsiDirs{2};
otsi3Dir=otsiDirs{3};
mkdir(otsi1Dir);
mkdir(otsi2Dir);
mkdir(otsi3Dir);
alltsi1=dir(tsi1Dir);
[nfiles1,~]=size(alltsi1);
imma1_total=zeros(UNDISTH,UNDISTW,3);
imma2_total=zeros(UNDISTH,UNDISTW,3);
imma3_total=zeros(UNDISTH,UNDISTW,3);
N1=zeros(UNDISTH,UNDISTW);
N2=zeros(UNDISTH,UNDISTW);
N3=zeros(UNDISTH,UNDISTW);


imma_avgf='./imma_avgf.mat';
t=load(imma_avgf,'imma1_avg','imma2_avg','imma3_avg');
imma1_avg=t.imma1_avg;
imma2_avg=t.imma2_avg;
imma3_avg=t.imma3_avg;

for i =1:nfiles1
    filename=alltsi1(i).name;
    f1=sprintf('%s%s',tsi1Dir,filename);
    tmdv=GetDVFromImg(filename);
    if tmdv==0
        continue;
    end
    filename2=GetImgFromDV_BNL(tmdv,TSI2);
    filename3=GetImgFromDV_BNL(tmdv,TSI3);
    f2=sprintf('%s%s.bmp',tsi2Dir,filename2(1:end-4));
    f3=sprintf('%s%s.bmp',tsi3Dir,filename3(1:end-4));
    try
        imma1=imread(f1);
        imma2=imread(f2);
        imma3=imread(f3);
    catch
        continue;
    end
    
%     location=location1;
%     time.year    =  tmdv(1);
%     time.month   =  tmdv(2);
%     time.day     =  tmdv(3);
%     time.hour    =  tmdv(4);
%     time.min    =   tmdv(5);
%     time.sec    =   tmdv(6);
%     time.UTC    =   0;
%     sun=sun_position(time,location);
%     Z=sun.zenith;
%     Z=Z/180*pi;
%     A=sun.azimuth;
%     A=A/180*pi;
%     S=[sin(A)*tan(Z) cos(A)*tan(Z) 1];
%     Vec_OS=S;
%     Angles_cos=zeros(UNDISTH,UNDISTW);
%     for tmy= 1:UNDISTH
%         for tmx=1:UNDISTW
%             Ux=tmx-UNDISTR;
%             Uy=UNDISTR-tmy;
% %            Ur=sqrt(Ux^2+Uy^2);
% %             Z_u_tan=Ur/UNDISTR*sqrt(3);
% %             Uz=Ur/Z_u_tan;
%             Uz=UNDISTR/sqrt(3);
%             tmVec=[Ux Uy Uz];
%             a=acos(dot(Vec_OS,tmVec)/(norm(Vec_OS)*norm(tmVec)));
%             Angles_cos(tmy,tmx)=a;
%         end
%     end
    
%     figure();
%     subplot(2,3,1);
%     imshow(imma1);
%     subplot(2,3,2);
%     imshow(imma2);
%     subplot(2,3,3);
%     imshow(imma3);
%     imma1_cld=test_GenCldMask(imma1);
%     imma2_cld=test_GenCldMask(imma2);
%     imma3_cld=test_GenCldMask(imma3);
%     subplot(2,3,4);
%     imshow(imma1_cld);
%     subplot(2,3,5);
%     imshow(imma2_cld);
%     subplot(2,3,6);
%     imshow(imma3_cld);
% 
% 
%     
%     
%     r3=imma3(:,:,1);
%     g3=imma3(:,:,2);
%     b3=imma3(:,:,3);
%     hr3=imhist(r3,70);
%     hg3=imhist(g3,70);
%     hb3=imhist(b3,70);
%     r1=imma1(:,:,1);
%     g1=imma1(:,:,2);
%     b1=imma1(:,:,3);
%     r1_a=histeq(r1,hr3);
%     g1_a=histeq(g1,hg3);
%     b1_a=histeq(b1,hb3);
%     imma1_a=imma1;
%     imma1_a(:,:,1)=r1_a(:,:);
%     imma1_a(:,:,2)=g1_a(:,:);
%     imma1_a(:,:,3)=b1_a(:,:);

    [ma1,cldind1]=test_GenCldMask_2r(imma1,imma1_avg);
    [ma2,cldind2]=test_GenCldMask_2r(imma2,imma2_avg);
    [ma3,cldind3]=test_GenCldMask_2r(imma3,imma3_avg);
%     figure();
%     subplot(2,3,1);
%     imshow(imma1);
%     subplot(2,3,2);
%     imshow(imma2);
%     subplot(2,3,3);
%     imshow(imma3);
%     subplot(2,3,4);
%     imshow(ma1);
%     subplot(2,3,5);
%     imshow(ma2);
%     subplot(2,3,6);
%     imshow(ma3);
    imma1_ma=test_setImgZero(cldind1,imma1);
    imma2_ma=test_setImgZero(cldind2,imma2);
    imma3_ma=test_setImgZero(cldind3,imma3);
    imma1_total=imma1_total+double(imma1_ma);
    imma2_total=imma2_total+double(imma2_ma);
    imma3_total=imma3_total+double(imma3_ma);
    N1=N1+double(~cldind1);
    N2=N2+double(~cldind2);
    N3=N3+double(~cldind3);
end

imma1_avg=uint8(test_genavgImg(N1,imma1_total));
imma2_avg=uint8(test_genavgImg(N2,imma2_total));
imma3_avg=uint8(test_genavgImg(N3,imma3_total));
figure();
subplot(2,3,1);
imshow(imma1_avg);
subplot(2,3,2);
imshow(imma2_avg);
subplot(2,3,3);
imshow(imma3_avg);

imma_avgf='./imma_avgf.mat';
save(imma_avgf,'imma1_avg','imma2_avg','imma3_avg');
end