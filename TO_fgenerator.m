%   TO_fgenerator is to generate the parallel fhandle based on current fhandle and its inputparams
%   and outputparams
%   Rule 1: Replace original "function" line as function Ffunction_p(inmat,outmat)
%   Rule 2: Add load in the front of function
%   Rule 3: Add save at the end of function
%
%   VERSION 1.0     2013/10/02
function fhandle_parallel=TO_fgenerator(fhandle,inputparams,outputparams)
F=functions(fhandle);
Fpath=F.file;
Ffunction=F.function;
assert(exist(Fpath,'file')~=0,'Function does not exist!');
Fcontent=fileread(Fpath);

FDir=GetDirPathFromPath(Fpath);
FDir=FormatDirName(FDir);
% Suppose new function has the same directory path
% Suppose new function name is Ffunction_parallel
Ffunction_p=sprintf('%s_parallel',Ffunction);
Fpath_p=sprintf('%s%s.m',FDir,Ffunction_p);


% Rule 1: Replace original "function" line as function Ffunction_p(inmat,outmat)
% 1. Find headline or "function" line
delimiters={'\n'};
tlines=strsplit(Fcontent,delimiters);
lc=length(tlines);
EXP_comment='^\s*%';
EXP_functionkeyword='^\s*function';
EXP_lastend='^\s*end\s*$';

ninputs=length(inputparams);
if ninputs~=0
    % if no output, then no need to change function header
    for i=1:lc
        tline=tlines{i};
        starti= regexp(tline,EXP_comment, 'once');
        if ~isempty(starti);
            continue;
        end
        starti= regexp(tline,EXP_functionkeyword, 'once');
        if isempty(starti);
            continue;
        else
            break;
        end
    end
    fhindex=i;
    % 2. Construct new parallel format and replace it
    newfunctionline=sprintf('function %s(inmat,outmat)\n',Ffunction_p);
    newfunctionheader='';
    tmline='parallel_t=load(inmat';
    tmline1='';
    
    for i=1:ninputs
        tmline=sprintf('%s,''%s''',tmline,inputparams{i});
        tmline1=sprintf('%s%s=parallel_t.%s;\n',tmline1,inputparams{i},inputparams{i});
    end
    tmline=sprintf('%s);\n',tmline);
    newfunctionheader=[tmline tmline1];
    tlines{fhindex}=[newfunctionline newfunctionheader];
end

% Rule 2: Add save in front of last end
% 1. Find last end
noutputs=length(outputparams);
if noutputs~=0
    % if no output, then no need to change last end tag
    for i=lc:-1:1
        tline=tlines{i};
        starti= regexp(tline,EXP_comment, 'once');
        if ~isempty(starti);
            continue;
        end
        starti= regexp(tline,EXP_lastend, 'once');
        if isempty(starti);
            continue;
        else
            break;
        end
    end
    lastendindex=i;
    tmline='save(outmat';
    for i=1:noutputs
        tmline=sprintf('%s,''%s''',tmline,outputparams{i});
    end
    tmline=sprintf('%s);\n',tmline);
    tlines{lastendindex}=[tmline 'end'];
end

% reconstruct new function string and put into file
tmstr='';
tml=length(tlines);
for i=1:tml
    tmstr=[tmstr tlines{i}];
end
fid=fopen(Fpath_p,'w');
fprintf(fid,'%s',tmstr);
fhandle_parallel=str2func(Ffunction_p);



end