function imma_eq=RGBHistEq(imma,im_ref)

Red1 = im_ref(:, :, 1);
Green1 = im_ref(:, :, 2);
Blue1 = im_ref(:, :, 3);
HnRed1=imhist(Red1);
HnGreen1=imhist(Green1);
HnBlue1=imhist(Blue1);
imma_eq=imma;
Red2 = imma(:, :, 1);
Green2 = imma(:, :, 2);
Blue2 = imma(:, :, 3);
red2n = histeq(Red2,HnRed1);
green2n = histeq(Green2,HnGreen1);
blue2n = histeq(Blue2,HnBlue1);
imma_eq(:,:,1)=red2n;
imma_eq(:,:,2)=green2n;
imma_eq(:,:,3)=blue2n;

end