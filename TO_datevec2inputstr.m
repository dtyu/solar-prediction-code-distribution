function inputstring=TO_datevec2inputstr(datevector)

inputstring=sprintf('[%d %d %d %d %d %d]',datevector(1),datevector(2),...
    datevector(3),datevector(4),datevector(5),datevector(6));


end