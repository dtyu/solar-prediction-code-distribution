function imma_mask=test_setImgZero(cldmask,imma)
r1=imma(:,:,1);
g1=imma(:,:,2);
b1=imma(:,:,3);
r1(cldmask)=0;
g1(cldmask)=0;
b1(cldmask)=0;
imma_mask(:,:,1)=r1;
imma_mask(:,:,2)=g1;
imma_mask(:,:,3)=b1;

end