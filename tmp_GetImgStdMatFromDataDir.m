%   Temp version of get image from data directory. The function is to get mat file and its image
%   matrix when given filename or datevector
%   INPUT:
%       imgObj              --- Datevector or String(filepath)
%       site(Optional)      --- String, tsi1,tsi2,tsi3
%       StdBlockSize
%       dataDir(Optional)   --- String, date directory
%                               default is '/data/workdata/mtsi_stich/'
%   OUTPUT:
%       imma                ----   Matrix, image matrix
%       matf                ----    Absolute path of mat file
%   VERSION 1.0     2013-07-29
%   VERSION 1.1     2013-11-01      Add configfpath as variable
function [imma_std,imma_std_invalid,matf]=tmp_GetImgStdMatFromDataDir(imgObj,site,StdBlockSize,dataDir,configfpath)
%label of site
if ~exist('configfpath','var')
    configfpath='mtsi_startup_mini';
end
run(configfpath);
assert(nargin==1|nargin==2|nargin==4|nargin==5,'Wrong input! Please check the number of input variables');

if nargin==1
    imgf=GetImgFromDV_BNL(imgObj,site);
    site=TO_isTSIImg(imgObj);
    StdBlockSize=11;
    dataDir='/data/workdata/mtsi_stich/';
elseif nargin==2
    StdBlockSize=11;
    dataDir='/data/workdata/mtsi_stich/';
end
if ischar(imgObj)
    imgf=imgObj;
elseif isfloat(imgObj) && 6==length(imgObj)
    imgf=GetImgFromDV_BNL(imgObj,site);
else
    disp('Unknown type of input! Please check your input');
end


filename=GetFilenameFromAbsPath(imgf);
fname=[filename(1:end-4) '.std.mat'];

stdstr=int2str(StdBlockSize);
tsi1D=[dataDir 'tsi1_undist_std_' stdstr '/'];
tsi1Dir=[dataDir 'tsi1_undist_mat/'];
tsi2D=[dataDir 'tsi2_undist_std_' stdstr '/'];
tsi2Dir=[dataDir 'tsi2_undist_mat/'];
tsi3D=[dataDir 'tsi3_undist_std_' stdstr '/'];
tsi3Dir=[dataDir 'tsi3_undist_mat/'];
if strcmp(site,TSI1)
    matf=[tsi1D fname];
elseif strcmp(site,TSI2)
    matf=[tsi2D fname];
elseif strcmp(site,TSI3)
    matf=[tsi3D fname];
else
    disp('Unknown site. site should be tsi1, tsi2, tsi3');
end

%if 1
if ~exist(matf,'file')
    tmdv=GetDVFromImg(filename);
    %     if strcmp(site,TSI1)
    %         TO_GenTSISTD_all(tsi1Dir,tsi1D,StdBlockSize,[tmdv;tmdv+[0 0 0 0 0 1]],configfpath);
    %     elseif strcmp(site,TSI2)
    %         TO_GenTSISTD_all(tsi2Dir,tsi2D,StdBlockSize,[tmdv;tmdv+[0 0 0 0 0 1]],configfpath);
    %     elseif strcmp(site,TSI3)
    %         %disp(tsi3Dir);
    %         TO_GenTSISTD_all(tsi3Dir,tsi3D,StdBlockSize,[tmdv;tmdv+[0 0 0 0 0 1]],configfpath);
    %     end
    % If one of the file doesn't exist, try to generate all 3 at the same time
    TO_GenTSISTD_all(tsi1Dir,tsi1D,StdBlockSize,[tmdv;tmdv+[0 0 0 0 0 1]],configfpath);
    TO_GenTSISTD_all(tsi2Dir,tsi2D,StdBlockSize,[tmdv;tmdv+[0 0 0 0 0 1]],configfpath);
    TO_GenTSISTD_all(tsi3Dir,tsi3D,StdBlockSize,[tmdv;tmdv+[0 0 0 0 0 1]],configfpath);
end
t=load(matf);
assert(StdBlockSize==t.StdBlockSize,'The StdBlockSize doen''t match!');
imma_std=t.imma_std;
imma_std_invalid=t.imma_std_invalid;


end