function newsbma=sbrotate(sbma,azdegree,rcentrep)

[H,W]=size(sbma);
cen_x=round(W/2);
cen_y=round(H/2);
rcen_x=rcentrep.x;
rcen_y=rcentrep.y;
diffx=rcen_x-cen_x;
diffy=rcen_y-cen_y;


%   1. Move sbma to the rotation center
%   [rcen_y,rcenx]->[cen_y,cenx]
sbma_fill=[zeros(H,30) sbma zeros(H,30)];
sbma_fill=[zeros(30,W+60);sbma_fill;zeros(30,W+60)];
resultma=sbma_fill;
resultma(31:H+30,31:W+30)=sbma_fill(31+diffy:H+30+diffy,31+diffx:W+30+diffx);
resultma1=resultma(31:H+30,31:W+30);
% figure();imshow(sbma_fill);
% figure();imshow(resultma1);

%   2. Rotate the image
resultma2=imrotate(resultma1,azdegree,'crop');


%   3. Move result ma back to original space
%   [cen_y,cenx] -> [rcen_y,rcenx]
sbma_fill=[zeros(H,30) resultma2 zeros(H,30)];
sbma_fill=[zeros(30,W+60);sbma_fill;zeros(30,W+60)];
resultma=sbma_fill;
resultma(31:H+30,31:W+30)=sbma_fill(31-diffy:H+30-diffy,31-diffx:W+30-diffx);
resultma3=resultma(31:H+30,31:W+30);

%figure();imshow(resultma3);
newsbma=logical(resultma3);
end