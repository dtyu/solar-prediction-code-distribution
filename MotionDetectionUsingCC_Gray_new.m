% Author: Hao Huang
% Release version: 3
% Release date: 4/22/11
% New version data: 6/23/11

%revised 2012/04/08
%% use to detect motion vector on the whole image

function MotionDetectionUsingCC_Gray_new(M1path, M2path, OutputPath, InnerBlockSize, BlockSize,SearchWinSize,CorThreshold,BlackThreshold);

%% input:
%% M1path: path for the 1st image
%% M2path: path for the 2nd image
%% OutputPath: path for the output mat file
%% BlockSize: size of each block
%% SearchWinSize: size of search window
%% CorThreshold: threshold for cross correlation
%% step: the resolution step in the Search Window (1 is the highest)
s=load(M1path,'-mat');
M1=double(s.outputm);
s=load(M2path,'-mat');
M2=double(s.outputm);

% M1=double(imread(M1path)); % first flame
% M2=double(imread(M2path)); % second flame


% BlackThreshold=40; % elements small than this are considered as totally black
%InnerBlockSize=5;  % Larger BlockSize: size of block in the 1st frame to extract correlation
OutterBlockSize=(BlockSize-InnerBlockSize)/2;
SmallOutterBlockSize=OutterBlockSize/2;
SmallSearchWinSize=SearchWinSize; % Smaller Search Window Size
ExtendCurrent=OutterBlockSize+SearchWinSize;
SmallExtendCurrent=SmallOutterBlockSize+SmallSearchWinSize;

%% padding
LeftRight=zeros(size(M1,1),SearchWinSize);
UpDown=zeros(SearchWinSize,size(M1,2)+SearchWinSize*2);
M1=[LeftRight,M1,LeftRight];
M1=[UpDown;M1;UpDown];
M2=[LeftRight,M2,LeftRight];
M2=[UpDown;M2;UpDown];
%M1_rbr = [LeftRight,M1_rbr,LeftRight];
%M1_rbr=[UpDown;M1_rbr;UpDown];

dimy=size(M1,1);
dimx=size(M1,2);
hdimy=dimy/2;   %height of frame
hdimx=dimx/2;   %width of frame

% area where to pick im1 block
Im1y1=ExtendCurrent+1;
Im1y2=dimy-ExtendCurrent;
Im1x1=ExtendCurrent+1;
Im1x2=dimx-ExtendCurrent;

%% size of motion vector
nRowBlock=floor((Im1y2-Im1y1)/InnerBlockSize);
nColBlock=floor((Im1x2-Im1x1)/InnerBlockSize);
matchy=zeros(nRowBlock,nColBlock);
matchx=zeros(nRowBlock,nColBlock);

%% two boundary, 1st for big block and 2nd small block
boundary1=(hdimy-BlockSize-SearchWinSize);
boundary2=(hdimx-BlockSize-0.2*SearchWinSize);

%% start main loop for motion detection
for loopi=1:nRowBlock
    ybound1=(loopi-1)*InnerBlockSize+Im1y1;
    ybound2=ybound1+InnerBlockSize-1;
    y_c=(ybound1+ybound2)/2-hdimy;
    ylp1=ybound1-OutterBlockSize;
    ylp2=ybound2+OutterBlockSize;
    ylc1=ybound1-ExtendCurrent;
    ylc2=ybound2+ExtendCurrent;
    for loopj=1:nColBlock
        % area of the block in im1
        xbound1=(loopj-1)*InnerBlockSize+Im1x1;
        xbound2=xbound1+InnerBlockSize-1;
        x_c=(xbound1+xbound2)/2-hdimx;
        %l=sqrt(y_c^2+x_c^2);   % distance to center point
        %if (l<boundary1) % only consider area in the circle
        if 1
            %            previous_rbr=M1_rbr(ylp1:ylp2,xbound1-OutterBlockSize:xbound2+OutterBlockSize); % larger block
            %            if rbrAVGDstb(previous_rbr, 0.7, 0.1)
            previous=M1(ylp1:ylp2,xbound1-OutterBlockSize:xbound2+OutterBlockSize); % larger block
            current =M2(ylc1:ylc2,xbound1-ExtendCurrent:xbound2+ExtendCurrent);   % larger search window
            [tempy1 tempx1 Cor1 R]=CorrelationDetectionUsingBlackFiltering4(previous,current,BlackThreshold); % correlation detection
            if Cor1>CorThreshold  % only consider correlation large enough
                tempy1=tempy1(1,1);
                tempx1=tempx1(1,1);
                matchy(loopi,loopj)=tempy1(1)-SearchWinSize-1; % move the origin to the center
                matchx(loopi,loopj)=tempx1(1)-SearchWinSize-1;
            end
            %            end
        elseif (l<boundary2)
            %            previous_rbr=M1_rbr(ybound1-SmallOutterBlockSize:ybound2+SmallOutterBlockSize,xbound1-SmallOutterBlockSize:xbound2+SmallOutterBlockSize); % smaller block
            %            if rbrAVGDstb(previous_rbr, 0.7, 0.1)
            previous=M1(ybound1-SmallOutterBlockSize:ybound2+SmallOutterBlockSize,xbound1-SmallOutterBlockSize:xbound2+SmallOutterBlockSize); % smaller block
            current=M2(ybound1-SmallExtendCurrent:ybound2+SmallExtendCurrent,xbound1-SmallExtendCurrent:xbound2+SmallExtendCurrent);   % smaller search window
            [tempy1 tempx1 Cor1 R]=CorrelationDetectionUsingBlackFiltering4(previous,current,BlackThreshold); % correlation detection
            if Cor1>CorThreshold  % only consider correlation large enough
                tempy1=tempy1(1,1);
                tempx1=tempx1(1,1);
                matchy(loopi,loopj)=tempy1(1)-SmallSearchWinSize-1; % move the origin to the center
                matchx(loopi,loopj)=tempx1(1)-SmallSearchWinSize-1;
            end
            %            end
        end
    end
end

matchx1=matchx;
matchy1=matchy;

%drawWinField(matchx1,matchy1);
[matchx1,matchy1]=VectorsRemoval(matchx1, matchy1, 1, 40);


%% save result
save(OutputPath);

