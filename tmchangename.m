% temp change name for BNL hamasks c1 -> C1
function tmchangename(hamaskDir,newhamaskDir)
haallfiles=dir(hamaskDir);
[m1,~]=size(haallfiles);
hamaskDir=FormatDirName(hamaskDir);
newhamaskDir=FormatDirName(newhamaskDir);
if ~exist(newhamaskDir,'dir')
    mkdir(newhamaskDir);
end
for i=1:m1
    if(haallfiles(i).isdir==1)
        continue;
    end
    filename=haallfiles(i).name;
    tmdv=GetDVFromImg(filename);
    newf=GetImgFromDV_BNLC1(tmdv);
    inputf=sprintf('%s%s',hamaskDir,filename);
    outputf=sprintf('%s%s.mat',newhamaskDir,newf);
    copyfile(inputf,outputf);
end

end