function imma_n=TO_histeq_TSISky(imma_0,ref_sky_0)
[Hr,Wr,Dim]=size(ref_sky_0);
if Hr==256&&Wr==3
   ref_sky_hist=ref_sky_0; 
elseif Dim==3
    Red1 = ref_sky_0(:, :, 1);
    Green1 = ref_sky_0(:, :, 2);
    Blue1 = ref_sky_0(:, :, 3);
    HnRed1=imhist(Red1);
    HnGreen1=imhist(Green1);
    HnBlue1=imhist(Blue1);
    HnRed1(1)=0;HnGreen1(1)=0;HnBlue1(1)=0;     % 0 means not valid
    ref_sky_hist=[HnRed1 HnGreen1 HnBlue1];
else
    disp('Wrong input of ref_sky_0. This variable should be TSI RGB hist or RGB image');
    imma_n=nan;
    return;
end
imma_n=imma_0;
R = imma_0(:, :, 1);
G = imma_0(:, :, 2);
B = imma_0(:, :, 3);
invalidma=(R==0&B==0&G==0);
validma=~invalidma;
%validind=find(~invalidma);

Rn = histeq(R(validma),ref_sky_hist(:,1));
Gn = histeq(G(validma),ref_sky_hist(:,2));
Bn = histeq(B(validma),ref_sky_hist(:,3));

R(validma)=Rn;
G(validma)=Gn;
B(validma)=Bn;
imma_n(:,:,1)=R;
imma_n(:,:,2)=G;
imma_n(:,:,3)=B;


end