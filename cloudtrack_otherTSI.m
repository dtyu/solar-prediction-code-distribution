% cloudtrack_otherTSI
%   This function is to generate tracking results of TSI1 and TSI2(based on DB name).
function cloudtrack_otherTSI(cldtrkmatout_recheck,cldtrkmatout_reg,cldtrkmatout_tsis,currentdatevec,cldmodelmatf,dataDir,configfpath)
run(configfpath);
tmdv=currentdatevec;
tmdv_1faf=datevec(datenum(tmdv)+TENSECS_DN);

[imma1_cldma_n,imma2_cldma_n,~]= clouddetector(tmdv,cldmodelmatf,dataDir,configfpath);
imma1_ori=tmp_GetImgMatFromDataDir(tmdv,TSI1,dataDir);
imma2_ori=tmp_GetImgMatFromDataDir(tmdv,TSI2,dataDir);
imma3=tmp_GetImgMatFromDataDir(tmdv,TSI3,dataDir);
Red1 = imma3(:, :, 1);
Green1 = imma3(:, :, 2);
Blue1 = imma3(:, :, 3);
HnRed1=imhist(Red1);
HnGreen1=imhist(Green1);
HnBlue1=imhist(Blue1);
HnRed1(1)=0;HnGreen1(1)=0;HnBlue1(1)=0;     % 0 means not valid
ref_sky_hist=[HnRed1 HnGreen1 HnBlue1];
imma1=TO_histeq_TSISky(imma1_ori,ref_sky_hist);
imma2=TO_histeq_TSISky(imma2_ori,ref_sky_hist);
imma1_lum=single(TO_rgb2lum(imma1));
imma2_lum=single(TO_rgb2lum(imma2));
%imma3_lum=single(TO_rgb2lum(imma3));

t=load(cldtrkmatout_recheck);
H=t.H;
Conn_sx=t.Conn_sx;    Conn_sy=t.Conn_sy;
Conn_H=t.Conn_H;      Conn_W=t.Conn_W;
HMV_x=t.HMV_x; HMV_y=t.HMV_y;
MAXCC=t.MAXCC;
nConn=t.nCF;
t=load(cldtrkmatout_reg);
H_Reg=t.H_Reg;  Conn_Reg=t.Conn_Reg;    CldC_Reg=t.CldC_Reg;
MV_x_Reg=t.MV_x_Reg;    MV_y_Reg=t.MV_y_Reg;
HMV_x_Reg=t.HMV_x_Reg;    HMV_y_Reg=t.HMV_y_Reg;
RegH=t.RegH;    RegHMV_x=t.RegHMV_x;    RegHMV_y=t.RegHMV_y;
RegMV_x=t.RegMV_x;  RegMV_y=t.RegMV_y;

imma1_ori_1faf=tmp_GetImgMatFromDataDir(tmdv_1faf,TSI1,dataDir);
imma2_ori_1faf=tmp_GetImgMatFromDataDir(tmdv_1faf,TSI2,dataDir);
%imma3_ori_3faf=tmp_GetImgMatFromDataDir(tmdv_1faf,TSI3,dataDir);

imma1_1faf=TO_histeq_TSISky(imma1_ori_1faf,ref_sky_hist);
imma2_1faf=TO_histeq_TSISky(imma2_ori_1faf,ref_sky_hist);
%imma3_1faf=TO_histeq_TSISky(imma3_ori_1faf,ref_sky_hist);

imma1_lum_1faf=single(TO_rgb2lum(imma1_1faf));
imma2_lum_1faf=single(TO_rgb2lum(imma2_1faf));
%imma3_lum=single(TO_rgb2lum(imma3));

tmmask1=imma1_cldma_n;  tmmask2=imma2_cldma_n;
for tmi=1:nConn
    if RegH(tmi)==0
        continue;
    end
    minsx1=round(Conn_sx(tmi)+RegHMV_x(tmi,2)); minsy1=round(Conn_sy(tmi)+RegHMV_y(tmi,2));
    minsx2=round(Conn_sx(tmi)+RegHMV_x(tmi,1)); minsy2=round(Conn_sy(tmi)+RegHMV_y(tmi,1));
    recH=Conn_H(tmi); recW=Conn_W(tmi);
    tmvals=false(recH,recW);
    tmmask1=logical(TO_SetImgVal_Pad([minsy1 minsx1],tmvals,tmmask1));
    tmmask2=logical(TO_SetImgVal_Pad([minsy2 minsx2],tmvals,tmmask2));
    %rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
    %quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi),HMV_y(tmi));
end

[CFMaskMa1,nCF1]=cloudfieldext(tmmask1,configfpath);
[CFMaskMa2,nCF2]=cloudfieldext(tmmask2,configfpath);


MV_x1=zeros(nCF1,1); MV_y1=zeros(nCF1,1);
HMV_x1=zeros(nCF1,2); HMV_y1=zeros(nCF1,2);
H1=zeros(nCF1,1);     MAXCC1=zeros(nCF1,1);
Conn_H1=zeros(nCF1,1); Conn_W1=zeros(nCF1,1);
Conn_sx1=zeros(nCF1,1);Conn_sy1=zeros(nCF1,1);
Conn_cldcount1=zeros(nCF1,1);
MV_x2=zeros(nCF2,1); MV_y2=zeros(nCF2,1);
HMV_x2=zeros(nCF2,2); HMV_y2=zeros(nCF2,2);
H2=zeros(nCF2,1);     MAXCC2=zeros(nCF2,1);
Conn_H2=zeros(nCF2,1); Conn_W2=zeros(nCF2,1);
Conn_sx2=zeros(nCF2,1);Conn_sy2=zeros(nCF2,1);
Conn_cldcount2=zeros(nCF2,1);

if H_Reg(2)==0
    bSingleLayer=true;
    mvpickrange=[MV_y_Reg(1) MV_x_Reg(1)];
else
    bSingleLayer=false;
    mvpickrange=[MV_y_Reg MV_x_Reg];
end

[~,mostlayeri]=max(CldC_Reg);
for tmi=1:nCF1
    tmind=find(CFMaskMa1==tmi);
    Conn_cldcount1(tmi)=length(tmind);
    [y,x]=ind2sub([UNDISTH,UNDISTW],tmind);
    minsy=min(y);
    minsx=min(x);
    recH=max(y)-min(y)+1;
    recW=max(x)-min(x)+1;
    Conn_sx1(tmi)=minsx;Conn_sy1(tmi)=minsy;
    Conn_H1(tmi)=recH;Conn_W1(tmi)=recW;
    cc_vals=ncc_mvpick(...
        imma1_lum,imma1_lum_1faf,[minsy minsx],[recH recW],NextFrameMVSearchWinSize,mvpickrange);
    cc_vals(cc_vals<ncc_thres)=0;
    [maxval,maxi]=max(cc_vals);
    if maxval==0
        % choose the frequent layer
        maxi=mostlayeri;
    else
        maxi=maxi;
    end
    H1(tmi)=H_Reg(maxi);
    MV_x1(tmi)=MV_x_Reg(maxi);
    MV_y1(tmi)=MV_y_Reg(maxi);
    HMV_x1(tmi,1)=HMV_x_Reg(maxi,1);
    HMV_y1(tmi,1)=HMV_y_Reg(maxi,1);
    HMV_x1(tmi,2)=HMV_x_Reg(maxi,2);
    HMV_y1(tmi,2)=HMV_y_Reg(maxi,2);
end

for tmi=1:nCF2
    tmind=find(CFMaskMa2==tmi);
    Conn_cldcount2(tmi)=length(tmind);
    [y,x]=ind2sub([UNDISTH,UNDISTW],tmind);
    minsy=min(y);
    minsx=min(x);
    recH=max(y)-min(y)+1;
    recW=max(x)-min(x)+1;
    Conn_sx2(tmi)=minsx;Conn_sy2(tmi)=minsy;
    Conn_H2(tmi)=recH;Conn_W2(tmi)=recW;
    cc_vals=ncc_mvpick(...
        imma2_lum,imma2_lum_1faf,[minsy minsx],[recH recW],NextFrameMVSearchWinSize,mvpickrange);
    cc_vals(cc_vals<ncc_thres)=0;
    [maxval,maxi]=max(cc_vals);
    if maxval==0
        % choose the frequent layer
        maxi=mostlayeri;
    else
        maxi=maxi;
    end
    H2(tmi)=H_Reg(maxi);
    MV_x2(tmi)=MV_x_Reg(maxi);
    MV_y2(tmi)=MV_y_Reg(maxi);
    HMV_x2(tmi,1)=HMV_x_Reg(maxi,1);
    HMV_y2(tmi,1)=HMV_y_Reg(maxi,1);
    HMV_x2(tmi,2)=HMV_x_Reg(maxi,2);
    HMV_y2(tmi,2)=HMV_y_Reg(maxi,2);
end
save(cldtrkmatout_tsis,'MV_x1','MV_x2','MV_y1','MV_y2','HMV_x1','nCF1','nCF2',...
    'Conn_sx1','Conn_sx2','Conn_sy1','Conn_sy2','Conn_H1','Conn_H2','Conn_W1','Conn_W2',...
    'HMV_x1','HMV_y1','HMV_x2','HMV_y2','H1','H2'...
    );

end