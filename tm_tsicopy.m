function tm_tsicopy(inputDir,outputDir,SEC_RAN)
inputDir=FormatDirName(inputDir);
outputDir=FormatDirName(outputDir);
if ~exist(outputDir,'dir')
    mkdir(outputDir);
end
SEC_RAN_DN=datenum([0 0 0 0 0 SEC_RAN]);
ONEDAY_DN=datenum([0 0 1 0 0 0]);


datevectors=[
    [2012 07 21 0 0 0],
    [2012 07 27 0 0 0],
    [2012 07 30 0 0 0],
    [2012 08 05 0 0 0],
    [2012 08 09 0 0 0],
    [2012 08 26 0 0 0],
    [2012 08 28 0 0 0],
    [2012 09 16 0 0 0],
    [2012 09 24 0 0 0]];
dvs=datevectors;
dns=datenum(dvs);
ndays=length(dns);
allfiles=dir(inputDir);
[nfiles,~]=size(allfiles);
oriimgdirname=GetFilenameFromAbsPath(inputDir);
avail_f=sprintf('avail_%s.mat',oriimgdirname);
if ~exist(avail_f,'file')
    tmdns=zeros(nfiles,1);
    for i =1:nfiles
        if allfiles(i).isdir==1
            continue;
        end
        filename=allfiles(i).name;
        tmdv=GetDVFromImg(filename);
        tmdn=datenum(tmdv);
        tmdns(i)=tmdn;
    end
    save(avail_f,'tmdns');
end
t=load(avail_f);
tmdns=t.tmdns;
% intersection of chosen period
maxdn=max(tmdns);
mindn=min(tmdns>0);
startdn=mindn;
enddn=maxdn;

for i =1:ndays
    odstartdn=dns(i);
    odenddn=odstartdn+ONEDAY_DN;
    ind=find(tmdns>=odstartdn&tmdns<odenddn);
    oddns=tmdns(ind);
    odnfiles=length(oddns);
    predn=0;
    for j =1:odnfiles
        tmdn=oddns(j);
        if tmdn-predn<SEC_RAN_DN
            continue;
        end
        predn=tmdn;
        indexi=ind(j);
        filename=allfiles(indexi).name;
        inputf=sprintf('%s%s',inputDir,filename);
        outputf=sprintf('%s%s',outputDir,filename);
        copyfile(inputf,outputf);
    end
end


end