function [x y c]=FindMinCorr(corrR,startp,distrange)
[H,W]=size(corrR);
corrR_serial=corrR(:);
[corrR_s,i_s]=sort(corrR_serial,'descend');
sy=startp(1);
sx=startp(2);
bFound=false;
x=0;y=0;c=0;
for tmi=1:length(corrR_s)
    [ypeak, xpeak] = ind2sub([H W],i_s(tmi));
    l=sqrt((ypeak-sy)^2+(xpeak-sx)^2);
    if l<distrange
        bFound=true;
        break;
    end
end
if bFound
    x=xpeak;
    y=ypeak;
    c=corrR_s(tmi);
end

end