function SBMaskGeneration_Ro(DataDir,testfilldir,SBMatfile,SBMaskOutDir)
if(SBMaskOutDir(end)~='/')
    SBMaskOutDir=sprintf('%s%s',SBMaskOutDir,'/');
end
if(testfilldir(end)~='/')
    testfilldir=sprintf('%s%s',testfilldir,'/');
end
if(DataDir(end)~='/')
    DataDir=sprintf('%s%s',DataDir,'/');
end
mkdir(SBMaskOutDir);
mkdir(testfilldir);

load(SBMatfile);
load('UndistortionParameters.mat');
orimg=sbmaskma;
firstH=Hi;firstW=Wi;
CenShiftX=0;
CenShiftY=0;
paddingx=ones(firstH,abs(CenShiftX));
paddingy=ones(abs(CenShiftY),firstW);
if(CenShiftX>0)
    firstpadimg=[paddingx,orimg(:,1:end-abs(CenShiftX))];
else
    firstpadimg=[orimg(:,1+abs(CenShiftX):end),paddingx];
end
if(CenShiftY>0) 
    secondpadimg=[paddingy;firstpadimg(1:end-abs(CenShiftY),:)];
else
    secondpadimg=[firstpadimg(1+abs(CenShiftY):end,:); paddingy];
end
roimg=secondpadimg;
roimg=~roimg;
roimg(roimg>0)=1;
% roimg=~logical(roimg);

datevector=datevec(sbmaskdn);
time.year    =  datevector(1);
time.month   =  datevector(2);
time.day     =  datevector(3);
time.hour    =  datevector(4);
time.min    =   datevector(5);
time.sec    =   datevector(6);
time.UTC    =   0;
sun = sun_position(time, location);
az_mask=sun.azimuth/180*pi;
ze_mask=sun.zenith/180*pi;
ORI_WIDTH=Wi;
ORI_HEIGHT=Hi;
rcentrep.x=ORI_WIDTH/2+CenShiftX;     %real original center(rotating center)
rcentrep.y=ORI_HEIGHT/2+CenShiftY;

ro_cenx=ORI_WIDTH/2;
ro_ceny=ORI_HEIGHT/2;
ro_icenx=ro_cenx-CenShiftX;
ro_iceny=ro_ceny-CenShiftY;

allfiles=dir(DataDir);
[m,~]=size(allfiles);
tmcolor=zeros(Hi,Wi);
tmcolor=uint8(tmcolor);

paddingx=zeros(firstH,abs(CenShiftX));
paddingy=zeros(abs(CenShiftY),firstW);
for i =3:m
    filename=allfiles(i).name;
    if(allfiles(i).bytes<=1000)
        continue;
    end
    tmdv=GetDVFromImg(filename);
    inputf=sprintf('%s%s',DataDir,filename);
    img1=imread(inputf);    %original image readin
    outputf=sprintf('%s%s',testfilldir,filename);
    outfma=sprintf('%s%s.mat',SBMaskOutDir,filename);
    datevector=tmdv;
    time.year    =  datevector(1);
    time.month   =  datevector(2);
    time.day     =  datevector(3);
    time.hour    =  datevector(4);
    time.min    =   datevector(5);
    time.sec    =   datevector(6);
    time.UTC    =   0;
    sun = sun_position(time, location);
    az=sun.azimuth/180*pi;
    ze=sun.zenith/180*pi;
    RotatedMa=imrotate(roimg,(az_mask-az)/pi*180);
%     RotatedMa=imrotate(roimg,180);
    [RoH,RoW]=size(RotatedMa);
    ro_ceny=round(RoH/2);ro_cenx=round(RoW/2);
    NormalMa=RotatedMa(ro_ceny-ORI_HEIGHT/2+1:ro_ceny+ORI_HEIGHT/2,ro_cenx-ORI_WIDTH/2+1:ro_cenx+ORI_WIDTH/2);
    i
    if(CenShiftX<0)
        firstpadimg=[paddingx,NormalMa(:,1:end-abs(CenShiftX))];
    else
        firstpadimg=[NormalMa(:,1+abs(CenShiftX):end),paddingx];
    end
    if(CenShiftY<0)
        secondpadimg=[paddingy;firstpadimg(1:end-abs(CenShiftY),:)];
    else
        secondpadimg=[firstpadimg(1+abs(CenShiftY):end,:); paddingy];
    end
    secondpadimg(secondpadimg>0)=1;
    imaskma=logical(secondpadimg);
    for j=1:3
        %         img2(totalmaskma,j)=img1(totalmaskma,j);
        tmcolor(:,:)=img1(:,:,j);
        tmcolor(imaskma)=0;
        img1(:,:,j)=tmcolor(:,:);
    end
    imwrite(img1,outputf,'jpg');
end