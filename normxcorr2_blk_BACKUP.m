% version 1.0
%   Add control of A or template = ALLBLACK
%   1. if no black c = normxcorr2(template,A)
%   2. if ALLBLACK, c = 0 
%   3. if template or A NONBlack std = 0, c=0
function cc=normxcorr2_blk_BACKUP(template,A)
template=single(template);
A=single(A);
template(template==0)=nan;
A(A==0)=nan;

cc=0;
t1=isnan(template);
t2=isnan(A);
if ~isempty(find(t1==1,1))
    bBlack_t=true;
else
    bBlack_t=false;
end
if ~isempty(find(t2==1,1))
    bBlack_A=true;
else
    bBlack_A=false;
end

% bBlack_A=false;
% bBlack_t=true;

if ~bBlack_A && ~bBlack_t
    cc=normxcorr2(template,A);
    return;
end

[H,W]=size(A);
[Ht,Wt]=size(template);

if isempty(find(t1==0,1)) || isempty(find(t2==0,1))
    % template or A are all black/not valid. cc=zero
    cc=zeros(H+Ht-1,W+Wt-1);
    return;
end



nb_indma=~t1;
% nb_std_t=nanstd(template(:));
% nb_mean_t=nanmean(template(:));
nb_std_t=std(template(nb_indma),1);
nb_mean_t=mean(template(nb_indma),1);

Diff_t=template(nb_indma)-nb_mean_t;


cc_vals=zeros(H,W);
std_tms=zeros(H,W);
mean_tms=zeros(H,W);
Diff_all=zeros(H,W);
if ~bBlack_A
    N=length(find(nb_indma));
    for y=1:H-Ht+1
        for x=1:W-Wt+1
            tmblk=A(y:y+Ht-1,x:x+Wt-1);
            tmvals=tmblk(nb_indma);
            nb_std_tm=nanstd(tmvals,1);
            nb_mean_tm=nanmean(tmvals);
            Diff_tm=tmvals-nb_mean_tm;
            Diff_all(y,x)=dot(Diff_tm,Diff_t);
            std_tms(y,x)=nb_std_tm;
        end
    end
    tmtail=nb_std_t*N;
    cc_vals=Diff_all./std_tms./tmtail;
    cc_vals(isnan(cc_vals))=0;
    
    cc(Ht:Ht+H-1,Wt:Wt+W-1)=cc_vals(:,:);
else
    std_ts=zeros(H,W);
    mean_ts=zeros(H,W);
    nc=zeros(H,W);
    for y=1:H-Ht+1
        for x=1:W-Wt+1
            tmblk=A(y:y+Ht-1,x:x+Wt-1);
            nb_indma_all=~isnan(tmblk)&nb_indma;
            N=length(find(nb_indma_all));
            tmvals1=template(nb_indma_all);
            tmvals2=tmblk(nb_indma_all);
            std_ts(y,x)=std(tmvals1,1);
            mean_ts(y,x)=mean(tmvals1);
            std_tms(y,x)=std(tmvals2,1);
            mean_tms(y,x)=mean(tmvals2);
            tmfval=std_tms(y,x)*std_ts(y,x);
            cc_single=dot((tmvals1-mean_ts(y,x)),(tmvals2-mean_tms(y,x)))/tmfval;
            cc_vals(y,x)=cc_single;
            nc(y,x)=N;
        end
    end
    cc_vals=cc_vals./nc;
    cc(Ht:Ht+H-1,Wt:Wt+W-1)=cc_vals(:,:);
end


end