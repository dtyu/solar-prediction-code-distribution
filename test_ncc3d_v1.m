% version 1.0:
%   1. consider the black area
%   2. Add SearchWinSize_ext as input parameter
function [mv_x mv_y H ux31 uy31 maxcc]=test_ncc3d_v1(imma21,imma22,imma31,imma32,ux21,uy21,recW,recH,SearchWinSize_ext,H_start)
mtsi_startup;

uy22=uy21-SearchWinSize_ext;
ux22=ux21-SearchWinSize_ext;
%SearchWinSize=SearchWinSize_ext*2+1;
uy21_e=uy21+recH-1;
ux21_e=ux21+recW-1;
uy22_e=uy21_e+SearchWinSize_ext;
ux22_e=ux21_e+SearchWinSize_ext;

%   1. Generate NCC result for TSI2 pair
%blk21=imma21(uy21:uy21_e,ux21:ux21_e);
blk21=single(TO_GetImgVal_Pad([uy21 ux21],[recH recW],imma21,SearchWinSize_ext));
blk22_search=single(TO_GetImgVal_Pad([uy22 ux22],[uy22_e-uy22+1 ux22_e-ux22+1],imma22,SearchWinSize_ext));
%blk22_search=imma22(uy22:uy22_e,ux22:ux22_e);
cc=normxcorr2_blk(blk21,blk22_search);



[Hc,Wc]=size(cc);
%   2. Generate NCC result for TSI3 pair based on different H
% H_ran=[1000:100:5000 5500:1000:15000];
% H_l=length(H_ran);

% Calculate cc of blk31 ~ blk21 (sim_all)
t=load('tmhma_new.mat');
H_all=t.H_all;
H_valid=H_all;
[HMV_H HMV_W]=size(H_valid);
sim_all=zeros(HMV_H,HMV_W);
HMV_validma=(H_valid>H_start);
H_ran_ind=find(HMV_validma);
H_ran=H_valid(HMV_validma);
[H_ran si]=sort(H_ran);
H_ran_ind=H_ran_ind(si);

[HMV_y HMV_x]=ind2sub([HMV_H HMV_W],H_ran_ind);
HMV_x=HMV_x-241;
HMV_y=HMV_y-241;
H_l=length(H_ran_ind);
cc_all=zeros(Hc,Wc,H_l);
sim_all=zeros(H_l,1);
ux31_all=zeros(H_l,1);
uy31_all=zeros(H_l,1);

for i=1:H_l
   tmHMV_x=HMV_x(i);
   tmHMV_y=HMV_y(i);
   ux31=round(ux21+tmHMV_x);
   uy31=round(uy21+tmHMV_y);
   ux31_e=round(ux31+recW-1);
   uy31_e=round(uy31+recH-1);
   blk31=single(TO_GetImgVal_Pad([uy31 ux31],[recH recW],imma31,SearchWinSize_ext));
   
   if std(blk31(:))==0
       continue;
   end
   tmcc=normxcorr2_blk(blk21,blk31);
   sim_all(i)=tmcc(recH,recW);
end
sim_max=max(sim_all(:));
for i=1:H_l
    %   2.1 (ux,uy) + H TSI2 ->(ux_new,uy_new) on TSI3
    %H=H_ran(i);
    if sim_all(i)<=sim_max/2 || isnan(sim_all(i)) || sim_all(i)==0
        continue;
    end
    tmHMV_x=HMV_x(i);
    tmHMV_y=HMV_y(i);
    ux31=round(ux21+tmHMV_x);
    uy31=round(uy21+tmHMV_y);
    ux31_e=round(ux31+recW-1);
    uy31_e=round(uy31+recH-1);
    uy32=round(uy31-SearchWinSize_ext);
    ux32=round(ux31-SearchWinSize_ext);
    uy32_e=round(uy31_e+SearchWinSize_ext);
    ux32_e=round(ux31_e+SearchWinSize_ext);
    ux31_all(i)=ux31;
    uy31_all(i)=uy31;
    %   2.2 Generate NCC result for TSI3 pair
    if uy31>UNDISTH || ux31>UNDISTW || uy31<1 || ux31<1 ||...
        uy31_e>UNDISTH || ux31_e>UNDISTW || uy31_e<1 || ux31_e<1
        continue;
    end
    if uy32>UNDISTH || ux32>UNDISTW || uy32<1 || ux32<1 ||...
        uy32_e>UNDISTH || ux32_e>UNDISTW || uy32_e<1 || ux32_e<1
        continue;
    end
    blk31=single(imma31(uy31:uy31_e,ux31:ux31_e));
    blk32_search=single(imma32(uy32:uy32_e,ux32:ux32_e));
    if std(blk31(:))~=0
        cc_3=normxcorr2_blk(blk31,blk32_search);
    else
        cc_3=zeros(Hc,Wc);
    end
    
    %   2.3 Calculate similarity here
    if sim_all(i)>0
        cc_all(:,:,i)=sim_all(i)+(cc+cc_3);
        %cc_all(:,:,i)=cc;
    end
end


% for i=1:H_l
%     %   2.1 (ux,uy) + H TSI2 ->(ux_new,uy_new) on TSI3
%     H=H_ran(i);
% %     if i~=10
% %         continue;
% %     end
%     if H<4600
%         continue;
%     end
%     [rx21 ry21 rz21]=TO_Undist2RelativeCoor(ux21,uy21,H);
%     [rx31 ry31 rz31]=TO_RelativeCoor2RelativeCoor(rx21,ry21,rz21,location2,location3);
%     [ux31 uy31]=TO_RelativeCoor2Undist(rx31,ry31,rz31);
%     ux31=round(ux31);
%     uy31=round(uy31);
%     uy31_e=round(uy31+recH-1);
%     ux31_e=round(ux31+recW-1);
%     uy32=round(uy31-SearchWinSize_ext);
%     ux32=round(ux31-SearchWinSize_ext);
%     uy32_e=round(uy31_e+SearchWinSize_ext);
%     ux32_e=round(ux31_e+SearchWinSize_ext);
%     ux31_all(i)=ux31;
%     uy31_all(i)=uy31;
%     %   2.2 Generate NCC result for TSI3 pair
%     if uy31>UNDISTH || ux31>UNDISTW || uy31<1 || ux31<1 ||...
%         uy31_e>UNDISTH || ux31_e>UNDISTW || uy31_e<1 || ux31_e<1
%         continue;
%     end
%     if uy32>UNDISTH || ux32>UNDISTW || uy32<1 || ux32<1 ||...
%         uy32_e>UNDISTH || ux32_e>UNDISTW || uy32_e<1 || ux32_e<1
%         continue;
%     end
%     blk31=single(imma31(uy31:uy31_e,ux31:ux31_e));
%     if std(blk31(:))~=0
%         tmcc=normxcorr2_blk(blk21,blk31);
%         sim_all(i)=tmcc(recH,recW);
%     else
%         sim_all(i)=0;
%     end
% end


% sim_max=max(sim_all);
% 
% for i=1:H_l
%     %   2.1 (ux,uy) + H TSI2 ->(ux_new,uy_new) on TSI3
%     H=H_ran(i);
%     if sim_all(i)<=sim_max/2 && ~isnan(sim_all(i))
%         continue;
%     end
% 
%     [rx21 ry21 rz21]=TO_Undist2RelativeCoor(ux21,uy21,H);
%     [rx31 ry31 rz31]=TO_RelativeCoor2RelativeCoor(rx21,ry21,rz21,location2,location3);
%     [ux31 uy31]=TO_RelativeCoor2Undist(rx31,ry31,rz31);
%     ux31=round(ux31);
%     uy31=round(uy31);
%     uy31_e=round(uy31+recH-1);
%     ux31_e=round(ux31+recW-1);
%     uy32=round(uy31-SearchWinSize_ext);
%     ux32=round(ux31-SearchWinSize_ext);
%     uy32_e=round(uy31_e+SearchWinSize_ext);
%     ux32_e=round(ux31_e+SearchWinSize_ext);
%     ux31_all(i)=ux31;
%     uy31_all(i)=uy31;
%     %   2.2 Generate NCC result for TSI3 pair
%     if uy31>UNDISTH || ux31>UNDISTW || uy31<1 || ux31<1 ||...
%         uy31_e>UNDISTH || ux31_e>UNDISTW || uy31_e<1 || ux31_e<1
%         continue;
%     end
%     if uy32>UNDISTH || ux32>UNDISTW || uy32<1 || ux32<1 ||...
%         uy32_e>UNDISTH || ux32_e>UNDISTW || uy32_e<1 || ux32_e<1
%         continue;
%     end
%     blk31=single(imma31(uy31:uy31_e,ux31:ux31_e));
%     blk32_search=single(imma32(uy32:uy32_e,ux32:ux32_e));
%     if std(blk31(:))~=0
%         cc_3=normxcorr2_blk(blk31,blk32_search);
%     else
%         cc_3=zeros(Hc,Wc);
%     end
%     
%     %   2.3 Calculate similarity here
%     if sim_all(i)>0
%         cc_all(:,:,i)=sim_all(i)*(cc+cc_3);
%         %cc_all(:,:,i)=cc;
%     end
% end

%   3. Find the maximum ncc value and its mv_x,mv_y,H
% h_ind=find(H_ran>3000,1);
% cc_all_high=cc_all(:,:,h_ind:end);
% cc_all_low=cc_all(:,:,1:h_ind);
% [maxval_h,ind]=max(cc_all_high(:));
% [tmy tmx Hi]=ind2sub(size(cc_all_high),ind);
% Hi=Hi+h_ind-1;
% H_h=H_ran(Hi);
% ux31_h=ux31_all(Hi);
% uy31_h=uy31_all(Hi);
% mv_x_h=tmx-recW-SearchWinSize_ext;
% mv_y_h=tmy-recH-SearchWinSize_ext;
% 
% [maxval_l,ind]=max(cc_all_low(:));
% [tmy tmx Hi]=ind2sub(size(cc_all_low),ind);
% H_l=H_ran(Hi);
% ux31_l=ux31_all(Hi);
% uy31_l=uy31_all(Hi);
% mv_x_l=tmx-recW-SearchWinSize_ext;
% mv_y_l=tmy-recH-SearchWinSize_ext;

% find peaks of similarity matrix
H_select_ran=1000:100:5000;
H_select_ran=[H_select_ran 5500:500:20000];
len_s=length(H_select_ran);

for i=1:len_s
    
end

[pks,locs]=findpeaks_last(sim_all);
[pks_sort,indsort]=sort(pks);
locs_sort=locs(indsort);
pks=pks_sort;
locs=locs_sort;

len_pks=length(pks);
if len_pks>=2
    locs_pick=locs(end-1:end);
    seg_loc=round(mean(locs_pick));
%     if abs(seg_loc-locs_pick)<5
%         tmind=find(abs(locs_pick(end)-locs)>=10);
%         if ~isempty(tmind)
%             % choose the largest sim in pks list choose the middle segmentaion ind between two peaks
%             %   locs_pick(end) the largest pks, locs(tmind(end)) the largest pks away from locs_pick(end)
%             seg_loc=round(mean([locs(tmind(end)) locs_pick(end)])); 
%         end
%     end
elseif len_pks==1
    seg_loc=find(H_ran>2500,1);
else
    % no pks found
    seg_loc=find(H_ran>2500,1);
end
h_ind=seg_loc;
cc_all_high=cc_all(:,:,h_ind:end);
cc_all_low=cc_all(:,:,1:h_ind);
[maxval_h,ind]=max(cc_all_high(:));
[tmy,tmx,Hi]=ind2sub(size(cc_all_high),ind);
Hi=Hi+h_ind-1;
H_h=H_ran(Hi);
ux31_h=ux31_all(Hi);
uy31_h=uy31_all(Hi);
mv_x_h=tmx-recW-SearchWinSize_ext;
mv_y_h=tmy-recH-SearchWinSize_ext;

[maxval_l,ind]=max(cc_all_low(:));
[tmy,tmx,Hi]=ind2sub(size(cc_all_low),ind);
H_l=H_ran(Hi);
ux31_l=ux31_all(Hi);
uy31_l=uy31_all(Hi);
mv_x_l=tmx-recW-SearchWinSize_ext;
mv_y_l=tmy-recH-SearchWinSize_ext;    


H=[H_h H_l];
ux31=[ux31_h ux31_l];
uy31=[uy31_h uy31_l];
mv_x=[mv_x_h mv_x_l];
mv_y=[mv_y_h mv_y_l];
maxcc=[maxval_h maxval_l];



end