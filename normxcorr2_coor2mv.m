function [mv_y,mv_x]=normxcorr2_coor2mv(coor_p,templateSize,ASize)
SearchWinSizeH=round((ASize(1)-templateSize(1))/2);
SearchWinSizeW=round((ASize(2)-templateSize(2))/2);
mv_y=coor_p(1)-templateSize(1)-SearchWinSizeH;
mv_x=coor_p(2)-templateSize(2)-SearchWinSizeW;
end