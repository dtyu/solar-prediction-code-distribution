% TO_visualizeTSI
%
%   VERSION 2.0     2013-11-26
%   USAGE:
%       TO_visualizeTSI('/data/workdata/mtsi_stich/',
%           '/data/workdata/mtsi_stich/tsi3_undist_jpg_sample1/',
%           'tsi3',[2013 05 07 14 0 0],[2013 05 08 0 0 0],300);

function TO_visualizeTSI(dataDir,tsijpgoutDir,site,startdv,enddv,freq)
startdn=datenum(startdv);
enddn=datenum(enddv);
tsijpgoutDir=FormatDirName(tsijpgoutDir);
if ~exist(tsijpgoutDir,'dir')
    mkdir(tsijpgoutDir);
end
assert(freq>=10,'Freq should be larger than the minimum timespan. In this case 10s');
TENSECS_DN=datenum([0 0 0 0 0 10]);
FREQ_DN=datenum([0 0 0 0 0 freq]);


tmdn=startdn;
oddns=[];
i=0;
while(tmdn<enddn)
    tmdn=startdn+i*FREQ_DN;
    oddns=[oddns;tmdn];
    i=i+1;
end
oddvs=datevec(oddns);
oddns_n=length(oddns);
for i =1:oddns_n-1
    tmdv=oddvs(i,:);
    try
        imma1=tmp_GetImgMatFromDataDir(tmdv,site,dataDir);
    catch
        continue;
    end
    filename=GetImgFromDV_BNL(tmdv,site);
    outputf=sprintf('%s%s',tsijpgoutDir,filename);
    imwrite(imma1,outputf,'jpg');
end

end