function test_hamasktsi12tsi2(inputDir,outputDir)
inputDir=FormatDirName(inputDir);
outputDir=FormatDirName(outputDir);
mkdir(outputDir);

allfiles=dir(inputDir);
[nfiles,~]=size(allfiles);
TSI2='tsi2';
for i =1:nfiles
    if allfiles(i).isdir==1
        continue;
    end
    filename=allfiles(i).name;
    tmdv=GetDVFromImg(filename);
    f2=GetImgFromDV_BNL(tmdv,TSI2);
    inputf=sprintf('%s%s',inputDir,filename);
    outputf=sprintf('%s%s.mat',outputDir,f2);
    copyfile(inputf,outputf);
end
end