function [imma_cld cldmask]=test_GenCldMask(imma)
r1=imma(:,:,1);
g1=imma(:,:,2);
b1=imma(:,:,3);

% ind=(b1>r1+20)&(r1<=80);
rbr=double(r1)./double(b1);
rgr=double(r1)./double(g1);
ind=(rbr<=0.8);

r2=r1;
g2=g1;
b2=b1;
imma2=imma;
r2(ind)=0;
g2(ind)=0;
b2(ind)=0;

imma2(:,:,1)=r2;
imma2(:,:,2)=g2;
imma2(:,:,3)=b2;

cldmask=ind;
imma_cld=imma2;

end