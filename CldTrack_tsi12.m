function CldTrack_tsi12(cldtrkmatoutf_recheck,cldtrkmatoutf_tsi12,cldmodelmatf,dataDir)

filename=GetFilenameFromAbsPath(cldtrkmatoutf_recheck);
tmdv=GetDVFromImg(filename);
[imma1_cldma_n,imma2_cldma_n,imma3_cldma_n]=clouddetector(tmdv,cldmodelmatf,dataDir);

% load the TSI3 CF information
t=load(cldtrkmatoutf_recheck);
H=t.H;
Conn_sx=t.Conn_sx;    Conn_sy=t.Conn_sy;
Conn_H=t.Conn_H;      Conn_W=t.Conn_W;
HMV_x=t.HMV_x; HMV_y=t.HMV_y;
MAXCC=t.MAXCC;
nConn=t.nCF;
ma1=imma1_cldma_n;
ma2=imma2_cldma_n;
%   Find non-collapse area on TSI1 and TSI2
for i=1:nConn
   if MAXCC(i) ==0
       continue;
   end
   sx1=Conn_sx(i)+HMV_x(2); sy1=Conn_sy(i)+HMV_y(2);
   sx2=Conn_sx(i)+HMV_x(1); sy2=Conn_sy(i)+HMV_y(1);
   recH=Conn_H(i); recW=Conn_W(i);
   ex1=sx1+recW;  ey1=sy1+recH;
   ex2=sx2+recW;  ey2=sy2+recH;
   ma1(sy1:ey1,sx1:ex1)=false;
   ma2(sy2:ey2,sx2:ex2)=false;
end

%   Get CF on non-collapse area on TSI1 and TSI2
[CFMaskMa1,nCF1]=cloudfieldext(ma1);
[CFMaskMa2,nCF2]=cloudfieldext(ma2);



    
    
end