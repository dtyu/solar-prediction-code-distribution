g1= single(rgb2gray(imma2_1mbf));
g2= single(rgb2gray(imma2));
invalidma=g1<50|g2<50;
diff_g=(g2-g1);
diff_g(invalidma)=0;
norm_g=TO_NormOfMatrix(diff_g);
figure();
imshow(norm_g);
