% Tool to get modified time using java. Use TO_isDirChanged to judge whether the timestamp is newest or not
%   VERSION 1.0 2014-05-08
function lastmodified = TO_getDirModtime(inDir)
f = java.io.File(inDir);
lastmodified=f.lastModified;
end