function imObj_shift=TO_imshift(imObj,shiftvec)
shift_h=shiftvec(1);
shift_w=shiftvec(2);
[H,W,nDim]=size(imObj);
assert(nDim==1||nDim==3,'The input image object should be RGB (HxWx3) or Greyscale image (HxW)');
imObj_shift=zeros(size(imObj));
if abs(shift_h)>H ||abs(shift_w)>W
    %imObj_shift=zeros(size(imObj));
    return;
end
imObj_shift=TO_SetImgVal_Pad([1+shift_h 1+shift_w],imObj,imObj_shift);

end