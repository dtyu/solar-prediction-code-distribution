function indexi=GetAZIndex_wThres(tsdn,azs,zes,tmdn,az,ze)

%   Method 1
% When azs contains az
% index=find(abs(azs-az)<azthres);
% if(isempty(index)==1)
%     indexi=-1;
%     return;
% end
% [~,mini]=min(abs(zes(index)-ze));
% indexi=index(mini);
% indexi=min(index);
%[minval,mini]=min(abs(azs-az));
%indexi=mini;

%   Method 2
% [minval,mini]=min(abs(azs-az));
% if(minval<azthres)
%     indexi=mini;
% else
%     indexi=-1;
% end

DNThres=datenum([0 0 0 0 3 0]);
DAYThres=datenum([0 0 4 0 0 0]);
azthres=2/180*pi;

%   Option 1: in nearest days pick nearest mask minutes ahead or after
ddns=abs(tsdn-tmdn);
[diffdn,diffi]=min(ddns);
if diffdn<DNThres  % trust -3~3 minites data before getting az match
    indexi=diffi;
    return;
end

%   Option 2: in nearest days pick nearest az if in range
%Get -2 ~ 2 days mask files, choose most match by az diff
ind=(ddns<=DAYThres);
ind_index=find(ind);
az_ran=azs(ind);
[diffaz,diffi]=min(abs(az_ran-az));
if diffaz<azthres
    indexi=ind_index(diffi);
    return;
end

%   Option 3: choose nearest az if in range
% last option is to choose nearest az
[minval,mini]=min(abs(azs-az));
if(minval<azthres)
    indexi=mini;
else
    indexi=-1;
end

end