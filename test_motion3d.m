function test_motion3d
mtsi_startup;

tsiDirs={'./cloudy_multilayer/tsi1_undist_20130605/','./cloudy_multilayer/tsi2_undist_20130605/','./cloudy_multilayer/tsi3_undist_20130605/'};
otsiDirs={'./otsi1/','./otsi2/','./otsi3/'};

nTSI=3;
tsi1Dir=tsiDirs{1};
tsi2Dir=tsiDirs{2};
tsi3Dir=tsiDirs{3};
otsi1Dir=otsiDirs{1};
otsi2Dir=otsiDirs{2};
otsi3Dir=otsiDirs{3};
mkdir(otsi1Dir);
mkdir(otsi2Dir);
mkdir(otsi3Dir);
alltsi1=dir(tsi1Dir);
[nfiles1,~]=size(alltsi1);
imma1_total=zeros(UNDISTH,UNDISTW,3);
imma2_total=zeros(UNDISTH,UNDISTW,3);
imma3_total=zeros(UNDISTH,UNDISTW,3);
N1=zeros(UNDISTH,UNDISTW);
N2=zeros(UNDISTH,UNDISTW);
N3=zeros(UNDISTH,UNDISTW);

imma_avgf='./imma_avgf.mat';
t=load(imma_avgf,'imma1_avg','imma2_avg','imma3_avg');
imma1_avg=t.imma1_avg;
imma2_avg=t.imma2_avg;
imma3_avg=t.imma3_avg;

OutputPath='./tmptestme.mat';
InnerBlockSize=10;
BlockSize=50;
SearchWinSize=50;
CorThreshold=0.5;
BlackThreshold=0;

MEOutDir='./ME_test_img';
FillOutDir='./Fillimg';
MEOutDir=FormatDirName(MEOutDir);
FillOutDir=FormatDirName(FillOutDir);
if ~exist(MEOutDir,'dir')
    mkdir(MEOutDir);
end
if ~exist(FillOutDir,'dir')
    mkdir(FillOutDir);
end
ONEMIN_DN=datenum([0 0 0 0 1 0]);
TENSECS_DN=datenum([0 0 0 0 0 10]);


for i =1:nfiles1
    filename=alltsi1(i).name;
    f1=sprintf('%s%s',tsi1Dir,filename);
    tmdv=GetDVFromImg(filename);
    if tmdv==0
        continue;
    end
    tmdn=datenum(tmdv);
%     if tmdn<datenum([2013 06 05 15 00 50])
%         continue;
%     end
    if tmdn<datenum([2013 06 05 14 42 50])
        continue;
    end

    tmdn_1maf=tmdn+TENSECS_DN;
    tmdn_1mbf=tmdn-ONEMIN_DN;
    f2_1maf=GetImgFromDV_BNL(datevec(tmdn_1maf),TSI2);
    f2_1mbf=GetImgFromDV_BNL(datevec(tmdn_1mbf),TSI2);
    f3_1maf=GetImgFromDV_BNL(datevec(tmdn_1maf),TSI3);
    filename2=GetImgFromDV_BNL(tmdv,TSI2);
    filename3=GetImgFromDV_BNL(tmdv,TSI3);
%     f2=sprintf('%s%s.bmp',tsi2Dir,filename2(1:end-4));
%     f3=sprintf('%s%s.bmp',tsi3Dir,filename3(1:end-4));
    f2=sprintf('%s%s.jpg',tsi2Dir,filename2(1:end-4));
    f2_1maf=sprintf('%s%s.jpg',tsi2Dir,f2_1maf(1:end-4));
    f2_1mbf=sprintf('%s%s.jpg',tsi2Dir,f2_1mbf(1:end-4));
    f3=sprintf('%s%s.jpg',tsi3Dir,filename3(1:end-4));
    f3_1maf=sprintf('%s%s.jpg',tsi3Dir,f3_1maf(1:end-4));
    try
        imma1=tmp_GetImgMatFromDataDir(f1);
        imma2=tmp_GetImgMatFromDataDir(f2);
        imma3=tmp_GetImgMatFromDataDir(f3);
    catch
        continue;
    end
    imma2_1maf=tmp_GetImgMatFromDataDir(f2_1maf);
    imma2_1mbf=tmp_GetImgMatFromDataDir(f2_1mbf);
    imma3_1maf=tmp_GetImgMatFromDataDir(f3_1maf);

    
    %% New algorithm for std change
    [ma2,cldind2]=test_GenCldMask_2r(imma2,imma2_avg);
    [bstarty,bstartx,bendy,bendx,nBlocky,nBlockx,blockvals]=DivideBlocks(...
        rgb2gray(imma2),[InnerBlockSize InnerBlockSize]);
    stdma=zeros(nBlocky,nBlockx);
    meanma=zeros(nBlocky,nBlockx);
    blkclass=zeros(nBlocky,nBlockx);
    blkH=zeros(nBlocky,nBlockx);
    blkmv_x=zeros(nBlocky,nBlockx);
    blkmv_y=zeros(nBlocky,nBlockx);
    blkmv_Hy=zeros(nBlocky,nBlockx);
    blkmv_Hx=zeros(nBlocky,nBlockx);
    SearchWin_Ext=15; 
    for bi=1:nBlocky
        for bj=1:nBlockx
            tmblk=blockvals(bi,bj,:,:);
            tmblk_cld=cldind2(bstarty(bi,bj):bendy(bi,bj),bstartx(bi,bj):bendx(bi,bj));
            if ~isempty(find(tmblk<20,1))
                tmvals=tmblk(tmblk>=20);
                if length(tmvals)<50
                    stdma(bi,bj)=nan;
                    meanma(bi,bj)=nan;
                else
                    stdma(bi,bj)=std(tmvals(:));
                    meanma(bi,bj)=mean(tmvals(:));
                end
            else
                stdma(bi,bj)=std(tmblk(:));
                meanma(bi,bj)=mean(tmblk(:));
            end
            if isempty(find(tmblk_cld==1,1))
                blkclass(bi,bj)=0;
            else
                tml=length(find(tmblk_cld==1));
                if tml/100>0.4
                    blkclass(bi,bj)=1;
                else
                    blkclass(bi,bj)=0;
                end
            end
        end
    end
    if ~exist('tmpindmat.mat','file')
        cldmask_blk=blkclass;
        cldstdvals=stdma(logical(cldmask_blk));
        [IDX C]=kmeans(cldstdvals(:),2);
        segC=(C(1)+C(2))/2;
        BigStdindma=stdma>segC&~isnan(stdma)&cldmask_blk;
        SmallStdindma=stdma<=segC&~isnan(stdma)&cldmask_blk;
        
        trustma_high=test_blktrustveri(bstarty,bstartx,bendy,bendx,nBlocky,nBlockx,imma2,imma3,'','',3000:100:5000);
        SmallStdindma=SmallStdindma&trustma_high;
        
        meangreyThres=10;
        [ConnMap_s,ConnValid_s]=test_stdblkmerge(SmallStdindma,blockvals,segC,10,0.5,meangreyThres);

    nConn_s=length(ConnValid_s);
    corrblks=cell(nConn_s,1);
    minsy3=zeros(nConn_s,1);
    minsx3=zeros(nConn_s,1);
    maxey3=zeros(nConn_s,1);
    maxex3=zeros(nConn_s,1);
    fig1=figure();
    subplot(1,2,1);
    imshow(imma2);
    hold on;
    for ci=1:nConn_s
        if false==ConnValid_s(ci)
            continue;
        end
        disp(ci);
        tmind=find(ConnMap_s==ci);
        tmConn_ind=tmind;
        tmsy=bstarty(tmind);
        tmsx=bstartx(tmind);
        tmey=bendy(tmind);
        tmex=bendx(tmind);
        minsy=min(tmsy);
        minsx=min(tmsx);
        
        maxey=max(tmey);
        maxex=max(tmex);
        tmfillind=find(bstarty>=minsy&bendy<=maxey&bstartx>=minsx&bendx<=maxex&ConnMap_s~=ci);
        tml=length(tmfillind);
        tmimma2=imma2;
        for li=1:tml
            tmind=tmfillind(li);
            tmsy=bstarty(tmind);
            tmsx=bstartx(tmind);
            tmey=bendy(tmind);
            tmex=bendx(tmind);
            tmimma2(tmsy:tmey,tmsx:tmex,:)=0;
        end
        
        recH=maxey-minsy+1;
        recW=maxex-minsx+1;
        [mv_x mv_y H ux31 uy31 maxcc]=test_ncc3d_v1(rgb2gray(tmimma2),...
            rgb2gray(imma2_1maf),rgb2gray(imma3),rgb2gray(imma3_1maf),...
            minsx,minsy,recW,recH,SearchWin_Ext,3500);
        [~,choosei]=max(maxcc);
        
        if maxcc(choosei)==0
            continue;
        end
        
        
        [mv_Hx,mv_Hy]=TO_H2MV('./tmhma_new.mat',H(choosei));
        tmcorrblk=imma2(minsy:maxey,minsx:maxex,:);
        tml=length(tmConn_ind);
        for li=1:tml
            tmind=tmConn_ind(li);
            blkH(tmind)=H(choosei);
            blkmv_x(tmind)=mv_x(choosei);
            blkmv_y(tmind)=mv_y(choosei);
            blkmv_Hy(tmind)=mv_Hy;
            blkmv_Hx(tmind)=mv_Hx;
        end
        if H<2500
            disp(H);
        end
        
        if 1
            rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
            corrblks{ci}=tmcorrblk;
            %imshow(tmcorrblk);
            %tmcorrblk(tmcorrblk==0)=105;
            blkrbr=GetRBR(tmcorrblk);
            blkrbr(blkrbr==inf)=10;
            blkrbr(isnan(blkrbr))=0.2;
            ma3rbr=GetRBR(imma3);
            ma3rbr(isnan(ma3rbr))=0.2;
            tmceny=round((maxey+minsy)/2);
            tmcenx=round((maxex+minsx)/2);
            
            tempx=ux31(choosei);
            tempy=uy31(choosei);
            minsx3(ci)=tempx;
            minsy3(ci)=tempy;
            maxex3(ci)=tempx+recW;
            maxey3(ci)=tempy+recH;
            quiver(tmcenx,tmceny,mv_Hx,mv_Hy);
            if H(choosei)>=10000
                text(tmcenx,tmceny,['\color{red}' '\geq 10000'],'FontSize',18);
            else
                text(tmcenx,tmceny,['\color{red}' sprintf('%d',round(H(choosei)))],'FontSize',18);
            end
            %text(tmcenx,tmceny,['\color{red}' sprintf('%d',round(H(choosei)))],'FontSize',18);
        end
    end
    hold off;
    
    subplot(1,2,2);
    imshow(imma3);
    hold on;
    for ci =1:nConn_s
        if minsx3(ci)==0
            continue;
        end
        recW=maxex3(ci)-minsx3(ci)+1;
        recH=maxey3(ci)-minsy3(ci)+1;
        rectangle('Position',[minsx3(ci) minsy3(ci) recW recH],'LineStyle','--','EdgeColor','r');
    end
    hold off;
        


    [ConnMap,nConn]=bwlabeln(BigStdindma);
    corrblks=cell(nConn,1);
    minsy3=zeros(nConn,1);
    minsx3=zeros(nConn,1);
    maxey3=zeros(nConn,1);
    maxex3=zeros(nConn,1);
    fig1=figure();
    subplot(1,2,1);
    imshow(imma2);
    hold on;
    greyimma3=double(rgb2gray(imma3));
    for ci =1:nConn
        tmind=find(ConnMap==ci);
        tmConn_ind=tmind;
        tmsy=bstarty(tmind);
        tmsx=bstartx(tmind);
        tmey=bendy(tmind);
        tmex=bendx(tmind);
        minsy=min(tmsy);
        minsx=min(tmsx);
        maxey=max(tmey);
        maxex=max(tmex);
        recH=maxey-minsy+1;
        recW=maxex-minsx+1;
%         if ci~=4
%             continue;
%         end
        tmcorrblk=imma2(minsy:maxey,minsx:maxex,:);
        if length(tmcorrblk)<=20
            corrblks{ci}=0;
            continue;
        end
        [mv_x,mv_y,H,ux31,uy31, maxcc]=test_ncc3d_v1(rgb2gray(imma2),...
            rgb2gray(imma2_1maf),rgb2gray(imma3),rgb2gray(imma3_1maf),...
            minsx,minsy,recW,recH,SearchWin_Ext,1000);
        
        [~,choosei]=max(maxcc);
        [mv_Hx,mv_Hy]=TO_H2MV('./tmhma_new.mat',H(choosei));
        
        tml=length(tmConn_ind);
        for li=1:tml
            tmind=tmConn_ind(li);
            blkH(tmind)=H(choosei);
            blkmv_x(tmind)=mv_x(choosei);
            blkmv_y(tmind)=mv_y(choosei);
            blkmv_Hy(tmind)=mv_Hy;
            blkmv_Hx(tmind)=mv_Hx;
        end
        
        if 1
            rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
            corrblks{ci}=tmcorrblk;
            %imshow(tmcorrblk);
            %tmcorrblk(tmcorrblk==0)=105;
            blkrbr=GetRBR(tmcorrblk);
            blkrbr(blkrbr==inf)=10;
            blkrbr(isnan(blkrbr))=0.2;
            ma3rbr=GetRBR(imma3);
            ma3rbr(isnan(ma3rbr))=0.2;
            tmceny=round((maxey+minsy)/2);
            tmcenx=round((maxex+minsx)/2);
            
            
            tempx=ux31(choosei);
            tempy=uy31(choosei);
            minsx3(ci)=tempx;
            minsy3(ci)=tempy;
            maxex3(ci)=tempx+recW;
            maxey3(ci)=tempy+recH;
            quiver(tmcenx,tmceny,mv_Hx,mv_Hy);
            text(tmcenx,tmceny,['\color{red}' sprintf('%d',round(H(choosei)))],'FontSize',18);
        end
    end
    hold off;
    
    subplot(1,2,2);
    imshow(imma3);
    hold on;
    for ci =1:nConn
        if minsx3(ci)==0
            continue;
        end
        recW=maxex3(ci)-minsx3(ci)+1;
        recH=maxey3(ci)-minsy3(ci)+1;
        rectangle('Position',[minsx3(ci) minsy3(ci) recW recH],'LineStyle','--','EdgeColor','r');
    end
    hold off;
    save('tmpindmat.mat');
    else
        load('tmpindmat.mat');
    end
    
    % Extract the best of all
    H_high=median(blkH(SmallStdindma));
    SmallStdindma=stdma<=segC&~isnan(stdma)&cldmask_blk;
    blkH(SmallStdindma)=H_high;
    for bi=1:nBlocky
        for bj=1:nBlockx
            if 1==SmallStdindma(bi,bj)
                tmHvals=blkH(bi-1:bi+1,bj-1:bj+1);
                BigStdNeighbors=BigStdindma(bi-1:bi+1,bj-1:bj+1);
                if length(find(BigStdNeighbors==1))>=3
                    blkH(bi,bj)=median(tmHvals(BigStdNeighbors));
                elseif 0==blkH(bi,bj)
                    blkH(bi,bj)=median(tmHvals(:));
                end
            elseif 0==blkH(bi,bj) && 1==BigStdindma(bi,bj)
                tmHvals=blkH(bi-1:bi+1,bj-1:bj+1);
                blkH(bi,bj)=median(tmHvals(BigStdNeighbors));
            end
        end
    end
    
    imma3_filled=TO_FillBlack_new(imma2,imma3,blkH,bstarty,bstartx,bendy,bendx);
    
    trustma=test_blktrustveri(bstarty,bstartx,bendy,bendx,nBlocky,nBlockx,imma2,imma3,'','');
    indma=stdma>6&~isnan(stdma)&trustma;
    mv_xs=zeros(nBlocky,nBlockx);
    mv_ys=zeros(nBlocky,nBlockx);
    Hs=zeros(nBlocky,nBlockx);
    mv_Hxs=zeros(nBlocky,nBlockx);
    mv_Hys=zeros(nBlocky,nBlockx);
    for bi=1:nBlocky
        for bj=1:nBlockx
            if false==indma(bi,bj)
                continue;
            end
            ux21=bstartx(bi,bj);
            uy21=bstarty(bi,bj);
            recW=InnerBlockSize;
            recH=InnerBlockSize;
            [mv_x mv_y H ux31 uy31]=test_ncc3d(rgb2gray(imma2),rgb2gray(imma2_1maf),...
                rgb2gray(imma3),rgb2gray(imma3_1maf),ux21,uy21,recW,recH);
            [mv_Hx,mv_Hy]=TO_H2MV('./tmhma.mat',H);
            mv_xs(bi,bj)=mv_x;
            mv_ys(bi,bj)=mv_y;
            Hs(bi,bj)=H;
            mv_Hxs(bi,bj)=mv_Hx;
            mv_Hys(bi,bj)=mv_Hy;
        end
    end
    
    
    % Plot test
    figure();
    subplot(2,2,1);
    imshow(imma2);
    
    
    ux21=190;
    uy21=370;
    recW=10;recH=10;
    [mv_x mv_y H ux31 uy31]=test_ncc3d(rgb2gray(imma2),rgb2gray(imma2_1maf),...
        rgb2gray(imma3),rgb2gray(imma3_1maf),ux21,uy21,recW,recH);
    [mv_Hx,mv_Hy]=TO_H2MV('./tmhma.mat',H);
    figure();
    subplot(2,2,1);
    imshow(imma2);
    hold on;
    rectangle('Position',[ux21 uy21 recW recH],'LineStyle','--','EdgeColor','r');
    quiver(ux21+5,uy21+5,mv_x,mv_y,0);
    hold off;
    subplot(2,2,2);
    imshow(imma3);
    hold on;
    rectangle('Position',[ux31 uy31 recW recH],'LineStyle','--','EdgeColor','r');
    rectangle('Position',[ux21+mv_Hx uy21+mv_Hy recW recH],'LineStyle','--','EdgeColor','b');
    hold off;
    subplot(2,2,3);
    imshow(imma2_1maf);
    hold on;
    rectangle('Position',[ux21+mv_x uy21+mv_y recW recH],'LineStyle','--','EdgeColor','r');
    hold off;
    subplot(2,2,4);
    imshow(imma3_1maf);
    hold on;
    rectangle('Position',[ux31+mv_x uy31+mv_y recW recH],'LineStyle','--','EdgeColor','r');
    rectangle('Position',[ux21+mv_Hx+mv_x uy21+mv_Hy+mv_y recW recH],'LineStyle','--','EdgeColor','b');
    hold off;
    
    
end



end