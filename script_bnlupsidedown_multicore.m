function script_bnlupsidedown_multicore(oriimgDir,flipimgDir,startdv,enddv)
oriimgDir=FormatDirName(oriimgDir);
flipimgDir=FormatDirName(flipimgDir);
startdn=datenum(startdv);
enddn=datenum(enddv);
ONEDAY_DN=datenum([0 0 1 0 0 0]);
matlabinst=20;  %matlab inst number

allfiles=dir(oriimgDir);
[nfiles,~]=size(allfiles);
oriimgdirname=GetFilenameFromAbsPath(oriimgDir);
avail_f=sprintf('avail_%s.mat',oriimgdirname);
if ~exist(avail_f,'file')
    tmdns=zeros(nfiles,1);
    for i =1:nfiles
        if allfiles(i).isdir==1
            continue;
        end
        filename=allfiles(i).name;
        tmdv=GetDVFromImg(filename);
        tmdn=datenum(tmdv);
        tmdns(i)=tmdn;
    end
    save(avail_f,'tmdns');
end
t=load(avail_f);
tmdns=t.tmdns;

odstartdv=datevec(startdn);
odstartdv(4:6)=0;
odstartdn=datenum(odstartdv);
while odstartdn<enddn
    odenddn=odstartdn+ONEDAY_DN;
    %BNLUpsideDown_multicore(oriimgDir,flipimgDir,odstartdn,odenddn)
    cmmd=sprintf('BNLUpsideDown_multicore(''%s'',''%s'',%f,%f)',...
        oriimgDir,flipimgDir,odstartdn,odenddn);
    disp(cmmd);
    subprocess_matlab(cmmd,matlabinst);
    odstartdn=odenddn;
end