function [bstarty,bstartx,bendy,bendx,nBlocky,nBlockx,blockvals]=...
    DivideBlocks(inputma,dimvec)
dimvecy=dimvec(1);
dimvecx=dimvec(2);
[H,W]=size(inputma);
% 1 ~ H and 1 ~ W
ystart=1;yend=H;
xstart=1;xend=W;
nBlocky=floor((H-(ystart+dimvecy-1))/dimvecy)+1;
nBlockx=floor((W-(xstart+dimvecx-1))/dimvecx)+1;
% 
% tmbstarty=ystart;
% tmbstartx=xstart;
% tmbendy=tmbstarty;
% tmbendx=tmbstartx;
% bstarty=[];bstartx=[];bendy=[];bendx=[];
% while tmbendy<=yend
%     tmbstartx=xstart;    % repeat for every iteration of new H block
%     tmbendx=tmbstartx+dimvecx-1;
%     while tmbendx<=xend
%         tmbendy=tmbstarty+dimvecy-1;
%         tmbendx=tmbstartx+dimvecx-1;
%         bstarty=[bstarty tmbstarty];
%         bstartx=[bstartx tmbstartx];
%         bendy=[bendy tmbendy];
%         bendx=[bendx tmbendx];
%         
%         tmbstartx=tmbstartx+dimvecx;
%         tmbendx=tmbstartx+dimvecx-1;
%     end
%     tmbstarty=tmbstarty+dimvecy;
%     tmbendy=tmbstarty+dimvecy-1;
% end

bstarty=zeros(nBlocky,nBlockx);
bstartx=zeros(nBlocky,nBlockx);
bendy=zeros(nBlocky,nBlockx);
bendx=zeros(nBlocky,nBlockx);
blockvals=zeros(nBlocky,nBlockx,dimvecy,dimvecx);
for i=1:nBlocky
    for j=1:nBlockx
        bstarty(i,j)=ystart+(i-1)*dimvecy;
        bstartx(i,j)=xstart+(j-1)*dimvecx;
        bendy(i,j)=bstarty(i,j)+dimvecy-1;
        bendx(i,j)=bstartx(i,j)+dimvecx-1;
        blockvals(i,j,:,:)=inputma(bstarty(i,j):bendy(i,j),bstartx(i,j):bendx(i,j));
    end
end