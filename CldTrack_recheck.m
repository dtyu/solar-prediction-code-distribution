%   HMV_x=[HMVX32 HMVX31;HMVX32 HMVX31;HMVX32 HMVX31;...]
%   HMV_y=[HMVY32 HMVY31;HMVY32 HMVY31;HMVY32 HMVY31;...]
function CldTrack_recheck(cldtrkmatoutf,cldtrkmatoutf_recheck,tsi3invalidmask,configfpath)
run(configfpath);
%mtsi_startup_mini;
%NEIGHBOR_NTHRES=2;


t=load(cldtrkmatoutf);
H=t.H;
Conn_sx=t.Conn_sx;    Conn_sy=t.Conn_sy;
Conn_H=t.Conn_H;      Conn_W=t.Conn_W;
HMV_x=t.HMV_x; HMV_y=t.HMV_y;
MV_x=t.MV_x;    MV_y=t.MV_y;
MAXCC=t.MAXCC;
nCF=t.nCF;
CFMaskMa=t.CFMaskMa;


nConn=length(H);
recheckCFind=[];

for i=1:nConn
   if MAXCC(i) ~=0
       continue;
   end
   sx=Conn_sx(i); sy=Conn_sy(i);
   recH=Conn_H(i); recW=Conn_W(i);
   ex=sx+recW;  ey=sy+recH;
   tmmask=tsi3invalidmask(sy:ey,sx:ex);
   if ~isempty(find(tmmask,1))
       recheckCFind=[recheckCFind i];
   end
end

% Rule 1: If the invalid CF contains BLACK, choose neighbors' average
NoNeighborInd=-1;
while ~isempty(recheckCFind) && recheckCFind(1)~=NoNeighborInd
    i=recheckCFind(1);
    sx=Conn_sx(i); sy=Conn_sy(i);
    recH=Conn_H(i); recW=Conn_W(i);
    ex=sx+recW;  ey=sy+recH;
    % simply choose neighbor layers as its layer information
    neighbormask=false(size(tsi3invalidmask));
    neighbormask(sy-1:ey+1,sx-1:ex+1)=true;
    neighbormask(sy:ey,sx:ex)=false;
    neighborH=[];
    neighborMVx=[];
    neighborMVy=[];
    for j=1:nConn
        if MAXCC(j) ==0
            continue;
        end
        sx1=Conn_sx(j); sy1=Conn_sy(j);
        recH1=Conn_H(j); recW1=Conn_W(j);
        ex1=sx1+recW1;  ey1=sy1+recH1;
        tmvals=neighbormask(sy1:ey1,sx1:ex1);
        if isempty(find(tmvals,1))
            continue;
        end
        neighborH=[neighborH H(j)];
        neighborMVx=[neighborMVx MV_x(j)];
        neighborMVy=[neighborMVy MV_y(j)];
    end
    if ~isempty(neighborH)
        % remove it from head of queue and give mean H to it
        recheckCFind=recheckCFind(2:end);
        H(i)=mean(neighborH);
        MV_x(i)=mean(neighborMVx);
        MV_y(i)=mean(neighborMVy);
        [tmHMV_x,tmHMV_y]=TO_H2HMV(H(i),TSI3);
        HMV_x(i,2)=tmHMV_x(1);  HMV_x(i,1)=tmHMV_x(2);
        HMV_y(i,2)=tmHMV_y(1);  HMV_y(i,1)=tmHMV_y(2);
        MAXCC(i)=-1;
        NoNeighborInd=-1;
    else
        % no neighbor put it back to queue
        recheckCFind(1:end-1)=recheckCFind(2:end);
        recheckCFind(end)=i;
        if NoNeighborInd==-1
            NoNeighborInd=i;
        end
    end
end


% Rule 2: If the surrounding CFs are defined, choose the average of H
recheckCFind=[];
for i=1:nConn
   if MAXCC(i) ~=0
       continue;
   end
   recheckCFind=[recheckCFind i];
end


tml=length(recheckCFind);
for tmi=1:tml
    i=recheckCFind(tmi);
    sx=Conn_sx(i); sy=Conn_sy(i);
    recH=Conn_H(i); recW=Conn_W(i);
    ex=sx+recW;  ey=sy+recH;
    % simply choose neighbor layers as its layer information
    neighbormask=false(size(tsi3invalidmask));
    neighbormask(sy-1:ey+1,sx-1:ex+1)=true;
    neighbormask(sy:ey,sx:ex)=false;
    neighborH=[];
    neighborMVx=[];
    neighborMVy=[];
    for j=1:nConn
        if MAXCC(j) ==0
            continue;
        end
        sx1=Conn_sx(j); sy1=Conn_sy(j);
        recH1=Conn_H(j); recW1=Conn_W(j);
        ex1=sx1+recW1;  ey1=sy1+recH1;
        tmvals=neighbormask(sy1:ey1,sx1:ex1);
        if isempty(find(tmvals,1))
            continue;
        end
        neighborH=[neighborH H(j)];
        neighborMVx=[neighborMVx MV_x(j)];
        neighborMVy=[neighborMVy MV_y(j)];
    end
    if length(neighborH)<NEIGHBOR_NTHRES
        continue;
    end
    % neighbor CFs are more than theshold, choose the mean of it
    H(i)=mean(neighborH);
    MV_x(i)=mean(neighborMVx);
    MV_y(i)=mean(neighborMVy);
    [tmHMV_x,tmHMV_y]=TO_H2HMV(H(i),TSI3);
    HMV_x(i,2)=tmHMV_x(1);  HMV_x(i,1)=tmHMV_x(2);
    HMV_y(i,2)=tmHMV_y(1);  HMV_y(i,1)=tmHMV_y(2);
    MAXCC(i)=-1;
end


% Rule 3: 
% recheckCFind=[];
% for i=1:nConn
%    if MAXCC(i) ~=0
%        continue;
%    end
%    recheckCFind=[recheckCFind i];
% end


% tml=length(recheckCFind);
% for tmi=1:tml
%     i=recheckCFind(tmi);
%     sx=Conn_sx(i); sy=Conn_sy(i);
%     recH=Conn_H(i); recW=Conn_W(i);
%     ex=sx+recW;  ey=sy+recH;
%     % simply choose neighbor layers as its layer information
%     neighbormask=false(size(tsi3invalidmask));
%     neighbormask(sy-1:ey+1,sx-1:ex+1)=true;
%     neighbormask(sy:ey,sx:ex)=false;
%     neighborH=[];
%     neighborMVx=[];
%     neighborMVy=[];
%     for j=1:nConn
%         if MAXCC(j) ==0
%             continue;
%         end
%         sx1=Conn_sx(j); sy1=Conn_sy(j);
%         recH1=Conn_H(j); recW1=Conn_W(j);
%         ex1=sx1+recW1;  ey1=sy1+recH1;
%         tmvals=neighbormask(sy1:ey1,sx1:ex1);
%         if isempty(find(tmvals,1))
%             continue;
%         end
%         neighborH=[neighborH H(j)];
%         neighborMVx=[neighborMVx MV_x(j)];
%         neighborMVy=[neighborMVy MV_y(j)];
%     end
%     if length(neighborH)<NEIGHBOR_NTHRES
%         continue;
%     end
%     % neighbor CFs are more than theshold, choose the mean of it
%     H(i)=mean(neighborH);
%     MV_x(i)=mean(neighborMVx);
%     MV_y(i)=mean(neighborMVy);
%     [tmHMV_x,tmHMV_y]=TO_H2HMV(H(i),TSI3);
%     HMV_x(i,2)=tmHMV_x(1);  HMV_x(i,1)=tmHMV_x(2);
%     HMV_y(i,2)=tmHMV_y(1);  HMV_y(i,1)=tmHMV_y(2);
%     MAXCC(i)=-1;
% end




save(cldtrkmatoutf_recheck,'H','MV_y','MV_x','HMV_x','HMV_y','MAXCC','CFMaskMa','nCF','Conn_sx','Conn_sy','Conn_H','Conn_W');


end