function [ConnLabelma_n,nConn_n]=bwlabel_adjust(ConnLabelma,ConnMinSize,ConnMaxSize)
tmvals=ConnLabelma(ConnLabelma~=0);
nConn=max(tmvals);
[H,W]=size(ConnLabelma);
% 1. Remove all small CF labels smaller than ConnMinSize
lvalid=true(nConn,1);
ConnLabelma_n=ConnLabelma;
for tmi=1:nConn
    tmind=find(ConnLabelma_n==tmi);
    [y,x]=ind2sub([H,W],tmind);
    tmrecH=max(y)-min(y)+1;
    tmrecW=max(x)-min(x)+1;
    if tmrecH<ConnMinSize || tmrecW<ConnMinSize
        lvalid(tmi)=false;
        ConnLabelma_n(tmind)=0;
    end
end
lvalidind=find(lvalid);
nConn_n=length(lvalidind);
for i=1:nConn_n
    Conni=lvalidind(i);
    tmindma=ConnLabelma_n==Conni;
    ConnLabelma_n(tmindma)=i;
end

% 2. Divide large CF into smaller ones
ConnLabelma=ConnLabelma_n;
nConn=nConn_n;
bDividable=true;    % if dividable then create new connection fields
while bDividable
    bDividable=false;   % set as false, if there is a large CF found, set as true and do division
    ConnLabelma_n=ConnLabelma;
    lvalid=true(nConn,1);
    newConn=nConn+1;
    for tmi=1:nConn
%         if tmi==16
%             tmi
%         end
        tmind=find(ConnLabelma==tmi);
        [y,x]=ind2sub([H,W],tmind);
        tmrecH=max(y)-min(y)+1;
        tmrecW=max(x)-min(x)+1;
        if tmrecH>ConnMaxSize || tmrecW>ConnMaxSize
            bDividable=true;
            sy1=min(y);
            sx1=min(x);
            if tmrecH>tmrecW
                tmrecH1=round(tmrecH/2);
                tmrecW1=tmrecW;
                sy2=sy1+tmrecH1;
                ey1=sy2-1;
                ey2=sy1+tmrecH-1;
                sx2=sx1;
                ex1=sx1+tmrecW-1;
                ex2=ex1;
            else
                tmrecH1=tmrecH;
                tmrecW1=round(tmrecW/2);
                sx2=sx1+tmrecW1;
                ex1=sx2-1;
                ex2=sx1+tmrecW-1;
                sy2=sy1;
                ey1=sy1+tmrecH-1;
                ey2=ey1;
            end
            lvalid(tmi)=false;
            tmmask1=false(H,W);tmmask2=false(H,W);
            tmmask1(sy1:ey1,sx1:ex1)=true;
            tmmask2(sy2:ey2,sx2:ex2)=true;
            ConnLabelma_n(tmmask1&ConnLabelma==tmi)=newConn;
            newConn=newConn+1;
            ConnLabelma_n(tmmask2&ConnLabelma==tmi)=newConn;
            newConn=newConn+1;
        end
    end
    
    if bDividable==true
        lvalid_n=true(newConn-1,1);
        lvalid_n(1:nConn)=lvalid;
        labels=1:newConn-1;
        ind=lvalid_n~=0;
        validlabels=labels(ind);
        nConn_n=length(validlabels);
        for labeli=1:nConn_n
            ConnLabelma_n(ConnLabelma_n==validlabels(labeli))=labeli;
        end
        ConnLabelma=ConnLabelma_n;
        nConn=max(max(ConnLabelma));
    end
end
ConnLabelma_n=ConnLabelma;
nConn_n=max(max(ConnLabelma));



end