r=double(img1(:,:,1));
g=double(img1(:,:,2));
b=double(img1(:,:,3));
imma2=rgb2gray(img1);
im1_grey=imma2;
r(~tmbmpmask)=0;
g(~tmbmpmask)=0;
b(~tmbmpmask)=0;
imma2(~tmbmpmask)=0;
newf=uint8(g*0.7+b*0.3);
tmma1=edge(r,'sobel');
tmma2=edge(g,'sobel');
tmma3=edge(b,'sobel');
tmma4=edge(imma2,'sobel');
tmma=tmma4&edge(im1_grey,'canny');
figure();
subplot(1,2,1);
imshow(tmma1);
subplot(1,2,2);
imshow(tmma2);