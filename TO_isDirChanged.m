% Tool to judge whether the modifiedtime is different from previous record. 
%   Previous timestamp is extracted from TO_getDirModtime
%   VERSION 1.0 2014-05-08
function bChanged = TO_isDirChanged(inDir,ts)
f = java.io.File(inDir);
if ts==f.lastModified
    bChanged = false;
else
    bChanged = true;
end
end