function test_motiontest_new

bServer=false;


mtsi_startup;

set(0,'DefaultFigureVisible', 'on');
%set(0,'DefaultFigureVisible', 'off');

%tsiDirs={'./tsi1_undist/','./tsi2_undist/','./tsi3_undist/'};
% Cloudy and multi-layer test
tsiDirs={'./cloudy_multilayer/tsi1_undist_20130605/','./cloudy_multilayer/tsi2_undist_20130605/','./cloudy_multilayer/tsi3_undist_20130605/'};
otsiDirs={'./otsi1/','./otsi2/','./otsi3/'};
matoutputDir='hcldoutDir';
matoutputDir=FormatDirName(matoutputDir);

if bServer
    dataDir='/cewit/home/zpeng/workdata/mtsi_stich/';
else
    dataDir='/data/workdata/mtsi_stich/';
end
dataDir=FormatDirName(dataDir);

nTSI=3;
tsi1Dir=tsiDirs{1};
tsi2Dir=tsiDirs{2};
tsi3Dir=tsiDirs{3};
otsi1Dir=otsiDirs{1};
otsi2Dir=otsiDirs{2};
otsi3Dir=otsiDirs{3};
mkdir(otsi1Dir);
mkdir(otsi2Dir);
mkdir(otsi3Dir);
alltsi1=dir(tsi1Dir);
[nfiles1,~]=size(alltsi1);
imma1_total=zeros(UNDISTH,UNDISTW,3);
imma2_total=zeros(UNDISTH,UNDISTW,3);
imma3_total=zeros(UNDISTH,UNDISTW,3);
N1=zeros(UNDISTH,UNDISTW);
N2=zeros(UNDISTH,UNDISTW);
N3=zeros(UNDISTH,UNDISTW);

imma_avgf='./imma_avgf.mat';
t=load(imma_avgf,'imma1_avg','imma2_avg','imma3_avg');
imma1_avg=t.imma1_avg;
imma2_avg=t.imma2_avg;
imma3_avg=t.imma3_avg;

OutputPath='./tmptestme.mat';
InnerBlockSize=10;
BlockSize=50;
SearchWinSize=50;
CorThreshold=0.5;
BlackThreshold=0;

MEOutDir='./ME_test_img';
FillOutDir='./Fillimg';
MEOutDir=FormatDirName(MEOutDir);
FillOutDir=FormatDirName(FillOutDir);
if ~exist(MEOutDir,'dir')
    mkdir(MEOutDir);
end
if ~exist(FillOutDir,'dir')
    mkdir(FillOutDir);
end
ONEMIN_DN=datenum([0 0 0 0 1 0]);
TENSECS_DN=datenum([0 0 0 0 0 10]);


for i =1:nfiles1
    
    filename=alltsi1(i).name;
    f1=sprintf('%s%s',tsi1Dir,filename);
    tmdv=GetDVFromImg(filename);
    if tmdv==0
        continue;
    end
    tmdn=datenum(tmdv);
    if tmdn~=datenum([2013 06 05 14 42 50])
        continue;
    end
    tmpmatfile='20130730.mat';
    if ~exist(tmpmatfile,'file')
        tmpimat=sprintf('%stmp%d.mat',matoutputDir,i);
        t=load(tmpimat);
        nConn_s=t.nConn_s;
        ConnMap_hcld=TO_blkConvert(t.ConnMap_s,t.InnerBlockSize,InnerBlockSize);
        SearchBlkMaskma=(ConnMap_hcld==0);
        
        tmdn_1faf=tmdn+TENSECS_DN;
        tmdv_1faf=datevec(tmdn_1faf);
        try
            imma1=tmp_GetImgMatFromDataDir(tmdv,TSI1,dataDir);
            imma2=tmp_GetImgMatFromDataDir(tmdv,TSI2,dataDir);
            imma3=tmp_GetImgMatFromDataDir(tmdv,TSI3,dataDir);
            imma1_1faf=tmp_GetImgMatFromDataDir(tmdv_1faf,TSI1,dataDir);
            imma2_1faf=tmp_GetImgMatFromDataDir(tmdv_1faf,TSI2,dataDir);
            imma3_1faf=tmp_GetImgMatFromDataDir(tmdv_1faf,TSI3,dataDir);
        catch
            continue;
        end
        imma2_g=TO_rgb2lum(imma2);
        imma3_g=TO_rgb2lum(imma3);
        imma2_1faf_g=TO_rgb2lum(imma2_1faf);
        imma3_1faf_g=TO_rgb2lum(imma3_1faf);
        
        
        %% New algorithm for std change
        [ma3,cldind3]=test_GenCldMask_2r(imma3,imma3_avg);
        [bstarty,bstartx,bendy,bendx,nBlocky,nBlockx,blockvals]=DivideBlocks(...
            imma3_g,[InnerBlockSize InnerBlockSize]);
        stdma=zeros(nBlocky,nBlockx);
        meanma=zeros(nBlocky,nBlockx);
        blkclass=zeros(nBlocky,nBlockx);
        blkH=zeros(nBlocky,nBlockx);
        blkmv_x=zeros(nBlocky,nBlockx);
        blkmv_y=zeros(nBlocky,nBlockx);
        blkmv_Hy=zeros(nBlocky,nBlockx);
        blkmv_Hx=zeros(nBlocky,nBlockx);
        blkCldclass=zeros(nBlocky,nBlockx);
        pixelCldclass=zeros(UNDISTH,UNDISTW);
        
        SearchWin_Ext=15;
        for bi=1:nBlocky
            for bj=1:nBlockx
                %   Condt 1: high altitude cloud, skip
                if ~SearchBlkMaskma(bi,bj)
                    continue;
                end
                tmblk=blockvals(bi,bj,:,:);
                tmblk_cld=cldind3(bstarty(bi,bj):bendy(bi,bj),bstartx(bi,bj):bendx(bi,bj));
                
                %   Condt 2: invalid more than 50%(50 out of 100) set it as nan
                %   otherwise use non-zero to do std and mean
                if ~isempty(find(tmblk==0,1))
                    tmvals=tmblk(tmblk>=20);
                    if length(tmvals)<50
                        stdma(bi,bj)=0;
                        meanma(bi,bj)=0;
                    else
                        stdma(bi,bj)=std(tmvals(:));
                        meanma(bi,bj)=mean(tmvals(:));
                    end
                else
                    stdma(bi,bj)=std(tmblk(:));
                    meanma(bi,bj)=mean(tmblk(:));
                end
                
                % Mark Cloudy blk
                if isempty(find(tmblk_cld==1,1))
                    blkclass(bi,bj)=0;
                else
                    tml=length(find(tmblk_cld==1));
                    if tml/100>0.4
                        blkclass(bi,bj)=1;
                    else
                        blkclass(bi,bj)=0;
                    end
                end
            end
        end
        stdma_invalid=(stdma==0)&(meanma==0);
        cldmask_blk=blkclass;
        cldstdvals=stdma(logical(cldmask_blk));
        [IDX C]=kmeans(cldstdvals(:),2);
        segC=(C(1)+C(2))/2;
        BigStdindma=stdma>segC&~stdma_invalid&cldmask_blk;
        SmallStdindma=stdma<=segC&~stdma_invalid&cldmask_blk;
        
        meangreyThres=10;
        
        [ConnMap_s,ConnValid_s]=test_stdblkmerge(SmallStdindma,blockvals,segC,10,0.5,meangreyThres);
        
        
        nConn_s=max(max(ConnMap_s));
        corrblks=cell(nConn_s,1);
        minsy2=zeros(nConn_s,1);
        minsx2=zeros(nConn_s,1);
        maxey2=zeros(nConn_s,1);
        maxex2=zeros(nConn_s,1);
        minsy1=zeros(nConn_s,1);
        minsx1=zeros(nConn_s,1);
        maxey1=zeros(nConn_s,1);
        maxex1=zeros(nConn_s,1);
        minsy3=zeros(nConn_s,1);
        minsx3=zeros(nConn_s,1);
        maxey3=zeros(nConn_s,1);
        maxex3=zeros(nConn_s,1);
        
        
        %% hcld plot
        fig1=figure();
        subplot(1,2,1);
        imshow(imma3);
        hold on;
        for ci=1:nConn_s
            %         if ci~=32
            %             continue;
            %         end
            if ~ConnValid_s(ci)
                continue;
            end
            disp(ci);
            tmind=find(ConnMap_s==ci);
            tmConn_ind=tmind;
            tmsy=bstarty(tmind);
            tmsx=bstartx(tmind);
            tmey=bendy(tmind);
            tmex=bendx(tmind);
            minsy=min(tmsy);
            minsx=min(tmsx);
            maxey=max(tmey);
            maxex=max(tmex);
            tmcenx=round((minsx+maxex)/2);
            tmceny=round((minsy+maxey)/2);
            tmfillind=find(bstarty>=minsy&bendy<=maxey&bstartx>=minsx&bendx<=maxex&ConnMap_s~=ci);
            tml=length(tmfillind);
            tmblkmask=true(size(imma3_g));
            for li=1:tml
                tmind=tmfillind(li);
                tmsy=bstarty(tmind);
                tmsx=bstartx(tmind);
                tmey=bendy(tmind);
                tmex=bendx(tmind);
                tmblkmask(tmsy:tmey,tmsx:tmex)=false;
            end
            
            recH=maxey-minsy+1;
            recW=maxex-minsx+1;
            %         [mv_x mv_y H ux31 uy31 maxcc]=test_ncc3d_v1(rgb2gray(tmimma2),...
            %             rgb2gray(imma2_1maf),rgb2gray(imma3),rgb2gray(imma3_1maf),...
            %             minsx,minsy,recW,recH,SearchWin_Ext,3500);
            ncc_thres=0;
            H_sran=1000:500:20000;
            [mv_x mv_y H tmHMV31_x tmHMV31_y maxcc]=ncc3d_tsi32(...
                tmdv,[minsy minsx],[recH recW],SearchWin_Ext,dataDir,H_sran,ncc_thres,tmblkmask);
            choosei=1;
            if maxcc<1
                continue;
            end
            
            tml=length(tmConn_ind);
            for li=1:tml
                tmind=tmConn_ind(li);
                blkH(tmind)=H;
                blkmv_x(tmind)=mv_x;
                blkmv_y(tmind)=mv_y;
                blkmv_Hy(tmind)=tmHMV31_x;
                blkmv_Hx(tmind)=tmHMV31_y;
                if H>=5000
                    blkCldclass(tmind)=2;
                else
                    blkCldclass(tmind)=1;
                end
            end
            
            rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
            
            minsx2(ci)=minsx+tmHMV31_x;
            minsy2(ci)=minsy+tmHMV31_y;
            maxex2(ci)=minsx2(ci)+recW;
            maxey2(ci)=minsy2(ci)+recH;
            quiver(tmcenx,tmceny,tmHMV31_x,tmHMV31_y);
            if H(choosei)>=10000
                text(tmcenx,tmceny,['\color{red}' '\geq 10000'],'FontSize',18);
            else
                text(tmcenx,tmceny,['\color{red}' sprintf('%d',round(H(choosei)))],'FontSize',18);
            end
            %text(tmcenx,tmceny,['\color{red}' sprintf('%d',round(H(choosei)))],'FontSize',18);
            
        end
        hold off;
        
        subplot(1,2,2);
        imshow(imma2);
        hold on;
        for ci =1:nConn_s
            if minsx2(ci)==0
                continue;
            end
            recW=maxex2(ci)-minsx2(ci)+1;
            recH=maxey2(ci)-minsy2(ci)+1;
            rectangle('Position',[minsx2(ci) minsy2(ci) recW recH],'LineStyle','--','EdgeColor','r');
        end
        hold off;
        
        
        
        
        
        %% low plot
        [ConnMap,nConn]=bwlabeln(BigStdindma);
        %     [ConnMap,ConnValid]=test_stdblkmerge(BigStdindma,blockvals,1000,10,0.5,20);
        corrblks=cell(nConn,1);
        minsy2=zeros(nConn,1);
        minsx2=zeros(nConn,1);
        maxey2=zeros(nConn,1);
        maxex2=zeros(nConn,1);
        fig2=figure();
        subplot(1,2,1);
        imshow(imma3);
        hold on;
        for ci =1:nConn
            tmind=find(ConnMap==ci);
            tmConn_ind=tmind;
            tmsy=bstarty(tmind);
            tmsx=bstartx(tmind);
            tmey=bendy(tmind);
            tmex=bendx(tmind);
            minsy=min(tmsy);
            minsx=min(tmsx);
            maxey=max(tmey);
            maxex=max(tmex);
            tmcenx=round((minsx+maxex)/2);
            tmceny=round((minsy+maxey)/2);
            recH=maxey-minsy+1;
            recW=maxex-minsx+1;
            %         if ci~=4
            %             continue;
            %         end
            tmcorrblk=imma3(minsy:maxey,minsx:maxex,:);
            if length(tmcorrblk)<=20
                corrblks{ci}=0;
                continue;
            end
            
            tmfillind=find(bstarty>=minsy&bendy<=maxey&bstartx>=minsx&bendx<=maxex&ConnMap~=ci);
            tml=length(tmfillind);
            tmblkmask=true(size(imma3_g));
            for li=1:tml
                tmind=tmfillind(li);
                tmsy=bstarty(tmind);
                tmsx=bstartx(tmind);
                tmey=bendy(tmind);
                tmex=bendx(tmind);
                tmblkmask(tmsy:tmey,tmsx:tmex)=false;
            end
            
            
            
            
            ncc_thres=0;
            %H_sran=[1000:200:5000 5500:500:20000];
            H_sran=[1000:200:3000];
            [mv_x mv_y H tmHMV31_x tmHMV31_y maxcc]=ncc3d_tsi32(...
                tmdv,[minsy minsx],[recH recW],SearchWin_Ext,dataDir,H_sran,ncc_thres);
            if maxcc<1
                continue;
            end
            choosei=1;
            tml=length(tmConn_ind);
            for li=1:tml
                tmind=tmConn_ind(li);
                blkH(tmind)=H;
                blkmv_x(tmind)=mv_x;
                blkmv_y(tmind)=mv_y;
                blkmv_Hy(tmind)=tmHMV31_x;
                blkmv_Hx(tmind)=tmHMV31_y;
                if H>=5000
                    blkCldclass(tmind)=2;
                else
                    blkCldclass(tmind)=1;
                end
            end
            
            rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
            
            minsx2(ci)=minsx+tmHMV31_x;
            minsy2(ci)=minsy+tmHMV31_y;
            maxex2(ci)=minsx2(ci)+recW;
            maxey2(ci)=minsy2(ci)+recH;
            quiver(tmcenx,tmceny,tmHMV31_x,tmHMV31_y);
            if H(choosei)>=10000
                text(tmcenx,tmceny,['\color{red}' '\geq 10000'],'FontSize',18);
            else
                text(tmcenx,tmceny,['\color{red}' sprintf('%d',round(H(choosei)))],'FontSize',18);
            end
        end
        hold off;
        
        subplot(1,2,2);
        imshow(imma2);
        hold on;
        for ci =1:nConn
            if minsx2(ci)==0
                continue;
            end
            recW=maxex2(ci)-minsx2(ci)+1;
            recH=maxey2(ci)-minsy2(ci)+1;
            rectangle('Position',[minsx2(ci) minsy2(ci) recW recH],'LineStyle','--','EdgeColor','r');
        end
        hold off;
        
        save(tmpmatfile);
    else
        load(tmpmatfile)
    end
    
    %% Get final result
    % 1. set low cloud to median value 
    lowcldblkmask=blkCldclass==1;
    lowH=median(blkH(lowcldblkmask));
    blkCldclass(BigStdindma)=1;
    
    % 2. set high altitude got from test_Hcld
    blkCldclass(~SearchBlkMaskma)=2;
    blkCldclass(blkH>3000)=2;
    
    % 3. show blkCldclass
    pixelCldclass=TO_blkConvert(blkCldclass,InnerBlockSize,1);
    
    
    r=imma3(:,:,1);
    g=imma3(:,:,2);
    b=imma3(:,:,3);
    g(pixelCldclass==1)=0;
    b(pixelCldclass==1)=0;
    r(pixelCldclass==2)=0;
    g(pixelCldclass==2)=0;
    r(~cldind3)=0;
    g(~cldind3)=0;
    b(~cldind3)=0;
    imma3_t(:,:,1)=r;
    imma3_t(:,:,2)=g;
    imma3_t(:,:,3)=b;
    
    
end
