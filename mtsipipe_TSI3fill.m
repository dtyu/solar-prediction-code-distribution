% mtsipipe_TSI3fill
%   This is a function to get filled image based on height calculation. This is NOT FINAL version
%   since the current VERSION only uses the fixed height to fill images. NEED to be modified
% INPUT:
%       datevecObj              --- datevector_obj, containing startdv and enddv
%       mode                    --- String, for parsing running environment
%       cldmodelmatf            --- MAT file, SVM model for cloud detection
%       FilledTSI3Dir           --- Directory, containing all the filled TSI3 images
%       configfpath             --- String, mtsi_startup filepath
%       RefHeight               --- Reference Height for filling
% OUTPUT:
%       FilledTSI3Dir/*         --- BMP files, filled TSI3 images
%
% PREREQUISITE:
%       mtsipipe2 ---  Get layers information and tracking results
% VERSION: 1.0      2013/12/04
%   Previously test_cldHdection_href_fill
% USAGE:
%   mtsipipe_TSI3fill([2013 05 14 0 0 0;2013 05 15 0 0 0],'laptop','model_rbr.mat','./testfill/','mtsi_startup',1800,'./skyfill.bmp');
%
function mtsipipe_TSI3fill(datevecObj,mode,cldmodelmatf,FilledTSI3Dir,configfpath,HRefDir, SKYFILE)
%% Presettings
%mtsi_startup;
run(configfpath);
assert((exist(cldmodelmatf,'file')~=0),'Cloud Model mat file doen''s exist!');
bMulti=modepaser(mode,'multicore');
bServer=modepaser(mode,'server');
bPlot=modepaser(mode,'plot');
bSkip=modepaser(mode,'skip');
bRun=modepaser(mode,'run');
%bCldMatMod=modepaser(mode,'cldmatmod');
bPlotVisible=modepaser(mode,'visible');
if bPlotVisible
    set(0,'DefaultFigureVisible', 'on');
else
    set(0,'DefaultFigureVisible', 'off');
end
if bServer
    dataDir='~/workdata/mtsi_stich/';
else
    dataDir='/data/workdata/mtsi_stich/';
end

FilledTSI3Dir=FormatDirName(FilledTSI3Dir);
if ~exist(FilledTSI3Dir,'dir')
    mkdir(FilledTSI3Dir);
end
HRefDir=FormatDirName(HRefDir);

% Get Avail mat files for database
[startdv,enddv,startdn,enddn]=datevectorparser(datevecObj);
mtsi_tsiavailf='mtsi_tsiavail.mat';
if ~exist(mtsi_tsiavailf,'file')
    tmdv=[2013 05 14 18 0 0];   % sample for extraction of directory
    f1=tmp_IsImgMatInDataDir(tmdv,TSI1,dataDir);    % only used for extraction of parent directory path
    tsi1Dir=GetDirPathFromPath(f1);
    [TSIavail,TSIavail_days]=TO_CheckTSIAvail(tsi1Dir);
    save('mtsi_tsiavail.mat','TSIavail','TSIavail_days');
else
    t=load(mtsi_tsiavailf);
    TSIavail=t.TSIavail;
    TSIavail_days=t.TSIavail_days;
end
alldns=TSIavail(TSIavail>=startdn & TSIavail<enddn);
tml=length(alldns);
alldns_ind=false(tml,1);
for i=1:tml
    tmdn=alldns(i);
    tmdv=datevec(tmdn);
    tmf=GetImgFromDV_BNL(tmdv,TSI3);
    cldtrkmatoutf_reg=sprintf('%s%s.cldtrkreg.mat',HRefDir,tmf(1:end-4));
    if exist(cldtrkmatoutf_reg,'file')
        alldns_ind(i)=true;
    end
end
dns=alldns(alldns_ind);
NFRAMES=length(dns);
% % Generate the available/searching datenumbers
% ndays=length(TSIavail_days);
% ODSTART_DN=datenum([0 0 0 14 0 0]);
% ODEND_DN=datenum([0 0 0 20 0 0]);
% NSLOTS=round((ODEND_DN-ODSTART_DN)/ONEMIN_DN)+1;
% oddns=zeros(ndays,NSLOTS);
% for i=1:ndays
%     odstartdn=datenum(TSIavail_days(i,:))+ODSTART_DN;
%     for j=1:NSLOTS
%         oddns(i,j)=odstartdn+(j-1)*ONEMIN_DN;
%     end
% end

nframes_lookup=5;

skyfillma=imread(SKYFILE);
MAXCLOUDFRAC=0.8;
%%

for i=1:NFRAMES
    tmdn=dns(i);
%     if tmdn<datenum([2013 06 09 16 27 0])
%         continue;
%     end
    
    tmdv=datevec(tmdn);
    tmf=GetImgFromDV_BNL(tmdv,TSI3);
    cldtrkmatoutf_reg=sprintf('%s%s.cldtrkreg.mat',HRefDir,tmf(1:end-4));
    cldtrkmatoutf_2=sprintf('%s%s.cldtrk2.mat',HRefDir,tmf(1:end-4));
    cldtrk13matoutf=sprintf('%s%s.cldtrk13.mat',HRefDir,tmf(1:end-4));
%     if ~exist(cldtrkmatoutf_reg,'file') || ~exist(cldtrkmatoutf_2,'file') || ~exist(cldtrk13matoutf,'file')
%         continue;
%     end
    if ~exist(cldtrkmatoutf_reg,'file') || ~exist(cldtrkmatoutf_2,'file')
        continue;
    end
%     outputf=sprintf('%s%s_shifttest.jpg',FilledTSI3Dir,tmf);
%     if exist(outputf,'file')
%         continue;
%     end
    
    
    [HRef MVRef]=tmp_ChooseHFromHRegfiles(HRefDir,tmdv,nframes_lookup);
    HRef=HRef(HRef~=0);
    hl=length(HRef);
    if hl==0
        imma3=tmp_GetImgMatFromDataDir(tmdv,TSI3,dataDir);
        imma3_lum=TO_rgb2lum(imma3);
        imma3_fill_indma=(imma3_lum==0);
        imma3_fill=imma3;
        tmr=imma3(TO_3DMaskfrom2D(~imma3_fill_indma,1));
        tmg=imma3(TO_3DMaskfrom2D(~imma3_fill_indma,2));
        tmb=imma3(TO_3DMaskfrom2D(~imma3_fill_indma,3));
        meanr=round(mean(tmr(:)));
        meang=round(mean(tmg(:)));
        meanb=round(mean(tmb(:)));
        imma3_fill(TO_3DMaskfrom2D(imma3_fill_indma,1))=meanr;
        imma3_fill(TO_3DMaskfrom2D(imma3_fill_indma,2))=meang;
        imma3_fill(TO_3DMaskfrom2D(imma3_fill_indma,3))=meanb;
        outputf=sprintf('%s%s_shifttest.jpg',FilledTSI3Dir,tmf);
        imwrite(imma3_fill,outputf,'jpg');
        outputf=sprintf('%s%s_fillparas.mat',FilledTSI3Dir,tmf);
        save(outputf,'HRef','MVRef','nframes_lookup');    
        continue;
    end
    %continue;
%     continue;
    %HRef=[0 1800]; %DEBUG ONLY
    % load 3 results
    [imma1_cldma_n,imma2_cldma_n,imma3_cldma_n]=...
        clouddetector(tmdv,cldmodelmatf,dataDir,configfpath);
    imma1_ori=tmp_GetImgMatFromDataDir(tmdv,TSI1,dataDir);
    imma2_ori=tmp_GetImgMatFromDataDir(tmdv,TSI2,dataDir);
    imma3=tmp_GetImgMatFromDataDir(tmdv,TSI3,dataDir);
    Red1 = imma3(:, :, 1);
    Green1 = imma3(:, :, 2);
    Blue1 = imma3(:, :, 3);
    HnRed1=imhist(Red1);
    HnGreen1=imhist(Green1);
    HnBlue1=imhist(Blue1);
    HnRed1(1)=0;HnGreen1(1)=0;HnBlue1(1)=0;     % 0 means not valid
    ref_sky_hist=[HnRed1 HnGreen1 HnBlue1];
    imma1=TO_histeq_TSISky(imma1_ori,ref_sky_hist);
    imma2=TO_histeq_TSISky(imma2_ori,ref_sky_hist);
    imma1_lum=TO_rgb2lum(imma1);
    imma2_lum=TO_rgb2lum(imma2);
    imma3_lum=TO_rgb2lum(imma3);
    imma1_fill_indma=(imma1_lum==0);
    imma2_fill_indma=(imma2_lum==0);
    imma3_fill_indma=(imma3_lum==0);
    
    pixelN=length(find(~imma3_fill_indma));
    cldN=length(find(imma3_cldma_n));
    cloudfraction3=cldN/pixelN;
    
    imma3_reffill=cell(hl,1);
    for hi=1:hl
        [tmHMV_x,tmHMV_y]=TO_H2HMV(HRef(hi),TSI3,configfpath);
        tmHMV_x31=tmHMV_x(1);tmHMV_x32=tmHMV_x(2);
        tmHMV_y31=tmHMV_y(1);tmHMV_y32=tmHMV_y(2);
        imma1_lum=single(TO_rgb2lum(imma1));
        imma2_lum=single(TO_rgb2lum(imma2));
        imma3_lum=single(TO_rgb2lum(imma3));
        
        % Generate imma3_fill image using low alttitude cloud
        imma3_fill=imma3;
        imma3_fill=single(imma3_fill);
        imma3_fill_indma=imma3_lum==0;
        imma1_s=TO_imshift(imma1,[-tmHMV_y31 -tmHMV_x31]);
        imma2_s=TO_imshift(imma2,[-tmHMV_y32 -tmHMV_x32]);
        tmc1_ma=TO_rgb2lum(imma1_s)==0;
        tmc2_ma=TO_rgb2lum(imma2_s)==0;
        tmc1_ma_3d=logical(TO_3DMaskfrom2D(tmc1_ma));
        tmc2_ma_3d=logical(TO_3DMaskfrom2D(tmc2_ma));
        imma1_s(tmc1_ma_3d)=imma2_s(tmc1_ma_3d);
        imma2_s(tmc2_ma_3d)=imma1_s(tmc2_ma_3d);
        imma3_fill_indma_3d=logical(TO_3DMaskfrom2D(imma3_fill_indma));
        imma3_fill(imma3_fill_indma_3d)=(single(imma1_s(imma3_fill_indma_3d))+single(imma2_s(imma3_fill_indma_3d)))./2;
        imma3_fill=uint8(imma3_fill);
        invalidma=TO_rgb2lum(imma3_fill)==0;
        if cloudfraction3<MAXCLOUDFRAC
            imma3_fill(TO_3DMaskfrom2D(invalidma))=skyfillma(TO_3DMaskfrom2D(invalidma));
        else
            rvals=imma3(TO_3DMaskfrom2D(imma3_cldma_n,1));
            gvals=imma3(TO_3DMaskfrom2D(imma3_cldma_n,2));
            bvals=imma3(TO_3DMaskfrom2D(imma3_cldma_n,3));
            meanr=mean(rvals(:));   meang=mean(gvals(:));   meanb=mean(bvals(:));
            imma3_fill(TO_3DMaskfrom2D(invalidma,1))=meanr;
            imma3_fill(TO_3DMaskfrom2D(invalidma,2))=meang;
            imma3_fill(TO_3DMaskfrom2D(invalidma,3))=meanb;
        end
        imma3_fill_l=uint8(imma3_fill);
        imma3_reffill{hi}=imma3_fill_l;
    end
    if hl>=1
        outputf=sprintf('%s%s_shifttest.jpg',FilledTSI3Dir,tmf);
        imwrite(imma3_reffill{1},outputf,'jpg');
        
        outputf=sprintf('%s%s_fillparas.mat',FilledTSI3Dir,tmf);
        save(outputf,'HRef','MVRef');
    end
    
    continue;
    
    
    
    
    
    

    
    
    t=load(cldtrkmatoutf_2);
    %H=t.H;
    Conn_sx=t.Conn_sx;    Conn_sy=t.Conn_sy;
    Conn_H=t.Conn_H;      Conn_W=t.Conn_W;
    HMV_x=t.HMV_x; HMV_y=t.HMV_y;
    MAXCC=t.MAXCC;
    nConn=t.nCF;
    t=load(cldtrkmatoutf_reg);
    H_Reg=t.H_Reg;  Conn_Reg=t.Conn_Reg;    CldC_Reg=t.CldC_Reg;
    MV_x_Reg=t.MV_x_Reg;    MV_y_Reg=t.MV_y_Reg;
    HMV_x_Reg=t.HMV_x_Reg;    HMV_y_Reg=t.HMV_y_Reg;
    RegH=t.RegH;    RegHMV_x=t.RegHMV_x;    RegHMV_y=t.RegHMV_y;
    if isempty(find(RegH~=0,1))
        % there is no cloud since H = 0. Skip sunny case for current test
        continue;
    end
    
    
    RegMV_x=t.RegMV_x;  RegMV_y=t.RegMV_y;
    t=load(cldtrk13matoutf);
    MV_x1=t.MV_x1;  MV_x2=t.MV_x2;
    MV_y1=t.MV_x1;  MV_y1=t.MV_y1;
    nCF1=t.nCF1;    nCF2=t.nCF2;    nConn1=nCF1;    nConn2=nCF2;
    Conn_sx1=t.Conn_sx1; Conn_sx2=t.Conn_sx2;
    Conn_sy1=t.Conn_sy1; Conn_sy2=t.Conn_sy2;
    Conn_H1=t.Conn_H1; Conn_H2=t.Conn_H2;
    Conn_W1=t.Conn_W1; Conn_W2=t.Conn_W2;
    HMV_x1=t.HMV_x1; HMV_x2=t.HMV_x2;
    HMV_y1=t.HMV_x1; HMV_y2=t.HMV_y2;
    H1=t.H1;    H2=t.H2;	H3=RegH;
    [~,si3]=sort(H3,'descend'); [~,si1]=sort(H1,'descend'); [~,si2]=sort(H2,'descend');
    [HMV_x_tsi3,HMV_y_tsi3]=TO_H2HMV(H3,TSI3,configfpath);
    [HMV_x_tsi1,HMV_y_tsi1]=TO_H2HMV(H1,TSI3,configfpath);
    [HMV_x_tsi2,HMV_y_tsi2]=TO_H2HMV(H2,TSI3,configfpath);
    
    imma3_tmma=false(UNDISTH,UNDISTW);
    imma1_fill=imma1;   imma2_fill=imma2;   imma3_fill=imma3;
    
    % 1. Fill TSI1 and TSI2 with TSI3 reg files
    for tmi=1:nConn
        j=si3(tmi);
        recH=Conn_H(j); recW=Conn_W(j);
        sx3=Conn_sx(j); sy3=Conn_sy(j);
        ex3=sx3+recW-1;   ey3=sy3+recH-1;
        mvx31=HMV_x_tsi3(j,1);    mvx32=HMV_x_tsi3(j,2);
        mvy31=HMV_y_tsi3(j,1);    mvy32=HMV_y_tsi3(j,2);
        sx1=sx3+mvx31;  sy1=sy3+mvy31;  sx2=sx3+mvx32;  sy2=sy3+mvy32;
        ex1=ex3+mvx31;  ey1=ey3+mvy31;  ex2=ex3+mvx32;  ey2=ey3+mvy32;
        
        % Generate TSI3 cloud mask in this cloud field
        %imma3_tmma(sy3:ey3,sx3:ex3)=true;
        imma3_tmma=false(UNDISTH,UNDISTW);
        imma3_tmma(sy3:ey3,sx3:ex3)=imma3_cldma_n(sy3:ey3,sx3:ex3);
        imma31_tmma=imma3_tmma; imma32_tmma=imma3_tmma;
        if isempty(find(imma3_tmma(sy3:ey3,sx3:ex3),1))
            disp('[ERROR]: there is no cloud in this cloud field on TSI3');
            return;
        end
        if ~isempty(find(imma3_fill_indma(sy3:ey3,sx3:ex3)==1,1))
            bFillTSI3=true;
        else
            bFillTSI3=false;
        end
        
        % Generate TSI1/TSI2 mask to fill
        imma1_tmma=false(UNDISTH,UNDISTW);
        imma2_tmma=false(UNDISTH,UNDISTW);
        
        if sx1<1 || sy1 <1 || ey1>UNDISTH || ex1>UNDISTW
            tmblkvals=TO_GetImgVal_Pad([sy1 sx1],[recH recW],imma1_fill_indma)&imma3_tmma(sy3:ey3,sx3:ex3);
            imma1_tmma=TO_SetImgVal_Pad([sy1 sx1],tmblkvals,imma1_tmma);
            imma31_tmma=TO_SetImgVal_Pad([sy3 sx3],tmblkvals,imma31_tmma);
        else
            imma1_tmma(sy1:ey1,sx1:ex1)=imma1_fill_indma(sy1:ey1,sx1:ex1)&imma3_tmma(sy3:ey3,sx3:ex3);
            imma31_tmma(sy3:ey3,sx3:ex3)=imma1_tmma(sy1:ey1,sx1:ex1);
        end
        if sx2<1 || sy2 <1 || ey2>UNDISTH || ex2>UNDISTW
            tmblkvals=TO_GetImgVal_Pad([sy2 sx2],[recH recW],imma2_fill_indma)&imma3_tmma(sy3:ey3,sx3:ex3);
            imma2_tmma=TO_SetImgVal_Pad([sy2 sx2],tmblkvals,imma2_tmma);
            imma32_tmma=TO_SetImgVal_Pad([sy3 sx3],tmblkvals,imma32_tmma);
        else
            imma2_tmma(sy2:ey2,sx2:ex2)=imma2_fill_indma(sy2:ey2,sx2:ex2)&imma3_tmma(sy3:ey3,sx3:ex3);
            imma32_tmma(sy3:ey3,sx3:ex3)=imma2_tmma(sy2:ey2,sx2:ex2);
        end
        imma31_tmma_3d=logical(TO_3DMaskfrom2D(imma31_tmma));
        imma32_tmma_3d=logical(TO_3DMaskfrom2D(imma32_tmma));
        imma1_tmma_3d=logical(TO_3DMaskfrom2D(imma1_tmma));
        imma2_tmma_3d=logical(TO_3DMaskfrom2D(imma2_tmma));
        imma1_fill(imma1_tmma_3d)=imma3(imma31_tmma_3d);
        imma2_fill(imma2_tmma_3d)=imma3(imma32_tmma_3d);
        
        
        % look back at TSI3
        if bFillTSI3
            ma3=false(UNDISTH,UNDISTW);
            tmblkmask=imma3_fill_indma(sy3:ey3,sx3:ex3);
            ma3(sy3:ey3,sx3:ex3)=tmblkmask;
            ma3_3d=logical(TO_3DMaskfrom2D(ma3));
            tmblkmask_3d=logical(TO_3DMaskfrom2D(tmblkmask));
            blk1=TO_GetImgVal_Pad([sy1 sx1],[recH recW],imma1);
            blk2=TO_GetImgVal_Pad([sy2 sx2],[recH recW],imma2);
            blk3=TO_GetImgVal_Pad([sy3 sx3],[recH recW],imma3);
            tmvals1=blk1(tmblkmask_3d);
            tmvals2=blk2(tmblkmask_3d);
            count1=tmvals1~=0;  count2=tmvals2~=0;
            tmcount=double(count1)+double(count2);
            tminvalid=tmcount==0;
            tmvals=(tmvals1+tmvals2)./tmcount;
            blk3(tmblkmask_3d)=tmvals;
            imma3_fill=TO_SetImgVal_Pad([sy3 sx3],blk3,imma3_fill);
        end
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Plot test of using reg file to fill other TSIs (TSI1 and TSI2)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     f1=figure();
%     subplot(1,3,1);imshow(imma1_fill);
%     subplot(1,3,2);imshow(imma3_fill);
%     subplot(1,3,3);imshow(imma2_fill);
%     tmf=GetImgFromDV_BNL(tmdv,TSI3);
%     outputf=sprintf('%s%s_regfill.jpg',FilledTSI3Dir,tmf);
%     print(f1,outputf,'-djpeg');
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    

    
    
    % 2. Fill TSI2 and TSI3 with TSI1 tracking file
    imma1_lum=TO_rgb2lum(imma1_fill);
    imma2_lum=TO_rgb2lum(imma2_fill);
    imma3_lum=TO_rgb2lum(imma3_fill);
    imma1_fill_indma=(imma1_lum==0);
    imma2_fill_indma=(imma2_lum==0);
    imma3_fill_indma=(imma3_lum==0);
    for tmi=1:nConn1
        j=si1(tmi);
        recH=Conn_H1(j); recW=Conn_W1(j);
        sx1=Conn_sx1(j); sy1=Conn_sy1(j);
        ex1=sx1+recW-1;   ey1=sy1+recH-1;
        mvx31=HMV_x_tsi1(j,1);    mvx32=HMV_x_tsi1(j,2);
        mvy31=HMV_y_tsi1(j,1);    mvy32=HMV_y_tsi1(j,2);
        sx3=sx1-mvx31;  sy3=sy1-mvy31;  sx2=sx3+mvx32;  sy2=sy3+mvy32;
        ex3=ex1-mvx31;  ey3=ey1-mvy31;  ex2=ex3+mvx32;  ey2=ey3+mvy32;
        
        
        % Generate TSI3 cloud mask in this cloud field
        %imma3_tmma(sy3:ey3,sx3:ex3)=true;
        imma1_tmma=false(UNDISTH,UNDISTW);
        imma13_tmma=imma1_tmma; imma12_tmma=imma1_tmma;
        imma1_tmma(sy1:ey1,sx1:ex1)=imma1_cldma_n(sy1:ey1,sx1:ex1);
        
        if isempty(find(imma1_tmma(sy1:ey1,sx1:ex1),1))
            disp('[ERROR]: there is no cloud in this cloud field on TSI1');
            return;
        end
        % Generate TSI1/TSI2 mask to fill
        imma2_tmma=false(UNDISTH,UNDISTW);
        imma3_tmma=false(UNDISTH,UNDISTW);
        
        if sx3<1 || sy3 <1 || ey3>UNDISTH || ex3>UNDISTW    
            tmblkvals=TO_GetImgVal_Pad([sy3 sx3],[recH recW],imma3_fill_indma)&imma1_tmma(sy1:ey1,sx1:ex1);
            imma3_tmma=TO_SetImgVal_Pad([sy3 sx3],tmblkvals,imma3_tmma);
            imma13_tmma=TO_SetImgVal_Pad([sy1 sx1],tmblkvals,imma13_tmma);
        else
            imma3_tmma(sy3:ey3,sx3:ex3)=imma3_fill_indma(sy3:ey3,sx3:ex3)&imma1_tmma(sy1:ey1,sx1:ex1);
            imma13_tmma(sy1:ey1,sx1:ex1)=imma3_tmma(sy3:ey3,sx3:ex3);
        end
        
        if sx2<1 || sy2 <1 || ey2>UNDISTH || ex2>UNDISTW
            tmblkvals=TO_GetImgVal_Pad([sy2 sx2],[recH recW],imma2_fill_indma)&imma1_tmma(sy1:ey1,sx1:ex1);
            imma2_tmma=TO_SetImgVal_Pad([sy2 sx2],tmblkvals,imma2_tmma);
            imma12_tmma=TO_SetImgVal_Pad([sy1 sx1],tmblkvals,imma12_tmma);
        else
            imma2_tmma(sy2:ey2,sx2:ex2)=imma2_fill_indma(sy2:ey2,sx2:ex2)&imma1_tmma(sy1:ey1,sx1:ex1);
            imma12_tmma(sy1:ey1,sx1:ex1)=imma2_tmma(sy2:ey2,sx2:ex2);
        end
        
        if ~isempty(find(imma13_tmma,1))
            imma13_tmma_3d=logical(TO_3DMaskfrom2D(imma13_tmma));
            imma3_tmma_3d=logical(TO_3DMaskfrom2D(imma3_tmma));
            imma3_fill(imma3_tmma_3d)=imma1(imma13_tmma_3d);
        end
        if ~isempty(find(imma12_tmma,1))
            imma12_tmma_3d=logical(TO_3DMaskfrom2D(imma12_tmma));
            imma2_tmma_3d=logical(TO_3DMaskfrom2D(imma2_tmma));
            imma2_fill(imma2_tmma_3d)=imma1(imma12_tmma_3d);
        end
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Plot test of using reg file to fill other TSIs (TSI1 and TSI2)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     f1=figure();
%     subplot(1,3,1);imshow(imma1_fill);
%     subplot(1,3,2);imshow(imma3_fill);
%     subplot(1,3,3);imshow(imma2_fill);
%     tmf=GetImgFromDV_BNL(tmdv,TSI3);
%     outputf=sprintf('%s%s_tsi1fill.jpg',FilledTSI3Dir,tmf);
%     print(f1,outputf,'-djpeg');
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    

    
    
    
    % 3. Fill TSI3 and TSI1 with TSI2 tracking file
    imma1_lum=TO_rgb2lum(imma1_fill);
    imma2_lum=TO_rgb2lum(imma2_fill);
    imma3_lum=TO_rgb2lum(imma3_fill);
    imma1_fill_indma=(imma1_lum==0);
    imma2_fill_indma=(imma2_lum==0);
    imma3_fill_indma=(imma3_lum==0);
    for tmi=1:nConn2
        j=si2(tmi);
        recH=Conn_H2(j); recW=Conn_W2(j);
        sx2=Conn_sx2(j); sy2=Conn_sy2(j);
        ex2=sx2+recW-1;   ey2=sy2+recH-1;
        mvx31=HMV_x_tsi2(j,1);    mvx32=HMV_x_tsi2(j,2);
        mvy31=HMV_y_tsi2(j,1);    mvy32=HMV_y_tsi2(j,2);
        sx3=sx2-mvx32;  sy3=sy2-mvy32;  sx1=sx3+mvx31;  sy1=sy3+mvy31;
        ex3=ex2-mvx32;  ey3=ey2-mvy32;  ex1=ex3+mvx31;  ey1=ey3+mvy31;
        
        
        % Generate TSI3 cloud mask in this cloud field
        %imma3_tmma(sy3:ey3,sx3:ex3)=true;
        imma2_tmma=false(UNDISTH,UNDISTW);
        imma23_tmma=imma2_tmma; imma21_tmma=imma2_tmma;
        imma2_tmma(sy2:ey2,sx2:ex2)=imma2_cldma_n(sy2:ey2,sx2:ex2);
        
        if isempty(find(imma2_tmma(sy2:ey2,sx2:ex2),1))
            disp('[ERROR]: there is no cloud in this cloud field on TSI1');
            return;
        end
        % Generate TSI1/TSI2 mask to fill
        imma1_tmma=false(UNDISTH,UNDISTW);
        imma3_tmma=false(UNDISTH,UNDISTW);
        
        if sx3<1 || sy3 <1 || ey3>UNDISTH || ex3>UNDISTW    
            tmblkvals=TO_GetImgVal_Pad([sy3 sx3],[recH recW],imma3_fill_indma)&imma2_tmma(sy2:ey2,sx2:ex2);
            imma3_tmma=TO_SetImgVal_Pad([sy3 sx3],tmblkvals,imma3_tmma);
            imma23_tmma=TO_SetImgVal_Pad([sy2 sx2],tmblkvals,imma23_tmma);
        else
            imma3_tmma(sy3:ey3,sx3:ex3)=imma3_fill_indma(sy3:ey3,sx3:ex3)&imma2_tmma(sy2:ey2,sx2:ex2);
            imma23_tmma(sy2:ey2,sx2:ex2)=imma3_tmma(sy3:ey3,sx3:ex3);
        end
        
        if sx1<1 || sy1 <1 || ey1>UNDISTH || ex1>UNDISTW
            tmblkvals=TO_GetImgVal_Pad([sy1 sx1],[recH recW],imma1_fill_indma)&imma2_tmma(sy2:ey2,sx2:ex2);
            imma1_tmma=TO_SetImgVal_Pad([sy1 sx1],tmblkvals,imma1_tmma);
            imma21_tmma=TO_SetImgVal_Pad([sy2 sx2],tmblkvals,imma21_tmma);
        else
            imma1_tmma(sy1:ey1,sx1:ex1)=imma1_fill_indma(sy1:ey1,sx1:ex1)&imma2_tmma(sy2:ey2,sx2:ex2);
            imma21_tmma(sy2:ey2,sx2:ex2)=imma1_tmma(sy1:ey1,sx1:ex1);
        end
        
        if ~isempty(find(imma23_tmma,1))
            imma23_tmma_3d=logical(TO_3DMaskfrom2D(imma23_tmma));
            imma3_tmma_3d=logical(TO_3DMaskfrom2D(imma3_tmma));
            imma3_fill(imma3_tmma_3d)=imma2(imma23_tmma_3d);
        end
        if ~isempty(find(imma21_tmma,1))
            imma21_tmma_3d=logical(TO_3DMaskfrom2D(imma21_tmma));
            imma1_tmma_3d=logical(TO_3DMaskfrom2D(imma1_tmma));
            imma1_fill(imma1_tmma_3d)=imma2(imma21_tmma_3d);
        end
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Plot test of using reg file to fill other TSIs (TSI1 and TSI2)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     f1=figure();
%     subplot(1,3,1);imshow(imma1_fill);
%     subplot(1,3,2);imshow(imma3_fill);
%     subplot(1,3,3);imshow(imma2_fill);
%     tmf=GetImgFromDV_BNL(tmdv,TSI3);
%     outputf=sprintf('%s%s_tsi2fill.jpg',FilledTSI3Dir,tmf);
%     print(f1,outputf,'-djpeg');
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    imma3_fill_l=imma3_reffill{1};
    fillma=TO_rgb2lum(imma3_fill)==0;
    fillma_3d=logical(TO_3DMaskfrom2D(fillma)); 
    imma3_fill(fillma_3d) = imma3_fill_l(fillma_3d);
    
%    disp(tmdv);
%     f1=figure();
%     subplot(1,3,1);imshow(imma1_fill);
%     subplot(1,3,2);imshow(imma3_fill);
%     subplot(1,3,3);imshow(imma2_fill);
%     tmf=GetImgFromDV_BNL(tmdv,TSI3);
%     outputf=sprintf('%s%s_allfill.jpg',FilledTSI3Dir,tmf);
%     print(f1,outputf,'-djpeg');
    
    
    
    
    
    
            
            
%             if ~isempty(find(imma1_tmma,1))
%                 imma1_tmma
%             end
%             if ~isempty(find(imma2_tmma,1))
%                 
%             end
%             TO_3DMaskfrom2D()
%             
%             
%             if RegH(tmi)==0
%                 rectangle('Position',[minsx minsy recW recH],'LineStyle','-','EdgeColor','c');
%             elseif RegH(tmi)==H_Reg(1)
%                 rectangle('Position',[minsx minsy recW recH],'LineStyle','-','EdgeColor','r');
%                 quiver(minsx+round(recW/2),minsy+round(recH/2),RegHMV_x(tmi,1),RegHMV_y(tmi,1));
%                 quiver(minsx+round(recW/2),minsy+round(recH/2),RegHMV_x(tmi,2),RegHMV_y(tmi,2));
%                 if bShowText1==false
%                     text(minsx+round(recW/2),minsy+round(recH/2),['\color{red} ' int2str(RegH(tmi))],'FontSize',18);
%                     bShowText1=true;
%                 end
%             elseif RegH(tmi)==H_Reg(2)
%                 rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','y');
%                 quiver(minsx+round(recW/2),minsy+round(recH/2),RegHMV_x(tmi,1),RegHMV_y(tmi,1));
%                 quiver(minsx+round(recW/2),minsy+round(recH/2),RegHMV_x(tmi,2),RegHMV_y(tmi,2));
%                 if bShowText2==false
%                     text(minsx+round(recW/2),minsy+round(recH/2),['\color{yellow} ' int2str(RegH(tmi))],'FontSize',18);
%                     bShowText2=true;
%                 end
%             end
%         end
%        
%         
%         
%         f1=figure();
%         subplot(1,3,2);
%         imshow(imma3);
%         hold on;
%         bShowText1=false;   bShowText2=false;
%         for tmi=1:nConn
%             minsx=Conn_sx(tmi); minsy=Conn_sy(tmi);
%             recH=Conn_H(tmi); recW=Conn_W(tmi);
%             if RegH(tmi)==0
%                 rectangle('Position',[minsx minsy recW recH],'LineStyle','-','EdgeColor','c');
%             elseif RegH(tmi)==H_Reg(1)
%                 rectangle('Position',[minsx minsy recW recH],'LineStyle','-','EdgeColor','r');
%                 quiver(minsx+round(recW/2),minsy+round(recH/2),RegHMV_x(tmi,1),RegHMV_y(tmi,1));
%                 quiver(minsx+round(recW/2),minsy+round(recH/2),RegHMV_x(tmi,2),RegHMV_y(tmi,2));
%                 if bShowText1==false
%                     text(minsx+round(recW/2),minsy+round(recH/2),['\color{red} ' int2str(RegH(tmi))],'FontSize',18);
%                     bShowText1=true;
%                 end
%             elseif RegH(tmi)==H_Reg(2)
%                 rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','y');
%                 quiver(minsx+round(recW/2),minsy+round(recH/2),RegHMV_x(tmi,1),RegHMV_y(tmi,1));
%                 quiver(minsx+round(recW/2),minsy+round(recH/2),RegHMV_x(tmi,2),RegHMV_y(tmi,2));
%                 if bShowText2==false
%                     text(minsx+round(recW/2),minsy+round(recH/2),['\color{yellow} ' int2str(RegH(tmi))],'FontSize',18);
%                     bShowText2=true;
%                 end
%             end
%         end
%         
%         for tmi=1:nCF1
%             minsx=Conn_sx1(tmi)-HMV_x1(tmi,2); minsy=Conn_sy1(tmi)-HMV_y1(tmi,2);
%             recH=Conn_H1(tmi); recW=Conn_W1(tmi);
%             if H1(tmi)==0
%                 rectangle('Position',[minsx minsy recW recH],'LineStyle',':','EdgeColor','c');
%             elseif H1(tmi)==H_Reg(1)
%                 rectangle('Position',[minsx minsy recW recH],'LineStyle',':','EdgeColor','r');
%                 quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x1(tmi,1),HMV_y1(tmi,1));
%                 quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x1(tmi,2),HMV_y1(tmi,2));
%                 if bShowText1==false
%                     text(minsx+round(recW/2),minsy+round(recH/2),['\color{red} ' int2str(RegH(tmi))],'FontSize',18);
%                     bShowText1=true;
%                 end
%             elseif H1(tmi)==H_Reg(2)
%                 rectangle('Position',[minsx minsy recW recH],'LineStyle',':','EdgeColor','y');
%                 quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x1(tmi,1),HMV_y1(tmi,1));
%                 quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x1(tmi,2),HMV_y1(tmi,2));
%                 if bShowText2==false
%                     text(minsx+round(recW/2),minsy+round(recH/2),['\color{yellow} ' int2str(H1(tmi))],'FontSize',18);
%                     bShowText2=true;
%                 end
%             end
%         end
%         for tmi=1:nCF2
%             minsx=Conn_sx2(tmi)-HMV_x2(tmi,1); minsy=Conn_sy2(tmi)-HMV_y2(tmi,1);
%             recH=Conn_H2(tmi); recW=Conn_W2(tmi);
%             if H2(tmi)==0
%                 rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','c');
%             elseif H2(tmi)==H_Reg(1)
%                 rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
%                 quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x2(tmi,1),HMV_y2(tmi,1));
%                 quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x2(tmi,2),HMV_y2(tmi,2));
%                 if bShowText1==false
%                     text(minsx+round(recW/2),minsy+round(recH/2),['\color{red} ' int2str(RegH(tmi))],'FontSize',18);
%                     bShowText1=true;
%                 end
%             elseif H2(tmi)==H_Reg(2)
%                 rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','y');
%                 quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x2(tmi,1),HMV_y2(tmi,1));
%                 quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x2(tmi,2),HMV_y2(tmi,2));
%                 if bShowText2==false
%                     text(minsx+round(recW/2),minsy+round(recH/2),['\color{yellow} ' int2str(H2(tmi))],'FontSize',18);
%                     bShowText2=true;
%                 end
%             end
%         end
%         hold off;
%         
%         
%         subplot(1,3,3);
%         imshow(imma2);
%         hold on;
%         for tmi=1:nConn
%             if RegH(tmi)==0
%                 continue;
%             end
%             minsx=Conn_sx(tmi)+RegHMV_x(tmi,1); minsy=Conn_sy(tmi)+RegHMV_y(tmi,1);
%             recH=Conn_H(tmi); recW=Conn_W(tmi);
%             if RegH(tmi)==H_Reg(1)
%                 rectangle('Position',[minsx minsy recW recH],'LineStyle','-','EdgeColor','r');
%             else
%                 rectangle('Position',[minsx minsy recW recH],'LineStyle','-','EdgeColor','y');
%             end
%             %rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
%             %quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi),HMV_y(tmi));
%         end
%         for tmi=1:nCF1
%             if H1(tmi)==0
%                 continue;
%             end
%             minsx=Conn_sx1(tmi)-HMV_x1(tmi,2)+HMV_x1(tmi,1); minsy=Conn_sy1(tmi)-HMV_y1(tmi,2)+HMV_y1(tmi,1);
%             recH=Conn_H1(tmi); recW=Conn_W1(tmi);
%             if H1(tmi)==H_Reg(1)
%                 rectangle('Position',[minsx minsy recW recH],'LineStyle',':','EdgeColor','r');
%             else
%                 rectangle('Position',[minsx minsy recW recH],'LineStyle',':','EdgeColor','y');
%             end
%             %rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
%             %quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi),HMV_y(tmi));
%         end
%         for tmi=1:nCF2
%             if H2(tmi)==0
%                 continue;
%             end
%             minsx=Conn_sx2(tmi); minsy=Conn_sy2(tmi);
%             recH=Conn_H2(tmi); recW=Conn_W2(tmi);
%             if H2(tmi)==H_Reg(1)
%                 rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
%             else
%                 rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','y');
%             end
%             %rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
%             %quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi),HMV_y(tmi));
%         end
%         hold off;
%         subplot(1,3,1);
%         imshow(imma1);
%         hold on;
%         for tmi=1:nConn
%             if RegH(tmi)==0
%                 continue;
%             end
%             minsx=Conn_sx(tmi)+RegHMV_x(tmi,2); minsy=Conn_sy(tmi)+RegHMV_y(tmi,2);
%             recH=Conn_H(tmi); recW=Conn_W(tmi);
%             if RegH(tmi)==H_Reg(1)
%                 rectangle('Position',[minsx minsy recW recH],'LineStyle','-','EdgeColor','r');
%             else
%                 rectangle('Position',[minsx minsy recW recH],'LineStyle','-','EdgeColor','y');
%             end
%             %rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
%             %quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi),HMV_y(tmi));
%         end
%         
%         for tmi=1:nCF1
%             if H1(tmi)==0
%                 continue;
%             end
%             minsx=Conn_sx1(tmi); minsy=Conn_sy1(tmi);
%             recH=Conn_H1(tmi); recW=Conn_W1(tmi);
%             if H1(tmi)==H_Reg(1)
%                 rectangle('Position',[minsx minsy recW recH],'LineStyle',':','EdgeColor','r');
%             else
%                 rectangle('Position',[minsx minsy recW recH],'LineStyle',':','EdgeColor','y');
%             end
%             %rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
%             %quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi),HMV_y(tmi));
%         end
%         for tmi=1:nCF2
%             if H2(tmi)==0
%                 continue;
%             end
%             minsx=Conn_sx2(tmi)-HMV_x2(tmi,1)+HMV_x2(tmi,2); minsy=Conn_sy2(tmi)-HMV_y2(tmi,1)+HMV_y2(tmi,2);
%             recH=Conn_H2(tmi); recW=Conn_W2(tmi);
%             if H2(tmi)==H_Reg(1)
%                 rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
%             else
%                 rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','y');
%             end
%             %rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
%             %quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi),HMV_y(tmi));
%         end
%         hold off;
%         tmfilename=GetImgFromDV_BNL(tmdv,TSI1);
%         tmoutf=sprintf('%s%s.jpg',tmoutDir,tmfilename(1:end-4));
%         print(f1,tmoutf,'-djpeg');
%     end
    
    
    
    
end
return;





tmdn=startdn;
oddns=[];
i=0;
while(tmdn<enddn)
    tmdn=startdn+i*TENSECS_DN;
    oddns=[oddns;tmdn];
    i=i+1;
end
oddvs=datevec(oddns);
oddns_n=length(oddns);


for i =1:oddns_n-1
    tmdn=oddns(i);
    tmdv=oddvs(i,:);
    tmdv_1faf=oddvs(i+1,:);
    tmdn_1faf=oddns(i+1);
    %     if tmdn<datenum([2013 06 17 14 00 00])
    %         continue;
    %     end
    %     if rem(i,6)~=0
    %         continue;
    %     end
    
    try
        imma1_ori=tmp_GetImgMatFromDataDir(tmdv,TSI1,dataDir);
        imma2_ori=tmp_GetImgMatFromDataDir(tmdv,TSI2,dataDir);
        imma3=tmp_GetImgMatFromDataDir(tmdv,TSI3,dataDir);
        Red1 = imma3(:, :, 1);
        Green1 = imma3(:, :, 2);
        Blue1 = imma3(:, :, 3);
        HnRed1=imhist(Red1);
        HnGreen1=imhist(Green1);
        HnBlue1=imhist(Blue1);
        HnRed1(1)=0;HnGreen1(1)=0;HnBlue1(1)=0;     % 0 means not valid
        ref_sky_hist=[HnRed1 HnGreen1 HnBlue1];
        
        %         imma1_1faf=tmp_GetImgMatFromDataDir(tmdv_1faf,TSI1,dataDir);
        %         imma2_1faf=tmp_GetImgMatFromDataDir(tmdv_1faf,TSI2,dataDir);
        %         imma3_1faf=tmp_GetImgMatFromDataDir(tmdv_1faf,TSI3,dataDir);
    catch
        continue;
    end
    imma1=TO_histeq_TSISky(imma1_ori,ref_sky_hist);
    imma2=TO_histeq_TSISky(imma2_ori,ref_sky_hist);
    [tmHMV_x,tmHMV_y]=TO_H2HMV(HRef,TSI3);
    tmHMV_x31=tmHMV_x(1);tmHMV_x32=tmHMV_x(2);
    tmHMV_y31=tmHMV_y(1);tmHMV_y32=tmHMV_y(2);
    imma1_lum=single(TO_rgb2lum(imma1));
    imma2_lum=single(TO_rgb2lum(imma2));
    imma3_lum=single(TO_rgb2lum(imma3));
    
    
    % Generate imma3_fill image using low alttitude cloud
    imma3_fill=imma3;
    imma3_fill=single(imma3_fill);
    imma3_fill_indma=imma3_lum==0;
    imma1_s=TO_imshift(imma1,[-tmHMV_y31 -tmHMV_x31]);
    imma2_s=TO_imshift(imma2,[-tmHMV_y32 -tmHMV_x32]);
    tmc1_ma=TO_rgb2lum(imma1_s)==0;
    tmc2_ma=TO_rgb2lum(imma2_s)==0;
    tmc1_ma_3d=logical(TO_3DMaskfrom2D(tmc1_ma));
    tmc2_ma_3d=logical(TO_3DMaskfrom2D(tmc2_ma));
    imma1_s(tmc1_ma_3d)=imma2_s(tmc1_ma_3d);
    imma2_s(tmc2_ma_3d)=imma1_s(tmc2_ma_3d);
    imma3_fill_indma_3d=logical(TO_3DMaskfrom2D(imma3_fill_indma));
    imma3_fill(imma3_fill_indma_3d)=(single(imma1_s(imma3_fill_indma_3d))+single(imma2_s(imma3_fill_indma_3d)))./2;
    imma3_fill=uint8(imma3_fill);
    invalidma=TO_rgb2lum(imma3_fill)==0;
    imma3_fill(TO_3DMaskfrom2D(invalidma))=skyfillma(TO_3DMaskfrom2D(invalidma));
    imma3_fill_l=uint8(imma3_fill);
    
    %imma1_s_l=imma1_s;
    %imma2_s_l=imma2_s;
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Show two filling matrix of imma3 for debug
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %     figure();
    %     subplot(2,2,1);imshow(imma3);
    %     subplot(2,2,2);imshow(uint8(imma3_fill_l));
    %     subplot(2,2,3);imshow(uint8(imma1_s_l));
    %     subplot(2,2,4);imshow(uint8(imma2_s_l));
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    filename=GetImgFromDV_BNL(tmdv,TSI3);
    outputf=sprintf('%s%s_fill.bmp',FilledTSI3Dir,filename(1:end-4));
    imwrite(imma3_fill_l,outputf,'bmp');
    
    
    
    
end
