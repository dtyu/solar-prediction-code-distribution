function histfiles=GetHistFrames(startfile,tsiDir,nhist)
allfiles=dir(tsiDir);
fi=1;

filename=GetFilenameFromAbsPath(startfile);
histfiles=cell(1,nhist);
while ~strcmpi(allfiles(fi).name,filename)
    fi=fi+1;
end
histfiles{1}=startfile;

for j=fi+1:fi+nhist-1
    filename=allfiles(j).name;
    tmf=sprintf('%s%s',tsiDir,filename);
    histfiles{j-fi+1}=tmf;
end




end