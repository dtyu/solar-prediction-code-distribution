    function [x,y]=ReGenArrayOnBoundary(xin,yin,xs,ys,SunRan)
        %Assum x y have the same size
        dist=sqrt((xin-xs).*(xin-xs)+(yin-ys).*(yin-ys));
        newi=dist>SunRan;
        x=xin(newi);
        y=yin(newi);
    end

