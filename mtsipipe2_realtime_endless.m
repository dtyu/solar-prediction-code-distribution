%mtsipipe2_realtime_endless
% keeps monitoring the active folder and try to call mtsipipe2 to extract the layers and height
otsi1Dir='./data/oTSI1';
fliptsi1Dir='./data/filpTSI1';
utsi1Dir='./data/tsi1_undist_mat';
otsi2Dir='./data/oTSI2';
fliptsi2Dir='./data/filpTSI2';
utsi2Dir='./data/tsi2_undist_mat';
otsi3Dir='./data/oTSI3';
fliptsi3Dir='./data/filpTSI3';
utsi3Dir='./data/tsi3_undist_mat';

dataDir='./data/';
HRefDir='./data/href/';
dataDir=FormatDirName(dataDir);
HRefDir=FormatDirName(HRefDir);
DNDIFF = datenum([10 0 0 0 10 0]); % consider only 30 minutes ahead
set_dn=java.util.HashSet;
cldmodelmatf='model_rbr.mat';
configfpath='mtsi_startup';
latestdn=0;
while(1)
    files3= dir(utsi3Dir);
    m=size(files3,1);
    utc_time = java.lang.System.currentTimeMillis;
    a=utc_time/1000; % conver to UNIX TIMESTAMP
    nowdn=(a/86400 + datenum(1970,1,1)); % UTC now datenum
    startdn=nowdn-DNDIFF;
    tmdns=[];
    for i = 1:m
        filename=files3(i).name;
        tmdv=GetDVFromImg(filename);
        if (0==tmdv) continue; end % not a regular file
        tmdn=datenum(tmdv);
        if(tmdn<startdn || set_dn.contains(tmdn)) continue; end
        tmdns=[tmdns tmdn];
    end
    tmdns=sort(tmdns,'descend'); % sort and get the lastest
    for i = 1:length(tmdns)
        tmdn=tmdns(i);
        tmdv=datevec(tmdn);
%         if tmdn~=datenum([2014 04 09 19 01 20])
%             continue;
%         end
        disp(['Cloud Tracking at ' datestr(tmdv) ' will be executed.']);
        bStatus=mtsipipe2_realtime(tmdv,HRefDir,cldmodelmatf,configfpath);
        set_dn.add(tmdn);
        if(bStatus==true) 
            break; % recent file is executed well, then restart from the latest ts
        end
        
    end
    pause(1);
end