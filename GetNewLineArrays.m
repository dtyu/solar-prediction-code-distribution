function [x,y]=GetNewLineArrays(a,b,x0,y0,thres,LPDirect)
[subtmm,~]=size(x0);
x=zeros(subtmm,1);
y=zeros(subtmm,1);
if(LPDirect=='x')
    A=a;
    B=-1;
    C=b;

else
    A=-1;
    B=a;
    C=b;
end

for subi=1:subtmm
    tmdist=abs(A*x0+B*y0+C)/sqrt(A^2+B^2);
    if(tmdist>thres)
        x(subi)=-9999;
        y(subi)=-9999;
    else
        x(subi)=x0(subi);
        y(subi)=y0(subi);
    end
end

x=x(x~=-9999);
y=y(y~=-9999);
end