%function paperuse_cloudmotion

%imwrite(imma1_ori,'cldtrack_ori1.bmp');imwrite(imma2_ori,'cldtrack_ori2.bmp');imwrite(imma3,'cldtrack_ori3.bmp');
%imwrite(imma1_1faf,'cldtrack_ori4.bmp');imwrite(imma2_1faf,'cldtrack_ori5.bmp');imwrite(imma3_1faf,'cldtrack_ori6.bmp');

%connind=H_major==Href_l&MV_x_major~=0&MV_y_major~=0;
%connind=H_multi==Href_l1|H_multi==Href_l2;
set(0,'DefaultFigureVisible', 'on');
% set(0,'DefaultFigureVisible', 'off');

connind=(MV_x_multi~=0|MV_y_multi~=0);
bL1=Conn_sx;
bL1(:)=false;
set(gca,'position',[0 0 1 1],'units','normalized')
f3=figure();
imshow(imma3);
hold on;
for tmi=1:nConn
    if connind(tmi)==0
        continue;
    end
    minsx=Conn_sx(tmi); minsy=Conn_sy(tmi);
    recH=Conn_H(tmi); recW=Conn_W(tmi);
    if H_multi(tmi)<=2000
        bL1(tmi)=true;
        rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r','LineWidth',2);
    elseif H_multi(tmi)>2000
        rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','g','LineWidth',2);
    end
    quiver(minsx+round(recW/2),minsy+round(recH/2),HMV31_x_multi(tmi),HMV31_y_multi(tmi),'b','LineWidth',2);
    quiver(minsx+round(recW/2),minsy+round(recH/2),HMV32_x_multi(tmi),HMV32_y_multi(tmi),'c','LineWidth',2);
    %text(minsx+round(recW/2),minsy+round(recH/2),['\color{red} ' int2str(H_multi(tmi))],'FontSize',18);
end
hold off;
%     subplot(1,3,1);
%     imshow(imma1);
%     hold on;
%     for tmi=1:nConn
%         if MAXCC_hcld(tmi)==0
%             continue;
%         end
%         minsx=Conn_sx(tmi)+HMV_hcld_x31(tmi); minsy=Conn_sy(tmi)+HMV_hcld_y31(tmi);
%         recH=Conn_H(tmi); recW=Conn_W(tmi);
%         rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
%         %quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi),HMV_y(tmi));
%     end
%     hold off;
set(gca,'position',[0 0 1 1],'units','normalized')
f2=figure();
imshow(imma2_ori);
hold on;
for tmi=1:nConn
    if connind(tmi)==0
        continue;
    end
    minsx=Conn_sx(tmi)+HMV32_x_multi(tmi); minsy=Conn_sy(tmi)+HMV32_y_multi(tmi);
    recH=Conn_H(tmi); recW=Conn_W(tmi);
    if bL1(tmi)
        rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r','LineWidth',2);
        quiver(minsx+round(recW/2),minsy+round(recH/2),MV_x_multi(tmi),MV_y_multi(tmi),5,'r','LineWidth',2);
        %quiver(minsx+round(recW/2),minsy+round(recH/2),MV_x_multi(tmi),MV_y_multi(tmi),5,'r');
    else
        rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','g','LineWidth',2);
        quiver(minsx+round(recW/2),minsy+round(recH/2),MV_x_multi(tmi),MV_y_multi(tmi),5,'g','LineWidth',2);
        %quiver(minsx+round(recW/2),minsy+round(recH/2),MV_x_multi(tmi),MV_y_multi(tmi),5,'g');
    end
end
hold off;

set(gca,'position',[0 0 1 1],'units','normalized')
f1=figure();
imshow(imma1_ori);
hold on;
for tmi=1:nConn
    if connind(tmi)==0
        continue;
    end
    minsx=Conn_sx(tmi)+HMV31_x_multi(tmi); minsy=Conn_sy(tmi)+HMV31_y_multi(tmi);
    recH=Conn_H(tmi); recW=Conn_W(tmi);
    if bL1(tmi)
        rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r','LineWidth',2);
        quiver(minsx+round(recW/2),minsy+round(recH/2),MV_x_multi(tmi),MV_y_multi(tmi),5,'r','LineWidth',2);
    else
        rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','g','LineWidth',2);
        quiver(minsx+round(recW/2),minsy+round(recH/2),MV_x_multi(tmi),MV_y_multi(tmi),5,'g','LineWidth',2);
    end
    %quiver(minsx+round(recW/2),minsy+round(recH/2),MV_x_multi(tmi),MV_y_multi(tmi),10,'r','LineWidth',2);
end
hold off;
set(gca,'position',[0 0 1 1],'units','normalized')

set(f1,'position',[0,0,500,500]);
set(f2,'position',[0,0,500,500]);
set(f3,'position',[0,0,500,500]);
print(f1,'cldtrack_tsi1.bmp','-dbmp');
print(f2,'cldtrack_tsi2.bmp','-dbmp');
print(f3,'cldtrack_tsi3.bmp','-dbmp');


set(gca,'position',[0 0 1 1],'units','normalized')
set(gcf,'PaperUnits','inches','PaperPosition',[0 0 5 5]);

print(f1,'cldtrack_tsi1.bmp','-dbmp','-r100');
print(f2,'cldtrack_tsi2.bmp','-dbmp','-r100');
print(f3,'cldtrack_tsi3.bmp','-dbmp','-r100');


%end