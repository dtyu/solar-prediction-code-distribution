%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Show cloud field full search results
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
t=load(cldtrkmatoutf);
%t=load('cldtrk2_test.mat');
H=t.H;
Conn_sx=t.Conn_sx;    Conn_sy=t.Conn_sy;
Conn_H=t.Conn_H;      Conn_W=t.Conn_W;
HMV_x=t.HMV_x; HMV_y=t.HMV_y;
MV_x=t.MV_x;    MV_y=t.MV_y;
MAXCC=t.MAXCC;
nConn=t.nCF;
tmoutDir=sprintf('CldTrackTestout_2/');
if ~exist(tmoutDir,'dir');
    mkdir(tmoutDir);
end

f1=figure();
subplot(1,3,2);
imshow(imma3);
hold on;
for tmi=1:nConn
    %         if MAXCC(tmi)==0
    %             continue;
    %         end
    
    minsx=Conn_sx(tmi); minsy=Conn_sy(tmi);
    recH=Conn_H(tmi); recW=Conn_W(tmi);
    [mcc,mi]=max(MAXCC{tmi});
    tmh=H{tmi}(mi);
    if isempty(MAXCC{tmi})
        rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','c');
    elseif tmh<3000
        rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
        [HMV_x,HMV_y]=TO_H2HMV(tmh,TSI3,configfpath);
        quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(1,1),HMV_x(1,1));
        quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(1,2),HMV_y(1,2));
        text(minsx+round(recW/2),minsy+round(recH/2),['\color{red} ' int2str(tmh)],'FontSize',18);
    elseif tmh>=3000
        rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','y');
        [HMV_x,HMV_y]=TO_H2HMV(tmh,TSI3,configfpath);
        quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(1,1),HMV_x(1,1));
        quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(1,2),HMV_y(1,2));
        text(minsx+round(recW/2),minsy+round(recH/2),['\color{red} ' int2str(tmh)],'FontSize',18);
    end
    %rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
    %quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi,1),HMV_y(tmi,1));
    %quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi,2),HMV_y(tmi,2));
    %text(minsx+round(recW/2),minsy+round(recH/2),['\color{red} ' int2str(H(tmi))],'FontSize',18);
end
hold off;
subplot(1,3,3);
imshow(imma2);
hold on;
for tmi=1:nConn
    if isempty(MAXCC{tmi})
        continue;
    end
    [mcc,mi]=max(MAXCC{tmi});
    tmh=H{tmi}(mi);
    [HMV_x,HMV_y]=TO_H2HMV(tmh,TSI3,configfpath);
    minsx=Conn_sx(tmi)+HMV_x(1,2); minsy=Conn_sy(tmi)+HMV_y(1,2);
    recH=Conn_H(tmi); recW=Conn_W(tmi);
    if tmh<3000
        rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
    else
        rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','y');
    end
    %rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
    %quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi),HMV_y(tmi));
end
hold off;
subplot(1,3,1);
imshow(imma1);
hold on;
for tmi=1:nConn
    if isempty(MAXCC{tmi})
        continue;
    end
    [mcc,mi]=max(MAXCC{tmi});
    tmh=H{tmi}(mi);
    [HMV_x,HMV_y]=TO_H2HMV(tmh,TSI3,configfpath);
    minsx=Conn_sx(tmi)+HMV_x(1,1); minsy=Conn_sy(tmi)+HMV_y(1,1);
    recH=Conn_H(tmi); recW=Conn_W(tmi);
    if tmh<3000
        rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
    else
        rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','y');
    end
    %rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
    %quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi),HMV_y(tmi));
end
hold off;
tmoutf=sprintf('%s%s',tmoutDir,GetImgFromDV_BNL(realtimedv,TSI1));
%print(f1,tmoutf,'-djpeg');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%