% This function is a module to extract history layers information with the format of HRef,MVRef pair
%   VERSION 1.0     2013/10/07
%       1. Choose N frames ahead and calculate most frequent(cloud pixel count) NHISTFRAMES layers
%       2. Get HRef and MVRef for these layers
%   VERSION 1.1     2013/11/04
%       Add configfpath as input 
function [HRef,MVRef]=GetHistRefLayers(HRefDir,oritmdv,configfpath)
run(configfpath);
%NHISTFRAMES=10;
%TENSECS_DN=datenum([0 0 0 0 0 10]);
ONEMIN_DN=datenum([0 0 0 0 1 0]);
HRefDir=FormatDirName(HRefDir);

oritmdn=datenum(oritmdv);
tmdns=zeros(NHISTFRAMES,1);
tmdns(:)=oritmdn;
diffdns=NHISTFRAMES:-1:1;    diffdns=diffdns'.*ONEMIN_DN;
tmdns=tmdns-diffdns;


RefLayers_sum=zeros(MAXNUMOFLAYER,1);
RefLayers_counts=zeros(MAXNUMOFLAYER,1);


bExistArr=false(NHISTFRAMES,1);
for fi=1:NHISTFRAMES
    tmdn=tmdns(fi);
    tmdv=datevec(tmdn);
    [matoutf,recheckmatf]=tmp_GetCldTrackMat(HRefDir,tmdv,configfpath);   % get last reference file
    if ~exist(matoutf,'file')
        continue;
    end
    try
        t=load(matoutf);
    catch e
        continue;
    end
    bExistArr(fi)=true;
end

NofValidHist=length(find(bExistArr));
H_Reg_all=[];
H_arr=[];
Cldc_arr=[];
RefLayers_H=[];
if NofValidHist<NOFVALIDHIST
    bNoHistRef=true;
    HRef=RefLayers_H;
else
    bNoHistRef=false;
    for fi=1:NHISTFRAMES
        if ~bExistArr(fi)
            continue;
        end
        tmdn=tmdns(fi);
        tmdv=datevec(tmdn);
        [matoutf,recheckmatf]=tmp_GetCldTrackMat(HRefDir,tmdv,configfpath);   % get last reference file
        t=load(matoutf);
        CldC_Reg=t.CldC_Reg;
        RegH=t.RegH;
        MV_x_Reg=t.MV_x_Reg;    MV_y_Reg=t.MV_y_Reg;
        H_Reg=t.H_Reg;
        H_Reg_all=[H_Reg_all H_Reg];
        if isempty(find(MV_x_Reg~=0,1)) && isempty(find(MV_y_Reg~=0,1))
            bNoHistRef=true;
        elseif (MV_x_Reg(1)==0&&MV_y_Reg(1)==0) || (MV_x_Reg(2)==0&&MV_y_Reg(2)==0)
            bSingleLayer=true;
        else
            bSingleLayer=false;
        end
        
        %assert(isempty(find(CldC_Reg==0,1)),'Cloud Count should not be zero!');
        for hi=1:MAXNUMOFLAYER
            RefLayers_counts(hi)=RefLayers_counts(hi)+CldC_Reg(hi);
            RefLayers_sum(hi)=RefLayers_sum(hi)+H_Reg(hi)*CldC_Reg(hi);
            H_arr=[H_arr H_Reg(hi)];
            Cldc_arr=[Cldc_arr CldC_Reg(hi)];
        end
    end
    
    tml=length(Cldc_arr);
    tmcs=round(Cldc_arr./100);
    tmHs=[];
    for li=1:tml
        tmH=zeros(tmcs(li),1);
        tmH(:)=H_arr(li);
        tmHs=[tmHs;tmH];
    end
    if isempty(tmHs)
        bNoHistRef=true;
        HRef=RefLayers_H;
    else
        bSingleLayered=false;
        if 1==length(unique(tmHs))
            bSingleLayered=true;
        else
            bClusterd=false;
            while false==bClusterd
                try
                    [~,C]=kmeans(tmHs,MAXNUMOFLAYER);
                    C=sort(C);
                    bClusterd=true;
                catch e
                    bClusterd=false;
                end
            end
            if abs(C(1)-C(2))<1000
                bSingleLayered=true;
            end
        end
        if bSingleLayered
            HRef=[0 mean(tmHs)];
        else
            HRef=[C(1) C(2)];
        end
        
        t=load(HeightMatrixValidMatf);
        H_range_full=t.H_range_full;
        for tmi=1:MAXNUMOFLAYER
            if HRef(tmi)==0
                continue;
            end
            diffvals=abs(H_range_full-HRef(tmi));
            [~,mini]=min(diffvals);
            HRef(tmi)=H_range_full(mini);
        end
    end
end

MVRef=[0,0];



% HRef=0; MV_Href=0;MVx_class=0; MVy_class=0; MV_classcount=0;
% InnerBlockSize=10;
% StdBlockSize=11;
% StdBlockHalfSize=floor(StdBlockSize/2);
% StdCldThres=5;
% ConnMinSize=10;
% ConnMaxSize=60;
% NextFrameMVSearchWinSize=15;
% H_sran_low=1000:100:5000;
% H_sran_high=5500:500:20000;
% H_sran_simple=[2000:500:5000 5500:1000:20000];
% H_sran=[H_sran_low H_sran_high];
% ncc_thres=0.5;
% ncc_thres_tsi32=2.1;        % ncc31,32 + ncc31,32 + ncc21,22
% ncc_cloudfield_trust=0.7;   % ncc value which consider the movement is reasonable for valid cloud field
% LOWCLDHTHRES = 5000;        % Treat as low altittude
% HREF_DIFF_TOLERANCE=500;    % difference to reference height will be tolerated or considered the same as reference
% 
% H_range=H_sran;
% mtsi_startup_mini;
% 
% tmdns=datenum(tmdvs);
% FRAME_N=length(tmdns);
% nConn_MAX=100;
% MV_x=zeros(nConn_MAX,FRAME_N); MV_y=zeros(nConn_MAX,FRAME_N);
% HMV_x=zeros(nConn_MAX,FRAME_N); HMV_y=zeros(nConn_MAX,FRAME_N);
% H32=zeros(nConn_MAX,FRAME_N);     MAXCC=zeros(nConn_MAX,FRAME_N);
% Conn_H=zeros(nConn_MAX,FRAME_N); Conn_W=zeros(nConn_MAX,FRAME_N);
% Conn_sx=zeros(nConn_MAX,FRAME_N);Conn_sy=zeros(nConn_MAX,FRAME_N);
% Conn_cldcount=zeros(nConn_MAX,FRAME_N);
% bhistframe=true;
% cc_all=cell(nConn_MAX,FRAME_N);
% for fi=1:FRAME_N
%     tmdv=tmdvs(fi,:);
%     tmf=GetImgFromDV_BNL(tmdv,TSI1);
%     tmf=[tmf(1:end-4) 'cldtrack.mat'];
%     tmf=sprintf(HRefDir,tmf);
%     
%     try
%         imma3=tmp_GetImgMatFromDataDir(tmdv,TSI3,dataDir);
%     catch
%         bhistframe=false;
%         break;
%     end
%     for tmi=1:nConn
%         tmf=GetImgFromDV_BNL(tmdv,TSI1);
%         tmind=find(ConnLabelma==tmi);
%         Conn_cldcount(tmi,fi)=length(tmind);
%         [y,x]=ind2sub([UNDISTH,UNDISTW],tmind);
%         minsy=min(y);
%         minsx=min(x);
%         recH=max(y)-min(y)+1;
%         recW=max(x)-min(x)+1;
%         Conn_sx(tmi,fi)=minsx;Conn_sy(tmi,fi)=minsy;
%         Conn_H(tmi,fi)=recH;Conn_W(tmi,fi)=recW;
%         tmoutf=sprintf('%s_%d.mat',tmf(1:end-4),tmi);
%         outmatf=sprintf('%s%s',NCCFSOutMatDir,tmoutf);
%         t=load(outmatf);
%         H32(tmi,fi)=t.H;
%         MV_x(tmi,fi)=t.mv_x; MV_y(tmi,fi)=t.mv_y;
%         HMV_x(tmi,fi)=t.HMV_x; HMV_y(tmi,fi)=t.HMV_y;
%         MAXCC(tmi,fi)=t.maxcc;
%         cc_all{tmi,fi}=t.cc_all;
%     end
% end



end