function imma_skyfill= TO_skyfill(imma,fillmask,skymask)
mtsi_startup_mini;
[H,W,nDim]=size(imma);
assert(nDim==3,'Input should be RGB matrix');
winsize_ran=5:5:100;
winsize_ran_l=30:20:200;
ceny=UNDISTH/2;cenx=UNDISTW/2;
[x,y]=meshgrid(1:UNDISTW,1:UNDISTH);
distma=sqrt((x-cenx).^2+(y-ceny).^2);
edgemask=(distma>UNDISTR);
cornermask=(distma>UNDISTH/2);
r=imma(:,:,1);
g=imma(:,:,2);
b=imma(:,:,3);

imma_skyfill=imma;
for i=1:H
    for j=1:W
        if ~fillmask(i,j)
            continue;
        end
        if edgemask(i,j)
            continue;
        end
        bWinValid=false;
        for wi=length(winsize_ran)
            winsize=winsize_ran(wi);
            tmy=round(i-winsize/2);
            tmx=round(j-winsize/2);
            tmmasks=TO_GetImgVal_Pad([tmy tmx],[winsize winsize],skymask,winsize);
            if ~isempty(find(tmmasks==1,1))
                bWinValid=true;
                indexi=tmmasks==1;
                tmvals=TO_GetImgVal_Pad([tmy tmx],[winsize winsize],imma,winsize);
                tmr=tmvals(TO_3DMaskfrom2D(indexi,1));
                tmg=tmvals(TO_3DMaskfrom2D(indexi,2));
                tmb=tmvals(TO_3DMaskfrom2D(indexi,3));
                imma_skyfill(i,j,1)=mean(tmr);
                imma_skyfill(i,j,2)=mean(tmg);
                imma_skyfill(i,j,3)=mean(tmb);
                break;
            else
                % increase winsize
            end 
        end
    end
end


for i=1:H
    for j=1:W
        if ~edgemask(i,j)
            continue;
        end
        if ~fillmask(i,j)
            continue;
        end
        bWinValid=false;
        for wi=length(winsize_ran_l)
            winsize=winsize_ran_l(wi);
            tmy=round(i-winsize/2);
            tmx=round(j-winsize/2);
            tmmasks=TO_GetImgVal_Pad([tmy tmx],[winsize winsize],skymask,winsize);
            if ~isempty(find(tmmasks==1,1))
                bWinValid=true;
                indexi=tmmasks==1;
                tmvals=TO_GetImgVal_Pad([tmy tmx],[winsize winsize],imma,winsize);
                tmr=tmvals(TO_3DMaskfrom2D(indexi,1));
                tmg=tmvals(TO_3DMaskfrom2D(indexi,2));
                tmb=tmvals(TO_3DMaskfrom2D(indexi,3));
                imma_skyfill(i,j,1)=mean(tmr);
                imma_skyfill(i,j,2)=mean(tmg);
                imma_skyfill(i,j,3)=mean(tmb);
                break;
            else
                % increase winsize
            end 
        end
    end
end


end