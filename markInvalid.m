function invalidma=markInvalid(trainx,mode)
bNan=modepaser(mode,'nan');
bZero=modepaser(mode,'0');
bInf=modepaser(mode,'inf');
assert(bNan|bZero|bInf,'At least one invalid symbol must be given in mode!');
nanma=false(size(trainx));
zeroma=nanma;
infma=nanma;
nanma(:)=bNan;
zeroma(:)=bZero;
infma(:)=bInf;
invalidma=isnan(trainx)&nanma|(trainx==inf)&infma|(trainx==0)&zeroma;

end