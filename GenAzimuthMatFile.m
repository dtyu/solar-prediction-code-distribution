%GenAzimuthMatFile: This function is used for generate azimuthfile from
%   testfill directory. After picking clear sky library cases.All the images
%   left in the testfill directory will be used to extract datenum and
%   azimuth,zenith
%   IN:
%       testfilldir(in)     --  Directory, containning all the clear sky images
%       sbmaskdir(in)       --  Directory, containing sbmasks
%       hamaskdir(in)       --  Directory, containing hamasks
%       UndistortionParameters.mat(in) --   original image information
%       
%   OUT:
%       azimuth.mat         --  clear sky library
%       
function GenAzimuthMatFile
load('UndistortionParameters.mat');
testfillDir='testfill/';    %internal input
sbmaskDir='sbmask/';        %internal input
hamaskDir='hamask/';        %internal input

allfiles=dir(testfillDir);
[m,~]=size(allfiles);
tsdn=zeros(m,1);
azs=zeros(m,1);
zes=zeros(m,1);

for i=1 :m
    if(allfiles(i).isdir~=1)
        filename=allfiles(i).name;
        if(strcmpi(filename(end-3:end),'.jpg')==0)
            disp('ERROR: file is not a .jpg file,please check your input!');
            break;
        end
        sbmaskfp=sprintf('%s%s.mat',sbmaskDir,filename);
        hamaskfp=sprintf('%s%s.mat',hamaskDir,filename);
        if(exist(sbmaskfp,'file')==0||exist(hamaskfp,'file')==0)
            disp('ERROR: the sbmask or hamask does not exist!');
            break;
        end
        datevector=GetDVFromImg(filename);
        time.year    =  datevector(1);
        time.month   =  datevector(2);
        time.day     =  datevector(3);
        time.hour    =  datevector(4);
        time.min    =   datevector(5);
        time.sec    =   datevector(6);
        time.UTC    =   0;
        sun = sun_position(time, location);
        az=sun.azimuth/180*pi;
        ze=sun.zenith/180*pi;
        azs(i)=az;
        zes(i)=ze;
        tsdn(i)=datenum(datevector);
    end
end
tsdn=tsdn(3:end);
azs=azs(3:end);
zes=zes(3:end);
outputf='azimuth.mat';
save(outputf,'tsdn','azs','zes');




end