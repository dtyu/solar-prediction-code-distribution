% This function is to test extraction of hight altittude cloud from TS3,TSI1 and TSI3,TSI2 pairs
% using only one frame(frame t). The basice idea is that TSI3 is in the middle and make use of
% height transformation based on the displacement, we can find the most similiar displacement on
% images.
function test_HighCld_multicore(mode)
% set(0,'DefaultFigureVisible', 'on');
set(0,'DefaultFigureVisible', 'off');
bMulti=modepaser(mode,'multicore');
bServer=modepaser(mode,'server');
bPlot=modepaser(mode,'plot');
bRun=modepaser(mode,'run');


if bServer && bMulti
    nMatlabinst=16;
    severtype=32;
elseif  ~bServer && bMulti
    nMatlabinst=4;
    severtype=64;
end
if bServer
    dataDir='/cewit/home/zpeng/workdata/mtsi_stich/';
else
    dataDir='/data/workdata/mtsi_stich/';
end


mtsi_startup;

tsiDirs={'./cloudy_multilayer/tsi1_undist_20130605/','./cloudy_multilayer/tsi2_undist_20130605/','./cloudy_multilayer/tsi3_undist_20130605/'};
matoutputDir='hcldoutDir';
matoutputDir=FormatDirName(matoutputDir);
plotoutDir='hcldoutDir_plot';
plotoutDir=FormatDirName(plotoutDir);
if ~exist(matoutputDir,'file')
    mkdir(matoutputDir);
end
if ~exist(plotoutDir,'file')
    mkdir(plotoutDir);
end



DATADir=dataDir;

tsi1Dir=tsiDirs{1};
tsi2Dir=tsiDirs{2};
tsi3Dir=tsiDirs{3};


alltsi1=dir(tsi1Dir);
[nfiles1,~]=size(alltsi1);


imma_avgf='./imma_avgf.mat';
t=load(imma_avgf,'imma1_avg','imma2_avg','imma3_avg');
imma1_avg=t.imma1_avg;
imma2_avg=t.imma2_avg;
imma3_avg=t.imma3_avg;


InnerBlockSize=20;


ONEMIN_DN=datenum([0 0 0 0 1 0]);
TENSECS_DN=datenum([0 0 0 0 0 10]);




%   1. Generate mat for each file
if bRun
for i =1:nfiles1
    tmpimat=sprintf('%stmp%d.mat',matoutputDir,i);
    filename=alltsi1(i).name;
    tmdv=GetDVFromImg(filename);
    if tmdv==0
        continue;
    end
    if ~exist(tmpimat,'file')
        if ~bMulti
            GetHighCldBlkMask_single(filename,InnerBlockSize,tmpimat,dataDir);
        else
            bBusy=true;
            save(tmpimat,'bBusy');
            %GetHighCldBlkMask_single(tmdv,InnerBlockSize,tmpimat,dataDir);
            cmmd=sprintf('GetHighCldBlkMask_single(''%s'',%d,''%s'',''%s'');',filename,InnerBlockSize,tmpimat,dataDir);
            disp(cmmd);
            subprocess_matlab(cmmd,nMatlabinst,severtype);
        end
    end  
end

end

if bPlot
%   2. Plot each mat
for i =1:nfiles1
    tmpimat=sprintf('%stmp%d.mat',matoutputDir,i);
    if ~exist(tmpimat,'file')
        continue;
    end  
    filename=alltsi1(i).name;
    tmdv1=GetDVFromImg(filename);
    if tmdv1==0
        continue;
    end
    plotf=sprintf('%s%s.png',plotoutDir,filename);
    load(tmpimat);
    try
        [imma1,mf1]=tmp_GetImgMatFromDataDir(tmdv1,TSI1,DATADir);
        [imma2,mf2]=tmp_GetImgMatFromDataDir(tmdv1,TSI2,DATADir);
        [imma3,mf3]=tmp_GetImgMatFromDataDir(tmdv1,TSI3,DATADir);
    catch
        disp(['can not find the file please check' mf1 ' ' mf2 ' ' mf3]);
        continue;
    end
    
    fig1=figure();
    subplot(2,3,2);
    imshow(imma3);
    hold on;
        
    for ci=1:nConn_s
        minsx=minsx3(ci);
        minsy=minsy3(ci);
        recW=maxex3(ci)-minsx3(ci)+1;
        recH=maxey3(ci)-minsy3(ci)+1;
        tmcenx=(maxex3(ci)+minsx3(ci))/2;
        tmceny=(maxey3(ci)+minsy3(ci))/2;
        rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
        
        HMV_x1=minsx1(ci)-minsx;
        HMV_y1=minsy1(ci)-minsy;
        HMV_x2=minsx2(ci)-minsx;
        HMV_y2=minsy2(ci)-minsy;
        quiver(tmcenx,tmceny,HMV_x1,HMV_y1);
        quiver(tmcenx,tmceny,HMV_x2,HMV_y2);
        H=H_conn(ci);
        
        if H>=10000
            %text(tmcenx,tmceny,['\color{red}' '\geq 10000'],'FontSize',18);
        else
            text(tmcenx,tmceny,['\color{red}' sprintf('%d',round(H))],'FontSize',18);
        end
        %text(tmcenx,tmceny,['\color{red}' sprintf('%d',round(H(choosei)))],'FontSize',18);
    end
    hold off;
    
    subplot(2,3,1);
    imshow(imma1);
    hold on;
    for ci =1:nConn_s
        if minsx1(ci)==0
            continue;
        end
        recW=maxex1(ci)-minsx1(ci)+1;
        recH=maxey1(ci)-minsy1(ci)+1;
        rectangle('Position',[minsx1(ci) minsy1(ci) recW recH],'LineStyle','--','EdgeColor','r');
    end
    hold off;
    
    subplot(2,3,3);
    imshow(imma2);
    hold on;
    for ci =1:nConn_s
        if minsx1(ci)==0
            continue;
        end
        recW=maxex2(ci)-minsx2(ci)+1;
        recH=maxey2(ci)-minsy2(ci)+1;
        rectangle('Position',[minsx2(ci) minsy2(ci) recW recH],'LineStyle','--','EdgeColor','r');
    end
    hold off;
    
    
    
    imma3_m=imma3;
    imma2_m=imma2;
    imma1_m=imma1;
    imma3_indma=false(size(imma3,1),size(imma3,2));
    imma2_indma=imma3_indma;
    imma1_indma=imma3_indma;
    for bi=1:nBlocky
        for bj=1:nBlockx
            if ConnMap_s(bi,bj)==0
                continue;
            end
            tmsy=bstarty(bi,bj);
            tmsx=bstartx(bi,bj);
            tmey=bendy(bi,bj);
            tmex=bendx(bi,bj);
            tmsx1=tmsx+HMV31_x(bi,bj);
            tmsy1=tmsy+HMV31_y(bi,bj);
            tmsx2=tmsx+HMV32_x(bi,bj);
            tmsy2=tmsy+HMV32_y(bi,bj);
            tmex1=tmex+HMV31_x(bi,bj);
            tmey1=tmey+HMV31_y(bi,bj);
            tmex2=tmex+HMV32_x(bi,bj);
            tmey2=tmey+HMV32_y(bi,bj);
            
            imma3_indma(tmsy:tmey,tmsx:tmex)=true;
            imma2_indma(tmsy2:tmey2,tmsx2:tmex2)=true;
            imma1_indma(tmsy1:tmey1,tmsx1:tmex1)=true;
            
        end
    end
    
    
    subplot(2,3,5);
    imma3_m(TO_3DMaskfrom2D(imma3_indma,2))=0;
    imma3_m(TO_3DMaskfrom2D(imma3_indma,3))=0;
    imshow(imma3_m);
    subplot(2,3,4);
    imma1_m(TO_3DMaskfrom2D(imma1_indma,2))=0;
    imma1_m(TO_3DMaskfrom2D(imma1_indma,3))=0;
    imshow(imma1_m);
    subplot(2,3,6);
    imma2_m(TO_3DMaskfrom2D(imma2_indma,2))=0;
    imma2_m(TO_3DMaskfrom2D(imma2_indma,3))=0;
    imshow(imma2_m);
    
    
    
    print(fig1,plotf,'-dpng');
end

end




end
