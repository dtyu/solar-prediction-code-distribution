addpath(genpath('~/repos/lib'));
mtsi_startup;

InnerBlockSize=10;
BlockSize=50;
SearchWinSize=50;
CorThreshold=0.7;
BlackThreshold=0;
MaximumMovement=15;
CldPixelCountRatio=0.5;
BlkPixelCountRatio=0.4;
ContinousMVDistThres=3;

SearchWinSize_2r=30;
CorThreshold_2r=0.7;
BlackThreshold_2r=0;
MaximumMovement_2r=15;

MotionVectorAggThres=2;
HistThres=8;
