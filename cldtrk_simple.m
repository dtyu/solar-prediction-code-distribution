function [H_simple,bStatus] = cldtrk_simple(realtimedv,configfpath,logger)
run(configfpath);
loggerID=TO_getloggerID();

HSimRefDir=CldLayerRefSimDir;
HSimRefDir=FormatDirName(HSimRefDir);
if ~exist(HSimRefDir,'dir')
    mkdir(HSimRefDir);
end
H_simple=-1;
%MV_simple=[-1 -1];  % [mvx mvy]
bStatus=1;  % 1 = normal, -1 = less than SimpleCldTrkNCCThres
%% PART I
%   Simple cloud tracking using HEstimate to find best match H on 3 consecutive frames
realtimedn=datenum(realtimedv);
tmdn_10 = realtimedn- TENSECS_DN;
tmdn_20 = realtimedn- 2*TENSECS_DN;
tmdv_10=datevec(tmdn_10);
tmdv_20=datevec(tmdn_20);
f3 = GetImgFromDV_BNL(realtimedv,TSI3);
f2 = GetImgFromDV_BNL(tmdv_10,TSI3);
f1 = GetImgFromDV_BNL(tmdv_20,TSI3);
outputf3=sprintf('%s%s.mat',HSimRefDir,f3(1:end-4));
outputf2=sprintf('%s%s.mat',HSimRefDir,f2(1:end-4));
outputf1=sprintf('%s%s.mat',HSimRefDir,f1(1:end-4));
if ~exist(outputf3,'file')
    HEstimate(realtimedv,outputf3,configfpath);
end
if ~exist(outputf2,'file')
    HEstimate(tmdv_10,outputf2,configfpath);
end
if ~exist(outputf1,'file')
    HEstimate(tmdv_20,outputf1,configfpath);
end


% Combination algorithm
t1 = load(outputf3);
t2 = load(outputf2);
t3 = load(outputf1);
nccsum=t1.nccvals+t2.nccvals+t3.nccvals;
[maxnccval,maxi] = max(nccsum);

t=load(HeightMatrixValidMatf,'H_valid_32');
hma32_valid=t.H_valid_32;
HSearchRange=unique(hma32_valid);   % Total H search range
H_simple = HSearchRange(maxi);
% [tmHMV_x,tmHMV_y]=TO_H2HMV(HSearchRange,TSI3,configfpath);
% HMV_x31_simple=tmHMV_x(maxi,1);HMV_x32_simple=tmHMV_x(maxi,2);
% HMV_y31_simple=tmHMV_y(maxi,1);HMV_y32_simple=tmHMV_y(maxi,2);

if maxnccval/3<SimpleCldTrkNCCThres
    logger.debug(loggerID,sprintf('Simple tracking failed due to average nccval = %f < %f, status is set to -1',maxnccval/3,SimpleCldTrkNCCThres));
    bStatus=-1;
    return;
end

%%% PART II
% %   Fill the image using H_simple. Track the motion vectors based on 3 consecutive TSI3 images
% imma11=TO_imread(tmdv_20,utsi1Dir,TSI1);
% imma12=TO_imread(tmdv_10,utsi1Dir,TSI1);
% imma13=TO_imread(realtimedv,utsi1Dir,TSI1);
% imma21=TO_imread(tmdv_20,utsi2Dir,TSI2);
% imma22=TO_imread(tmdv_10,utsi2Dir,TSI2);
% imma23=TO_imread(realtimedv,utsi2Dir,TSI2);
% imma31=TO_imread(tmdv_20,utsi3Dir,TSI3);
% imma32=TO_imread(tmdv_10,utsi3Dir,TSI3);
% imma33=TO_imread(realtimedv,utsi3Dir,TSI3);
% 
% imma31_lum=TO_rgb2lum(imma31);
% imma32_lum=TO_rgb2lum(imma32);
% imma33_lum=TO_rgb2lum(imma33);
% 
% % image filling at 20s ahead
% imma31_fill=single(imma31);
% imma31_fill_indma=imma31_lum==0;
% imma11_s=TO_imshift(imma11,[-HMV_y31_simple -HMV_x31_simple]);
% imma21_s=TO_imshift(imma21,[-HMV_y32_simple -HMV_x32_simple]);
% tmc1_ma=TO_rgb2lum(imma11_s)==0;
% tmc2_ma=TO_rgb2lum(imma21_s)==0;
% tmc1_ma_3d=logical(TO_3DMaskfrom2D(tmc1_ma));
% tmc2_ma_3d=logical(TO_3DMaskfrom2D(tmc2_ma));
% imma11_s(tmc1_ma_3d)=imma21_s(tmc1_ma_3d);
% imma21_s(tmc2_ma_3d)=imma11_s(tmc2_ma_3d);
% imma31_fill_indma_3d=logical(TO_3DMaskfrom2D(imma31_fill_indma));
% imma31_fill(imma31_fill_indma_3d)=(single(imma11_s(imma31_fill_indma_3d))+single(imma21_s(imma31_fill_indma_3d)))./2;
% imma31_fill=uint8(imma31_fill);
% 
% % image filling at 10s ahead
% imma32_fill=single(imma32);
% imma32_fill_indma=imma32_lum==0;
% imma12_s=TO_imshift(imma12,[-HMV_y31_simple -HMV_x31_simple]);
% imma22_s=TO_imshift(imma22,[-HMV_y32_simple -HMV_x32_simple]);
% tmc1_ma=TO_rgb2lum(imma12_s)==0;
% tmc2_ma=TO_rgb2lum(imma22_s)==0;
% tmc1_ma_3d=logical(TO_3DMaskfrom2D(tmc1_ma));
% tmc2_ma_3d=logical(TO_3DMaskfrom2D(tmc2_ma));
% imma12_s(tmc1_ma_3d)=imma22_s(tmc1_ma_3d);
% imma22_s(tmc2_ma_3d)=imma12_s(tmc2_ma_3d);
% imma32_fill_indma_3d=logical(TO_3DMaskfrom2D(imma32_fill_indma));
% imma32_fill(imma32_fill_indma_3d)=(single(imma12_s(imma32_fill_indma_3d))+single(imma22_s(imma32_fill_indma_3d)))./2;
% imma32_fill=uint8(imma32_fill);
% 
% % image filling at realtime
% imma33_fill=single(imma33);
% imma33_fill_indma=imma33_lum==0;
% imma13_s=TO_imshift(imma13,[-HMV_y31_simple -HMV_x31_simple]);
% imma23_s=TO_imshift(imma23,[-HMV_y32_simple -HMV_x32_simple]);
% tmc1_ma=TO_rgb2lum(imma13_s)==0;
% tmc2_ma=TO_rgb2lum(imma23_s)==0;
% tmc1_ma_3d=logical(TO_3DMaskfrom2D(tmc1_ma));
% tmc2_ma_3d=logical(TO_3DMaskfrom2D(tmc2_ma));
% imma13_s(tmc1_ma_3d)=imma23_s(tmc1_ma_3d);
% imma23_s(tmc2_ma_3d)=imma13_s(tmc2_ma_3d);
% imma33_fill_indma_3d=logical(TO_3DMaskfrom2D(imma33_fill_indma));
% imma33_fill(imma33_fill_indma_3d)=(single(imma13_s(imma33_fill_indma_3d))+single(imma23_s(imma33_fill_indma_3d)))./2;
% imma33_fill=uint8(imma33_fill);
% 
% 
% % MVEstimate








end