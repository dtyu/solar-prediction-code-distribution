% function cloudfieldext is designed to generate cloud fields which are used for NCC similarity
% check later.
%   VERSION 1.0     2013/10/04
function [CFlabelma,ncf]=cloudfieldext(cldmask,configfpath)

run(configfpath);
%mtsi_startup;
% ConnMinSize=10;
% ConnMaxSize=80;
% MaxConnNum=30;


%   1. First round of connection field detection
[ConnLabelma1,nConn1]=bwlabel(cldmask);

%   2. Second round of connection splitting
[ConnLabelma,nConn]=bwlabel_adjust(ConnLabelma1,ConnMinSize,ConnMaxSize);
tmpConnMaxSize=ConnMaxSize;
prenConn=nConn;
while nConn>MaxConnNum
   tmpConnMaxSize=tmpConnMaxSize+10;
   [ConnLabelma,nConn]=bwlabel_adjust(ConnLabelma1,ConnMinSize,tmpConnMaxSize);
   if prenConn==nConn
       break; % if CF number doesn't change then treat it as final results
   else
       prenConn=nConn;
   end
end

CFlabelma=ConnLabelma;
ncf=nConn;

end