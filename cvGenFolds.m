function [XTrain,YTrain,XTest,YTest]=cvGenFolds(xtrain,ytrain,nfolds)

assert(size(ytrain,2)==1&&size(xtrain,1)==size(ytrain,1),'Wrong input xtrain and ytrain!');
cvinst=cvpartition(ytrain,'kfold',nfolds);
[trainma,testma]=CVInstDataExt(cvinst);
N=cvinst.N;
NumTestSets=cvinst.NumTestSets; % Total folds 
if(NumTestSets~=nfolds)
    disp('ERROR!');
end
YTrain=cell(NumTestSets,1);
XTrain=cell(NumTestSets,1);
YTest=cell(NumTestSets,1);
XTest=cell(NumTestSets,1);
for i=1:NumTestSets
    tmytrain=ytrain(trainma(:,i));
    tmxtrain=xtrain(trainma(:,i),:);
    tmytest=ytrain(testma(:,i));
    tmxtest=xtrain(testma(:,i),:);
    YTrain{i}=tmytrain;
    XTrain{i}=tmxtrain;
    YTest{i}=tmytest;
    XTest{i}=tmxtest;
end


end