% Author: Hao Huang
% Release version: 3
% Release date: 4/22/11
% New version data: 6/23/11

%% use to detect motion vector for each block and the corresponding searchwindow

function [tempy tempx Cof R]=CorrelationDetectionUsingBlackFiltering4(im1,im2_padded,BlackThreshold)
%% input:
%% im1: block from 1st image
%% im2_padded: search window from 2nd image
%% BlackThreshold: threshold for black area filtering
%% step: only used in previous version of the function 
%% output:
%% tempy: y direction of the final motion vectors
%% tempx: x direction of the final motion vectors
%% Cof: the maximum value of cross correlation
%% R: the whole matrix of cross correlation 

winsize1=size(im1,1); 
winsize2=size(im2_padded,1); 

%% if area with black texture is large, Cof = 0;
m=find(im1(:)<=BlackThreshold);
%n=find(im2_padded(:)<=20);
if ((size(m,1)/(winsize1^2))>0.1) %|| ((size(n,1)/(winsize2^2))>0.2) 
    Cof=0;
    tempy=0;tempx=0;R=0;
else
    %% im1 is filtered by mean
    im1tep=im1;  
    meanIm1=mean(im1tep(im1tep(:)>=BlackThreshold)); 
    im1tep(im1tep<BlackThreshold)=meanIm1;

    %% im2_padded is filtered by mean
    im2_padded_tep=im2_padded;
    meanIm2=mean(im2_padded(im2_padded(:)>=BlackThreshold)); % filtered by mean
    im2_padded_tep(im2_padded_tep<BlackThreshold)=meanIm2;
    im2_padded_tep=double(im2_padded_tep);
    im1tep=double(im1tep);
%     %% get the XC matrix
%     R=normxcorr2_mex(im1tep, im2_padded_tep, 'same');
%     
%     startp=floor(winsize1/2)+1;
%     endp=startp+winsize2-winsize1;
%     R=R(startp:endp,startp:endp);
% 
% %% get the max XC and its position
%     [maxcc,tempy] = max(R); [maxcc,tempx]=max(maxcc); tempy=tempy(tempx);
%     Cof=max(max(R));
    
    
    R=normxcorr2(im1tep, im2_padded_tep);
    [Cof, imax] = max(R(:));
    [ypeak, xpeak] = ind2sub(size(R),imax(1));
    corr_offset = [ (ypeak-size(im1tep,1)) (xpeak-size(im1tep,2)) ];
    tempy=corr_offset(1);tempx=corr_offset(2);
end



