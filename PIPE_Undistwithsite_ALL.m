% PIPE_Undistwithsite_ALL: Main Entrance of the pipeline of preprocessing. It
% needs to ClearSkyLibrary to do shadow band masking and holding arm. It
% needs site information only and then use it to generate
% UndistortionParameters.mat and undist2ori.mat. But the clear sky library,
% azimuth.mat, sbmask folder, hamask folder are still needed. It will call
% PIPE_UndistortionwithCSL.m to do the other parts.
% masking.
%   IN:
%       startv  --   start date vector(2010,10,0,x,x,x)
%       endv    --   end date vector(2010,10,0,x,x,x)
%       DataDirectory   --  Directory where you put input original images
%       undistoutDir   --  Directory name to put output undistortion
%                           images
%       SITEinfo -- site information which is required for mat file
%                   generateion
%           NOW Supported: 'TWP_C1'(default)
%       UndistortionParameters.mat(in)  --  related parameters
%       azimuth.mat(in) --  for clear sky library.IF NOT 'CL' mode
%               tsdn(clear sky timestamp),azs(sun azimuth),zes(sun zenith)
%       sbmaskDir(in)   --  'sbmask/' which contains all sb masks related
%                           to tsdn items
%       hamaskDir(in)   --  'hamask/' which contains all ha masks related
%                           to tsdn items
%   OUT:
%       outputDir/*     --  all the undistorted images
%
%   VERSION 1.0     2012-04-01
function PIPE_Undistwithsite_ALL(startv,endv,DataDirectory,undistoutDir,SITEinfo)
if strcmpi(SITEinfo,'TWP_C1')==0
    disp('Present version only support TWP C1 version. Other clear sky library are still under construction');
else
    GlobalSetting;
    if(strcmpi(SITE,'TWP')~=1||strcmpi(DEVICEN,'C1')~=1)
        disp('TWP C1 information are not included in undistorted parameters!');
    end
end

%   GlobalSettings will automatically generate these two mat file
%   If some pre-set parameters need to be changed, for instance the
%   Viewangle of undistorted image, you need to change GlobalSetting.m
load('UndistortionParameters.mat');     % load ALL the related parameters
load('undist2ori.mat');             % matrix that contains all undist->ori mapping

%   Load CLear sky library time stamps and sun angles
load('azimuth.mat');                % all timenumbers and azumith

%strcmpi(PRO_MODE,'REMOVE')==1
%   Set start time vector and end time vector
starttimevec=startv;
starttimevec(4)=0;
starttimevec(5)=0;
starttimevec(6)=0;
endtimevec=endv;
endtimevec(4)=0;
endtimevec(5)=0;
endtimevec(6)=0;

%   Basic parameters and prerequisites checking
hamaskDir='hamask/';
sbmaskDir='sbmask/';
disp(SITE);
if(exist(DataDirectory,'dir')==0)
    disp('input data file does not exist!');
end
if(exist(sbmaskDir,'dir')==0)
    disp('WARNING: sbmask does not exist');
end
if(exist(hamaskDir,'dir')==0)
    disp('WARNING: hamask does not exist');
    if(exist('azimuth.mat','file')==0)
        disp('WARNING: azimuth does not exist');
    end
    if(exist('UndistortionParameters.mat','file')==0)
        disp('ERROR: UndistortionParameters.mat doesnot exist!');
    end
    if(exist('undist2ori.mat','file')==0)
        disp('ERROR: undist2ori.mat doesnot exist!');
    end
    if(DataDirectory(end)~='/')
        DataDirectory=sprintf('%s%s',DataDirectory,'/');
    end
    
    holdma=zeros(Hi,Wi);
    shadowma=zeros(Hi,Wi);
    totalmaskma=zeros(Hi,Wi);
    totalmaskma=uint8(totalmaskma);
    unimg=zeros(Hu,Wu,3);
    unimg=uint8(unimg);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    tmstep=datevec(0,0,0,0,0,30);       %tmstep -- time step of consecutive images.
    %which is 30s by default Here can be changed to 1s for BNL
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    nfiles=0;
    tmvec=starttimevec;
    while(tmvec~=endtimevec)
        nfiles=nfiles+1;
        tmvec=tmvec+tmstep;
    end
    tmvec=starttimevec;
    while(tmvec~=endtimevec)
        filename=GetImgfromDV(tmvec);
        inputfilep=sprintf('%s%s',DataDirectory,filename);
        oriimg=imread(inputfilep);
        time.year    =  tmvec(1);
        time.month   =  tmvec(2);
        time.day     =  tmvec(3);
        time.hour    =  tmvec(4);
        time.min    =   tmvec(5);
        time.sec    =   tmvec(6);
        time.UTC    =   0;
        sun = sun_position(time, location);
        az=sun.azimuth/180*pi;
        ze=sun.zenith/180*pi;
        indexi=GetAZIndex(azs,zes,az,ze,AZThres);
        tmdv=datevec(tsdn(indexi));
        %         ii=indexi+2;    %   indexi=i+2 to avoid directory
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % for TWP C1 format only. SGP, BNL may needs different img format
        maskfilename=GetImgFromDV_TWPC1(tmdv);
        maskfilename=sprintf('%s%s',maskfilename,'.mat');
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        hamaskf=sprintf('%s%s',hamaskDir,maskfilename);
        sbmaskf=sprintf('%s%s',sbmaskDir,maskfilename);
        s=load(hamaskf, '-mat', 'hama');
        holdma=s.hama;  %   tmma is the final name of holding arm
        s=load(sbmaskf, '-mat', 'sbma');
        shadowma=s.sbma;
        totalmaskma(:,:)=holdma(:,:)&shadowma(:,:);
        for j=1:3
            oriimg(:,:,j)=oriimg(:,:,j).*totalmaskma;
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Undistortion Mapping here
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        for Xu=1:Wu
            for Yu=1:Hu
                %                 if(Xu==240&&Yu==240)
                %                     Xu
                %                 end
                yo=round(orim(Yu,Xu,1));
                xo=round(orim(Yu,Xu,2));
                if(xo==0&&yo==0)
                    continue;
                    %                     unimg(Yu,Xu,1)=0;%black
                    %                     unimg(Yu,Xu,2)=0;
                    %                     unimg(Yu,Xu,3)=0;
                else
                    unimg(Yu,Xu,1)=oriimg(yo,xo,1);
                    unimg(Yu,Xu,2)=oriimg(yo,xo,2);
                    unimg(Yu,Xu,3)=oriimg(yo,xo,3);
                end
            end
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Output images. BMP files are generated here
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        outp=sprintf('%s%s.bmp',undistoutDir,filename(1:end-4));
        imwrite(unimg,outp,'bmp');
        
    end
end


end