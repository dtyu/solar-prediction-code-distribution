%   This function is a module of cloud tracking algorithm
%   INPUT:
%       HRef        --- 2x1 Int, [HRef(1) HRef(2)] reference layer
%       MVRef       --- 2x2 Int, [mv_y1 mv_x1;mv_y2 mv_x2] reference layer
%       CFMaskMa    --- [UNDISTH,UNDISTW] Int, CF lables matrix
%       tmdv        --- Datevector, current frame
%       dataDir     --- String, directory of TSIs
%       outmatf     --- String, output mat file, given as input
%   VERSION 1.0     2013/10/07
function CldTrack(HRef,MVRef,CFMaskMa,tmdv,dataDir,outmatf,configfpath)
% if exist(outmatf,'file')
%     %   If exist this output file, then exist
%     return;
% end
%mtsi_startup_mini;
if isempty(find(HRef~=0,1))
    bFullSearch=true;
else
    bFullSearch=false;
    if HRef(2)~=0
        assert(HRef(1)<HRef(2),'The HRef should be ascendent!');
    end
end


run(configfpath);
%ncc_thres=0.6;
%TENSECS_DN=datenum([0 0 0 0 0 10]);

%H_sran_low=1000:100:5000;
%H_sran_high=5500:500:20000;
%H_sran_simple=[2000:500:5000 5500:1000:20000];
%H_sran=[H_sran_low H_sran_high];
%NextFrameMVSearchWinSize=15;
%CLDFRACTHRES=0.3

tmdn=datenum(tmdv);
tmdns=[tmdn tmdn+TENSECS_DN];


tmvals=CFMaskMa(:);
nCF=max(tmvals);  % from 1 ~ maxval
nConn=nCF;
ConnLabelma=CFMaskMa;




MV_x=zeros(nConn,1); MV_y=zeros(nConn,1);
HMV_x=zeros(nConn,2); HMV_y=zeros(nConn,2);
H=zeros(nConn,1);     MAXCC=zeros(nConn,1);
Conn_H=zeros(nConn,1); Conn_W=zeros(nConn,1);
Conn_sx=zeros(nConn,1);Conn_sy=zeros(nConn,1);
Conn_cldcount=zeros(nConn,1);
if bFullSearch==false
    if HRef(1)~=0
        HRan1=TO_GenHSearchRange(HRef(1));
    else
        HRan1=[];
    end
    if HRef(2)~=0
        HRan2=TO_GenHSearchRange(HRef(2));
    else
        HRan2=[];
    end
        
    Hsran_quick=[HRan1 HRan2];
    Hsran_quick=unique(Hsran_quick);    % make sure there is no collapse
    %Hsran_quick=Hsran_quick(Hsran_quick>=1000);
else
    Hsran_quick=H_sran;
end

for tmi=1:nCF
%     if tmi~=4
%         continue;
%     end
    disp(tmi);
    tmind=find(ConnLabelma==tmi);
    Conn_cldcount(tmi)=length(tmind);
    [y,x]=ind2sub([UNDISTH,UNDISTW],tmind);
    minsy=min(y);
    minsx=min(x);
    recH=max(y)-min(y)+1;
    recW=max(x)-min(x)+1;
    Conn_sx(tmi)=minsx;Conn_sy(tmi)=minsy;
    Conn_H(tmi)=recH;Conn_W(tmi)=recW;
    tmblkmask=false(UNDISTH,UNDISTW);
    tmblkmask(minsy:minsy+recH-1,minsx:minsx+recW-1)=true;
    [tmH,MV,HMV,cc_all,maxcc]=nccsim(...
        tmdns,[minsy minsx],[recH recW],NextFrameMVSearchWinSize,...
        dataDir,Hsran_quick,ncc_thres,tmblkmask,CLDFRACTHRES,configfpath);
    if bFullSearch==false && maxcc==0
        [tmH,MV,HMV,cc_all,maxcc]=nccsim(...
            tmdns,[minsy minsx],[recH recW],NextFrameMVSearchWinSize,...
            dataDir,H_sran,ncc_thres,tmblkmask,CLDFRACTHRES,configfpath);
    end
    H(tmi)=tmH;
    MV_y(tmi)=MV(1);    MV_x(tmi)=MV(2);
    HMV_y(tmi,1)=HMV(1,1); HMV_x(tmi,1)=HMV(1,2); 
    HMV_y(tmi,2)=HMV(2,1); HMV_x(tmi,2)=HMV(2,2); 
    MAXCC(tmi)=maxcc;
end

save(outmatf,'H','MV_y','MV_x','HMV_x','HMV_y','MAXCC','CFMaskMa','nCF','Conn_sx','Conn_sy','Conn_H','Conn_W');

end