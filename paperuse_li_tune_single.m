function paperuse_li_tune_single(inmat,outmat)
load(inmat);
tmmae=zeros(nSub,1);
tmrmse=zeros(nSub,1);
for si=1:nSub
    tmparaset=poly_paraperm(si,:);
    c=tmparaset(1);
    ep=tmparaset(2);
    modelfile=sprintf('%sli_%d_%f_%f.mat',modelsDir,c,ep);
    svmoptions_li=sprintf('%s -c %d -p %f',opt1,2^c,ep);
    MAE_li=zeros(nfolds,1);
    RMSE_li=zeros(nfolds,1);
    for fi=1:nfolds
        model_li=svmtrain(ytrain{fi},xtrain{fi},svmoptions_li);
        [predicted_label, accuracy, y1]=svmpredict(ytrain_norm,xtrain_norm,model_li);
        y1(y1>1)=1; y1(y1<0)=0;
        MAE_li(fi)=mean(abs(y1-ytrain_norm));
        RMSE_li(fi)=sqrt(mean((y1-ytrain_norm).^2));
    end
    %MAE_ref=mean(abs(pre_y-fit_y));
    %RMSE_ref=sqrt(mean((pre_y-fit_y).^2));
    %if mean(MAE_li(:))<MAE_ref && mean(RMSE_li(:))<RMSE_ref
    tmmae(si)=mean(MAE_li(:));
    tmrmse(si)=mean(RMSE_li(:));
end

%tmmae,tmrmse
delete(inmat);
save(outmat,'tmmae','tmrmse','poly_paraperm');
end