function test_motiontest
startup;

set(0,'DefaultFigureVisible', 'on');
%set(0,'DefaultFigureVisible', 'off');

%tsiDirs={'./tsi1_undist/','./tsi2_undist/','./tsi3_undist/'};
% Cloudy and multi-layer test
tsiDirs={'./cloudy_multilayer/tsi1_undist_20130605/','./cloudy_multilayer/tsi2_undist_20130605/','./cloudy_multilayer/tsi3_undist_20130605/'};
otsiDirs={'./otsi1/','./otsi2/','./otsi3/'};
nTSI=3;
tsi1Dir=tsiDirs{1};
tsi2Dir=tsiDirs{2};
tsi3Dir=tsiDirs{3};
otsi1Dir=otsiDirs{1};
otsi2Dir=otsiDirs{2};
otsi3Dir=otsiDirs{3};
mkdir(otsi1Dir);
mkdir(otsi2Dir);
mkdir(otsi3Dir);
alltsi1=dir(tsi1Dir);
[nfiles1,~]=size(alltsi1);
imma1_total=zeros(UNDISTH,UNDISTW,3);
imma2_total=zeros(UNDISTH,UNDISTW,3);
imma3_total=zeros(UNDISTH,UNDISTW,3);
N1=zeros(UNDISTH,UNDISTW);
N2=zeros(UNDISTH,UNDISTW);
N3=zeros(UNDISTH,UNDISTW);

imma_avgf='./imma_avgf.mat';
t=load(imma_avgf,'imma1_avg','imma2_avg','imma3_avg');
imma1_avg=t.imma1_avg;
imma2_avg=t.imma2_avg;
imma3_avg=t.imma3_avg;

OutputPath='./tmptestme.mat';
InnerBlockSize=10;
BlockSize=50;
SearchWinSize=50;
CorThreshold=0.5;
BlackThreshold=0;

MEOutDir='./ME_test_img';
FillOutDir='./Fillimg';
MEOutDir=FormatDirName(MEOutDir);
FillOutDir=FormatDirName(FillOutDir);
if ~exist(MEOutDir,'dir')
    mkdir(MEOutDir);
end
if ~exist(FillOutDir,'dir')
    mkdir(FillOutDir);
end
ONEMIN_DN=datenum([0 0 0 0 1 0]);


for i =1:nfiles1
    filename=alltsi1(i).name;
    f1=sprintf('%s%s',tsi1Dir,filename);
    tmdv=GetDVFromImg(filename);
    if tmdv==0
        continue;
    end
    tmdn=datenum(tmdv);
    if tmdn<datenum([2013 06 05 14 42 50])
        continue;
    end
    tmdn_1mbf=tmdn-ONEMIN_DN;
    f2_1mbf=GetImgFromDV_BNL(datevec(tmdn_1mbf),TSI2);
    f3_1mbf=GetImgFromDV_BNL(datevec(tmdn_1mbf),TSI3);
    filename2=GetImgFromDV_BNL(tmdv,TSI2);
    filename3=GetImgFromDV_BNL(tmdv,TSI3);
%     f2=sprintf('%s%s.bmp',tsi2Dir,filename2(1:end-4));
%     f3=sprintf('%s%s.bmp',tsi3Dir,filename3(1:end-4));
    f2=sprintf('%s%s.jpg',tsi2Dir,filename2(1:end-4));
    f2_1mbf=sprintf('%s%s.jpg',tsi2Dir,f2_1mbf(1:end-4));
    f3=sprintf('%s%s.jpg',tsi3Dir,filename3(1:end-4));
    f3_1mbf=sprintf('%s%s.jpg',tsi3Dir,f3_1mbf(1:end-4));
    try
        imma1=imread(f1);
        imma2=imread(f2);
        imma3=imread(f3);
    catch
        continue;
    end
    imma2_1mbf=imread(f2_1mbf);
    imma3_1mbf=imread(f3_1mbf);
    imma2_1mbf_grey=rgb2gray(imma2_1mbf);
    imma3_1mbf_grey=rgb2gray(imma3_1mbf);
    imma2_grey=rgb2gray(imma2);
    imma3_grey=rgb2gray(imma3);
    invalidmaskma=(imma3(:,:,1)<15&imma3(:,:,2)<15&imma3(:,:,3)<15)|...
        (imma3_1mbf(:,:,1)<15&imma3_1mbf(:,:,2)<15|imma3_1mbf(:,:,1)<15);
    imma3_1mbf_grey(invalidmaskma)=0;
    imma3_grey(invalidmaskma)=0;
    tmma=TO_NormOfMatrix(abs(single(imma3_1mbf_grey)-single(imma3_grey)));
    invalidmaskma=invalidmaskma|(tmma==1);
    imma3_1mbf_grey(invalidmaskma)=0;
    imma3_grey(invalidmaskma)=0;
    tmma=TO_NormOfMatrix(abs(single(imma3_1mbf_grey)-single(imma3_grey)));

    %tmma(invalidmaskma)=0;
    %figure();imshow(tmma);
    cldind3=tmma>0.1;
    
    [ma1,cldind1]=test_GenCldMask_2r(imma1,imma1_avg);
    %[ma2,cldind2]=test_GenCldMask_2r(imma2,imma2_avg);
    %[ma3,cldind3]=test_GenCldMask_2r(imma3,imma3_avg);
    
    %   1. Block Dividing
    [bstarty,bstartx,bendy,bendx,nBlocky,nBlockx,blockvals]=DivideBlocks(...
        cldind3,[InnerBlockSize InnerBlockSize]);
    imma2_grey=rgb2gray(imma2);
    imma3_grey=rgb2gray(imma3);
    Cofma_low=zeros(nBlocky,nBlockx);
    Cofma_high=zeros(nBlocky,nBlockx);
    Hma_low=zeros(nBlocky,nBlockx);
    Hma_high=zeros(nBlocky,nBlockx);
    bstartx3=zeros(nBlocky,nBlockx);
    bstarty3=zeros(nBlocky,nBlockx);
    H_ran=500:100:5000;
    MultiHThres=2500;
    MultiHThresi=find(H_ran==MultiHThres,1);
%     trustma=test_blktrustveri(bstarty,bstartx,bendy,bendx,nBlocky,nBlockx,imma2,imma3,'','');
%     
%     figure();
%     subplot(1,2,1);
%     imshow(imma2);
%     hold on;
%     
%     for byi=1:nBlocky
%         for bxi=1:nBlockx
%             if 0==trustma(byi,bxi)
%                 continue;
%             end
%             tmbstarty=bstarty(byi,bxi);
%             tmbstartx=bstartx(byi,bxi);
%             tmbendy=bendy(byi,bxi);
%             tmbendx=bendx(byi,bxi);
%             %tmblkvals=imma2_grey(tmbstarty:tmbendy,tmbstartx:tmbendx);
%             corrstarty=tmbstarty-InnerBlockSize;
%             corrstartx=tmbstartx-InnerBlockSize;
%             correndy=tmbendy+InnerBlockSize;
%             correndx=tmbendx+InnerBlockSize;
%             tmblkvals=imma2_grey(corrstarty:correndy,corrstartx:correndx);
%             if isempty(find(tmblkvals>0,1))
%                 continue;
%             end
%             tmcenx=(tmbstartx+tmbendx)/2;
%             tmceny=(tmbstarty+tmbendy)/2;
%             l=sqrt((tmcenx-250)^2+(tmceny-250)^2);
%             if l>240
%                 continue;
%             end
%             [Cof H_ran mv_xs mv_ys]=NCC_direction(tmblkvals,imma3_grey,[tmbstarty tmbstartx],TSI2,TSI3);
%             [maxval,maxi]=max(Cof);
%             if isnan(maxval)    continue;   end
%             if maxval<0.7
%                 continue;   % case 1, not stable
%             else
%                 tmval1=max(Cof(1:MultiHThresi));
%                 tmval2=max(Cof(MultiHThresi+1:end));
%                 if tmval1>=0.7 && tmval2<0.7
%                     % lower cloud
% %                     Cofma(byi,bxi)=maxval;
% %                     Hma(byi,bxi)=H_ran(maxi);
% %                     H=H_ran(maxi);
% %                     mv_x=mv_xs(maxi);
% %                     mv_y=mv_ys(maxi);
% %                     tempx=tmbstartx+mv_x;
% %                     tempy=tmbstarty+mv_y;
% %                     bstartx3(byi,bxi)=tempx;
% %                     bstarty3(byi,bxi)=tempy;
% %                     tmcenx=(tmbstartx+tmbendx)/2;
% %                     tmceny=(tmbstarty+tmbendy)/2;
% %                     rectangle('Position',[tmbstartx tmbstarty InnerBlockSize InnerBlockSize],'LineStyle','--','EdgeColor','r');
% %                     quiver(tmcenx,tmceny,mv_x,mv_y);
%                 elseif tmval2>=0.7 && tmval1<0.7
%                     % higher cloud
%                     Cofma(byi,bxi)=maxval;
%                     Hma(byi,bxi)=H_ran(maxi);
%                     H=H_ran(maxi);
%                     mv_x=mv_xs(maxi);
%                     mv_y=mv_ys(maxi);
%                     tempx=tmbstartx+mv_x;
%                     tempy=tmbstarty+mv_y;
%                     bstartx3(byi,bxi)=tempx;
%                     bstarty3(byi,bxi)=tempy;
%                     tmcenx=(tmbstartx+tmbendx)/2;
%                     tmceny=(tmbstarty+tmbendy)/2;
%                     rectangle('Position',[tmbstartx tmbstarty InnerBlockSize InnerBlockSize],'LineStyle','--','EdgeColor','r');
%                     quiver(tmcenx,tmceny,mv_x,mv_y);
%                 end
% %                 Cofma(byi,bxi)=maxval;
% %                 Hma(byi,bxi)=H_ran(maxi);
% %                 H=H_ran(maxi);
% %                 mv_x=mv_xs(maxi);
% %                 mv_y=mv_ys(maxi);
% %                 tempx=tmbstartx+mv_x;
% %                 tempy=tmbstarty+mv_y;
% %                 bstartx3(byi,bxi)=tempx;
% %                 bstarty3(byi,bxi)=tempy;
% %                 tmcenx=(tmbstartx+tmbendx)/2;
% %                 tmceny=(tmbstarty+tmbendy)/2;
% %                 rectangle('Position',[tmbstartx tmbstarty InnerBlockSize InnerBlockSize],'LineStyle','--','EdgeColor','r');
% %                 quiver(tmcenx,tmceny,mv_x,mv_y,0);
% %                 %text(tmcenx,tmceny,['\color{red}' sprintf('%d',round(H))],'FontSize',18);
%             end
%         end
%     end
%     hold off;
%     
%     subplot(1,2,2);
%     imshow(imma3);
%     hold on;
%     for byi=1:nBlocky
%         for bxi=1:nBlockx
%             if 0==bstartx3(byi,bxi)
%                 continue;
%             end
%             tmbstarty=bstarty3(byi,bxi);
%             tmbstartx=bstartx3(byi,bxi);
%             rectangle('Position',[tmbstartx tmbstarty InnerBlockSize InnerBlockSize],'LineStyle','--','EdgeColor','r');
%         end
%     end
%     hold off;
    
    %   2. Block classfication(1 = cloud ,0 = sky)
    blkclass=zeros(nBlocky,nBlockx);
    for byi=1:nBlocky
        for bxi=1:nBlockx
            if isempty(find(blockvals(byi,bxi,:,:)==1,1))
                blkclass(byi,bxi)=0;
            else
                tml=length(find(blockvals(byi,bxi,:,:)==1));
                if tml/100>0.5
                    blkclass(byi,bxi)=1;
                else
                    blkclass(byi,bxi)=0;
                end
            end
        end
    end
    
    %   3. Get Connection Area
%     for byi=1:nBlocky
%         for bxi=1:nBlockx
%             
%         end
%     end
    [ConnMap,nConn]=bwlabeln(blkclass);
%     for ci=1:nConn
%         ci_len=length(find(ConnMap==ci));
%         if ci_len>25
%             test_BigBlk2SmallBlk(ConnMap,ci,nConn);
%         end
%     end
        
    corrblks=cell(nConn,1);
    minsy3=zeros(nConn,1);
    minsx3=zeros(nConn,1);
    maxey3=zeros(nConn,1);
    maxex3=zeros(nConn,1);
    fig1=figure();
    subplot(1,2,1);
    imshow(imma2);
    hold on;
    greyimma3=double(rgb2gray(imma3));
    for ci =1:nConn
        tmind=find(ConnMap==ci);
        tmsy=bstarty(tmind);
        tmsx=bstartx(tmind);
        tmey=bendy(tmind);
        tmex=bendx(tmind);
        minsy=min(tmsy);
        minsx=min(tmsx);
        maxey=max(tmey);
        maxex=max(tmex);
        recH=maxey-minsy+1;
        recW=maxex-minsx+1;
        tmcorrblk=imma2(minsy:maxey,minsx:maxex,:);
        if length(tmcorrblk)<=20
            corrblks{ci}=0;
            continue;
        end
        if 1
            rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
            corrblks{ci}=tmcorrblk;
            %imshow(tmcorrblk);
            %tmcorrblk(tmcorrblk==0)=105;
            blkrbr=GetRBR(tmcorrblk);
            blkrbr(blkrbr==inf)=10;
            blkrbr(isnan(blkrbr))=0.2;
            ma3rbr=GetRBR(imma3);
            ma3rbr(isnan(ma3rbr))=0.2;
            tmceny=round((maxey+minsy)/2);
            tmcenx=round((maxex+minsx)/2);
            
            
            [R_h,h_ran,mv_x_d,mv_y_d]=NCC_direction(rgb2gray(tmcorrblk),rgb2gray(imma3),[minsy minsx],TSI2,TSI3);
            [maxval,maxi]=max(R_h);
            mv_x=mv_x_d(maxi);
            mv_y=mv_y_d(maxi);
            H=h_ran(maxi);
            tempx=minsx+mv_x;
            tempy=minsy+mv_y;
            minsx3(ci)=tempx;
            minsy3(ci)=tempy;
            maxex3(ci)=maxex+mv_x;
            maxey3(ci)=maxey+mv_y;
            quiver(tmcenx,tmceny,mv_x,mv_y);
            text(tmcenx,tmceny,['\color{red}' sprintf('%d',round(H))],'FontSize',18);
            
%             R=normxcorr2(rgb2gray(tmcorrblk), rgb2gray(imma3));
%             %R=normxcorr2(blkrbr, ma3rbr);
%             greyblk=rgb2gray(tmcorrblk);
%             tmg=double(greyblk);
%             tmg(tmg==0)=nan;
%             %R1=ncc_test(tmg, greyimma3);
%             R_samesize=zeros(size(imma3,1),size(imma3,2));
% %             R_samesize(1:end-1,1:end-1)=R1(size(tmcorrblk,1)+1:size(tmcorrblk,1)+UNDISTH-1,...
% %                 size(tmcorrblk,2)+1:size(tmcorrblk,2)+UNDISTW-1);
%             R_samesize(1:end-1,1:end-1)=R(size(tmcorrblk,1)+1:size(tmcorrblk,1)+UNDISTH-1,...
%                 size(tmcorrblk,2)+1:size(tmcorrblk,2)+UNDISTW-1);
%             [tempx tempy c]=FindMinCorr(R_samesize,[minsy minsx],80);
% %             [Cof, imax] = max(R(:));
% %             [ypeak, xpeak] = ind2sub(size(R),imax(1));
% %             corr_offset = [ (ypeak-size(tmcorrblk,1)) (xpeak-size(tmcorrblk,2)) ];
% %             tempy=corr_offset(1);tempx=corr_offset(2);
% 
%             mv_x=tempx-minsx;
%             mv_y=tempy-minsy;
%             minsx3(ci)=tempx;
%             minsy3(ci)=tempy;
%             maxex3(ci)=maxex+mv_x;
%             maxey3(ci)=maxey+mv_y;
%             [H_x,H_y]=TO_Undist2H(tmcenx,tmceny,tmcenx+mv_x,tmceny+mv_y,TSI2,TSI3);
%             H_cen=round((H_x+H_y)/2);
%             [H_x,H_y]=TO_Undist2H(minsx,minsy,minsx+mv_x,minsy+mv_y,TSI2,TSI3);
%             H_lt=round((H_x+H_y)/2);
%             [H_x,H_y]=TO_Undist2H(maxex,maxey,maxex+mv_x,maxey+mv_y,TSI2,TSI3);
%             H_rb=round((H_x+H_y)/2);
%             if mv_y>10
%                 mv_y
%             end
%             quiver(tmcenx,tmceny,mv_x,mv_y);
%             text(tmcenx,tmceny,['\color{red}' sprintf('%d',H_cen)],'FontSize',18);
        else
            % The connection area is huge and need to be divided
            %[ConnArea_new]=test_DivideBigConnArea2SmallConns(tmcorrblk,50);
        end
    end
    hold off;
    
    subplot(1,2,2);
    imshow(imma3);
    hold on;
    for ci =1:nConn
        if minsx3(ci)==0
            continue;
        end
        recW=maxex3(ci)-minsx3(ci)+1;
        recH=maxey3(ci)-minsy3(ci)+1;
        rectangle('Position',[minsx3(ci) minsy3(ci) recW recH],'LineStyle','--','EdgeColor','r');
    end
    hold off;
    outputf=sprintf('%s%s_ME.png',MEOutDir,datestr(tmdv,'yyyy-mm-dd_HH-MM-SS'));
    print(fig1,outputf,'-dpng');
    
    
    outputf2=sprintf('%s%s_tsi2fill.png',FillOutDir,datestr(tmdv,'yyyy-mm-dd_HH-MM-SS'));
    outputf3=sprintf('%s%s_tsi3fill.png',FillOutDir,datestr(tmdv,'yyyy-mm-dd_HH-MM-SS'));
    H=4900;
    [mv_x,mv_y]=TO_H2MV('./tmhma.mat',H);
%     mv_x=38;
%     mv_y=-59;
    imma2_filled=TO_FillBlack(imma2,imma3,[mv_x,mv_y]);
    fig2=figure();
    subplot(1,2,1);
    imshow(imma2);
    subplot(1,2,2);
    imshow(imma2_filled);
    print(fig2,outputf2,'-dpng');
    
    imma3_filled=TO_FillBlack(imma3,imma2,[-mv_x,-mv_y]);
    fig3=figure();
    subplot(1,2,1);
    imshow(imma3);
    subplot(1,2,2);
    imshow(imma3_filled);
    print(fig3,outputf3,'-dpng');
    
%     %   4. Find displacement of it
%     for ci =1:nConn
%         tmcorrblk=corrblks{ci};
%         if tmcorrblk==0
%             continue;
%         end
%         R=normxcorr2(rgb2gray(tmcorrblk), rgb2gray(imma3));
%         
%         [Cof, imax] = max(R(:));
%         [ypeak, xpeak] = ind2sub(size(R),imax(1));
%         corr_offset = [ (ypeak-size(tmcorrblk,1)) (xpeak-size(tmcorrblk,2)) ];
%         tempy=corr_offset(1);tempx=corr_offset(2);
%     end
    
    
    %break;
%     ME_2f_stichtest(rgb2gray(imma2), rgb2gray(imma3),cldind2,cldind3, ...
%         OutputPath, InnerBlockSize, BlockSize,SearchWinSize,CorThreshold,BlackThreshold)
    
end