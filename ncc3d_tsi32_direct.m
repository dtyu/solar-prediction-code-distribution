% This function is trying to find the best high altitude cloud using
%   It starts from startp on TSI3, gets recSize rectangle area as reference area. It will search the
%   whole area to find the best H using nccsumall
%   Previous version test_ncc3d_v1.m
%       fomula:     similarity = ncc31_21+ncc32_22+ncc_21_22+ncc_31_32
%       
%   INPUT:
%       datevector_obj  --- obj of current and next frame time
%       startp          --- 2x1 double, [starty startx]
%       recSize         --- 2x1 double, [recH recW]
%       TSIDir          --- String, TSI mat Root directory
%       H_range         --- H range for test
%       ncc_thres       --- Minimum ncc for matching
%   OUTPUT:
%       H               --- Most possible height
%       maxcc           --- max ncc value
%       HMV_x           --- Height --> TSI3->TSI1 displacement 
%                           HMV_x=[HMV31_x(ind) HMV32_x(ind)];
%       HMV_y           --- Height --> TSI3->TSI2 displacement 
%                           HMV_y=[HMV31_y(ind) HMV32_y(ind)];
%
%   Version 1.0:        2013-08-28
function [mv_x,mv_y,H,HMV_x,HMV_y,maxcc]=ncc3d_tsi32_direct(imgCellObj,startp,recSize,SearchWinSize_ext,H_range,ncc_thres,tsi3BlkMask)
mv_x=0;mv_y=0;H=0;HMV_x=0;HMV_y=0;maxcc=0;
mtsi_startup;

% Parse imgCellObj
imma31=imgCellObj{1};
imma32=imgCellObj{2};
imma21=imgCellObj{3};
imma22=imgCellObj{4};
recH=recSize(1);
recW=recSize(2);
undistp31=startp;
uy31=undistp31(1);
ux31=undistp31(2);

t=load(HeightMatrixValidMatf);
hma32_valid=t.H_valid_32;
HMV32_x=t.HMVx_valid_32;
HMV32_y=t.HMVy_valid_32;
H_range_full=t.H_range_full;
H_l=length(H_range);
full_valid=zeros(H_l,1);
for tmi=1:H_l
    ind=find(H_range_full==H_range(tmi),1);
    full_valid(tmi)=ind;
end

hma32_valid=hma32_valid(full_valid);
HMV32_x=HMV32_x(full_valid);
HMV32_y=HMV32_y(full_valid);


uy32=uy31-SearchWinSize_ext;
ux32=ux31-SearchWinSize_ext;
%SearchWinSize=SearchWinSize_ext*2+1;
uy31_e=uy31+recH-1;
ux31_e=ux31+recW-1;
uy32_e=uy31_e+SearchWinSize_ext;
ux32_e=ux31_e+SearchWinSize_ext;


% imma21=TO_rgb2lum(tmp_GetImgMatFromDataDir(tmdv,TSI2,TSIDir));
% imma31=TO_rgb2lum(tmp_GetImgMatFromDataDir(tmdv,TSI3,TSIDir));
% imma22=TO_rgb2lum(tmp_GetImgMatFromDataDir(tmdv_next,TSI2,TSIDir));
% imma32=TO_rgb2lum(tmp_GetImgMatFromDataDir(tmdv_next,TSI3,TSIDir));



%   1. Generate NCC result for TSI3 pair
%blk21=imma21(uy21:uy21_e,ux21:ux21_e);
blk31=single(TO_GetImgVal_Pad([uy31 ux31],[recH recW],imma31,SearchWinSize_ext));
blk31_mask=TO_GetImgVal_Pad([uy31 ux31],[recH recW],tsi3BlkMask,20);
blk31(~blk31_mask)=0;  % invalid false=0

blk32_search=single(TO_GetImgVal_Pad([uy32 ux32],[uy32_e-uy32+1 ux32_e-ux32+1],imma32,SearchWinSize_ext));
%blk22_search=imma22(uy22:uy22_e,ux22:ux22_e);
cc=normxcorr2_blk(blk31,blk32_search);

sim_all=zeros(H_l,1);
for i=1:H_l
   tmHMV_x=HMV32_x(i);
   tmHMV_y=HMV32_y(i);
   ux21=round(ux31+tmHMV_x);
   uy21=round(uy31+tmHMV_y);
   blk21=single(TO_GetImgVal_Pad([uy21 ux21],[recH recW],imma21,max(recSize)));
   if std(blk21(:))==0
       continue;
   end
   tmcc=normxcorr2_blk(blk31,blk21);
   sim_all(i)=tmcc(recH,recW);
   if sim_all(i)<=ncc_thres
       sim_all(i)=0;
   end
end
sim_max=max(sim_all(:));
if sim_max==0
    dispstr=sprintf(...
    '[WARNING]: similarity of TSI2 and TSI3 is below the ncc_thres. No more H search is needed. For further search please change ncc_thres = %f to be smaller!'...
        ,ncc_thres);
    disp(dispstr);
    return;
end
[Hcc,Wcc]=size(cc);
cc_all=zeros(Hcc,Wcc,H_l);
for i=1:H_l
    %   2.1 (ux,uy) + H TSI2 ->(ux_new,uy_new) on TSI3
    %H=H_ran(i);
    if sim_all(i)<=sim_max/2 || isnan(sim_all(i)) || sim_all(i)==0
        continue;
    end
    tmHMV_x=HMV32_x(i);
    tmHMV_y=HMV32_y(i);
    ux21=round(ux31+tmHMV_x);
    uy21=round(uy31+tmHMV_y);
    ux21_e=round(ux21+recW-1);
    uy21_e=round(uy21+recH-1);
    uy22=round(uy21-SearchWinSize_ext);
    ux22=round(ux21-SearchWinSize_ext);
    uy22_e=round(uy21_e+SearchWinSize_ext);
    ux22_e=round(ux21_e+SearchWinSize_ext);
    ux21_all(i)=ux21;
    uy21_all(i)=uy21;
    %   2.2 Generate NCC result for TSI3 pair
    if uy21>UNDISTH || ux21>UNDISTW || uy21<1 || ux21<1 ||...
        uy21_e>UNDISTH || ux21_e>UNDISTW || uy21_e<1 || ux21_e<1
        continue;
    end
    %     if uy22>UNDISTH || ux22>UNDISTW || uy22<1 || ux22<1 ||...
    %         uy22_e>UNDISTH || ux22_e>UNDISTW || uy22_e<1 || ux22_e<1
    %         continue;
    %     end
    blk21=single(TO_GetImgVal_Pad([uy21 ux21],[recH recW],imma21,SearchWinSize_ext));
    blk22_search=single(TO_GetImgVal_Pad([uy22 ux22],[uy22_e-uy22+1 ux22_e-ux22+1],imma22,SearchWinSize_ext));
    %blk21=single(imma21(uy21:uy21_e,ux21:ux21_e));
    %blk22_search=single(imma22(uy22:uy22_e,ux22:ux22_e));
    if std(blk21(:))~=0
        cc_2=normxcorr2_blk(blk21,blk22_search);
    else
        cc_2=zeros(Hc,Wc);
    end
    
    %   2.3 Calculate similarity here
    if sim_all(i)>0
        cc_all(:,:,i)=sim_all(i)+(cc+cc_2);
        %cc_all(:,:,i)=cc;
    end
end

[maxcc,ind]=max(cc_all(:));
[tmy,tmx,hind]=ind2sub(size(cc_all),ind);
H=H_range(hind);
HMV_x= HMV32_x(hind);
HMV_y= HMV32_y(hind);
mv_x=tmx-recW-SearchWinSize_ext;
mv_y=tmy-recH-SearchWinSize_ext;    



end