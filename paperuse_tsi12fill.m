%paperuse_tsi12fill

% use H_multi_all3 to fill other images gap
tmHran=unique(H_multi_all3(:));
tmHran=tmHran(tmHran~=0);
%tmHran(1)=1200;
tmHran=sort(tmHran);
tmHl=length(tmHran);
hmvx31=zeros(tmHl,1);
hmvy31=zeros(tmHl,1);
hmvx32=zeros(tmHl,1);
hmvy32=zeros(tmHl,1);
[tmHMV_x,tmHMV_y]=TO_H2HMV(tmHran,TSI3);
hmvx31=tmHMV_x(:,1); hmvx32=tmHMV_x(:,2);
hmvy31=tmHMV_y(:,1); hmvy32=tmHMV_y(:,2);
imma1_fill=imma1;
imma2_fill=imma2;

for tmi=1:tmHl
    fillma1=TO_rgb2lum(imma1_fill)==0;
    fillma2=TO_rgb2lum(imma2_fill)==0;
    fillma1_3d=TO_3DMaskfrom2D(fillma1);
    fillma2_3d=TO_3DMaskfrom2D(fillma2);
    tmh=tmHran(tmi);
    pixelma=(H_multi_all3==tmh);
    tmma3=imma3;
    tmma3(TO_3DMaskfrom2D(~pixelma))=0;
    imma3_s1=TO_imshift(tmma3,[hmvy31(tmi) hmvx31(tmi)]);
    imma3_s2=TO_imshift(tmma3,[hmvy32(tmi) hmvx32(tmi)]);
    imma1_fill(fillma1_3d)=imma3_s1(fillma1_3d);
    imma2_fill(fillma2_3d)=imma3_s2(fillma2_3d);
    %figure();imshow(imma1_fill);
end
skyfillfpath='skyfill.bmp';
skyfillma=imread(skyfillfpath);
fillma1=TO_rgb2lum(imma1_fill)==0;
fillma2=TO_rgb2lum(imma2_fill)==0;
fillma1_3d=TO_3DMaskfrom2D(fillma1);
fillma2_3d=TO_3DMaskfrom2D(fillma2);
imma1_fill(fillma1_3d)=skyfillma(fillma1_3d);
imma2_fill(fillma2_3d)=skyfillma(fillma2_3d);
figure();imshow(imma1_fill);
figure();imshow(imma2_fill);
imwrite(imma1_fill,'tsi1fill.bmp','bmp');
imwrite(imma2_fill,'tsi2fill.bmp','bmp');
imma1_fill;