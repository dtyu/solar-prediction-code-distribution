% Author: Hao Huang
% Release version: 3
% Release date: 4/22/11
% New version data: 6/23/11

%revised 2012/04/08
%% use to detect motion vector on the whole image

function [matchx,matchy,x_onnext,y_onnext,blkcenx,blkceny]=ME_2f_stichtest(...
    M1, M2,M1_cldmask,M2_cldmask, OutputPath,memode)

startup;

if strfind(memode,'debug')
    bshowplot=true;
    bLocateNext=true;
else
    bshowplot=false;
    bLocateNext=true;
end

%% step: the resolution step in the Search Window (1 is the highest)
% s=load(M1path,'-mat');
% M1=double(s.outputm);
% s=load(M2path,'-mat');
% M2=double(s.outputm);

% M1=double(imread(M1path)); % first flame
% M2=double(imread(M2path)); % second flame


% BlackThreshold=40; % elements small than this are considered as totally black
%InnerBlockSize=5;  % Larger BlockSize: size of block in the 1st frame to extract correlation
OutterBlockSize=(BlockSize-InnerBlockSize)/2;
SmallOutterBlockSize=OutterBlockSize/2;
SmallSearchWinSize=SearchWinSize; % Smaller Search Window Size
ExtendCurrent=OutterBlockSize+SearchWinSize;
SmallExtendCurrent=SmallOutterBlockSize+SmallSearchWinSize;

% %% padding
% LeftRight=zeros(size(M1,1),SearchWinSize);
% UpDown=zeros(SearchWinSize,size(M1,2)+SearchWinSize*2);
% M1_ext=[LeftRight,M1,LeftRight];
% M1_ext=[UpDown;M1_ext;UpDown];
% M2_ext=[LeftRight,M2,LeftRight];
% M2_ext=[UpDown;M2_ext;UpDown];
% M1_cld_ext=[LeftRight,M1_cldmask,LeftRight];
% M1_cld_ext=[UpDown;M1_cld_ext;UpDown];
% M2_cld_ext=[LeftRight,M2_cldmask,LeftRight];
% M2_cld_ext=[UpDown;M2_cld_ext;UpDown];
[ori_H,ori_W]=size(M1);
M1_ext=zeros(ori_H+2*SearchWinSize,ori_W+2*SearchWinSize);
M2_ext=zeros(ori_H+2*SearchWinSize,ori_W+2*SearchWinSize);
M1_cld_ext=zeros(ori_H+2*SearchWinSize,ori_W+2*SearchWinSize);
M2_cld_ext=zeros(ori_H+2*SearchWinSize,ori_W+2*SearchWinSize);
M1_ext(SearchWinSize+1:SearchWinSize+ori_H,SearchWinSize+1:SearchWinSize+ori_W)=M1(:,:);
M2_ext(SearchWinSize+1:SearchWinSize+ori_H,SearchWinSize+1:SearchWinSize+ori_W)=M2(:,:);
M1_cld_ext(SearchWinSize+1:SearchWinSize+ori_H,SearchWinSize+1:SearchWinSize+ori_W)=M1_cldmask(:,:);
M2_cld_ext(SearchWinSize+1:SearchWinSize+ori_H,SearchWinSize+1:SearchWinSize+ori_W)=M2_cldmask(:,:);
dimy=size(M1_ext,1);
dimx=size(M1_ext,2);
hdimy=dimy/2;   %height of frame
hdimx=dimx/2;   %width of frame

% area where to pick im1 block
Im1y1=ExtendCurrent+1;
Im1y2=dimy-ExtendCurrent;
Im1x1=ExtendCurrent+1;
Im1x2=dimx-ExtendCurrent;

%% size of motion vector
nRowBlock=floor((Im1y2-Im1y1)/InnerBlockSize);
nColBlock=floor((Im1x2-Im1x1)/InnerBlockSize);
matchy=zeros(nRowBlock,nColBlock);
matchx=zeros(nRowBlock,nColBlock);
y_onnext=zeros(nRowBlock,nColBlock);
x_onnext=zeros(nRowBlock,nColBlock);
blkcenx=zeros(nRowBlock,nColBlock);
blkceny=zeros(nRowBlock,nColBlock);

%% two boundary, 1st for big block and 2nd small block
boundary1=(hdimy-BlockSize-SearchWinSize);
boundary2=(hdimx-BlockSize-0.2*SearchWinSize);
xbounds=Im1x1:InnerBlockSize:(nColBlock-1)*InnerBlockSize;
ybounds=Im1y1:InnerBlockSize:(nRowBlock-1)*InnerBlockSize;

cldpixelsthres=round(CldPixelCountRatio*SearchWinSize*SearchWinSize);
blkpixelsthres=round(BlkPixelCountRatio*SearchWinSize*SearchWinSize);
%% start main loop for motion detection
for loopi=1:nRowBlock
    ybound1=(loopi-1)*InnerBlockSize+Im1y1;
    ybound2=ybound1+InnerBlockSize-1;
    y_c=(ybound1+ybound2)/2-hdimy;
    blkceny(loopi,:)=(ybound1+ybound2)/2;
    ylp1=ybound1-OutterBlockSize;
    ylp2=ybound2+OutterBlockSize;
    ylc1=ybound1-OutterBlockSize-MaximumMovement;
    ylc2=ybound2+OutterBlockSize+MaximumMovement;
    for loopj=1:nColBlock
        % area of the block in im1
        xbound1=(loopj-1)*InnerBlockSize+Im1x1;
        xbound2=xbound1+InnerBlockSize-1;
        x_c=(xbound1+xbound2)/2-hdimx;
        blkcenx(loopi,loopj)=(xbound1+xbound2)/2;
        l=sqrt(y_c^2+x_c^2);   % distance to center point
        if (l>UNDISTR-10)   % Cont.1 Not in range
            matchy(loopi,loopj)=nan;
            matchx(loopi,loopj)=nan;
            continue; 
        end % The search range is in the UNDISTR range(250 pixels)
        xlp1=xbound1-OutterBlockSize;
        xlp2=xbound2+OutterBlockSize;
        xlc1=xbound1-OutterBlockSize-MaximumMovement;
        xlc2=xbound2+OutterBlockSize+MaximumMovement;
        
        cldblk1=M1_cld_ext(ylp1:ylp2,xlp1:xlp2); % larger block
        %if isempty(find(cldblk1==0,1)) continue; end % The search Block must contains cloud in cld mask
        previous=M1_ext(ylp1:ylp2,xlp1:xlp2); % larger block
        if isempty(find(previous~=0,1)) % Cont.2 ALL BLACK or ALL INVALID
            matchy(loopi,loopj)=nan;
            matchx(loopi,loopj)=nan;
            continue; 
        end % The search Block should not be all black
        blkpixelcount=length(find(previous==0));
        if blkpixelcount>=blkpixelsthres % Cont.3 BLACK PIXELS IS LARGER THAN THRESHOLD
            matchy(loopi,loopj)=nan;
            matchx(loopi,loopj)=nan;
            continue; 
        end % The search Block should not be all black
        
        
        %         previous_cld=M1_cld_ext(ylp1:ylp2,xbound1-OutterBlockSize:xbound2+OutterBlockSize); % larger block
        %         if isempty(find(previous_cld==1,1))
        %             continue;
        %         end
        current =M2_ext(ylc1:ylc2,xlc1:xlc2);   % larger search window
        current_nomotion =M2_ext(ylp1:ylp2,xlp1:xlp2);   % larger search window
        R_nomotion=normxcorr2(previous,current_nomotion);
        %if R_nomotion(SearchWinSize+1,SearchWinSize+1)>CorThreshold     %(0,0)->(SearchWinSize+1,SearchWinSize+1)
        cldpixelcount=length(find(cldblk1==1));
        %if cldpixelcount<=cldpixelcount && max(max(R_nomotion))>CorThreshold     %(0,0)->(SearchWinSize+1,SearchWinSize+1)
        if cldpixelcount<=cldpixelsthres && max(max(R_nomotion))>=0.8
            %   trust no motion
            matchy(loopi,loopj)=0;
            matchx(loopi,loopj)=0;
        else
            %[tempy1 tempx1 Cor1 R]=CorrelationDetectionUsingBlackFiltering4(previous,current,BlackThreshold); % correlation detection
            R=normxcorr2(previous, current);
            [Cof, imax] = max(R(:));
            [ypeak, xpeak] = ind2sub(size(R),imax(1));
            corr_offset = [ (ypeak-SearchWinSize-MaximumMovement) (xpeak-SearchWinSize-MaximumMovement)];
            tempy=corr_offset(1);tempx=corr_offset(2);
            %   double check the movement
            smallblk =M1_ext(ybound1:ybound2,xbound1:xbound2);   % original size/small blk
            smallblk_motion =M2_ext(ybound1+tempy:ybound2+tempy,xbound1+tempx:xbound2+tempx);   % moved small blk
            if std(smallblk(:))==0 || std(smallblk_motion(:))==0
                % std = 0 trust bigger blocks
                smallwindowR=1;
            else
                R_motion=normxcorr2(smallblk,smallblk_motion);
                smallwindowR=max(R_motion(:));
            end
            if Cof>CorThreshold && smallwindowR>CorThreshold % only consider correlation large enough
                matchy(loopi,loopj)=tempy;
                matchx(loopi,loopj)=tempx;
            else
                matchy(loopi,loopj)=nan;
                matchx(loopi,loopj)=nan;
            end
        end
        %current_cld =M2_cld_ext(ylc1:ylc2,xbound1-ExtendCurrent:xbound2+ExtendCurrent);   % larger search window
        %tmcldind=logical(current_cld);
        %current(~tmcldind)=0;
        
        if bLocateNext
            if 0==matchy(loopi,loopj) && 0==matchx(loopi,loopj)
                continue;
            end
            if isnan(matchy(loopi,loopj)) || isnan(matchx(loopi,loopj))
                continue;
            end
            xbound1_n=xbound1+matchx(loopi,loopj);
            ybound1_n=ybound1+matchy(loopi,loopj);
            [tmminval,tmindex]=min(abs(xbounds-xbound1_n));
            x_onnext(loopi,loopj)=xbounds(tmindex);
            [tmminval,tmindex]=min(abs(ybounds-ybound1_n));
            y_onnext(loopi,loopj)=ybounds(tmindex);
        end
    end
end


if bshowplot==true
    figure();
    M1_show=M1_ext(SearchWinSize+1:end-SearchWinSize,SearchWinSize+1:end-SearchWinSize);
    M2_show=M2_ext(SearchWinSize+1:end-SearchWinSize,SearchWinSize+1:end-SearchWinSize);
    blkcenx_show=round(blkcenx-SearchWinSize);
    blkceny_show=round(blkceny-SearchWinSize);
    subplot(1,2,1);imshow(uint8(M1_show));hold on;
    quiver(blkcenx_show,blkceny_show,matchx,matchy,0);
    nanindice=find(isnan(matchx));
    tmx=blkcenx_show(nanindice);tmy=blkceny_show(nanindice);
    plot(tmx,tmy,'r+');
    subplot(1,2,2);
    imshow(uint8(M2_show));
end
%drawWinField(matchx1,matchy1);
%[matchx1,matchy1]=VectorsRemoval(matchx1, matchy1, 1, 40);


%% save result
save(OutputPath);

