%testofnorm




load('crash.mat');
cc=0;
for i=1:100
%     cc=normxcorr2_blk(template,A);
%     ccp=normxcorr2_blk_BACKUP(template,A);
    t=round(rand(100)*255);
    a=round(rand(300)*255);
    %a(a==0)=240;
    bT=~isempty(find(t==0,1));
    bA=~isempty(find(a==0,1));
    if bT && bA
        disp('bT && bA');
    end
    if ~bT && bA
        disp('~bT && bA');
    end
    if bT && ~bA
        disp('bT && ~bA');
    end
    if ~bT && ~bA
        disp('~bT && ~bA');
    end
    t1=cputime;
    cc1=normxcorr2_c1(uint8(t),uint8(a));
    e2=cputime-t1;
    t1=cputime;
    %cc=normxcorr2_blk(t,a);
    cc=normxcorr2_c(uint8(t),uint8(a));
    e1=cputime-t1;
    
    disp([e1 e2]);
    diff=abs(cc1-cc);
    ind=find(abs(diff)>0.001,1);
    if ~isempty(ind)
        disp(ind);
    end
%     ccp(isnan(ccp))=0;
%     diff=abs(ccp-cc);
%     ind=find(abs(diff)>0.01,1);
%     if ~isempty(ind)
%         disp(ind(1));
%     end
    
end

