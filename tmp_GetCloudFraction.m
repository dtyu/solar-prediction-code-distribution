function CF=tmp_GetCloudFraction(tmdv,site,cldma,validmaskma,configfpath)



length(validmaskma);


run(configfpath);

DataDirectory=FormatDirName(MASKDATAROOTDIR);
if site==TSI1
    lon=location1.longitude;
    lat=location1.latitude;
    alt=location1.altitude;
    azshift=AzumithShift1;
    zeshift=ZenithShift1;
    t=load(tsi1undistmat);
    sbmaskDir=sprintf('%ssbmask1/',DataDirectory);
    hamaskDir=sprintf('%shamask1/',DataDirectory);
    sbazmatf='sbazimuth_tsi1.mat';
    haazmatf='haazimuth_tsi1.mat';
elseif site == TSI2
    lon=location2.longitude;
    lat=location2.latitude;
    alt=location2.altitude;
    azshift=AzumithShift2;
    zeshift=ZenithShift2;
    t=load(tsi2undistmat);
    sbmaskDir=sprintf('%ssbmask2/',DataDirectory);
    hamaskDir=sprintf('%shamask2/',DataDirectory);
    sbazmatf='sbazimuth_tsi2.mat';
    haazmatf='haazimuth_tsi2.mat';
elseif site == TSI3
    lon=location3.longitude;
    lat=location3.latitude;
    alt=location3.altitude;
    azshift=AzumithShift3;
    zeshift=ZenithShift3;
    t=load(tsi3undistmat);
    sbmaskDir=sprintf('%ssbmask3/',DataDirectory);
    hamaskDir=sprintf('%shamask3/',DataDirectory);
    sbazmatf='sbazimuth_tsi3.mat';
    haazmatf='haazimuth_tsi3.mat';
end


sbmaskDir=GetAbspathFromFilename(sbmaskDir);
hamaskDir=GetAbspathFromFilename(hamaskDir);
%   Re-gen the sb and ha azimuth list
GenAvailAzMat_SBHA(hamaskDir,sbmaskDir,haazmatf,sbazmatf,site);

sbazimuth=load(sbazmatf);                % load all sbmask timenumbers and azumith
haazimuth=load(haazmatf);                % load all hamask timenumbers and azumith

tmvec=tmdv;
sun=INNER_GETAandZ(tmvec,lat,lon,alt);
az=sun.azimuth/180*pi+azshift/180*pi;
ze=sun.zenith/180*pi+zeshift/180*pi;    
haindexi=GetAZIndex(haazimuth.azs,haazimuth.zes,az,ze);
sbindexi=GetAZIndex_wThres(sbazimuth.tsdn,sbazimuth.azs,sbazimuth.zes,datenum(tmdv),az,ze);
if(sbindexi==-1)
    disp(datestr(tmvec));
    disp('No SB Mask Found!');
    return;
end
    
hatmdv=datevec(haazimuth.tsdn(haindexi));
sbtmdv=datevec(sbazimuth.tsdn(sbindexi));

hamaskfilename=GetImgFromDV_BNL(hatmdv,site);
hamaskfilename=sprintf('%s%s',hamaskfilename,'.mat');
sbmaskfilename=GetImgFromDV_BNL(sbtmdv,site);
sbmaskfilename=sprintf('%s%s',sbmaskfilename,'.mat');
hamaskf=sprintf('%s%s',hamaskDir,hamaskfilename);
sbmaskf=sprintf('%s%s',sbmaskDir,sbmaskfilename);
s=load(hamaskf, '-mat', 'hama');
holdma=s.hama;  %   tmma is the final name of holding arm
s=load(sbmaskf, '-mat', 'sbma');
shadowma=s.sbma;
totalmaskma=holdma&shadowma;
totalmaskma=~totalmaskma;


N=length(find(totalmaskma));
cldvals=cldma(totalmaskma);
CLDN=length(find(cldvals));

CF=CLDN/N;



end

