% Previous version is test_HighCld which is a test version of all files. This function will generate
% a single file hight cloud mask

function GetHighCldBlkMask_single(filename,InnerBlockSize,outputmatf,dataDir)
mtsi_startup;

%% Pre-settings for High cloud extraction
ncc_thres=0.6;          % minimum ncc threshold for identical test
HighCldHThres= 5000;    % minimum height that is treated as high altitude cloud
meangreyThres=12;       % mean grey difference tolerance for block merging
maxmergeblklength=10;   % maximum merge block length(10 blocks at most in each dimension)
HighCldSimFunction=@ncc3d_HighCld; % similarity function

% if exist(outputmatf,'file')
%     t=load(outputmatf);
%     hcldmask=t.hcldmask;
%     return;
% end

tmdv=GetDVFromImg(filename);
try
%     [imma1,mf1]=tmp_GetImgMatFromDataDir(tmdv,TSI1,dataDir);
%     [imma2,mf2]=tmp_GetImgMatFromDataDir(tmdv,TSI2,dataDir);
    [imma3,mf3]=tmp_GetImgMatFromDataDir(tmdv,TSI3,dataDir);
    imma3_g=TO_rgb2lum(imma3);
catch
    disp(['can not find the file please check' mf1 ' ' mf2 ' ' mf3]);
end

%   1. Divide TSI3 into blocks [InnerBlockSize InnerBlockSize]
 [bstarty,bstartx,bendy,bendx,nBlocky,nBlockx,blockvals]=DivideBlocks(...
            rgb2gray(imma3),[InnerBlockSize InnerBlockSize]);


        
%   2. Find identical block for each on TSI1, TSI2 using similarity function  ncc3d_HighCld
H_blks=zeros(nBlocky,nBlockx);
cc_blks=zeros(nBlocky,nBlockx);
HMV31_x=zeros(nBlocky,nBlockx);
HMV31_y=zeros(nBlocky,nBlockx);
HMV32_x=zeros(nBlocky,nBlockx);
HMV32_y=zeros(nBlocky,nBlockx);
for bi=1:nBlocky
    for bj=1:nBlockx
        %             if bi~=17 || bj~=12
        %                 continue;
        %             end
        tmsy=bstarty(bi,bj);
        tmsx=bstartx(bi,bj);
        tmey=bendy(bi,bj);
        tmex=bendx(bi,bj);
        tmblk3=imma3_g(tmsy:tmey,tmsx:tmex);
        if length(find(tmblk3==0))>10
            continue;
        end
        if std(single(tmblk3(:)))==0
            continue;
        end
        
        %             [H,maxcc,HMV_x,HMV_y]=test_ncc3d_v2(imma1_g,imma2_g,imma3_g,[tmsy tmsx],[InnerBlockSize InnerBlockSize],3000:1000:20000);
        [H,maxcc,HMV_x,HMV_y]=...
            HighCldSimFunction(tmdv,[tmsy tmsx],[InnerBlockSize InnerBlockSize],dataDir,3000:1000:20000,ncc_thres);

        HMV31_x(bi,bj)=HMV_x(1);
        HMV32_x(bi,bj)=HMV_x(2);
        HMV31_y(bi,bj)=HMV_y(1);
        HMV32_y(bi,bj)=HMV_y(2);
        H_blks(bi,bj)=H;
        cc_blks(bi,bj)=maxcc;
    end
end

trustindma=cc_blks>3*ncc_thres&H_blks>HighCldHThres;

segC=1000;

%   2. Merge all hcld blks into bigger blocks and do ncc similarity check again for final height
[ConnMap_s,ConnValid_s]=test_stdblkmerge(trustindma,blockvals,segC,maxmergeblklength,0,meangreyThres);
nConn_s=max(max(ConnMap_s));
corrblks=cell(nConn_s,1);
minsy1=zeros(nConn_s,1);
minsx1=zeros(nConn_s,1);
maxey1=zeros(nConn_s,1);
maxex1=zeros(nConn_s,1);
minsy2=zeros(nConn_s,1);
minsx2=zeros(nConn_s,1);
maxey2=zeros(nConn_s,1);
maxex2=zeros(nConn_s,1);
minsy3=zeros(nConn_s,1);
minsx3=zeros(nConn_s,1);
maxey3=zeros(nConn_s,1);
maxex3=zeros(nConn_s,1);
maxcc=zeros(nConn_s,1);
H_conn=zeros(nConn_s,1);

for ci=1:nConn_s
    tmind=find(ConnMap_s==ci);
    tmsy=bstarty(tmind);
    tmsx=bstartx(tmind);
    tmey=bendy(tmind);
    tmex=bendx(tmind);
    minsy=min(tmsy);
    minsx=min(tmsx);
    maxey=max(tmey);
    maxex=max(tmex);
    recH=maxey-minsy+1;
    recW=maxex-minsx+1;
    tmfillind=find(bstarty>=minsy&bendy<=maxey&bstartx>=minsx&bendx<=maxex&ConnMap_s~=ci);
    tml=length(tmfillind);
    tmblkmask=true(size(imma3_g));
    for li=1:tml
        tmind=tmfillind(li);
        tmsy=bstarty(tmind);
        tmsx=bstartx(tmind);
        tmey=bendy(tmind);
        tmex=bendx(tmind);
        tmblkmask(tmsy:tmey,tmsx:tmex)=false;
    end
    [H,maxcc(ci),HMV_x,HMV_y]=HighCldSimFunction...
        (tmdv,[minsy minsx],[recH recW],dataDir,3000:1000:20000,0,tmblkmask);

    minsx1(ci)=minsx+HMV_x(1);
    minsx2(ci)=minsx+HMV_x(2);
    minsx3(ci)=minsx;
    minsy1(ci)=minsy+HMV_y(1);
    minsy2(ci)=minsy+HMV_y(2);
    minsy3(ci)=minsy;
    maxex1(ci)=minsx1(ci)+recW;
    maxex2(ci)=minsx2(ci)+recW;
    maxex3(ci)=minsx3(ci)+recW;
    maxey1(ci)=minsy1(ci)+recH;
    maxey2(ci)=minsy2(ci)+recH;
    maxey3(ci)=minsy3(ci)+recH;
    H_conn(ci)=H;
end


save(outputmatf);



end