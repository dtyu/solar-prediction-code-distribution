% sub-function for calculation of traindata X for model_rbr.mat
function tmxtrain=tmp_img2traindata_rbr(tmimma,h,x_min,x_max)
assert(isempty(find(x_min>x_max,1)),'xmin is larger than xmax!');
x1_max=x_max(1);x2_max=x_max(2);x3_max=x_max(3);x4_max=x_max(4);
x1_min=x_min(1);x2_min=x_min(2);x3_min=x_min(3);x4_min=x_min(4);
x1 = tmimma(:, :, 1);
x2 = tmimma(:, :, 2);
x3 = tmimma(:, :, 3);
x4 =imfilter(double(TO_rgb2lum(tmimma)),h);
x4 =uint8(abs(x4));
x5=single(x1)./single(x3); %rb ratio
x5(x5>1)=1;
x5(isnan(x5))=0;        % r,b=0
x5(x5==inf)=1;          % b=0
x1_n=(double(x1(:))-x1_min)./(x1_max-x1_min);
x2_n=(double(x2(:))-x2_min)./(x2_max-x2_min);
x3_n=(double(x3(:))-x3_min)./(x3_max-x3_min);
x4_n=(double(x4(:))-x4_min)./(x4_max-x4_min);
x5_n=double(x5(:));
tmxtrain=[x1_n x2_n x3_n x4_n x5_n];
end