%   This function includes name format and upside down code
%   Date Number format.
%       From: 2011-12-01_13_03_09.0.jpeg
%       to:   bnltsiskyimageC1.a1.20111201.130309.jpg.20111201130309.jpg
%   
%   IN:
%       inputDir        --- the directory which contains input image
%       outputDir       --- Directory, output images    
%   OUT:
%   VERSION 1.1 2012-04-20

function BNLUpsideDown_multicore(inputDir,outputDir,startdn,enddn)
%   BNLUptoDown is to put all images in inputDir upside down and change it
%   to formal format of the name.
if(inputDir(end)~='/')
    inputDir=sprintf('%s%s',inputDir,'/');
end
if(outputDir(end)~='/')
    outputDir=sprintf('%s%s',outputDir,'/');
end
mkdir(outputDir);

%   1. Read each file in and output upsidedown image to output/ directory
allfiles=dir(inputDir);
[m,~]=size(allfiles);
nfiles=m;
oriimgdirname=GetFilenameFromAbsPath(inputDir);
avail_f=sprintf('avail_%s.mat',oriimgdirname);
if ~exist(avail_f,'file')
    tmdns=zeros(nfiles,1);
    for i =1:nfiles
        if allfiles(i).isdir==1
            continue;
        end
        filename=allfiles(i).name;
        tmdv=GetDVFromImg(filename);
        tmdn=datenum(tmdv);
        tmdns(i)=tmdn;
    end
    save(avail_f,'tmdns');
end
t=load(avail_f);
tmdns=t.tmdns;



for i=1:m
    if tmdns(i)<startdn || tmdns(i)>enddn
        continue;
    end
    filename=allfiles(i).name;
    fulloutp=sprintf('%s%s',outputDir,filename);
    fullinputp=sprintf('%s%s',inputDir,filename);
    img1=imread(fullinputp);
    img1(:,:,1)=flipud(img1(:,:,1));
    img1(:,:,2)=flipud(img1(:,:,2));
    img1(:,:,3)=flipud(img1(:,:,3));
    %column upside down
    img1(:,:,1)=fliplr(img1(:,:,1));
    img1(:,:,2)=fliplr(img1(:,:,2));
    img1(:,:,3)=fliplr(img1(:,:,3));
    imwrite(img1,fulloutp,'jpg');
end

end