%GenAvailAzMat_SBHA: Generate avail matfile for sbmask and hamask
%directory. Ouput is 
%   azimuth,zenith
%   IN:
%       testfilldir(in)     --  Directory, containning all the clear sky images
%       sbmaskdir(in)       --  Directory, containing sbmasks
%       hamaskdir(in)       --  Directory, containing hamasks
%       UndistortionParameters.mat(in) --   original image information
%       
%   OUT:
%       azimuth.mat         --  clear sky library
%   VERSION 1.0     Add site as for TSI1,TSI2,TSI3
%   VRESION 2.0     2014-04-28
%       Use configfpath
function GenAvailAzMat_SBHA(hamaskDir,sbmaskDir,haoutf,sboutf,site,configfpath)
%mtsi_startup_mini;
run(configfpath);
%load('UndistortionParameters.mat');
% sbmaskDir='sbmask/';        %internal input
% hamaskDir='hamask/';        %internal input
% if ~exist(haoutf,'var')
%     haoutf='haazimuth.mat';
% end
% if ~exist(sboutf,'var')
%     sboutf='sbazimuth.mat';
% end
nsite =length(SITES);
for i=1:nsite
    if site==SITES{i};
        location=locations(i);
        AzumithShift=AzumithShifts(i);
        ZenithShift=ZenithShifts(i);
    end
end


haallfiles=dir(hamaskDir);
sballfiles=dir(sbmaskDir);

[m1,~]=size(haallfiles);
[m2,~]=size(sballfiles);
tsdn1=zeros(m1,1);
azs1=zeros(m1,1);
zes1=zeros(m1,1);
for i=1 :m1
    if(haallfiles(i).isdir==1)
        continue;
    end
    filename=haallfiles(i).name;
    datevector=GetDVFromImg(filename);
    if datevector==0
        continue;
    end
    time.year    =  datevector(1);
    time.month   =  datevector(2);
    time.day     =  datevector(3);
    time.hour    =  datevector(4);
    time.min    =   datevector(5);
    time.sec    =   datevector(6);
    time.UTC    =   0;
    sun = sun_position(time, location);
    az=sun.azimuth/180*pi+AzumithShift/180*pi;
    ze=sun.zenith/180*pi+ZenithShift/180*pi;
    azs1(i)=az;
    zes1(i)=ze;
    tsdn1(i)=datenum(datevector);
end
validind=tsdn1>0;
tsdn=tsdn1(validind);
azs=azs1(validind);
zes=zes1(validind);
[sortazs sortind]=sort(azs);
save(haoutf,'tsdn','azs','zes','sortazs','sortind');

tsdn2=zeros(m2,1);
azs2=zeros(m2,1);
zes2=zeros(m2,1);
for i=1 :m2
    if(sballfiles(i).isdir==1)
        continue;
    end
    filename=sballfiles(i).name;
    datevector=GetDVFromImg(filename);
    if datevector==0
        continue;
    end
    time.year    =  datevector(1);
    time.month   =  datevector(2);
    time.day     =  datevector(3);
    time.hour    =  datevector(4);
    time.min    =   datevector(5);
    time.sec    =   datevector(6);
    time.UTC    =   0;
    sun = sun_position(time, location);
    az=sun.azimuth/180*pi+AzumithShift/180*pi;
    ze=sun.zenith/180*pi+ZenithShift/180*pi;
    azs2(i)=az;
    zes2(i)=ze;
    tsdn2(i)=datenum(datevector);
end
validind=tsdn2>0;
tsdn=tsdn2(validind);
azs=azs2(validind);
zes=zes2(validind);
[sortazs sortind]=sort(azs);
save(sboutf,'tsdn','azs','zes','sortazs','sortind');

end