function [mv_arr_x,mv_arr_y,mv_arr_indma,nAggregation]=MotionVectorAggregation(mv_x,mv_y,MVAggDistThres,HISTThres)
mv_arr_x=0;
mv_arr_y=0;
nAggregation=0;
[nRow, nCol]=size(mv_x);

% maxmv_y=max(mv_y(:));
% maxmv_x=max(mv_x(:));
% minmv_y=min(mv_y(:));
% minmv_x=min(mv_x(:));
% gridx_n=ceil(abs(minmv_x)/HISTGRIDWIDTH)+ceil(abs(maxmv_x)/HISTGRIDWIDTH);
% gridy_n=ceil(abs(minmv_y)/HISTGRIDWIDTH)+ceil(abs(maxmv_y)/HISTGRIDWIDTH);
% mvgridcount=zeros(gridy_n,gridx_n);
% gridx_sx=ceil(minmv_x/HISTGRIDWIDTH)
% for i=1:gridy_n
%     for j=1:gridx_n
%         tmgridsy=
%         mvgridcount=length(find())
%     end
% end
% 
% histcentre_y=0:HISTGRIDWIDTH
% histcentre_x=0:HISTGRIDWIDTH:max(mv_y);
HISTGRIDWIDTH=2;
maxmv_y=max(mv_y(:));
maxmv_x=max(mv_x(:));
minmv_y=min(mv_y(:));
minmv_x=min(mv_x(:));

x=mv_x(:);
y=mv_y(:);
x=x(~isnan(x));
y=y(~isnan(y));
[N,C]=hist3([x,y],{minmv_x:HISTGRIDWIDTH:maxmv_x,minmv_y:HISTGRIDWIDTH:maxmv_y});
[nX,nY]=size(N);
n_x=C{1};
n_y=C{2};
binwidth_x=abs(n_x(2)-n_x(1));
binwidth_y=abs(n_y(2)-n_y(1));

tmpN=N(:);
[tmpN,sortindex]=sort(tmpN,'descend');
Nl=length(tmpN);
tmpN(tmpN<=HISTThres)=0;
nConn=length(find(tmpN~=0));
mv_x_nConn=cell(nConn,1);
mv_y_nConn=cell(nConn,1);
mv_indma_nConn=cell(nConn,1);
iConn=1;
for i=1:Nl
    if tmpN(i)==0
        break;
    end
    oldi=sortindex(i);
    [xi yi]=ind2sub(size(N),oldi);
    cx=n_x(xi);
    cy=n_y(yi);
    binsx=(cx-binwidth_x/2);
    binex=(cx+binwidth_x/2);
    binsy=(cy-binwidth_y/2);
    biney=(cy+binwidth_y/2);
    tmindma=mv_x>=binsx & mv_x<binex & mv_y>=binsy & mv_y<biney;
    mv_x_nConn{iConn}=mv_x(tmindma);
    mv_y_nConn{iConn}=mv_y(tmindma);
    mv_indma_nConn{iConn}=tmindma;
    iConn=iConn+1;
end
% for i=1:nX
%     for j=1:nY
%         if N(i,j)<=HISTThres
%             continue;
%         end
%         cx=n_x(i);
%         cy=n_y(j);
%         binsx=(cx-binwidth_x/2);
%         binex=(cx+binwidth_x/2);
%         binsy=(cy-binwidth_y/2);
%         biney=(cy+binwidth_y/2);
%         tmindma=mv_x>=binsx & mv_x<binex & mv_y>=binsy & mv_y<biney;
%         mv_x_nConn{iConn}=mv_x(tmindma);
%         mv_y_nConn{iConn}=mv_y(tmindma);
%         mv_indma_nConn{iConn}=tmindma;
%         iConn=iConn+1;
%     end
% end
mv_arr_x=mv_x_nConn;
mv_arr_y=mv_y_nConn;
mv_arr_indma=mv_indma_nConn;
nAggregation=nConn;


end