function [mv_x mv_y H ux31 uy31]=test_ncc3d(imma21,imma22,imma31,imma32,ux21,uy21,recW,recH)
mtsi_startup;

SearchWinSize_ext=10;
uy22=uy21-SearchWinSize_ext;
ux22=ux21-SearchWinSize_ext;
SearchWinSize=SearchWinSize_ext*2+1;
uy21_e=uy21+recH-1;
ux21_e=ux21+recW-1;
uy22_e=uy21_e+SearchWinSize_ext;
ux22_e=ux21_e+SearchWinSize_ext;

%   1. Generate NCC result for TSI2 pair
blk21=imma21(uy21:uy21_e,ux21:ux21_e);
blk22_search=imma22(uy22:uy22_e,ux22:ux22_e);
cc=normxcorr2(blk21,blk22_search);


[Hc,Wc]=size(cc);
%   2. Generate NCC result for TSI3 pair based on different H
H_ran=1000:100:5000;
H_l=length(H_ran);
cc_all=zeros(Hc,Wc,H_l);
sim_all=zeros(H_l,1);
ux31_all=zeros(H_l,1);
uy31_all=zeros(H_l,1);
for i=1:H_l
    %   2.1 (ux,uy) + H TSI2 ->(ux_new,uy_new) on TSI3
    H=H_ran(i);
%     if H~=1200
%         continue;
%     end
    [rx21 ry21 rz21]=TO_Undist2RelativeCoor(ux21,uy21,H);
    [rx31 ry31 rz31]=TO_RelativeCoor2RelativeCoor(rx21,ry21,rz21,location2,location3);
    [ux31 uy31]=TO_RelativeCoor2Undist(rx31,ry31,rz31);
    ux31=round(ux31);
    uy31=round(uy31);
    uy31_e=round(uy31+recH-1);
    ux31_e=round(ux31+recW-1);
    uy32=round(uy31-SearchWinSize_ext);
    ux32=round(ux31-SearchWinSize_ext);
    uy32_e=round(uy31_e+SearchWinSize_ext);
    ux32_e=round(ux31_e+SearchWinSize_ext);
    ux31_all(i)=ux31;
    uy31_all(i)=uy31;
    %   2.2 Generate NCC result for TSI3 pair
    if uy31>UNDISTH || ux31>UNDISTW || uy31<1 || ux31<1 ||...
        uy31_e>UNDISTH || ux31_e>UNDISTW || uy31_e<1 || ux31_e<1
        continue;
    end
    if uy32>UNDISTH || ux32>UNDISTW || uy32<1 || ux32<1 ||...
        uy32_e>UNDISTH || ux32_e>UNDISTW || uy32_e<1 || ux32_e<1
        continue;
    end
    blk31=single(imma31(uy31:uy31_e,ux31:ux31_e));
    if ~isempty(find(blk31==0,1))
        continue;
    end
    blk32_search=single(imma32(uy32:uy32_e,ux32:ux32_e));
    if std(blk31(:))~=0
        tmcc=normxcorr2(blk21,blk31);
        cc_3=normxcorr2(blk31,blk32_search);
        sim_all(i)=tmcc(recH,recW);
    else
        cc_3=zeros(Hc,Wc);
        sim_all(i)=0;
    end
    
    %   2.3 Calculate similarity here
    if sim_all(i)>0
        cc_all(:,:,i)=cc+sim_all(i)*cc_3;
    else
        cc_all(:,:,i)=cc;
    end
end


%   3. Find the maximum ncc value and its mv_x,mv_y,H
[maxval,ind]=max(cc_all(:));
[tmy tmx Hi]=ind2sub(size(cc_all),ind);
H=H_ran(Hi);
ux31=ux31_all(Hi);
uy31=uy31_all(Hi);
mv_x=tmx-recW-SearchWinSize_ext;
mv_y=tmy-recH-SearchWinSize_ext;




end