function [nmv_x,nmv_y]=test_GetNextFrameMV(mv_x,mv_y,nextloc_x,nextloc_y)
startup;
% InnerBlockSize=10;
% BlockSize=50;
% SearchWinSize=50;
% CorThreshold=0.8;
% BlackThreshold=0;


OutterBlockSize=(BlockSize-InnerBlockSize)/2;
SmallOutterBlockSize=OutterBlockSize/2;
SmallSearchWinSize=SearchWinSize; % Smaller Search Window Size
ExtendCurrent=OutterBlockSize+SearchWinSize;
SmallExtendCurrent=SmallOutterBlockSize+SmallSearchWinSize;
% dimy=size(M1_ext,1);
% dimx=size(M1_ext,2);
dimy=UNDISTH+2*SearchWinSize;
dimx=UNDISTW+2*SearchWinSize;
hdimy=dimy/2;   %height of frame
hdimx=dimx/2;   %width of frame
% area where to pick im1 block
Im1y1=ExtendCurrent+1;
Im1y2=dimy-ExtendCurrent;
Im1x1=ExtendCurrent+1;
Im1x2=dimx-ExtendCurrent;
nRowBlock=floor((Im1y2-Im1y1)/InnerBlockSize);
nColBlock=floor((Im1x2-Im1x1)/InnerBlockSize);
xbounds=Im1x1:InnerBlockSize:(nColBlock-1)*InnerBlockSize;
ybounds=Im1y1:InnerBlockSize:(nRowBlock-1)*InnerBlockSize;

if 1==length(nextloc_x)
    if 0==nextloc_x && 0==nextloc_y
        nmv_x=0;
        nmv_y=0;
    else
        [tmval,tmi]=min(abs(ybounds-nextloc_y));
        [tmval,tmj]=min(abs(xbounds-nextloc_x));     
        nmv_x=mv_x(tmi,tmj);
        nmv_y=mv_y(tmi,tmj);
    end
else
    nmv_x=nextloc_x;
    nmv_y=nextloc_y;
    [tmm,tmn]=size(nextloc_x);
    for i=1:tmm
        for j=1:tmn
            if 0==nextloc_x(i,j) && 0==nextloc_y(i,j)
                nmv_x(i,j)=0;
                nmv_y(i,j)=0;
            else
                [tmval,tmi]=min(abs(ybounds-nextloc_y(i,j)));
                [tmval,tmj]=min(abs(xbounds-nextloc_x(i,j)));
                nmv_x(i,j)=mv_x(tmi,tmj);
                nmv_y(i,j)=mv_y(tmi,tmj);
            end
        end
    end
end


end