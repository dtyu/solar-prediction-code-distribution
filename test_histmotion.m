function test_histmotion
startup;

%set(0,'DefaultFigureVisible', 'off');
set(0,'DefaultFigureVisible', 'on');

tsiDirs={'./undist_20130612/tsi1_mat/','./undist_20130612/tsi2_mat/','./undist_20130612/tsi3_mat/'};
otsiDirs={'./otsi1/','./otsi2/','./otsi3/'};
nTSI=3;
nhist=5;

OutputPath='./tmptestme.mat';
% InnerBlockSize=10;
% BlockSize=50;
% SearchWinSize=50;
% CorThreshold=0.6;
% BlackThreshold=0;

imma_avgf='./imma_avgf.mat';
t=load(imma_avgf);
imma1_avg=t.imma1_avg_2r;
imma2_avg=t.imma2_avg_2r;
imma3_avg=t.imma3_avg_2r;

tsi1Dir=tsiDirs{1};
tsi2Dir=tsiDirs{2};
tsi3Dir=tsiDirs{3};
otsi1Dir=otsiDirs{1};
otsi2Dir=otsiDirs{2};
otsi3Dir=otsiDirs{3};
mkdir(otsi1Dir);
mkdir(otsi2Dir);
mkdir(otsi3Dir);
allfiles1=dir(tsi1Dir);
[nfiles1,~]=size(allfiles1);
allfiles3=dir(tsi3Dir);
[nfiles3,~]=size(allfiles3);
imma1_total=zeros(UNDISTH,UNDISTW,3);
imma2_total=zeros(UNDISTH,UNDISTW,3);
imma3_total=zeros(UNDISTH,UNDISTW,3);
N1=zeros(UNDISTH,UNDISTW);
N2=zeros(UNDISTH,UNDISTW);
N3=zeros(UNDISTH,UNDISTW);



%   Block Dividing
[bstarty,bstartx,bendy,bendx,nBlocky,nBlockx,blockvals]=DivideBlocks(...
    N1,[InnerBlockSize InnerBlockSize]);
BLKW=size(bstartx);
BLKH=size(bstarty);

% 1. Cloud Pixel extraction and Connected-component selecting
histfiles=cell(nfiles1,nhist);
for i =1:nfiles1
    tmpfile=sprintf('tmp_%d.mat',i);
    if ~exist(tmpfile)
    %if 1
        %   1.1 Get hist files
        filename=allfiles1(i).name;
        tmdv=GetDVFromImg(filename);
        if tmdv==0
            continue;
        end
        tmdn=datenum(tmdv);
        if tmdn~=datenum([2013 06 09 17 0 0])
            continue;
        end
        inputf=sprintf('%s%s',tsi1Dir,filename);
        tmhistfiles=GetHistFrames(inputf,tsi1Dir,nhist);
        histfiles(i,:)=tmhistfiles(:);
        
        %	2. Get connected component for each image
        ConnMaps=zeros(BLKH,BLKW,nhist);
        nConns=zeros(1,nhist);
        for hi=1:nhist
            tmf=tmhistfiles{hi};
            try
                %imma1=imread(tmf);
                t1=load(tmf,'imma');
                imma1=t1.imma;
            catch
                continue;
            end
            %[tmma1,cldind1]=test_GenCldMask_2r(imma1,imma1_avg);
            [tmma1,cldind1]=test_GenCldMask_2r(imma1,imma1_avg);
            
            %   2.2. Block classfication(1 = cloud ,0 = sky)
            blkclass=zeros(nBlocky,nBlockx);
            for byi=1:nBlocky
                for bxi=1:nBlockx
                    tmsy=bstarty(byi,bxi);
                    tmsx=bstartx(byi,bxi);
                    tmey=bendy(byi,bxi);
                    tmex=bendx(byi,bxi);
                    if isempty(find(cldind1(tmsy:tmey,tmsx:tmex)==1,1))
                        blkclass(byi,bxi)=0;
                    else
                        blkclass(byi,bxi)=1;
                    end
                end
            end
            
            %   2.3. Get Connected component and tags
            [ConnMap,nConn]=bwlabeln(blkclass);
            ConnMaps(:,:,hi)=ConnMap(:,:);
            nConns(hi)=nConn;
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %   2.4. DEBUG plot out 5 hist images with connection rectangle(UNCOMMENT to use)
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%         figure();
%         for hi=1:nhist
%             tmf=tmhistfiles{hi};
%             try
%                 t1=load(tmf);
%                 imma1=t1.imma;
%             catch
%                 continue;
%             end
%             subplot(2,3,hi);
%             imshow(imma1);
%             hold on;
%             ConnMap=ConnMaps(:,:,hi);
%             nConn=nConns(hi);
%             for ci =1:nConn
%                 tmind=find(ConnMap==ci);
%                 tmsy=bstarty(tmind);
%                 tmsx=bstartx(tmind);
%                 tmey=bendy(tmind);
%                 tmex=bendx(tmind);
%                 minsy=min(tmsy);
%                 minsx=min(tmsx);
%                 maxey=max(tmey);
%                 maxex=max(tmex);
%                 recH=maxey-minsy+1;
%                 recW=maxex-minsx+1;
%                 tmcorrblk=imma1(minsy:maxey,minsx:maxex,:);
%                 if length(tmcorrblk)>20
%                     rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
%                 end
%             end
%             hold off;
%         end
        
        %   3. 2f Motion test of hist images
        %   3.1. Test of larger NCC threshold blocks
        memode='debug';
        motionvector_x=zeros(45,45,nhist-1);
        motionvector_y=zeros(45,45,nhist-1);
        nextbounds_x=zeros(45,45,nhist-1);
        nextbounds_y=zeros(45,45,nhist-1);
        blkcen_x=zeros(45,45,nhist-1);
        blkcen_y=zeros(45,45,nhist-1);
        for hi=1:nhist-1   %2f pair
            tmf=tmhistfiles{hi};
            tmf_next=tmhistfiles{hi+1};
            try
                %imma1=imread(tmf);
                %imma1_next=imread(tmf_next);
                t1=load(tmf,'imma');
                imma1=t1.imma;
                t1=load(tmf_next,'imma');
                imma1_next=t1.imma;
            catch
                continue;
            end
            [tmma1,cldind1]=test_GenCldMask_2r(imma1,imma1_avg);
            [tmma1_n,cldind1_n]=test_GenCldMask_2r(imma1_next,imma1_avg);
            [~,imma1_rbr]=test_plotrbr(imma1);
            [~,imma1_n_rbr]=test_plotrbr(imma1_next);
%             [mv_x,mv_y,nextloc_x,nextloc_y,blkcenx,blkceny]=ME_2f_stichtest(255.*imma1_rbr,255.*imma1_n_rbr,cldind1,cldind1_n, ...
%                 OutputPath, InnerBlockSize, BlockSize,SearchWinSize,CorThreshold,BlackThreshold,memode);
            [mv_x,mv_y,nextloc_x,nextloc_y,blkcenx,blkceny]=ME_2f_stichtest(rgb2gray(imma1), rgb2gray(imma1_next),cldind1,cldind1_n, ...
                OutputPath,memode);
            
            motionvector_x(:,:,hi)=mv_x(:,:);
            motionvector_y(:,:,hi)=mv_y(:,:);
            nextbounds_x(:,:,hi)=nextloc_x(:,:);
            nextbounds_y(:,:,hi)=nextloc_y(:,:);
            blkcen_x(:,:,hi)=blkcenx(:,:);
            blkcen_y(:,:,hi)=blkceny(:,:);
        end
        save(tmpfile);
    end
    load(tmpfile);
    for hi=1:nhist-2   %2f pair
    %for hi=1:1   %2f pair
        tmf=tmhistfiles{hi};
        tmf_next=tmhistfiles{hi+1};
        mv_x1=motionvector_x(:,:,hi);
        mv_x2=motionvector_x(:,:,hi+1);
        mv_y1=motionvector_y(:,:,hi);
        mv_y2=motionvector_y(:,:,hi+1);
        nextloc_x1=nextbounds_x(:,:,hi);
        nextloc_y1=nextbounds_y(:,:,hi);
        blkcenx=blkcen_x(:,:,hi);
        blkceny=blkcen_y(:,:,hi);
        blkcenx=round(blkcenx-50);
        blkceny=round(blkceny-50);
        try
            %imma1=imread(tmf);
            %imma1_next=imread(tmf_next);
            t1=load(tmf,'imma');
            imma1=t1.imma;
            t1=load(tmf_next,'imma');
            imma1_next=t1.imma;
        catch
            continue;
        end
        
        
        [nmv_x,nmv_y]=test_GetNextFrameMV(mv_x2,mv_y2,nextloc_x1,nextloc_y1);
        noisemvind=test_FindNoiseOnMVs(mv_x1,mv_y1,nmv_x,nmv_y,ContinousMVDistThres);
        removednoisemv_x=mv_x1;
        removednoisemv_y=mv_y1;
        removednoisemv_x(noisemvind)=nan;
        removednoisemv_y(noisemvind)=nan;
        nanindice=isnan(mv_x1)|isnan(mv_y1);
        removednoisemv_x(nanindice)=nan;
        removednoisemv_y(nanindice)=nan;
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %   3.2. PLOT show regularized motion vectors(UNCOMMENT TO USE)
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        fig1=figure();
        subplot(1,3,1);
        imshow(imma1);
        hold on;
        quiver(blkcenx,blkceny,mv_x1,mv_y1,0);
        nanindice=isnan(mv_x1)|isnan(mv_y1);
        tmx=blkcenx(nanindice);tmy=blkceny(nanindice);
        plot(tmx,tmy,'r+');
        hold off;
        subplot(1,3,2);
        imshow(imma1_next);
        subplot(1,3,3);
        imshow(imma1);
        hold on;
        nanindice=isnan(removednoisemv_x)|isnan(removednoisemv_y);
        quiver(blkcenx,blkceny,removednoisemv_x,removednoisemv_y,0);
        tmx=blkcenx(nanindice);tmy=blkceny(nanindice);
        plot(tmx,tmy,'r+');
        hold off;
        outputf=sprintf('%d.jpg',i);
        print(fig1,outputf,'-djpeg');
        
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %   3.3. Aggregate of motion textures/blocks
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        [mv_arr_x,mv_arr_y,mv_arr_indma,nAggregation]=MotionVectorAggregation(...
            removednoisemv_x,removednoisemv_y,MotionVectorAggThres,HistThres);
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %   PLOT OUT AGGREGATION OF MOTION VECTOR
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        figure();
        imshow(imma1);
        hold on;
        for tmindex=1:nAggregation
            tmindma=mv_arr_indma{tmindex};
            mvx_show=mv_arr_x{tmindex};
            mvx_show(:)=mean(mvx_show(:));
            mvy_show=mv_arr_y{tmindex};
            mvy_show(:)=mean(mvy_show(:));
            blkcenx_show=blkcenx(tmindma);
            blkceny_show=blkceny(tmindma);
            cr=double(bitand(tmindex,1)>0);
            cg=double(bitand(tmindex,2)>0);
            cb=double(bitand(tmindex,4)>0);
            plot(blkcenx_show,blkceny_show,'.','Color',[cr cg cb]);
            quiver(blkcenx_show,blkceny_show,mvx_show,mvy_show,0);
        end
        hold off;

        
    end
    
    
end

end