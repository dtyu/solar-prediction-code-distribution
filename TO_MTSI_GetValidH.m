%mtsi_startup used
function [Hmvx,Hmvy,H_possible]=TO_MTSI_GetValidH(Hma,H)
tmma=abs(Hma-H);
ind=find(tmma==min(min(tmma)));
if length(ind)>1
    ind=ind(1);
end
[tmy,tmx]=ind2sub(size(tmma),ind);
Hmvx=tmx-241;
Hmvy=tmy-241;
H_possible=Hma(ind);


end