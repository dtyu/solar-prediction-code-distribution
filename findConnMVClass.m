function [MVx_class,MVy_class,MV_classcount,nclass]=findConnMVClass(MV_x,MV_y,hind,Conn_cldcount)
tml=length(hind);
MVx_class=[];
MVy_class=[];
MV_classcount=[];
nclass=0;
for i=1:tml
    tmi=hind(i);
    tmmvx=MV_x(tmi);
    tmmvy=MV_y(tmi);
    class_found=isInMVClass(tmmvx,tmmvy);
    if class_found==0
        nclass=nclass+1;
        MVx_class=[MVx_class;tmmvx];
        MVy_class=[MVy_class;tmmvy];
        MV_classcount=[MV_classcount;Conn_cldcount(tmi)];
    else
        MV_classcount(class_found)=MV_classcount(class_found)+Conn_cldcount(tmi);
    end
end


    function class_found=isInMVClass(mvx,mvy)
        if nclass==0
            class_found=0;
        else
            class_found=find(MVx_class==mvx&MVy_class==mvy,1);
           if ~isempty(class_found)
               class_found=class_found(1);
           else
               class_found=0;
           end
        end
    end
end