% This function is a test version of small block trust test based on real images(RGB input)
%   Test version is working for TSI2->TSI3 only!!
%   Untrust cases:
%       1. The block contains BLACK/invalid
%       2. Possible searching Path contains BLACK/invalid
%           2.1. The search will be out of image(lowest H block is out of image)
%           2.2. The search will contains SB/HA area
%       3. Within UNDISTR-10 range
function trustma=test_blktrustveri(bstarty,bstartx,bendy,bendx,nBlocky,nBlockx,imma2,imma3,site1,site2,H_ran)
site1='tsi2';
site2='tsi3';

mtsi_startup;
if strcmpi(site1,TSI1)
    loc1=location1;
elseif strcmpi(site1,TSI2)
    loc1=location2;
elseif strcmpi(site1,TSI3)
    loc1=location3;
end
if strcmpi(site2,TSI1)
    loc2=location1;
elseif strcmpi(site2,TSI2)
    loc2=location2;
elseif strcmpi(site2,TSI3)
    loc2=location3;
end
if isempty(H_ran)
    H_ran=1000:100:5000;
end
H_l=length(H_ran);
tW=bendx(1)-bstartx(1)+1;
tH=bendy(1)-bstarty(1)+1;
trustma=zeros(nBlocky,nBlockx);
imma2_grey=rgb2gray(imma2);
imma3_grey=rgb2gray(imma3);
for byi=1:nBlocky
    for bxi=1:nBlockx
            
        tmbstarty=bstarty(byi,bxi);
        tmbstartx=bstartx(byi,bxi);
        tmbendy=bendy(byi,bxi);
        tmbendx=bendx(byi,bxi);
        tmbcenx=(tmbstartx+tmbendx)/2;
        tmbceny=(tmbstarty+tmbendy)/2;
        % Check case 3
        l=sqrt((tmbcenx-UNDISTW/2)^2+(tmbceny-UNDISTH/2)^2);
        if l>UNDISTR-10
            continue;
        end
        
        tmblkvals=single(imma2_grey(tmbstarty:tmbendy,tmbstartx:tmbendx));
        if ~isempty(find(tmblkvals==0,1))
            % trustma(byi,bxi)=0; % case 1
            continue;
        end
%         if max(tmblkvals(:))-min(tmblkvals(:))<=10
%             continue;
%         end
        H_lowest=H_ran(1);
        H_Highest=H_ran(end);
        rz=H_lowest;
        ux=tmbstartx;
        uy=tmbstarty;
        [az ze]=TO_UNDISTCoor2AZ(ux,uy);
        [rx,ry,rz]=AZH2SpaceCoor(az,ze,rz);
        [rx_new ry_new rz_new]=TO_RelativeCoor2RelativeCoor(rx,ry,rz,loc1,loc2);
        [az_new ze_new]=TO_RelativeCoor2AZ(rx_new,ry_new,rz_new);
        [ux_new uy_new]=TO_AZ2UNDISTCoor(az_new,ze_new);
        ux_new=round(ux_new);
        uy_new=round(uy_new);
        ux_end=round(ux_new+tW-1);
        uy_end=round(uy_new+tH-1);
        if ux_new>=1 && uy_new>=1 && ux_end<=UNDISTW && uy_end<=UNDISTH && ...
                ux_end>=1 && uy_end>=1 && ux_new<=UNDISTW && uy_new<=UNDISTH
            % check case 2.2
            bCase2=false;
            for i=1:H_l
                rz=H_ran(i);
                [az ze]=TO_UNDISTCoor2AZ(ux,uy);
                [rx,ry,rz]=AZH2SpaceCoor(az,ze,rz);
                [rx_new ry_new rz_new]=TO_RelativeCoor2RelativeCoor(rx,ry,rz,loc1,loc2);
                [az_new ze_new]=TO_RelativeCoor2AZ(rx_new,ry_new,rz_new);
                [ux_new1 uy_new1]=TO_AZ2UNDISTCoor(az_new,ze_new);
                ux_new1=round(ux_new1);
                uy_new1=round(uy_new1);
                ux_end1=round(ux_new1+tW-1);
                uy_end1=round(uy_new1+tH-1);
                y=single(imma3_grey(uy_new1:uy_end1,ux_new1:ux_end1));
                if ~isempty(find(y==0,1))
                    bCase2=true;
                    break;
                end
            end
            if false==bCase2
                trustma(byi,bxi)=1;
            else
                % trustma(byi,bxi)=0; % case 2.2
            end
        else
            % trustma(byi,bxi)=0; % case 2.1
        end
    end
end


end