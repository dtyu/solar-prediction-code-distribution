% function tmp_CheckFilledTSIAvail is to check the TSI availability of filled images and its para
% mat files.
%   VERSION 1.0     2013/12/30
%       Small modification from TO_CheckTSIAvail.m
%   USAGE:
%       [TSIavail,TSIavail_days]=tmp_CheckFilledTSIAvail('/home/mikegrup/D/mat/mtsi_skyrecovery/stiching/newfilled/');
%       See mtsipipe_GenDataset for details.
function [TSIavail,TSIavail_days]=tmp_CheckFilledTSIAvail(FilledTSI3Dir)
dirpath=FilledTSI3Dir;
if ~exist(dirpath,'dir')
    disp(dirpath);
    disp('No such input directory found!'); 
    TSIavail=-1;
    return;
end
dirpath=FormatDirName(dirpath);
allfiles=dir(dirpath);
[nfiles,~]=size(allfiles);
alltmdn=[];
for i=1:nfiles
    if allfiles(i).isdir==1
        continue;
    end
    f1=allfiles(i).name;
    tmdv=GetDVFromImg(f1);
    if tmdv(1)==0
        continue;
    end
    tmdn=datenum(tmdv);
    alltmdn=[alltmdn tmdn];
end
alltmdn=unique(alltmdn);
TSIavail=alltmdn;
alltmdv=datevec(alltmdn);
alltmdv(:,4:6)=0;
TSIavail_days=datenum(alltmdv);
TSIavail_days=unique(TSIavail_days);
TSIavail_days=TSIavail_days(TSIavail_days~=0);







end