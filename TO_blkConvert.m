% This function is to convert bigger block into smaller block.
function blkma_desti=TO_blkConvert(blkma,ori_blksize,desti_blksize)
TIMES=ori_blksize/desti_blksize;
[nBlky, nBlkx]=size(blkma);
blkma_desti=zeros(nBlky*TIMES,nBlkx*TIMES);

for i=1:nBlky
    i_s=1+TIMES*(i-1);
    i_e=TIMES*i;
    for j=1:nBlkx
        j_s=1+TIMES*(j-1);
        j_e=TIMES*j;
        blkma_desti(i_s:i_e,j_s:j_e)=blkma(i,j);
    end
end

end