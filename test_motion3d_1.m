function test_motion3d_1
mtsi_startup;

tsiDirs={'./cloudy_multilayer/tsi1_undist_20130605/','./cloudy_multilayer/tsi2_undist_20130605/','./cloudy_multilayer/tsi3_undist_20130605/'};
otsiDirs={'./otsi1/','./otsi2/','./otsi3/'};

nTSI=3;
tsi1Dir=tsiDirs{1};
tsi2Dir=tsiDirs{2};
tsi3Dir=tsiDirs{3};
otsi1Dir=otsiDirs{1};
otsi2Dir=otsiDirs{2};
otsi3Dir=otsiDirs{3};
mkdir(otsi1Dir);
mkdir(otsi2Dir);
mkdir(otsi3Dir);
alltsi1=dir(tsi1Dir);
[nfiles1,~]=size(alltsi1);
imma1_total=zeros(UNDISTH,UNDISTW,3);
imma2_total=zeros(UNDISTH,UNDISTW,3);
imma3_total=zeros(UNDISTH,UNDISTW,3);
N1=zeros(UNDISTH,UNDISTW);
N2=zeros(UNDISTH,UNDISTW);
N3=zeros(UNDISTH,UNDISTW);

imma_avgf='./imma_avgf.mat';
t=load(imma_avgf,'imma1_avg','imma2_avg','imma3_avg');
imma1_avg=t.imma1_avg;
imma2_avg=t.imma2_avg;
imma3_avg=t.imma3_avg;

OutputPath='./tmptestme.mat';
InnerBlockSize=10;
BlockSize=50;
SearchWinSize=50;
CorThreshold=0.5;
BlackThreshold=0;

MEOutDir='./ME_test_img';
FillOutDir='./Fillimg';
MEOutDir=FormatDirName(MEOutDir);
FillOutDir=FormatDirName(FillOutDir);
if ~exist(MEOutDir,'dir')
    mkdir(MEOutDir);
end
if ~exist(FillOutDir,'dir')
    mkdir(FillOutDir);
end
ONEMIN_DN=datenum([0 0 0 0 1 0]);
TENSECS_DN=datenum([0 0 0 0 0 10]);


for i =1:nfiles1
    filename=alltsi1(i).name;
    f1=sprintf('%s%s',tsi1Dir,filename);
    tmdv=GetDVFromImg(filename);
    if tmdv==0
        continue;
    end
    tmdn=datenum(tmdv);
    if tmdn<datenum([2013 06 05 14 42 50])
        continue;
    end

    tmdn_1maf=tmdn+TENSECS_DN;
    tmdn_1mbf=tmdn-ONEMIN_DN;
    f2_1maf=GetImgFromDV_BNL(datevec(tmdn_1maf),TSI2);
    f2_1mbf=GetImgFromDV_BNL(datevec(tmdn_1mbf),TSI2);
    f3_1maf=GetImgFromDV_BNL(datevec(tmdn_1maf),TSI3);
    filename2=GetImgFromDV_BNL(tmdv,TSI2);
    filename3=GetImgFromDV_BNL(tmdv,TSI3);
%     f2=sprintf('%s%s.bmp',tsi2Dir,filename2(1:end-4));
%     f3=sprintf('%s%s.bmp',tsi3Dir,filename3(1:end-4));
    f2=sprintf('%s%s.jpg',tsi2Dir,filename2(1:end-4));
    f2_1maf=sprintf('%s%s.jpg',tsi2Dir,f2_1maf(1:end-4));
    f2_1mbf=sprintf('%s%s.jpg',tsi2Dir,f2_1mbf(1:end-4));
    f3=sprintf('%s%s.jpg',tsi3Dir,filename3(1:end-4));
    f3_1maf=sprintf('%s%s.jpg',tsi3Dir,f3_1maf(1:end-4));
    try
        imma1=tmp_GetImgMatFromDataDir(f1);
        imma2=tmp_GetImgMatFromDataDir(f2);
        imma3=tmp_GetImgMatFromDataDir(f3);
    catch
        continue;
    end
    
    %[ma1,cldind1]=test_GenCldMask_2r(imma1,imma1_avg);
    [bstarty,bstartx,bendy,bendx,nBlocky,nBlockx,blockvals]=DivideBlocks(...
        rgb2gray(imma1),[InnerBlockSize InnerBlockSize]);
    imma1_g=rgb2gray(imma1);
    imma2_g=rgb2gray(imma2);
    cc=zeros(nBlocky,nBlockx);
    for bi=1:nBlocky
        for bj=1:nBlockx
            if bi==10 && bj==10
                bi
            end
            tmsy=bstarty(bi,bj);
            tmsx=bstartx(bi,bj);
            tmey=bendy(bi,bj);
            tmex=bendx(bi,bj);
            tmblk1=single(imma1_g(tmsy:tmey,tmsx:tmex));
            tmblk2=single(imma2_g(tmsy:tmey,tmsx:tmex));
            if 0==std(tmblk1(:))
                continue;
            end
            tmcc=normxcorr2_blk(tmblk1,tmblk2);
            cc(bi,bj)=tmcc(InnerBlockSize,InnerBlockSize);
        end
    end
    figure();
    hold on;
    subplot(1,2,1);
    imshow(imma1_g);
    subplot(1,2,2);
    imshow(imma2_g);
    hold off;


end