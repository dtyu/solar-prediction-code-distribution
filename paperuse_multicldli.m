%function paperuse_multicldli
set(0,'DefaultFigureVisible', 'on');
% set(0,'DefaultFigureVisible', 'off');
BPS_N=25;
lf_para=zeros(BPS_N,7);
MAE=zeros(BPS_N,3);
RMSE=zeros(BPS_N,3);
% plot out
outputDir='sftestplot/';
outputDir=FormatDirName(outputDir);
if ~exist(outputDir,'dir')
    mkdir(outputDir);
end


opt1='-s 4 -t 0';
opt2='-s 4 -t 2';

% test of tuning parameters for li
% nran=0.05:0.1:1;
% cran=1:15;
% meanmae=zeros(length(cran),length(nran));
% meanrmse=zeros(length(cran),length(nran));
% %model_li=svmtrain(ytrain,xtrain_norm,svmoptions_li);
% meanmae=zeros(10,5);
% meanrmse=zeros(10,5);
% models_li=cell(BPS_N,1);
% models_rbf=cell(BPS_N,1);
% models_rbr=cell(BPS_N,1);
% models_a=cell(BPS_N,1);
% predmatf=sprintf('sftmp.mat');
% t1=load(predmatf);
% od_r_n=t1.od_r_n;
% od_g_n=t1.od_g_n;
% od_b_n=t1.od_b_n;
% od_rbr_n=t1.od_rbr_n;
% od_prerbr_n=t1.od_prerbr_n;
% od_cf=t1.od_cf;
% od_rad_norm=t1.od_rad_norm;
% od_prerad_norm=t1.od_prerad_norm;
% showstarti=t1.showstarti;
% showendi=t1.showendi;
% timelabel=t1.timelabel;
% NinHour=t1.NinHour;
% for ci=1:length(cran)
%     for ni=1:length(nran)
%         n=nran(ni);
%         c=cran(ci);
%         svmoptions_li=sprintf('%s -c %d -n %f',opt1,2^c,n);
%         for i=1:BPS_N
%             %         if i~=6
%             %             continue
%             %         end
%             outputf=sprintf('%ssfmodel_bps%d.png',outputDir,i);
%             r=od_r_n(showstarti:showendi,i);
%             g=od_g_n(showstarti:showendi,i);
%             b=od_b_n(showstarti:showendi,i);
%             prerad=od_prerad_norm(showstarti:showendi,i);
%             rbr=od_rbr_n(showstarti:showendi,i);
%             prerbr=od_prerbr_n(showstarti:showendi,i);
%             cldf=od_cf(showstarti:showendi);
%             fit_y=od_rad_norm(showstarti:showendi,i);
% %             fit_x=[ones(size(r)) r g b rbr cldf prerad];
% %             fit_rbr_x=rbr-prerbr;
% %             fit_rbr_y=(fit_y-prerad);
% %             a=fit_x\fit_y;  % y=a1+a2*x1+a3*x2+a4*x3+a5*x4+a6*x6
% %             a_rbr=fit_rbr_x\fit_rbr_y;  % y=a1+a2*x1+a3*x2+a4*x3+a5*x4+a6*x6
% %             %lf_para(i,:)=a';
% %             pre_y=fit_x*a;
% %             pre_y(pre_y>1)=1;
% %             pre_y(pre_y<0)=0;
%             xtrain_norm=[r g b rbr cldf prerad];
%             model_li=svmtrain(fit_y,xtrain_norm,svmoptions_li);
%             %model_rbf=svmtrain(fit_y,xtrain_norm,svmoptions_rbf);
%             [predicted_label, accuracy, y1]=svmpredict(fit_y,xtrain_norm,model_li);
%             %[predicted_label, accuracy, y2]=svmpredict(fit_y,xtrain_norm,model_rbf);
%             y1(y1>1)=1; y1(y1<0)=0;
%             %y2(y2>1)=1; y2(y2<0)=0;
%             %MAE(i,1)=mean(abs(pre_y-fit_y));
%             %RMSE(i,1)=sqrt(mean((pre_y-fit_y).^2));
%             MAE(i,2)=mean(abs(y1-fit_y));
%             RMSE(i,2)=sqrt(mean((y1-fit_y).^2));
%             %MAE(i,3)=mean(abs(y2-fit_y));
%             %RMSE(i,3)=sqrt(mean((y2-fit_y).^2));
%             %models_a{i}=a;
%             %models_li{i}=model_li;
%             %models_rbf{i}=model_rbf;
%             %models_rbr{i}=a_rbr;
%             
%             %         f1=figure();
%             %         %titStr=sprintf('Estimated GHI of 3 models vs. GHI of Panel %d',i);
%             %         %title(titStr);
%             %         legStr={'li','SVR_{li}','SVR_{rbf}','GHI_{norm}'};
%             %         xStr='EST(UTC/GMT-5)';
%             %         grid on;
%             %         hold on;
%             %         xlabel(xStr,'Fontsize',16);
%             %         ylabel('Normalized GHI (0~1)','Fontsize',16);
%             %         plot(pre_y,'--*y','linewidth',1);
%             %         plot(y1,'--.b','linewidth', 1);
%             %         plot(y2,'--xr','linewidth', 1);
%             %         plot(fit_y,'-k','linewidth', 1);
%             %
%             %         set(gca,'Fontsize', 16);
%             %         axis([1 showendi-showstarti+1 0 1.1]);
%             %         set(gca,'XTick',1:NinHour:showendi-showstarti+1);
%             %         set(gca,'XTickLabel',timelabel);
%             %         legend(legStr,'location','North','Orientation','horizontal');
%             %         legend boxoff;
%             %         set(gcf,'PaperUnits','inches','PaperPosition',[0 0 10 6]);
%             %         hold off;
%             %         print(f1,outputf,'-dpng');
%             %         print(f1,'-depsc','-r300',[outputf(1:end-4) '.eps']);
%         end
%         meanmae(ci,ni)=mean(MAE(:,2));
%         meanrmse(ci,ni)=mean(RMSE(:,2));
%     end
% end
% save('tuning.mat');
% return;


bModeling=false; % generate and save sfmodels.mat 
%modelfile='sfmodels_rgb';   % rgb
%modelfile='sfmodels_rbr';   % rgb_rbf
modelfile='sfmodels_all.mat';   % rgb+rbf+cf
nfolds=5;
if bModeling
    %model_li=svmtrain(ytrain,xtrain_norm,svmoptions_li);
    meanmae=zeros(10,5);
    meanrmse=zeros(10,5);
    models_li=cell(BPS_N,5);
    models_rbf=cell(BPS_N,5);
    models_rbr=cell(BPS_N,1);
    models_a=cell(BPS_N,1);
    MAE_li=zeros(BPS_N,5);
    MAE_rbf=zeros(BPS_N,5);
    RMSE_li=zeros(BPS_N,5);
    RMSE_rbf=zeros(BPS_N,5);
    predmatf=sprintf('sftmp.mat');
    t1=load(predmatf);
    od_r_n=t1.od_r_n;
    od_g_n=t1.od_g_n;
    od_b_n=t1.od_b_n;
    od_prer_n=t1.od_prer_n;
    od_preg_n=t1.od_preg_n;
    od_preb_n=t1.od_preb_n;
    od_rbr_n=t1.od_rbr_n;
    od_prerbr_n=t1.od_prerbr_n;
    od_cf=t1.od_cf;
    od_rad_norm=t1.od_rad_norm;
    od_prerad_norm=t1.od_prerad_norm;
    showstarti=t1.showstarti;
    showendi=t1.showendi;
    timelabel=t1.timelabel;
    NinHour=t1.NinHour;
    ci=10;
    svmoptions_li=sprintf('%s -c %d ',opt1,2^ci);
    svmoptions_rbf=sprintf('%s -c %d ',opt2,2^ci);
    for i=1:BPS_N
        %         if i~=6
        %             continue
        %         end
        outputf=sprintf('%ssfmodel_bps%d.png',outputDir,i);
        r=od_r_n(showstarti:showendi,i);
        g=od_g_n(showstarti:showendi,i);
        b=od_b_n(showstarti:showendi,i);
        prer=od_r_n(showstarti:showendi,i);
        preg=od_g_n(showstarti:showendi,i);
        preb=od_b_n(showstarti:showendi,i);
        prerad=od_prerad_norm(showstarti:showendi,i);
        rbr=od_rbr_n(showstarti:showendi,i);
        prerbr=od_prerbr_n(showstarti:showendi,i);
        cldf=od_cf(showstarti:showendi);
        fit_y=od_rad_norm(showstarti:showendi,i);
        fit_x=[ones(size(r)) r g b rbr cldf prerad];
        fit_rbr_x=rbr-prerbr;
        fit_rbr_y=(fit_y-prerad);
        a=fit_x\fit_y;  % y=a1+a2*x1+a3*x2+a4*x3+a5*x4+a6*x6
        a_rbr=fit_rbr_x\fit_rbr_y;  % y=a1+a2*x1+a3*x2+a4*x3+a5*x4+a6*x6
        %lf_para(i,:)=a';
        pre_y=fit_x*a;
        pre_y(pre_y>1)=1;
        pre_y(pre_y<0)=0;
        
        xtrain_norm=[r g b prer preg preb prerbr rbr cldf prerad];
        ytrain_norm=fit_y;
        [xtrain ytrain xtest ytest]=cvGenFolds(xtrain_norm,ytrain_norm,nfolds);
        for fi=1:nfolds
            model_li=svmtrain(ytrain{fi},xtrain{fi},svmoptions_li);
            model_rbf=svmtrain(ytrain{fi},xtrain{fi},svmoptions_rbf);
            [predicted_label, accuracy, y1]=svmpredict(ytest{fi},xtest{fi},model_li);
            [predicted_label, accuracy, y2]=svmpredict(ytest{fi},xtest{fi},model_rbf);
            models_li{i,fi}=model_li;
            models_rbf{i,fi}=model_rbf;
            y1(y1>1)=1; y1(y1<0)=0;
            y2(y2>1)=1; y2(y2<0)=0;
            MAE_li(i,fi)=mean(abs(y1-ytest{fi}));
            RMSE_li(i,fi)=sqrt(mean((y1-ytest{fi}).^2));
            MAE_rbf(i,fi)=mean(abs(y2-ytest{fi}));
            RMSE_rbf(i,fi)=sqrt(mean((y2-ytest{fi}).^2));
        end
%         model_li=svmtrain(fit_y,xtrain_norm,svmoptions_li);
%         model_rbf=svmtrain(fit_y,xtrain_norm,svmoptions_rbf);
%         [predicted_label, accuracy, y1]=svmpredict(fit_y,xtrain_norm,model_li);
%         [predicted_label, accuracy, y2]=svmpredict(fit_y,xtrain_norm,model_rbf);        
%         MAE(i,1)=mean(abs(pre_y-fit_y));
%         RMSE(i,1)=sqrt(mean((pre_y-fit_y).^2));
%         MAE(i,2)=mean(abs(y1-fit_y));
%         RMSE(i,2)=sqrt(mean((y1-fit_y).^2));
%         MAE(i,3)=mean(abs(y2-fit_y));
%         RMSE(i,3)=sqrt(mean((y2-fit_y).^2));
        models_a{i}=a;
        models_rbr{i}=a_rbr;
%         models_li{i}=model_li;
%         models_rbf{i}=model_rbf;
        
        
        %         f1=figure();
        %         %titStr=sprintf('Estimated GHI of 3 models vs. GHI of Panel %d',i);
        %         %title(titStr);
        %         legStr={'li','SVR_{li}','SVR_{rbf}','GHI_{norm}'};
        %         xStr='EST(UTC/GMT-5)';
        %         grid on;
        %         hold on;
        %         xlabel(xStr,'Fontsize',16);
        %         ylabel('Normalized GHI (0~1)','Fontsize',16);
        %         plot(pre_y,'--*y','linewidth',1);
        %         plot(y1,'--.b','linewidth', 1);
        %         plot(y2,'--xr','linewidth', 1);
        %         plot(fit_y,'-k','linewidth', 1);
        %
        %         set(gca,'Fontsize', 16);
        %         axis([1 showendi-showstarti+1 0 1.1]);
        %         set(gca,'XTick',1:NinHour:showendi-showstarti+1);
        %         set(gca,'XTickLabel',timelabel);
        %         legend(legStr,'location','North','Orientation','horizontal');
        %         legend boxoff;
        %         set(gcf,'PaperUnits','inches','PaperPosition',[0 0 10 6]);
        %         hold off;
        %         print(f1,outputf,'-dpng');
        %         print(f1,'-depsc','-r300',[outputf(1:end-4) '.eps']);
    end
%     meanmae(ci,1)=mean(MAE(:,1));
%     meanmae(ci,2)=mean(MAE(:,2));
%     meanmae(ci,3)=mean(MAE(:,3));
%     meanrmse(ci,1)=mean(RMSE(:,1));
%     meanrmse(ci,2)=mean(RMSE(:,2));
%     meanrmse(ci,3)=mean(RMSE(:,3));
%     
    save(modelfile,'models_li','models_rbf','models_a','models_rbr');
end


timeran=0:60:300;
tl=length(timeran);
nfolds=5;
if ~bModeling
    t=load(modelfile);
    % RMSE and MAE test
    % li,svr_li,svr_rbf,rbrt,rshift
    meanmae=zeros(tl,5);
    meanrmse=zeros(tl,5);
    MAE_li=zeros(BPS_N,nfolds,tl);
    MAE_rbf=zeros(BPS_N,nfolds,tl);
    RMSE_li=zeros(BPS_N,nfolds,tl);
    RMSE_rbf=zeros(BPS_N,nfolds,tl);
    for ti=1:tl
        MAE=zeros(BPS_N,5);
        RMSE=zeros(BPS_N,5);
        timeahead=timeran(ti);
        if timeahead~=0
            predmatf=sprintf('sftmp_%d.mat',timeahead);
        else
            predmatf=sprintf('sftmp.mat');
        end
        %         rbr=od_b_n(showstarti:showendi,i);
        %         fit_y=od_rad_norm(showstarti:showendi,i);
        %         fit_x=[ones(size(r)) r g b rbr];
        t1=load(predmatf);
        od_r_n=t1.od_r_n;
        od_g_n=t1.od_g_n;
        od_b_n=t1.od_b_n;
            od_prer_n=t1.od_prer_n;
    od_preg_n=t1.od_preg_n;
    od_preb_n=t1.od_preb_n;
        od_rad_norm=t1.od_rad_norm;
        od_prerad_norm=t1.od_prerad_norm;
        od_prerbr_n=t1.od_prerbr_n;
        od_rbr_n=t1.od_rbr_n;
        od_cf=t1.od_cf;
        showstarti=t1.showstarti;
        showendi=t1.showendi;
        timelabel=t1.timelabel;
        NinHour=t1.NinHour;
        %od_rbr_n=t1.od_r_n;
        for i=1:BPS_N
%             if i~=6
%                 continue
%             end
            r=od_r_n(showstarti:showendi,i);
            g=od_g_n(showstarti:showendi,i);
            b=od_b_n(showstarti:showendi,i);
            prerad=od_prerad_norm(showstarti:showendi,i);
            prerbr=od_prerbr_n(showstarti:showendi,i);
            prer=od_r_n(showstarti:showendi,i);
            preg=od_g_n(showstarti:showendi,i);
            preb=od_b_n(showstarti:showendi,i);
            rbr=od_rbr_n(showstarti:showendi,i);
            ind=find(isnan(prerad));
            for tmi=1:length(ind)
                tmind=ind(tmi);
                if tmind>2
                    tmvals=prerad(tmind-2:tmind+2);
                else
                    tmvals=prerad(tmind:tmind+2);
                end
                prerad(tmind)=mean(tmvals(~isnan(tmvals)));
            end
            cldf=od_cf(showstarti:showendi);
            fit_y=od_rad_norm(showstarti:showendi,i);
            invalidind=isnan(r)|isnan(g)|isnan(b)|rbr==0|cldf==0|prerad==0;
            r1=r(~invalidind);  g1=g(~invalidind); b1=b(~invalidind);
            prer1=prer(~invalidind);  preg1=preg(~invalidind); preb1=preb(~invalidind);
            rbr1=rbr(~invalidind);   cldf1=cldf(~invalidind);   fit_y1=fit_y(~invalidind);
            prerad1=prerad(~invalidind); prerbr1=prerbr(~invalidind);
            if length(fit_y1)~=length(fit_y)
                disp(length(fit_y1));
            end
            fit_x=[ones(size(r1)) r1 g1 b1 rbr1 cldf1 prerad1];
            %xtrain_norm=[r1 g1 b1 rbr1 cldf1 prerad1];
            xtrain_norm=[r1 g1 b1 prer1 preg1 preb1 prerbr1 rbr1 cldf1 prerad1];
            %fit_x=[ones(size(r)) r g b rbr cldf prerad];
            a=t.models_a{i};  % y=a1+a2*x1+a3*x2+a4*x3+a5*x4+a6*x6
            a_rbr=t.models_rbr{i};
            fit_rbr_x=rbr1-prerbr1;
            %fit_rbr_y=(fit_y-prerad);
            pre_y_rbf=fit_rbr_x*a_rbr+prerad1;
            pre_y=fit_x*a;
            pre_y(pre_y>1)=1;   pre_y_rbf(pre_y_rbf>1)=1;
            pre_y(pre_y<0)=0;   pre_y_rbf(pre_y_rbf<0)=0;
            %xtrain_norm=[r g b rbr cldf prerad];
%             if ~isempty(find(isnan(xtrain_norm),1))
%                 i
%             end
           
            for fi=1:nfolds
                model_li=t.models_li{i,fi};
                model_rbf=t.models_rbf{i,fi};
                [predicted_label, accuracy, y1]=svmpredict(fit_y1,xtrain_norm,model_li);
                [predicted_label, accuracy, y2]=svmpredict(fit_y1,xtrain_norm,model_rbf);
                y1(y1>1)=1; y1(y1<0)=0;
                y2(y2>1)=1; y2(y2<0)=0;
                MAE_li(i,fi,ti)=mean(abs(y1-fit_y1));
                RMSE_li(i,fi,ti)=sqrt(mean((y1-fit_y1).^2));
                MAE_rbf(i,fi,ti)=mean(abs(y2-fit_y1));
                RMSE_rbf(i,fi,ti)=sqrt(mean((y2-fit_y1).^2));
            end
            MAE(i,1)=mean(abs(pre_y-fit_y1));
            RMSE(i,1)=sqrt(mean((pre_y-fit_y1).^2));
            MAE(i,4)=mean(abs(pre_y_rbf-fit_y1));
            RMSE(i,4)=sqrt(mean((pre_y_rbf-fit_y1).^2));
            MAE(i,5)=mean(abs(prerad1-fit_y1));
            RMSE(i,5)=sqrt(mean((prerad1-fit_y1).^2));
%             if timeahead==60
%                 f1=figure();
%                 outputf=sprintf('%spred%d_bps%d.png',outputDir,timeahead,i);
%                 %titStr=sprintf('Estimated GHI of 3 models vs. GHI of Panel %d',i);
%                 %title(titStr);
%                 legStr={'li','SVR_{li}','SVR_{rbf}','GHI_{norm}'};
%                 xStr='EST(UTC/GMT-5)';
%                 grid on;
%                 hold on;
%                 xlabel(xStr,'Fontsize',16);
%                 ylabel('Normalized GHI (0~1)','Fontsize',16);
%                 plot(pre_y,'--*y','linewidth',1);
%                 plot(y1,'--.b','linewidth', 1);
%                 plot(y2,'--xr','linewidth', 1);
%                 plot(fit_y1,'-k','linewidth', 1);
%                 
%                 set(gca,'Fontsize', 16);
%                 axis([1 showendi-showstarti+1 0 1.1]);
%                 set(gca,'XTick',1:NinHour:showendi-showstarti+1);
%                 set(gca,'XTickLabel',timelabel);
%                 legend(legStr,'location','North','Orientation','horizontal');
%                 legend boxoff;
%                 set(gcf,'PaperUnits','inches','PaperPosition',[0 0 10 6]);
%                 hold off;
%                 print(f1,outputf,'-dpng');
%                 print(f1,'-depsc','-r300',[outputf(1:end-4) '.eps']);
%             end
        end
        %for li
        tmmae=MAE_li(:,:,ti);
        meanvals=mean(tmmae);
        minmae=min(meanvals);
        meanmae(ti,2)=minmae;
        tmrmse=RMSE_li(:,:,ti);
        meanvals=mean(tmrmse);
        minrmse=min(meanvals);
        meanrmse(ti,2)=minrmse;
        % rbf
        tmmae=MAE_rbf(:,:,ti);
        meanvals=mean(tmmae);
        minmae=min(meanvals);
        meanmae(ti,3)=minmae;
        tmrmse=RMSE_rbf(:,:,ti);
        meanvals=mean(tmrmse);
        minrmse=min(meanvals);
        meanrmse(ti,3)=minrmse;
        
        
        meanmae(ti,1)=mean(MAE(:,1));
        %meanmae(ti,2)=mean(MAE(:,2));
        %meanmae(ti,3)=mean(MAE(:,3));
        meanmae(ti,4)=mean(MAE(:,4));
        meanmae(ti,5)=mean(MAE(:,5));
        meanrmse(ti,1)=mean(RMSE(:,1));
        %meanrmse(ti,2)=mean(RMSE(:,2));
        %meanrmse(ti,3)=mean(RMSE(:,3));
        meanrmse(ti,4)=mean(RMSE(:,4));
        meanrmse(ti,5)=mean(RMSE(:,5));
    end
end
%return;
%Generate bar graph for prediction
%indexiplot=[1 3 5 7 9 11 13]; %0,10,20,40,60

%% plot out prediction results
indexiplot=[1 2 3 4 5 6]; %0,10,20,40,60
f_b=figure();
hold on;
grid on;
bardata1=zeros(30,1);   bardata2=zeros(30,1); bardata3=zeros(30,1);
bardata4=zeros(30,1);   bardata5=zeros(30,1);
tmindex=1:30;
for tmi=1:30
    tmr=rem(tmi,5);
    if tmr==0
        tmindexi=indexiplot(floor(tmi/5));
    else
        tmindexi=indexiplot(floor(tmi/5)+1);
    end
    if tmr==1
        bardata1(tmi)=meanmae(tmindexi,1);
    elseif tmr==2
        bardata2(tmi)=meanmae(tmindexi,2);
    elseif tmr==3
        bardata3(tmi)=meanmae(tmindexi,3);
    elseif tmr==4
        bardata4(tmi)=meanmae(tmindexi,4);
    elseif tmr==0
        bardata5(tmi)=meanmae(tmindexi,5);
    end
end
h1=bar(bardata1,'r');
h2=bar(bardata2,'g');
h3=bar(bardata3,'b');
h4=bar(bardata4,'c');
h5=bar(bardata5,'y');

legStr={'li','SVR_{li}','SVR_{rbf}','RBR','RShift'};
set(gca,'Fontsize', 16);
%xlabels={'0','10s','20s','40s','1min'};
xlabels={'0','1min','2min','3min','4min','5min'};
%xlabel(xStr);
ylabel('Average MAE');
axis([0 31 0.02 0.11]);
set(gca,'Fontsize', 16);
set(gca,'XTick',3:5:28);
set(gca,'XTickLabel',xlabels);
legend(legStr,'location','North','Orientation','horizontal');
legend boxoff;
set(gcf,'PaperUnits','inches','PaperPosition',[0 0 10 6]);
hold off;
outputf='predmae.jpg';
print(f_b,'-djpeg ',outputf);
print(f_b,'-depsc','-r300',[outputf(1:end-4) '.eps']);




indexiplot=[1 2 3 4 5 6]; %0,10,20,40,60
f_b=figure();
hold on;
grid on;
bardata1=zeros(30,1);   bardata2=zeros(30,1); bardata3=zeros(30,1);
bardata4=zeros(30,1);   bardata5=zeros(30,1);
tmindex=1:30;
for tmi=1:30
    tmr=rem(tmi,5);
    if tmr==0
        tmindexi=indexiplot(floor(tmi/5));
    else
        tmindexi=indexiplot(floor(tmi/5)+1);
    end
    if tmr==1
        bardata1(tmi)=meanrmse(tmindexi,1);
    elseif tmr==2
        bardata2(tmi)=meanrmse(tmindexi,2);
    elseif tmr==3
        bardata3(tmi)=meanrmse(tmindexi,3);
    elseif tmr==4
        bardata4(tmi)=meanrmse(tmindexi,4);
    elseif tmr==0
        bardata5(tmi)=meanrmse(tmindexi,5);
    end
end
h1=bar(bardata1,'r');
h2=bar(bardata2,'g');
h3=bar(bardata3,'b');
h4=bar(bardata4,'c');
h5=bar(bardata5,'y');

legStr={'li','SVR_{li}','SVR_{rbf}','RBR','RShift'};
set(gca,'Fontsize', 16);
%xlabels={'0','10s','20s','40s','1min'};
xlabels={'0','1min','2min','3min','4min','5min'};
%xlabel(xStr);
ylabel('Average RMSE');
axis([0 31 0.08 0.2]);
set(gca,'Fontsize', 16);
set(gca,'XTick',3:5:28);
set(gca,'XTickLabel',xlabels);
legend(legStr,'location','North','Orientation','horizontal');
legend boxoff;
set(gcf,'PaperUnits','inches','PaperPosition',[0 0 10 6]);
hold off;
outputf='predrmse.jpg';
print(f_b,'-djpeg ',outputf);
print(f_b,'-depsc','-r300',[outputf(1:end-4) '.eps']);




f1=figure();
legStr={'li','SVR_{li}','SVR_{rbf}'};
xStr='Solar Panel #';
hold on;
grid on;
xlabel(xStr,'Fontsize', 16);
ylabel('Mean Absolute Error','Fontsize', 16);
plot(1:BPS_N,MAE(:,1),'--xr','linewidth', 2);
plot(1:BPS_N,MAE(:,2),'--+g','linewidth', 2);
plot(1:BPS_N,MAE(:,3),'--ob','linewidth', 2);

set(gca,'Fontsize', 16);
axis([1 25 0.055 0.1]);
set(gca,'XTick',1:25);
set(gca,'XTickLabel',1:25);
legend(legStr,'location','North','Orientation','horizontal');
legend boxoff;
set(gcf,'PaperUnits','inches','PaperPosition',[0 0 10 6]);
hold off;
outputf='modelsmae.jpg';
%print(f1,'-djpeg ',outputf);
%print(f1,'-depsc','-r300',[outputf(1:end-4) '.eps']);


f2=figure();
legStr={'li','SVR_{li}','SVR_{rbf}'};
xStr='Solar Panel #';
hold on;
grid on;
xlabel(xStr,'Fontsize', 16);
ylabel('Root Mean Squre Error','Fontsize', 16);
plot(1:BPS_N,RMSE(:,1),'--xr','linewidth', 2);
plot(1:BPS_N,RMSE(:,2),'--+g','linewidth', 2);
plot(1:BPS_N,RMSE(:,3),'--ob','linewidth', 2);

set(gca,'Fontsize', 16);
axis([1 25 0.09 0.15]);
set(gca,'XTick',1:25);
set(gca,'XTickLabel',1:25);
legend(legStr,'location','North','Orientation','horizontal');
legend boxoff;
set(gcf,'PaperUnits','inches','PaperPosition',[0 0 10 6]);
hold off;
outputf='modelsrmse.jpg';
%print(f2,'-djpeg ',outputf);
%print(f2,'-depsc','-r300',[outputf(1:end-4) '.eps']);

return;
sec1=1:10;
sec2=11:20;
sec3=21:25;
secs={sec1,sec2,sec3};
maes=zeros(3,3);
rmses=zeros(3,3);
for seci=1:3
   tmsec=secs{seci};
   maes(seci,1)=mean(MAE(tmsec,1));
   maes(seci,2)=mean(MAE(tmsec,2));
   maes(seci,3)=mean(MAE(tmsec,3));
   rmses(seci,1)=mean(RMSE(tmsec,1));
   rmses(seci,2)=mean(RMSE(tmsec,2));
   rmses(seci,3)=mean(RMSE(tmsec,3));
end
f3=figure();
hold on;
legStr={'li','SVR_{li}','SVR_{rbf}'};
set(gca,'Fontsize', 16);
h1=bar([maes(1,1) 0 0 maes(2,1) 0 0  maes(3,1) 0 0],'r');
h2=bar([0 maes(1,2) 0 0 maes(2,2) 0 0 maes(3,2) 0],'b');
h3=bar([0 0 maes(1,3) 0 0 maes(2,3) 0 0 maes(3,3)],'y');
xlabels={'Panel 1~10','Panel 11~20','Panel 21~25'};
%xlabel(xStr);
ylabel('Average of MAE');
axis([0 10 0.05 0.095]);
set(gca,'Fontsize', 16);
set(gca,'XTick',2:3:8);
set(gca,'XTickLabel',xlabels);
legend(legStr,'location','North','Orientation','horizontal');
legend boxoff;
set(gcf,'PaperUnits','inches','PaperPosition',[0 0 10 6]);
hold off;
outputf='secmae.jpg';
print(f3,'-djpeg ',outputf);
print(f3,'-depsc','-r300',[outputf(1:end-4) '.eps']);




MAE