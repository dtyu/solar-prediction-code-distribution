% This function is to test extraction of hight altittude cloud from TS3,TSI1 and TSI3,TSI2 pairs
% using only one frame(frame t). The basice idea is that TSI3 is in the middle and make use of
% height transformation based on the displacement, we can find the most similiar displacement on
% images.
function test_HighCld
set(0,'DefaultFigureVisible', 'on');
% set(0,'DefaultFigureVisible', 'off');

mtsi_startup;

tsiDirs={'./cloudy_multilayer/tsi1_undist_20130605/','./cloudy_multilayer/tsi2_undist_20130605/','./cloudy_multilayer/tsi3_undist_20130605/'};
otsiDirs={'./otsi1/','./otsi2/','./otsi3/'};


dataDir='/cewit/home/zpeng/workdata/mtsi_stich/';
% dataDir='/data/workdata/mtsi_stich/';


tsi1Dir=tsiDirs{1};
tsi2Dir=tsiDirs{2};
tsi3Dir=tsiDirs{3};
otsi1Dir=otsiDirs{1};
otsi2Dir=otsiDirs{2};
otsi3Dir=otsiDirs{3};
mkdir(otsi1Dir);
mkdir(otsi2Dir);
mkdir(otsi3Dir);
alltsi1=dir(tsi1Dir);
[nfiles1,~]=size(alltsi1);
imma1_total=zeros(UNDISTH,UNDISTW,3);
imma2_total=zeros(UNDISTH,UNDISTW,3);
imma3_total=zeros(UNDISTH,UNDISTW,3);
N1=zeros(UNDISTH,UNDISTW);
N2=zeros(UNDISTH,UNDISTW);
N3=zeros(UNDISTH,UNDISTW);

imma_avgf='./imma_avgf.mat';
t=load(imma_avgf,'imma1_avg','imma2_avg','imma3_avg');
imma1_avg=t.imma1_avg;
imma2_avg=t.imma2_avg;
imma3_avg=t.imma3_avg;

OutputPath='./tmptestme.mat';
InnerBlockSize=20;
BlockSize=50;
SearchWinSize=50;
CorThreshold=0.5;
BlackThreshold=0;

MEOutDir='./ME_test_img';
FillOutDir='./Fillimg';
outDir='highcldDir_0.6/';
ncc_thres=0.6;

outDir=FormatDirName(outDir);
if ~exist(outDir,'dir')
    mkdir(outDir);
end

MEOutDir=FormatDirName(MEOutDir);
FillOutDir=FormatDirName(FillOutDir);
if ~exist(MEOutDir,'dir')
    mkdir(MEOutDir);
end
if ~exist(FillOutDir,'dir')
    mkdir(FillOutDir);
end
ONEMIN_DN=datenum([0 0 0 0 1 0]);
TENSECS_DN=datenum([0 0 0 0 0 10]);





for i =1:nfiles1
    tmpimat=sprintf('tmp%d.mat',i);
    if ~exist(tmpimat,'file')
    %     if 1
        filename=alltsi1(i).name;
        f1=sprintf('%s%s',tsi1Dir,filename);
        tmdv=GetDVFromImg(filename);
        if tmdv==0
            continue;
        end
        
        
        tmdn=datenum(tmdv);
        %     if tmdn<datenum([2013 06 05 15 00 50])
        %         continue;
        %     end
        %     if tmdn~=datenum([2013 06 05 14 42 50])
        %         continue;
        %     end
        
        tmdn_1maf=tmdn+TENSECS_DN;
        tmdn_1mbf=tmdn-ONEMIN_DN;
        f2_1maf=GetImgFromDV_BNL(datevec(tmdn_1maf),TSI2);
        f2_1mbf=GetImgFromDV_BNL(datevec(tmdn_1mbf),TSI2);
        f3_1maf=GetImgFromDV_BNL(datevec(tmdn_1maf),TSI3);
        filename2=GetImgFromDV_BNL(tmdv,TSI2);
        filename3=GetImgFromDV_BNL(tmdv,TSI3);
        
        %     outputf=sprintf('%s%s.png',outDir,filename3);
        %     if exist(outputf,'file')
        %         continue;
        %     end
        
        %     f2=sprintf('%s%s.bmp',tsi2Dir,filename2(1:end-4));
        %     f3=sprintf('%s%s.bmp',tsi3Dir,filename3(1:end-4));
        f2=sprintf('%s%s.jpg',tsi2Dir,filename2(1:end-4));
        f2_1maf=sprintf('%s%s.jpg',tsi2Dir,f2_1maf(1:end-4));
        f2_1mbf=sprintf('%s%s.jpg',tsi2Dir,f2_1mbf(1:end-4));
        f3=sprintf('%s%s.jpg',tsi3Dir,filename3(1:end-4));
        f3_1maf=sprintf('%s%s.jpg',tsi3Dir,f3_1maf(1:end-4));
        try
            [imma1,mf1]=tmp_GetImgMatFromDataDir(f1,TSI1,dataDir);
            [imma2,mf2]=tmp_GetImgMatFromDataDir(f2,TSI2,dataDir);
            [imma3,mf3]=tmp_GetImgMatFromDataDir(f3,TSI3,dataDir);
        catch
            disp(['can not find the file please check' mf1 ' ' mf2 ' ' mf3]);
            continue;
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Test of three images;(DEBUG USED)
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %     figure();
        %     subplot(2,3,1);
        %     imshow(imma1);
        %     subplot(2,3,2);
        %     imshow(imma3);
        %     subplot(2,3,3);
        %     imshow(imma2);
        %     subplot(2,3,4);
        %     imshow(TO_rgb2lum(imma1));
        %     subplot(2,3,5);
        %     imshow(TO_rgb2lum(imma3));
        %     subplot(2,3,6);
        %     imshow(TO_rgb2lum(imma2));
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        imma1_g=TO_rgb2lum(imma1);
        imma2_g=TO_rgb2lum(imma2);
        imma3_g=TO_rgb2lum(imma3);
        [bstarty,bstartx,bendy,bendx,nBlocky,nBlockx,blockvals]=DivideBlocks(...
            rgb2gray(imma3),[InnerBlockSize InnerBlockSize]);
        
        H_blks=zeros(nBlocky,nBlockx);
        cc_blks=zeros(nBlocky,nBlockx);
        HMV31_x=zeros(nBlocky,nBlockx);
        HMV31_y=zeros(nBlocky,nBlockx);
        HMV32_x=zeros(nBlocky,nBlockx);
        HMV32_y=zeros(nBlocky,nBlockx);
        for bi=1:nBlocky
            for bj=1:nBlockx
                %             if bi~=17 || bj~=12
                %                 continue;
                %             end
                tmsy=bstarty(bi,bj);
                tmsx=bstartx(bi,bj);
                tmey=bendy(bi,bj);
                tmex=bendx(bi,bj);
                tmblk3=imma3_g(tmsy:tmey,tmsx:tmex);
                if length(find(tmblk3==0))>10
                    continue;
                end
                if std(single(tmblk3(:)))==0
                    continue;
                end
                
                %             [H,maxcc,HMV_x,HMV_y]=test_ncc3d_v2(imma1_g,imma2_g,imma3_g,[tmsy tmsx],[InnerBlockSize InnerBlockSize],3000:1000:20000);
                [H,maxcc,HMV_x,HMV_y]=...
                    ncc3d_HighCld(tmdv,[tmsy tmsx],[InnerBlockSize InnerBlockSize],dataDir,3000:1000:20000,ncc_thres);
                
                
                
                HMV31_x(bi,bj)=HMV_x(1);
                HMV32_x(bi,bj)=HMV_x(2);
                HMV31_y(bi,bj)=HMV_y(1);
                HMV32_y(bi,bj)=HMV_y(2);
                H_blks(bi,bj)=H;
                cc_blks(bi,bj)=maxcc;
            end
        end
        
        trustindma=cc_blks>3*ncc_thres&H_blks>5000;
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Plot of trustindma
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %     imma3_show=zeros(size(imma3));
        %     imma1_show=zeros(size(imma1));
        %     imma2_show=zeros(size(imma2));
        %
        %     for bi=1:nBlocky
        %         for bj=1:nBlockx
        %             if ~trustindma(bi,bj)
        %                 continue;
        %             end
        %             tmsy=bstarty(bi,bj);
        %             tmsx=bstartx(bi,bj);
        %             tmsy1=tmsy+HMV31_y(bi,bj);
        %             tmsx1=tmsx+HMV31_x(bi,bj);
        %             tmsy2=tmsy+HMV32_y(bi,bj);
        %             tmsx2=tmsx+HMV32_x(bi,bj);
        %             tmsx=bstartx(bi,bj);
        %             imma3_show=TO_SetRGBVals(imma3,imma3_show,[tmsy tmsx],[InnerBlockSize InnerBlockSize]);
        %             imma2_show=TO_SetRGBVals(imma2,imma2_show,[tmsy2 tmsx2],[InnerBlockSize InnerBlockSize]);
        %             imma1_show=TO_SetRGBVals(imma1,imma1_show,[tmsy1 tmsx1],[InnerBlockSize InnerBlockSize]);
        %             %             imma3g_show(tmsy:tmsy+InnerBlockSize-1,tmsx:tmsx+InnerBlockSize-1)=...
        %             %                 imma3_g(tmsy:tmsy+InnerBlockSize-1,tmsx:tmsx+InnerBlockSize-1);
        %             %             imma2g_show(tmsy2:tmsy2+InnerBlockSize-1,tmsx2:tmsx2+InnerBlockSize-1)=...
        %             %                 imma2_g(tmsy2:tmsy2+InnerBlockSize-1,tmsx2:tmsx2+InnerBlockSize-1);
        %             %             imma1g_show(tmsy1:tmsy1+InnerBlockSize-1,tmsx1:tmsx1+InnerBlockSize-1)=...
        %             %                 imma1_g(tmsy1:tmsy1+InnerBlockSize-1,tmsx1:tmsx1+InnerBlockSize-1);
        %         end
        %     end
        %     fig1=figure();
        %     subplot(2,3,1);
        %     imshow(uint8(imma1_show));
        %     subplot(2,3,2);
        %     imshow(uint8(imma3_show));
        %     subplot(2,3,3);
        %     imshow(uint8(imma2_show));
        %     subplot(2,3,4);
        %     imshow(uint8(imma1_g));
        %     subplot(2,3,5);
        %     imshow(uint8(imma3_g));
        %     subplot(2,3,6);
        %     imshow(uint8(imma2_g));
        %     print(fig1,outputf,'-dpng');
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        
        meangreyThres=12;
        segC=1000;
        [ConnMap_s,ConnValid_s]=test_stdblkmerge(trustindma,blockvals,segC,10,0.5,meangreyThres);
        
        nConn_s=max(max(ConnMap_s));
        corrblks=cell(nConn_s,1);
        minsy1=zeros(nConn_s,1);
        minsx1=zeros(nConn_s,1);
        maxey1=zeros(nConn_s,1);
        maxex1=zeros(nConn_s,1);
        minsy2=zeros(nConn_s,1);
        minsx2=zeros(nConn_s,1);
        maxey2=zeros(nConn_s,1);
        maxex2=zeros(nConn_s,1);
        minsy3=zeros(nConn_s,1);
        minsx3=zeros(nConn_s,1);
        maxey3=zeros(nConn_s,1);
        maxex3=zeros(nConn_s,1);
        H_conn=zeros(nConn_s,1);
        
        for ci=1:nConn_s
            tmind=find(ConnMap_s==ci);
            tmConn_ind=tmind;
            tmsy=bstarty(tmind);
            tmsx=bstartx(tmind);
            tmey=bendy(tmind);
            tmex=bendx(tmind);
            minsy=min(tmsy);
            minsx=min(tmsx);
            maxey=max(tmey);
            maxex=max(tmex);
            tmceny=round((minsy+maxey)/2);
            tmcenx=round((minsx+maxex)/2);
            recH=maxey-minsy+1;
            recW=maxex-minsx+1;
            tmfillind=find(bstarty>=minsy&bendy<=maxey&bstartx>=minsx&bendx<=maxex&ConnMap_s~=ci);
            tml=length(tmfillind);
            tmblkmask=true(size(imma2_g));
            for li=1:tml
                tmind=tmfillind(li);
                tmsy=bstarty(tmind);
                tmsx=bstartx(tmind);
                tmey=bendy(tmind);
                tmex=bendx(tmind);
                tmblkmask(tmsy:tmey,tmsx:tmex)=false;
            end
            [H,maxcc,HMV_x,HMV_y]=...
                ncc3d_HighCld(tmdv,[minsy minsx],[recH recW],dataDir,3000:1000:20000,0,tmblkmask);
            
            
            minsx1(ci)=minsx+HMV_x(1);
            minsx2(ci)=minsx+HMV_x(2);
            minsx3(ci)=minsx;
            minsy1(ci)=minsy+HMV_y(1);
            minsy2(ci)=minsy+HMV_y(2);
            minsy3(ci)=minsy;
            maxex1(ci)=minsx1(ci)+recW;
            maxex2(ci)=minsx2(ci)+recW;
            maxex3(ci)=minsx3(ci)+recW;
            maxey1(ci)=minsy1(ci)+recH;
            maxey2(ci)=minsy2(ci)+recH;
            maxey3(ci)=minsy3(ci)+recH;
            H_conn(ci)=H;
        end
        save(tmpimat);
    else
        load(tmpimat);
    end
    
    
    %     fig1=figure();
    %     subplot(1,3,2);
    %     imshow(imma3);
    %     hold on;
    %     for ci=1:nConn_s
    %         minsx=minsx3(ci);
    %         minsy=minsy3(ci);
    %         recW=maxex3(ci)-minsx3(ci)+1;
    %         recH=maxey3(ci)-minsy3(ci)+1;
    %         tmcenx=(maxex3(ci)+minsx3(ci))/2;
    %         tmceny=(maxey3(ci)+minsy3(ci))/2;
    %         rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
    %
    %         HMV_x1=minsx1(ci)-minsx;
    %         HMV_y1=minsy1(ci)-minsy;
    %         HMV_x2=minsx2(ci)-minsx;
    %         HMV_y2=minsy2(ci)-minsy;
    %         quiver(tmcenx,tmceny,HMV_x1,HMV_y1);
    %         quiver(tmcenx,tmceny,HMV_x2,HMV_y2);
    %         H=H_conn(ci);
    %
    %         if H>=10000
    %             text(tmcenx,tmceny,['\color{red}' '\geq 10000'],'FontSize',18);
    %         else
    %             text(tmcenx,tmceny,['\color{red}' sprintf('%d',round(H))],'FontSize',18);
    %         end
    %         %text(tmcenx,tmceny,['\color{red}' sprintf('%d',round(H(choosei)))],'FontSize',18);
    %     end
    %     hold off;
    %
    %     subplot(1,3,1);
    %     imshow(imma1);
    %     hold on;
    %     for ci =1:nConn_s
    %         if minsx1(ci)==0
    %             continue;
    %         end
    %         recW=maxex1(ci)-minsx1(ci)+1;
    %         recH=maxey1(ci)-minsy1(ci)+1;
    %         rectangle('Position',[minsx1(ci) minsy1(ci) recW recH],'LineStyle','--','EdgeColor','r');
    %     end
    %     hold off;
    %
    %     subplot(1,3,3);
    %     imshow(imma2);
    %     hold on;
    %     for ci =1:nConn_s
    %         if minsx1(ci)==0
    %             continue;
    %         end
    %         recW=maxex2(ci)-minsx2(ci)+1;
    %         recH=maxey2(ci)-minsy2(ci)+1;
    %         rectangle('Position',[minsx2(ci) minsy2(ci) recW recH],'LineStyle','--','EdgeColor','r');
    %     end
    %     hold off;
    
    
end




end