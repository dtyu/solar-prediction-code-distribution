%MergeAzimuthMatFile: This function is used for generate azimuthfile from
%   testfill directory and previous result of azimuth,sbmask,and hamask.
%   The result of new azimuth will be under the main directory of this
%   function. Other sbmask files and hamask files will be copied to its
%   local sbmaskDir and hamaskDir
%   azimuth,zenith
%   IN:
%       presbmaskDir        --  Directory, previous sbmaskDir
%       prehamaskDir        --  Directory, previous hamaskDir
%       prematfile          --  Mat file, previous azimuth.mat
%       testfilldir(in)     --  Directory, containning all the clear sky images
%       sbmaskdir(in)       --  Directory, containing sbmasks
%       hamaskdir(in)       --  Directory, containing hamasks
%       UndistortionParameters.mat(in) --   original image information
%
%   OUT:
%       azimuth.mat         --  clear sky library
%
function MergeAzimuthMatFile(presbmaskDir,prehamaskDir,prematfile)
load('UndistortionParameters.mat');   %need for site location information
testfillDir='testfill/';    %internal input
sbmaskDir='sbmask/';        %internal input
hamaskDir='hamask/';        %internal input

if(presbmaskDir(end)~='/')
    presbmaskDir=sprintf('%s%s',presbmaskDir,'/');
end
if(prehamaskDir(end)~='/')
    prehamaskDir=sprintf('%s%s',prehamaskDir,'/');
end

%   Check previous azimuth integrity
matdata=load(prematfile);
prets=matdata.tsdn;
[m,~]=size(prets);
for i=1:m
    tmdv=datevec(prets(i));
    twpfilename=GetImgFromDV_TWPC1(tmdv);
    matfilename=sprintf('%s%s',twpfilename,'.mat');
    presbmaskf=sprintf('%s%s',presbmaskDir,matfilename);
    prehamaskf=sprintf('%s%s',pregamaskDir,matfilename);
    sbmaskf=sprintf('%s%s',sbmaskDir,matfilename);
    hamaskf=sprintf('%s%s',gamaskDir,matfilename);
    if(exist(presbmaskf,'file')==0||exist(prehamaskf,'file')==0)
        disp('ERROR: the sbmask or hamask does not exist! Previous azimuth.mat extraction has some errors');
        break;
    end
    copyfile(presbmaskf,sbmaskf);
    copyfile(prehamaskf,sbhaskf);
    datevector=tmdv;
    time.year    =  datevector(1);
    time.month   =  datevector(2);
    time.day     =  datevector(3);
    time.hour    =  datevector(4);
    time.min    =   datevector(5);
    time.sec    =   datevector(6);
    time.UTC    =   0;
    sun = sun_position(time, location);
    az=sun.azimuth/180*pi;
    ze=sun.zenith/180*pi;
    if(matdata.azs(i)~=az||matdata.zes(i)~=ze)
        disp('ERROR: azs and zes do not match the date number');
        break;
    end
    
end

allfiles=dir(sbmaskDir);
[m,~]=size(allfiles);
allfiles=dir(hamaskDir);
[m1,~]=size(allfiles);
if(m~=m1)
    disp('ERROR: sbmask does not match hamask!');
end

tsdn=zeros(m,1);
azs=zeros(m,1);
zes=zeros(m,1);
for i=1 :m
    if(allfiles(i).isdir~=1)
        filename=allfiles(i).name;
        if(strcmpi(filename(end-3:end),'.mat')==0)
            disp('ERROR: mask file is not a .mat file,please check your input!');
            break;
        end
        sbmaskfp=sprintf('%s%s',sbmaskDir,filename);
        hamaskfp=sprintf('%s%s',hamaskDir,filename);
        if(exist(sbmaskfp,'file')==0||exist(hamaskfp,'file')==0)
            disp('ERROR: the sbmask or hamask does not exist!');
            break;
        end
        datevector=GetDVFromImg(filename);
        time.year    =  datevector(1);
        time.month   =  datevector(2);
        time.day     =  datevector(3);
        time.hour    =  datevector(4);
        time.min    =   datevector(5);
        time.sec    =   datevector(6);
        time.UTC    =   0;
        sun = sun_position(time, location);
        az=sun.azimuth/180*pi;
        ze=sun.zenith/180*pi;
        azs(i)=az;
        zes(i)=ze;
        tsdn(i)=datenum(datevector);
    end
end
tsdn=tsdn(3:end);
azs=azs(3:end);
zes=zes(3:end);

outputf='azimuth.mat';
save(outputf,'tsdn','azs','zes');
end