function [predicted_labels,predicted_values]=svmpredict_li(xtrain,w,b)
%w=model.SVs'*model.sv_coef;b=-model.rho;   % for libsvm

tml=length(w);
predicted_values=0;
for wi=1:tml
    predicted_values=predicted_values+w(wi)*xtrain(:,wi);
end
predicted_values=predicted_values+b;
predicted_labels=sign(predicted_values);




end