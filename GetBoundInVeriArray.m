%   Suppose Right now image is grey scale image which has 0-255 values, But
%   In previous step of preprocessing, only two level of grey scale left.
function [upp,lowp]=...
    GetBoundInVeriArray(vertiranx,vertirany,bthres,brightthres,imgin) %#ok<INUSD,INUSL>
[tmpm,~]=size(vertiranx);
if(tmpm==0)
    %   means no vertical pos in imgin
    upp=[-9999,-9999];
    lowp=[-9999,-9999];
    return;
end
ceni=ceil(tmpm/2);
isfound=0;
for loopj=ceni:-1:3
    if(vertirany(loopj)==-1)
        continue;
    end
    grayv=imgin(vertirany(loopj),vertiranx(loopj));
    grayv1=imgin(vertirany(loopj-1),vertiranx(loopj-1));
    dist1=abs(double(grayv)-double(grayv1));
    if(dist1>bthres)
        isfound=1;
        lowp=[vertiranx(loopj),vertirany(loopj)];
        break;      %first one then treat it as boundary
    end
end
if(isfound~=1)
    lowp=[-9999,-9999];      % no such boundary
end

isfound=0;
for loopj=ceni:1:tmpm-2
    if(vertirany(loopj)==-1)
        continue;
    end
    grayv=imgin(vertirany(loopj),vertiranx(loopj));
    grayv1=imgin(vertirany(loopj+1),vertiranx(loopj+1));
    %                 grayv2=imgin(round(vertirany(loopj+2)),round(vertiranx(loopj+2)));
    dist1=abs(double(grayv)-double(grayv1));
    if(dist1>bthres)
        %find possible edge points get the far point
        isfound=1;
        upp=[vertiranx(loopj),vertirany(loopj)];
        break;  %first one then treat it as boundary
    end
end
if(isfound~=1)
    upp=[-9999,-9999];      % no such boundary
end


end