% previous version is test_cldHdection
%   New version is based on modules
%   VERSION 1.0     2013-10-04
function mtsipipe(startdv,enddv,mode,cldmodelmatf,configfpath)
%%
bMulti=modepaser(mode,'multicore');
bServer=modepaser(mode,'server');
bPlot=modepaser(mode,'plot');
bRun=modepaser(mode,'run');
bPlotVisible=modepaser(mode,'visible');
if bPlotVisible
    set(0,'DefaultFigureVisible', 'on');
else
    set(0,'DefaultFigureVisible', 'off');
end




run(configfpath);

%startdv=[2013 05 14 14 0 0];
%startdv=[2013 05 14 17 54 0];
%enddv=[2013 05 15 0 0 0];
startdn=datenum(startdv);
enddn=datenum(enddv);
% InnerBlockSize=10;
% ConnMinSize=10;
% ConnMaxSize=60;
NextFrameMVSearchWinSize=15;
H_sran_low=1000:100:5000;
H_sran_high=5500:500:20000;
H_sran_simple=[2000:500:5000 5500:1000:20000];
H_sran=[H_sran_low H_sran_high];
ncc_thres=0.6;
ncc_thres_tsi32=2.1;        % ncc31,32 + ncc31,32 + ncc21,22
ncc_cloudfield_trust=0.7;   % ncc value which consider the movement is reasonable for valid cloud field

LOWCLDHTHRES = 5000;        % Treat as low altittude
HREF_DIFF_TOLERANCE=500;    % difference to reference height will be tolerated or considered the same as reference
MAXNUMOFLAYER=2;            % maximum layers for extraction


%NCCFSOutMatDir='ncc_fs_all';
HRefDir='HistRef/';
HRefDir_recheck='HistRef_recheck/';
HRefDir=FormatDirName(HRefDir);
HRefDir_recheck=FormatDirName(HRefDir_recheck);
if ~exist(HRefDir,'dir')
    mkdir(HRefDir);
end
if ~exist(HRefDir_recheck,'dir')
    mkdir(HRefDir_recheck);
end


%NCCFSOutMatDir='ncc_fs_all_server/ncc_fs_all/'; % ncc full search for ncc3d_32 results using multicore
%RefOutMatDir='HRef_matout/'; % ncc full search for ncc3d_32 results using multicore
%NCCFSOutMatDir=FormatDirName(NCCFSOutMatDir);
% if ~exist(NCCFSOutMatDir,'dir')
%     mkdir(NCCFSOutMatDir);
% end
% RefOutMatDir=FormatDirName(RefOutMatDir);
% if ~exist(RefOutMatDir,'dir')
%     mkdir(RefOutMatDir);
% end
% set(0,'DefaultFigureVisible', 'on');
% set(0,'DefaultFigureVisible', 'off');



ONEMIN_DN=datenum([0 0 0 0 1 0]); %#ok<NASGU>

tmdn=startdn;
oddns=[];
i=0;
while(tmdn<enddn)
    tmdn=startdn+i*TENSECS_DN;
    oddns=[oddns;tmdn];
    i=i+1;
end
oddvs=datevec(oddns);
oddns_n=length(oddns);


if bServer && bMulti
    nMatlabinst=32;
    severtype=64;
elseif  ~bServer && bMulti
    nMatlabinst=8;
    severtype=64;
end
if bServer
    dataDir='~/workdata/mtsi_stich/';
else
    dataDir='/data/workdata/mtsi_stich/';
end
dataDir=FormatDirName(dataDir);


OutputPath='./tmptestme.mat'; %#ok<NASGU>

HRefDir='cldtrack_hist';
HRefDir=FormatDirName(HRefDir);
if ~exist(HRefDir,'dir')
    mkdir(HRefDir);
end
SkyColorTestOutDir='SkyColorTestOut_histeq';
SkyColorTestOutDir=FormatDirName(SkyColorTestOutDir);
if ~exist(SkyColorTestOutDir,'dir')
    mkdir(SkyColorTestOutDir);
end

%HREF_PREF=[1200 3700];

for i =1:oddns_n-1
    tmdn=oddns(i);
    tmdv=oddvs(i,:);
    tmdv_1faf=oddvs(i+1,:);
    %tmdn_1faf=oddns(i+1);
    %if tmdn<datenum([2013 05 14 19 37 0])
    %if tmdn<datenum([2013 06 17 15 29 40])
    %if tmdn<datenum([2013 06 17 14 12 40]) % choose multilay cldmask
    %if tmdn<datenum([2013 06 09 17 13 5]) %choose for multi cld mask
    %if tmdn<datenum([2013 06 17 19 59 30]) % choose for exteme cloudy cldmask
    %if tmdn<datenum([2013 06 17 14 13 10]) %used for HRef output mat unittest
%     if tmdn<datenum([2013 06 17 15 11 50]) % used for build skyfill.bmp
%             continue;
%         end
    %if tmdn<datenum([2013 06 17 14 12 00]) %for cldtrack
    %if tmdn<datenum([2013 06 17 17 34 50]) % for single multilayer filling
    if tmdn<datenum([2013 05 14 15 09 50]) % for multiple multilayer filling
        continue;
    end
    if 0~=rem(i,30)
        continue;
    end
    %---------------------------------------------------------------------------------%
    % Module 1: Cloud Detection
    %---------------------------------------------------------------------------------%
    [imma1_cldma_n,imma2_cldma_n,imma3_cldma_n]=...
        clouddetector(tmdv,cldmodelmatf,dataDir,configfpath);
    
    try
        imma1_ori=tmp_GetImgMatFromDataDir(tmdv,TSI1,dataDir);
        imma2_ori=tmp_GetImgMatFromDataDir(tmdv,TSI2,dataDir);
        imma3=tmp_GetImgMatFromDataDir(tmdv,TSI3,dataDir);
        Red1 = imma3(:, :, 1);
        Green1 = imma3(:, :, 2);
        Blue1 = imma3(:, :, 3);
        HnRed1=imhist(Red1);
        HnGreen1=imhist(Green1);
        HnBlue1=imhist(Blue1);
        HnRed1(1)=0;HnGreen1(1)=0;HnBlue1(1)=0;     % 0 means not valid
        ref_sky_hist=[HnRed1 HnGreen1 HnBlue1];
    catch err
        disp(err.message);
        continue;
    end
    imma1=TO_histeq_TSISky(imma1_ori,ref_sky_hist);
    imma2=TO_histeq_TSISky(imma2_ori,ref_sky_hist);
    imma1_lum=single(TO_rgb2lum(imma1));
    imma2_lum=single(TO_rgb2lum(imma2));
    imma3_lum=single(TO_rgb2lum(imma3));
     
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % SkyColorTestOutDir plot for histeq based on imma3
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %     f1=figure();subplot(2,3,1);imshow(imma1);
    %     subplot(2,3,2);imshow(imma3);
    %     subplot(2,3,3);imshow(imma2);
    %     subplot(2,3,4);imshow(imma1_cldma);
    %     subplot(2,3,5);imshow(imma3_cldma);
    %     subplot(2,3,6);imshow(imma2_cldma);
    %     filename=GetImgFromDV_BNL(tmdv,TSI1);
    %     outputf=sprintf('%s%s',SkyColorTestOutDir,filename);
    %     print(f1,outputf,'-djpeg');
    %     continue;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % New Cloud Mask for imma using std
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %     f1=figure();subplot(2,3,1);imshow(imma1);
    %     subplot(2,3,2);imshow(imma3);
    %     subplot(2,3,3);imshow(imma2);
    %     subplot(2,3,4);imshow(imma1_cldma_n);
    %     subplot(2,3,5);imshow(imma3_cldma_n);
    %     subplot(2,3,6);imshow(imma2_cldma_n);
    %     filename=GetImgFromDV_BNL(tmdv,TSI1);
    %     outputf=sprintf('%s%s',SkyColorTestOutDir,filename);
    %     %print(f1,outputf,'-djpeg');
    %     continue;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %---------------------------------------------------------------------------------%
    % Module 2. Cloud fields extraction
    %---------------------------------------------------------------------------------%
    [CFMaskMa,nCF]=cloudfieldext(imma3_cldma_n,configfpath);
    
    
    
    %---------------------------------------------------------------------------------%
    % Module 3. History Reference
    %---------------------------------------------------------------------------------%
    %tmf=GetImgFromDV_BNL(tmdv,TSI1);
    %[HRef,MVRef]=GetHistRefLayers(HRefDir,tmdv);
    HRef=[1100 3700];
    MVRef=[];
    
    %---------------------------------------------------------------------------------%
    % Module 4. Cloud Tracking
    %   If There is H reference, then do hist check else full search
    %---------------------------------------------------------------------------------%
    if ~isempty(HRef)
        tmf=GetImgFromDV_BNL(tmdv,TSI3);
        cldtrkmatoutf=sprintf('%s%s.cldtrk.mat',HRefDir,tmf(1:end-4));
    else
        tmf=GetImgFromDV_BNL(tmdv,TSI3);
        cldtrkmatoutf=sprintf('%s%s.cldtrk.full.mat',HRefDir,tmf(1:end-4));
    end
    if ~exist(cldtrkmatoutf,'file')
    %if 1
        CldTrack(HRef,MVRef,CFMaskMa,tmdv,dataDir,cldtrkmatoutf,configfpath)
    end
    t=load(cldtrkmatoutf);
    H=t.H;
    Conn_sx=t.Conn_sx;    Conn_sy=t.Conn_sy;
    Conn_H=t.Conn_H;      Conn_W=t.Conn_W;
    HMV_x=t.HMV_x; HMV_y=t.HMV_y;
    MAXCC=t.MAXCC;
    nConn=t.nCF;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Show cloud field full search results
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     tmoutDir=sprintf('CldTrackTestout/');
%     if ~exist(tmoutDir,'dir');
%         mkdir(tmoutDir);
%     end
%     f1=figure();
%     subplot(1,3,2);
%     imshow(imma3);
%     hold on;
%     for tmi=1:nConn
%         %         if MAXCC(tmi)==0
%         %             continue;
%         %         end
%         
%         minsx=Conn_sx(tmi); minsy=Conn_sy(tmi);
%         recH=Conn_H(tmi); recW=Conn_W(tmi);
%         if MAXCC(tmi)==0
%             rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','c');
%         elseif H(tmi)<3000
%             rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
%             quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi,1),HMV_y(tmi,1));
%             quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi,2),HMV_y(tmi,2));
%         elseif H(tmi)>=3000
%             rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','y');    
%             quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi,1),HMV_y(tmi,1));
%             quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi,2),HMV_y(tmi,2));
%         end
%         %rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
%         %quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi,1),HMV_y(tmi,1));
%         %quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi,2),HMV_y(tmi,2));
%         %text(minsx+round(recW/2),minsy+round(recH/2),['\color{red} ' int2str(H(tmi))],'FontSize',18);
%     end
%     hold off;
%     subplot(1,3,3);
%     imshow(imma2);
%     hold on;
%     for tmi=1:nConn
%         if MAXCC(tmi)==0
%             continue;
%         end
%         minsx=Conn_sx(tmi)+HMV_x(tmi,1); minsy=Conn_sy(tmi)+HMV_y(tmi,1);
%         recH=Conn_H(tmi); recW=Conn_W(tmi);
%         if H(tmi)<3000
%             rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
%         else
%             rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','y');
%         end
%         %rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
%         %quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi),HMV_y(tmi));
%     end
%     hold off;
%     subplot(1,3,1);
%     imshow(imma1);
%     hold on;
%     for tmi=1:nConn
%         if MAXCC(tmi)==0
%             continue;
%         end
%         minsx=Conn_sx(tmi)+HMV_x(tmi,2); minsy=Conn_sy(tmi)+HMV_y(tmi,2);
%         recH=Conn_H(tmi); recW=Conn_W(tmi);
%         if H(tmi)<3000
%             rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
%         else
%             rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','y');
%         end
%         %rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
%         %quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi),HMV_y(tmi));
%     end
%     hold off;
%     tmoutf=sprintf('%s%s',tmoutDir,GetImgFromDV_BNL(tmdv,TSI1));
%     print(f1,tmoutf,'-djpeg');
%     %continue;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %---------------------------------------------------------------------------------%
    % Module 5. Re-check CF based on neighbors on TSI3
    %---------------------------------------------------------------------------------%
    tmf=GetImgFromDV_BNL(tmdv,TSI3);
    tsi3invalidma=imma3_lum==0;
    cldtrkmatoutf=sprintf('%s%s.cldtrk.mat',HRefDir,tmf(1:end-4));
    cldtrkmatoutf_2=sprintf('%s%s.cldtrk2.mat',HRefDir_recheck,tmf(1:end-4));
    cldtrkmatoutf_reg=sprintf('%s%s.cldtrkreg.mat',HRefDir_recheck,tmf(1:end-4));
    if ~exist(cldtrkmatoutf_2,'file')
    %if 1
        CldTrack_recheck(cldtrkmatoutf,cldtrkmatoutf_2,tsi3invalidma,configfpath);
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Show cloud field recheck results
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%         t=load(cldtrkmatoutf_2);
%     H=t.H;
%     Conn_sx=t.Conn_sx;    Conn_sy=t.Conn_sy;
%     Conn_H=t.Conn_H;      Conn_W=t.Conn_W;
%     HMV_x=t.HMV_x; HMV_y=t.HMV_y;
%     MAXCC=t.MAXCC;
%     nConn=t.nCF;
%     tmoutDir=sprintf('CldTrackTestout/');
%     if ~exist(tmoutDir,'dir');
%         mkdir(tmoutDir);
%     end
%     f1=figure();
%     subplot(1,3,2);
%     imshow(imma3);
%     hold on;
%     for tmi=1:nConn
%                 if MAXCC(tmi)==0
%                     continue;
%                 end
%         
%         minsx=Conn_sx(tmi); minsy=Conn_sy(tmi);
%         recH=Conn_H(tmi); recW=Conn_W(tmi);
%         if MAXCC(tmi)==0
%             rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','c');
%         elseif H(tmi)<3000
%             rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
%             quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi,1),HMV_y(tmi,1));
%             quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi,2),HMV_y(tmi,2));
%         elseif H(tmi)>=3000
%             rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','y');    
%             quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi,1),HMV_y(tmi,1));
%             quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi,2),HMV_y(tmi,2));
%         end
%         rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
%         quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi,1),HMV_y(tmi,1));
%         quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi,2),HMV_y(tmi,2));
%         text(minsx+round(recW/2),minsy+round(recH/2),['\color{red} ' int2str(H(tmi))],'FontSize',18);
%     end
%     hold off;
%     subplot(1,3,3);
%     imshow(imma2);
%     hold on;
%     for tmi=1:nConn
%         if MAXCC(tmi)==6
%             continue;
%         end
%         minsx=Conn_sx(tmi)+HMV_x(tmi,1); minsy=Conn_sy(tmi)+HMV_y(tmi,1);
%         recH=Conn_H(tmi); recW=Conn_W(tmi);
%         if H(tmi)<3000
%             rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
%         else
%             rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','y');
%         end
%         rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
%         quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi),HMV_y(tmi));
%     end
%     hold off;
%     subplot(1,3,1);
%     imshow(imma1);
%     hold on;
%     for tmi=1:nConn
%         if MAXCC(tmi)==0
%             continue;
%         end
%         minsx=Conn_sx(tmi)+HMV_x(tmi,2); minsy=Conn_sy(tmi)+HMV_y(tmi,2);
%         recH=Conn_H(tmi); recW=Conn_W(tmi);
%         if H(tmi)<3000
%             rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
%         else
%             rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','y');
%         end
%         rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
%         quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi),HMV_y(tmi));
%     end
%     hold off;
%     tmfilename=GetImgFromDV_BNL(tmdv,TSI1);
%     tmoutf=sprintf('%s%s.recheck.jpg',tmoutDir,tmfilename(1:end-4));
%     print(f1,tmoutf,'-djpeg');
%     %continue;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %---------------------------------------------------------------------------------%
    % Module 6. Generate Layers of CF based on current results
    %       1. Use kmeans to find centoid of MAXNUMOFLAYER
    %       2. For each centroid area heights, vote for H and MV
    %       3. Aggregation of layers  
    %---------------------------------------------------------------------------------%
    %if 1
    if ~exist(cldtrkmatoutf_reg,'file')
        CldTrack_Regulate(cldtrkmatoutf_2,cldtrkmatoutf_reg,tmdv,cldmodelmatf,dataDir,configfpath);
    end

    t=load(cldtrkmatoutf_2);
    H=t.H;
    Conn_sx=t.Conn_sx;    Conn_sy=t.Conn_sy;
    Conn_H=t.Conn_H;      Conn_W=t.Conn_W;
    HMV_x=t.HMV_x; HMV_y=t.HMV_y;
    MAXCC=t.MAXCC;
    nConn=t.nCF;
    
    t=load(cldtrkmatoutf_reg);
    H_Reg=t.H_Reg;  Conn_Reg=t.Conn_Reg;    CldC_Reg=t.CldC_Reg;
    MV_x_Reg=t.MV_x_Reg;    MV_y_Reg=t.MV_y_Reg;
    HMV_x_Reg=t.HMV_x_Reg;    HMV_y_Reg=t.HMV_y_Reg;
    RegH=t.RegH;    RegHMV_x=t.RegHMV_x;    RegHMV_y=t.RegHMV_y;
    RegMV_x=t.RegMV_x;  RegMV_y=t.RegMV_y;
    
    
 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Show cloud field recheck results
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     tmoutDir=sprintf('CldTrackLayers/');
%     if ~exist(tmoutDir,'dir');
%         mkdir(tmoutDir);
%     end
%     f1=figure();
%     subplot(1,3,2);
%     imshow(imma3);
%     hold on;
%     bShowText1=false;   bShowText2=false;
%     for tmi=1:nConn
%         %         if MAXCC(tmi)==0
%         %             continue;
%         %         end
%         minsx=Conn_sx(tmi); minsy=Conn_sy(tmi);
%         recH=Conn_H(tmi); recW=Conn_W(tmi);
%         if RegH(tmi)==0
%             rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','c');
%         elseif RegH(tmi)==H_Reg(1)
%             rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
%             quiver(minsx+round(recW/2),minsy+round(recH/2),RegHMV_x(tmi,1),RegHMV_y(tmi,1));
%             quiver(minsx+round(recW/2),minsy+round(recH/2),RegHMV_x(tmi,2),RegHMV_y(tmi,2));
%             if bShowText1==false
%                 text(minsx+round(recW/2),minsy+round(recH/2),['\color{red} ' int2str(RegH(tmi))],'FontSize',18);
%                 bShowText1=true;    
%             end
%         elseif RegH(tmi)==H_Reg(2)
%             rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','y');    
%             quiver(minsx+round(recW/2),minsy+round(recH/2),RegHMV_x(tmi,1),RegHMV_y(tmi,1));
%             quiver(minsx+round(recW/2),minsy+round(recH/2),RegHMV_x(tmi,2),RegHMV_y(tmi,2));
%             if bShowText2==false
%                 text(minsx+round(recW/2),minsy+round(recH/2),['\color{yellow} ' int2str(RegH(tmi))],'FontSize',18);
%                 bShowText2=true;    
%             end
%         end
%         %rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
%         %quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi,1),HMV_y(tmi,1));
%         %quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi,2),HMV_y(tmi,2));
%         
%     end
%     hold off;
%     subplot(1,3,3);
%     imshow(imma2);
%     hold on;
%     for tmi=1:nConn
%         if RegH(tmi)==0
%             continue;
%         end
%         minsx=Conn_sx(tmi)+RegHMV_x(tmi,1); minsy=Conn_sy(tmi)+RegHMV_y(tmi,1);
%         recH=Conn_H(tmi); recW=Conn_W(tmi);
%         if RegH(tmi)==H_Reg(1)
%             rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
%         else
%             rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','y');
%         end
%         %rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
%         %quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi),HMV_y(tmi));
%     end
%     hold off;
%     subplot(1,3,1);
%     imshow(imma1);
%     hold on;
%     for tmi=1:nConn
%         if RegH(tmi)==0
%             continue;
%         end
%         minsx=Conn_sx(tmi)+RegHMV_x(tmi,2); minsy=Conn_sy(tmi)+RegHMV_y(tmi,2);
%         recH=Conn_H(tmi); recW=Conn_W(tmi);
%         if RegH(tmi)==H_Reg(1)
%             rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
%         else
%             rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','y');
%         end
%         %rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
%         %quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi),HMV_y(tmi));
%     end
%     hold off;
%     tmfilename=GetImgFromDV_BNL(tmdv,TSI1);
%     tmoutf=sprintf('%s%s.jpg',tmoutDir,tmfilename(1:end-4));
%     print(f1,tmoutf,'-djpeg');
%     %continue;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    
    
    
    
    
    
    %---------------------------------------------------------------------------------%
    % Module 7. Generate CF on TSI1, TSI2, and assign layers info to them
    %       1. Divide into CFs for the rest of cloud masks on TSI1 and TSI2
    %       2. Sim check only for motion vectors
    %       3. If no-match, follow the major layer
    %---------------------------------------------------------------------------------%
    imma1_ori_1faf=tmp_GetImgMatFromDataDir(tmdv_1faf,TSI1,dataDir);
    imma2_ori_1faf=tmp_GetImgMatFromDataDir(tmdv_1faf,TSI2,dataDir);
    %imma3_ori_3faf=tmp_GetImgMatFromDataDir(tmdv_1faf,TSI3,dataDir);
    
    imma1_1faf=TO_histeq_TSISky(imma1_ori_1faf,ref_sky_hist);
    imma2_1faf=TO_histeq_TSISky(imma2_ori_1faf,ref_sky_hist);
    %imma3_1faf=TO_histeq_TSISky(imma3_ori_1faf,ref_sky_hist);
    
    imma1_lum_1faf=single(TO_rgb2lum(imma1_1faf));
    imma2_lum_1faf=single(TO_rgb2lum(imma2_1faf));
    %imma3_lum=single(TO_rgb2lum(imma3));
    
    
    
    tmmask1=imma1_cldma_n;  tmmask2=imma2_cldma_n;
    for tmi=1:nConn
        if RegH(tmi)==0
            continue;
        end
        minsx1=round(Conn_sx(tmi)+RegHMV_x(tmi,2)); minsy1=round(Conn_sy(tmi)+RegHMV_y(tmi,2));
        minsx2=round(Conn_sx(tmi)+RegHMV_x(tmi,1)); minsy2=round(Conn_sy(tmi)+RegHMV_y(tmi,1));
        recH=Conn_H(tmi); recW=Conn_W(tmi);
        tmvals=false(recH,recW);
        tmmask1=logical(TO_SetImgVal_Pad([minsy1 minsx1],tmvals,tmmask1));
        tmmask2=logical(TO_SetImgVal_Pad([minsy2 minsx2],tmvals,tmmask2));
        %rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
        %quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi),HMV_y(tmi));
    end
    
    [CFMaskMa1,nCF1]=cloudfieldext(tmmask1,configfpath);
    [CFMaskMa2,nCF2]=cloudfieldext(tmmask2,configfpath);
    
    
    MV_x1=zeros(nCF1,1); MV_y1=zeros(nCF1,1);
    HMV_x1=zeros(nCF1,2); HMV_y1=zeros(nCF1,2);
    H1=zeros(nCF1,1);     MAXCC1=zeros(nCF1,1);
    Conn_H1=zeros(nCF1,1); Conn_W1=zeros(nCF1,1);
    Conn_sx1=zeros(nCF1,1);Conn_sy1=zeros(nCF1,1);
    Conn_cldcount1=zeros(nCF1,1);
    MV_x2=zeros(nCF2,1); MV_y2=zeros(nCF2,1);
    HMV_x2=zeros(nCF2,2); HMV_y2=zeros(nCF2,2);
    H2=zeros(nCF2,1);     MAXCC2=zeros(nCF2,1);
    Conn_H2=zeros(nCF2,1); Conn_W2=zeros(nCF2,1);
    Conn_sx2=zeros(nCF2,1);Conn_sy2=zeros(nCF2,1);
    Conn_cldcount2=zeros(nCF2,1);
    
    if H_Reg(2)==0
        bSingleLayer=true;
        mvpickrange=[MV_y_Reg(1) MV_x_Reg(1)];
    else
        bSingleLayer=false;
        mvpickrange=[MV_y_Reg MV_x_Reg];
    end
    
    [~,mostlayeri]=max(CldC_Reg);
    for tmi=1:nCF1
        tmind=find(CFMaskMa1==tmi);
        Conn_cldcount1(tmi)=length(tmind);
        [y,x]=ind2sub([UNDISTH,UNDISTW],tmind);
        minsy=min(y);
        minsx=min(x);
        recH=max(y)-min(y)+1;
        recW=max(x)-min(x)+1;
        Conn_sx1(tmi)=minsx;Conn_sy1(tmi)=minsy;
        Conn_H1(tmi)=recH;Conn_W1(tmi)=recW;
        cc_vals=ncc_mvpick(...
            imma1_lum,imma1_lum_1faf,[minsy minsx],[recH recW],NextFrameMVSearchWinSize,mvpickrange);
        cc_vals(cc_vals<ncc_thres)=0;
        [maxval,maxi]=max(cc_vals);
        if maxval==0
            % choose the frequent layer
            maxi=mostlayeri;
        else
            maxi=maxi;
        end
        H1(tmi)=H_Reg(maxi);
        MV_x1(tmi)=MV_x_Reg(maxi);
        MV_y1(tmi)=MV_y_Reg(maxi);
        HMV_x1(tmi,1)=HMV_x_Reg(maxi,1);
        HMV_y1(tmi,1)=HMV_y_Reg(maxi,1);
        HMV_x1(tmi,2)=HMV_x_Reg(maxi,2);
        HMV_y1(tmi,2)=HMV_y_Reg(maxi,2);
    end
    
    for tmi=1:nCF2
        tmind=find(CFMaskMa2==tmi);
        Conn_cldcount2(tmi)=length(tmind);
        [y,x]=ind2sub([UNDISTH,UNDISTW],tmind);
        minsy=min(y);
        minsx=min(x);
        recH=max(y)-min(y)+1;
        recW=max(x)-min(x)+1;
        Conn_sx2(tmi)=minsx;Conn_sy2(tmi)=minsy;
        Conn_H2(tmi)=recH;Conn_W2(tmi)=recW;
        cc_vals=ncc_mvpick(...
            imma1_lum,imma1_lum_1faf,[minsy minsx],[recH recW],NextFrameMVSearchWinSize,mvpickrange);
        cc_vals(cc_vals<ncc_thres)=0;
        [maxval,maxi]=max(cc_vals);
        if maxval==0
            % choose the frequent layer
            maxi=mostlayeri;
        else
            maxi=maxi;
        end
        H2(tmi)=H_Reg(maxi);
        MV_x2(tmi)=MV_x_Reg(maxi);
        MV_y2(tmi)=MV_y_Reg(maxi);
        HMV_x2(tmi,1)=HMV_x_Reg(maxi,1);
        HMV_y2(tmi,1)=HMV_y_Reg(maxi,1);
        HMV_x2(tmi,2)=HMV_x_Reg(maxi,2);
        HMV_y2(tmi,2)=HMV_y_Reg(maxi,2);
        
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Show cloud field recheck results
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    tmoutDir=sprintf('CldTrackLayers_12/');
    if ~exist(tmoutDir,'dir');
        mkdir(tmoutDir);
    end
    f1=figure();
    subplot(1,3,2);
    imshow(imma3);
    hold on;
    bShowText1=false;   bShowText2=false;
    for tmi=1:nConn
        minsx=Conn_sx(tmi); minsy=Conn_sy(tmi);
        recH=Conn_H(tmi); recW=Conn_W(tmi);
        if RegH(tmi)==0
            rectangle('Position',[minsx minsy recW recH],'LineStyle','-','EdgeColor','c');
        elseif RegH(tmi)==H_Reg(1)
            rectangle('Position',[minsx minsy recW recH],'LineStyle','-','EdgeColor','r');
            quiver(minsx+round(recW/2),minsy+round(recH/2),RegHMV_x(tmi,1),RegHMV_y(tmi,1));
            quiver(minsx+round(recW/2),minsy+round(recH/2),RegHMV_x(tmi,2),RegHMV_y(tmi,2));
            if bShowText1==false
                text(minsx+round(recW/2),minsy+round(recH/2),['\color{red} ' int2str(RegH(tmi))],'FontSize',18);
                bShowText1=true;
            end
        elseif RegH(tmi)==H_Reg(2)
            rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','y');
            quiver(minsx+round(recW/2),minsy+round(recH/2),RegHMV_x(tmi,1),RegHMV_y(tmi,1));
            quiver(minsx+round(recW/2),minsy+round(recH/2),RegHMV_x(tmi,2),RegHMV_y(tmi,2));
            if bShowText2==false
                text(minsx+round(recW/2),minsy+round(recH/2),['\color{yellow} ' int2str(RegH(tmi))],'FontSize',18);
                bShowText2=true;
            end
        end
    end
    
    for tmi=1:nCF1
        minsx=Conn_sx1(tmi)-HMV_x1(tmi,2); minsy=Conn_sy1(tmi)-HMV_y1(tmi,2);
        recH=Conn_H1(tmi); recW=Conn_W1(tmi);
        if H1(tmi)==0
            rectangle('Position',[minsx minsy recW recH],'LineStyle',':','EdgeColor','c');
        elseif H1(tmi)==H_Reg(1)
            rectangle('Position',[minsx minsy recW recH],'LineStyle',':','EdgeColor','r');
            quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x1(tmi,1),HMV_y1(tmi,1));
            quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x1(tmi,2),HMV_y1(tmi,2));
            if bShowText1==false
                text(minsx+round(recW/2),minsy+round(recH/2),['\color{red} ' int2str(RegH(tmi))],'FontSize',18);
                bShowText1=true;    
            end
        elseif H1(tmi)==H_Reg(2)
            rectangle('Position',[minsx minsy recW recH],'LineStyle',':','EdgeColor','y');    
            quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x1(tmi,1),HMV_y1(tmi,1));
            quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x1(tmi,2),HMV_y1(tmi,2));
            if bShowText2==false
                text(minsx+round(recW/2),minsy+round(recH/2),['\color{yellow} ' int2str(H1(tmi))],'FontSize',18);
                bShowText2=true;    
            end
        end
    end
    for tmi=1:nCF2
        minsx=Conn_sx2(tmi)-HMV_x2(tmi,1); minsy=Conn_sy2(tmi)-HMV_y2(tmi,1);
        recH=Conn_H2(tmi); recW=Conn_W2(tmi);
        if H2(tmi)==0
            rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','c');
        elseif H2(tmi)==H_Reg(1)
            rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
            quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x2(tmi,1),HMV_y2(tmi,1));
            quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x2(tmi,2),HMV_y2(tmi,2));
            if bShowText1==false
                text(minsx+round(recW/2),minsy+round(recH/2),['\color{red} ' int2str(RegH(tmi))],'FontSize',18);
                bShowText1=true;
            end
        elseif H2(tmi)==H_Reg(2)
            rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','y');
            quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x2(tmi,1),HMV_y2(tmi,1));
            quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x2(tmi,2),HMV_y2(tmi,2));
            if bShowText2==false
                text(minsx+round(recW/2),minsy+round(recH/2),['\color{yellow} ' int2str(H2(tmi))],'FontSize',18);
                bShowText2=true;
            end
        end
    end
    hold off;
    
    
    subplot(1,3,3);
    imshow(imma2);
    hold on;
    for tmi=1:nConn
        if RegH(tmi)==0
            continue;
        end
        minsx=Conn_sx(tmi)+RegHMV_x(tmi,1); minsy=Conn_sy(tmi)+RegHMV_y(tmi,1);
        recH=Conn_H(tmi); recW=Conn_W(tmi);
        if RegH(tmi)==H_Reg(1)
            rectangle('Position',[minsx minsy recW recH],'LineStyle','-','EdgeColor','r');
        else
            rectangle('Position',[minsx minsy recW recH],'LineStyle','-','EdgeColor','y');
        end
        %rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
        %quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi),HMV_y(tmi));
    end
    for tmi=1:nCF1
        if H1(tmi)==0
            continue;
        end
        minsx=Conn_sx1(tmi)-HMV_x1(tmi,2)+HMV_x1(tmi,1); minsy=Conn_sy1(tmi)-HMV_y1(tmi,2)+HMV_y1(tmi,1);
        recH=Conn_H1(tmi); recW=Conn_W1(tmi);
        if H1(tmi)==H_Reg(1)
            rectangle('Position',[minsx minsy recW recH],'LineStyle',':','EdgeColor','r');
        else
            rectangle('Position',[minsx minsy recW recH],'LineStyle',':','EdgeColor','y');
        end
        %rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
        %quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi),HMV_y(tmi));
    end
    for tmi=1:nCF2
        if H2(tmi)==0
            continue;
        end
        minsx=Conn_sx2(tmi); minsy=Conn_sy2(tmi);
        recH=Conn_H2(tmi); recW=Conn_W2(tmi);
        if H2(tmi)==H_Reg(1)
            rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
        else
            rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','y');
        end
        %rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
        %quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi),HMV_y(tmi));
    end
    hold off;
    subplot(1,3,1);
    imshow(imma1);
    hold on;
    for tmi=1:nConn
        if RegH(tmi)==0
            continue;
        end
        minsx=Conn_sx(tmi)+RegHMV_x(tmi,2); minsy=Conn_sy(tmi)+RegHMV_y(tmi,2);
        recH=Conn_H(tmi); recW=Conn_W(tmi);
        if RegH(tmi)==H_Reg(1)
            rectangle('Position',[minsx minsy recW recH],'LineStyle','-','EdgeColor','r');
        else
            rectangle('Position',[minsx minsy recW recH],'LineStyle','-','EdgeColor','y');
        end
        %rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
        %quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi),HMV_y(tmi));
    end
    
    for tmi=1:nCF1
        if H1(tmi)==0
            continue;
        end
        minsx=Conn_sx1(tmi); minsy=Conn_sy1(tmi);
        recH=Conn_H1(tmi); recW=Conn_W1(tmi);
        if H1(tmi)==H_Reg(1)
            rectangle('Position',[minsx minsy recW recH],'LineStyle',':','EdgeColor','r');
        else
            rectangle('Position',[minsx minsy recW recH],'LineStyle',':','EdgeColor','y');
        end
        %rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
        %quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi),HMV_y(tmi));
    end
    for tmi=1:nCF2
        if H2(tmi)==0
            continue;
        end
        minsx=Conn_sx2(tmi)-HMV_x2(tmi,1)+HMV_x2(tmi,2); minsy=Conn_sy2(tmi)-HMV_y2(tmi,1)+HMV_y2(tmi,2);
        recH=Conn_H2(tmi); recW=Conn_W2(tmi);
        if H2(tmi)==H_Reg(1)
            rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
        else
            rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','y');
        end
        %rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
        %quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi),HMV_y(tmi));
    end
    hold off;
    tmfilename=GetImgFromDV_BNL(tmdv,TSI1);
    tmoutf=sprintf('%s%s.jpg',tmoutDir,tmfilename(1:end-4));
    print(f1,tmoutf,'-djpeg');
    continue;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    
    
    
    
    
    %% original
    tmf=GetImgFromDV_BNL(tmdv,TSI1);
    cldtrkmatoutf_tsi12=sprintf('%s%s.cldtrk.tsi12.mat',HRefDir,tmf(1:end-4));
    %if 1
    if ~exist(cldtrkmatoutf_tsi12,'file')
        CldTrack_tsi12(cldtrkmatoutf_2,cldtrkmatoutf_tsi12);
    end
    
    
    
    
    
    
    
    
    refmatoutf=sprintf('%s%s.mat',RefOutMatDir,tmf(1:end-4));
    if ~exist(refmatoutf,'file')
        tmseconds=1:2:11;   tmseconds=tmseconds';
        tmrefn=length(tmseconds);
        tmdns=zeros(tmrefn,1);tmdns(:)=tmdn;
        refdns=tmdns-TENSECS_DN*tmseconds;
        refdvs=datevec(refdns);
        [H_Ref,MVx_Ref,MVy_Ref,MV_classc_Ref]=test_GetHistRefH(dataDir,cldmodelmatf,refdvs,NCCFSOutMatDir);
        save(refmatoutf,'MVx_Ref','MVy_Ref','MV_classc_Ref','H_Ref','tmseconds','refdns','tmdn');
    else
        t1=load(refmatoutf);
        H_Ref=t1.H_Ref;
        MVx_Ref=t1.MVx_Ref;
        MVy_Ref=t1.MVy_Ref;
        MV_classc_Ref=t1.MV_classc_Ref;
    end
    %continue;       % Generate Ref mat output file only
    
    HRef=round(unique(H_Ref)./100)*100;
    HRef=sort(HRef);
    HRef_n=length(HRef);
    
    
    if HRef==0
        bFullSearch=true;
    else
        bFullSearch=false;
    end
    MV_x=zeros(nConn,1); MV_y=zeros(nConn,1);
    HMV_x=zeros(nConn,1); HMV_y=zeros(nConn,1);
    H32=zeros(nConn,1);     MAXCC=zeros(nConn,1);
    Conn_H=zeros(nConn,1); Conn_W=zeros(nConn,1);
    Conn_sx=zeros(nConn,1);Conn_sy=zeros(nConn,1);
    Conn_cldcount=zeros(nConn,1);
    tmdns=[tmdn tmdn+TENSECS_DN];
    
    %bFullSearch=true;       % debug for full search
    % 2.2 Quick search based on current reference
    if bFullSearch==false
        if HRef(1)-500>=1000
            Hsran_quick=[HRef(1)-500:100:HRef(1)+500 HRef(2)-500:100:HRef(2)+500 HRef(3)-2000:500:HRef(3)+2000];
        else
            Hsran_quick=[1000:100:HRef(1)+500 HRef(2)-500:100:HRef(2)+500 HRef(3)-2000:500:HRef(3)+2000];
        end
        for tmi=1:nConn
            %         if tmi~=13
            %             continue
            %         end
            tmind=find(ConnLabelma==tmi);
            Conn_cldcount(tmi)=length(tmind);
            [y,x]=ind2sub([UNDISTH,UNDISTW],tmind);
            minsy=min(y);
            minsx=min(x);
            recH=max(y)-min(y)+1;
            recW=max(x)-min(x)+1;
            Conn_sx(tmi)=minsx;Conn_sy(tmi)=minsy;
            Conn_H(tmi)=recH;Conn_W(tmi)=recW;
            tmblkmask=false(UNDISTH,UNDISTW);
            tmblkmask(minsy:minsy+recH-1,minsx:minsx+recW-1)=true;
            
            %         [mv_x mv_y H tmHMV32_x tmHMV32_y maxcc]=ncc3d_tsi32_direct(...
            %                 tmdns,[minsy minsx],[recH recW],NextFrameMVSearchWinSize,...
            %                 dataDir,H_sran,ncc_thres,tmblkmask);
            if ~bMulti
                [mv_x,mv_y,H,tmHMV32_x,tmHMV32_y,maxcc,cc_all]=ncc3d_tsi32(...
                    tmdns,[minsy minsx],[recH recW],NextFrameMVSearchWinSize,...
                    dataDir,Hsran_quick,ncc_thres,tmblkmask);
                H32(tmi)=H;
                MV_x(tmi)=mv_x; MV_y(tmi)=mv_y;
                HMV_x(tmi)=tmHMV32_x; HMV_y(tmi)=tmHMV32_y;
                MAXCC(tmi)=maxcc;
            end
        end
    end
    
    % 2.3 H Full Range Search
    if bFullSearch==true
        %---------------------------------------------------------------------------------%
        % Module 4. Full Search 
        %---------------------------------------------------------------------------------%
        for tmi=1:nConn
            %         if tmi~=13
            %             continue
            %         end
            tmind=find(ConnLabelma==tmi);
            Conn_cldcount(tmi)=length(tmind);
            [y,x]=ind2sub([UNDISTH,UNDISTW],tmind);
            minsy=min(y);
            minsx=min(x);
            recH=max(y)-min(y)+1;
            recW=max(x)-min(x)+1;
            Conn_sx(tmi)=minsx;Conn_sy(tmi)=minsy;
            Conn_H(tmi)=recH;Conn_W(tmi)=recW;
            tmblkmask=false(UNDISTH,UNDISTW);
            tmblkmask(minsy:minsy+recH-1,minsx:minsx+recW-1)=true;
            
            %         [mv_x mv_y H tmHMV32_x tmHMV32_y maxcc]=ncc3d_tsi32_direct(...
            %                 tmdns,[minsy minsx],[recH recW],NextFrameMVSearchWinSize,...
            %                 dataDir,H_sran,ncc_thres,tmblkmask);
            if ~bMulti
                [mv_x,mv_y,H,tmHMV32_x,tmHMV32_y,maxcc,cc_all]=ncc3d_tsi32(...
                    tmdns,[minsy minsx],[recH recW],NextFrameMVSearchWinSize,...
                    dataDir,H_sran,ncc_thres,tmblkmask);
                H32(tmi)=H;
                MV_x(tmi)=mv_x; MV_y(tmi)=mv_y;
                HMV_x(tmi)=tmHMV32_x; HMV_y(tmi)=tmHMV32_y;
                MAXCC(tmi)=maxcc;
            else
                tmf=GetImgFromDV_BNL(tmdv,TSI1);
                tmoutf=sprintf('%s_%d.mat',tmf(1:end-4),tmi);
                tminf=sprintf('%s_%d.train.mat',tmf(1:end-4),tmi);
                outmatf=sprintf('%s%s',NCCFSOutMatDir,tmoutf);
                if exist(outmatf,'file')
                    continue;
                end
                inmatf=sprintf('%s%s',NCCFSOutMatDir,tminf);
                datevector_obj=tmdns;
                startp=[minsy minsx];
                recSize=[recH recW];
                SearchWinSize_ext=NextFrameMVSearchWinSize;
                TSIDir=dataDir;
                H_range=H_sran;
                ncc_thres=ncc_thres;
                tsi3BlkMask=tmblkmask;
                save(inmatf,'datevector_obj','startp','recSize','SearchWinSize_ext','TSIDir','H_range'...
                    ,'ncc_thres','tsi3BlkMask');
                %save(outmatf,'bBusy');
                %ncc3d_tsi32_outmat(inmatf,outmatf);
                cmmd=sprintf('ncc3d_tsi32_outmat(''%s'',''%s'');',inmatf,outmatf);
                disp(cmmd);
                subprocess_matlab(cmmd,nMatlabinst,severtype);
            end
        end
        continue;
        
        if bMulti
            for tmi=1:nConn
                tmf=GetImgFromDV_BNL(tmdv,TSI1);
                tmoutf=sprintf('%s_%d.mat',tmf(1:end-4),tmi);
                outmatf=sprintf('%s%s',NCCFSOutMatDir,tmoutf);
                while ~exist(outmatf,'file')
                    pause(2);
                end
                t=load(outmatf);
                H32(tmi)=t.H;
                MV_x(tmi)=t.mv_x; MV_y(tmi)=t.mv_y;
                HMV_x(tmi)=t.HMV_x; HMV_y(tmi)=t.HMV_y;
                MAXCC(tmi)=t.maxcc;
            end
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Show cloud field full search results
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %         figure();
        %         subplot(1,2,1);
        %         imshow(imma3);
        %         hold on;
        %         for tmi=1:nConn
        %             if MAXCC(tmi)==0
        %                 continue;
        %             end
        %             minsx=Conn_sx(tmi); minsy=Conn_sy(tmi);
        %             recH=Conn_H(tmi); recW=Conn_W(tmi);
        %             rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
        %             quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi),HMV_y(tmi));
        %             text(minsx+round(recW/2),minsy+round(recH/2),['\color{red} ' int2str(H32(tmi))],'FontSize',18);
        %         end
        %         hold off;
        %         subplot(1,2,2);
        %         imshow(imma2);
        %         hold on;
        %         for tmi=1:nConn
        %             if MAXCC(tmi)==0
        %                 continue;
        %             end
        %             minsx=Conn_sx(tmi)+HMV_x(tmi); minsy=Conn_sy(tmi)+HMV_y(tmi);
        %             recH=Conn_H(tmi); recW=Conn_W(tmi);
        %             rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
        %             %quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi),HMV_y(tmi));
        %         end
        %         hold off;
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    end
    
    
    
    
    
    % 2.4 high altitude cloud
    H_hcld=zeros(nConn,1);MAXCC_hcld=zeros(nConn,1);
    HMV_hcld_x31=zeros(nConn,1);HMV_hcld_x32=zeros(nConn,1);
    HMV_hcld_y31=zeros(nConn,1);HMV_hcld_y32=zeros(nConn,1);
    
    for tmi=1:nConn
        H=H32(tmi);
        if H==0
            continue;
        end
        
        minsx=Conn_sx(tmi); minsy=Conn_sy(tmi);ux31=minsx; uy31=minsy;
        recH=Conn_H(tmi); recW=Conn_W(tmi);
        [tmHMV_x,tmHMV_y]=TO_H2HMV(H,TSI3);
        ux11=ux31+tmHMV_x(1); uy11=uy31+tmHMV_y(1);
        ux21=ux31+tmHMV_x(2); uy21=uy31+tmHMV_y(2);
        ux11_e=ux11+recW;   uy11_e=uy11+recH;
        ux21_e=ux11+recW;   uy21_e=uy21+recH;
        ux31_e=ux11+recW;   uy31_e=uy31+recH;
        tmpts_y=[uy11;uy11_e;uy21;uy21_e;uy31;uy31_e];
        tmpts_x=[ux11;ux11_e;ux21;ux21_e;ux31;ux31_e];
        bVals=TO_PinDISTR([UNDISTH/2 UNDISTW/2],[tmpts_y tmpts_x],UNDISTR);
        if ~isempty(find(bVals==0,1))
            % there is one out of search R, don't use ncc3d_HighCld to search
            continue;
        end
        if H<LOWCLDHTHRES
            [tmH,maxcc,tmHMV_x,tmHMV_y]=...
                ncc3d_HighCld(tmdv,[minsy minsx],[recH recW],dataDir,1000:200:H+1000,ncc_thres);
        else
            [tmH,maxcc,tmHMV_x,tmHMV_y]=...
                ncc3d_HighCld(tmdv,[minsy minsx],[recH recW],dataDir,H-2000:500:H+2000,ncc_thres);
        end
        
        if maxcc==0
            % no search result found
            continue;
        end
        MAXCC_hcld(tmi)=maxcc;
        H_hcld(tmi)=tmH;
        HMV_hcld_x31(tmi)=tmHMV_x(1);
        HMV_hcld_x32(tmi)=tmHMV_x(2);
        HMV_hcld_y31(tmi)=tmHMV_y(1);
        HMV_hcld_y32(tmi)=tmHMV_y(2);
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Show cloud field high cloud search results
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %     figure();
    %     subplot(1,3,2);
    %     imshow(imma3);
    %     hold on;
    %     for tmi=1:nConn
    %         if MAXCC_hcld(tmi)==0
    %             continue;
    %         end
    %         minsx=Conn_sx(tmi); minsy=Conn_sy(tmi);
    %         recH=Conn_H(tmi); recW=Conn_W(tmi);
    %         rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
    %         quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_hcld_x31(tmi),HMV_hcld_y31(tmi));
    %         quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_hcld_x32(tmi),HMV_hcld_y32(tmi));
    %         text(minsx+round(recW/2),minsy+round(recH/2),['\color{red} ' int2str(H32(tmi))],'FontSize',18);
    %     end
    %     hold off;
    %     subplot(1,3,1);
    %     imshow(imma1);
    %     hold on;
    %     for tmi=1:nConn
    %         if MAXCC_hcld(tmi)==0
    %             continue;
    %         end
    %         minsx=Conn_sx(tmi)+HMV_hcld_x31(tmi); minsy=Conn_sy(tmi)+HMV_hcld_y31(tmi);
    %         recH=Conn_H(tmi); recW=Conn_W(tmi);
    %         rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
    %         %quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi),HMV_y(tmi));
    %     end
    %     hold off;
    %     subplot(1,3,3);
    %     imshow(imma2);
    %     hold on;
    %     for tmi=1:nConn
    %         if MAXCC_hcld(tmi)==0
    %             continue;
    %         end
    %         minsx=Conn_sx(tmi)+HMV_hcld_x32(tmi); minsy=Conn_sy(tmi)+HMV_hcld_y32(tmi);
    %         recH=Conn_H(tmi); recW=Conn_W(tmi);
    %         rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
    %         %quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi),HMV_y(tmi));
    %     end
    %     hold off;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    
    % 2.5 Update Href MV_ref got from cloud field
    % Current version:
    %   1. Generate most possible at most 2 Href_l for low altittude
    %   2. Generate most possible Href_h for high altittude
    %   3. Double check Href_l and Href_h using threshold to correct previous searching results
    %   3. Use frequency to choose Href_l and Href_h, store them and their pixel counts
    
    H_ALL=H32;
    Conn_l_indma=H_ALL<LOWCLDHTHRES&H_ALL~=0;
    Conn_h_indma=H_ALL>=LOWCLDHTHRES&H_ALL~=0;
    Href_l=H32(Conn_l_indma);
    Href_h=round(mean(H32(Conn_h_indma)));
    tmind1=abs(Href_l-HREF_PREF(1))<HREF_DIFF_TOLERANCE;
    tmind2=abs(Href_l-HREF_PREF(2))<HREF_DIFF_TOLERANCE;
    Href_l1=round(mean(Href_l(tmind1)));
    Href_l2=round(mean(Href_l(tmind2)));
    Href_l1=round(Href_l1/100)*100;
    Href_l2=round(Href_l2/100)*100;
    Href_h=round(Href_h/100)*100;
    
    
    [tmHMV_x,tmHMV_y]=TO_H2HMV(Href_l1,TSI3);
    HMV_x31_l1=tmHMV_x(1);HMV_x32_l1=tmHMV_x(2);
    HMV_y31_l1=tmHMV_y(1);HMV_y32_l1=tmHMV_y(2);
    [tmHMV_x,tmHMV_y]=TO_H2HMV(Href_l2,TSI3);
    HMV_x31_l2=tmHMV_x(1);HMV_x32_l2=tmHMV_x(2);
    HMV_y31_l2=tmHMV_y(1);HMV_y32_l2=tmHMV_y(2);
    
    [tmHMV_x,tmHMV_y]=TO_H2HMV(Href_h,TSI3);
    HMV_x31_h=tmHMV_x(1);HMV_x32_h=tmHMV_x(2);
    HMV_y31_h=tmHMV_y(1);HMV_y32_h=tmHMV_y(2);
    %Href_h=median(H_cld(conn_h_indma));
    H_major=zeros(nConn,1);
    HMV31_x_major=zeros(nConn,1);HMV31_y_major=zeros(nConn,1);
    HMV32_x_major=zeros(nConn,1);HMV32_y_major=zeros(nConn,1);
    MV_x_major=zeros(nConn,1);MV_y_major=zeros(nConn,1);
    
    
    
    hind_l1=find(H_sran==Href_l1,1);
    hind_l2=find(H_sran==Href_l2,1);
    hind_h=find(H_sran==Href_h,1);
    for tmi=1:nConn
        tmf=GetImgFromDV_BNL(tmdv,TSI1);
        tmoutf=sprintf('%s_%d.mat',tmf(1:end-4),tmi);
        outmatf=sprintf('%s%s',NCCFSOutMatDir,tmoutf);
        t=load(outmatf);
        tmcc_all=t.cc_all;
        if tmcc_all==0
            continue;
        end
        %   For low altittude cloud layer 1
        cc_all_h=tmcc_all(:,:,hind_l1);
        [maxcc,ind]=max(cc_all_h(:));
        [tmy,tmx]=ind2sub(size(cc_all_h),ind);
        if maxcc>ncc_thres_tsi32
            H_major(tmi)=Href_l1;
            HMV31_x_major(tmi)=HMV_x31_l1;
            HMV31_y_major(tmi)=HMV_y31_l1;
            HMV32_x_major(tmi)=HMV_x32_l1;
            HMV32_y_major(tmi)=HMV_y32_l1;
            MV_x_major(tmi)=tmx-Conn_W(tmi)-NextFrameMVSearchWinSize;
            MV_y_major(tmi)=tmy-Conn_H(tmi)-NextFrameMVSearchWinSize;
            continue;
        end
        %   For low altittude cloud layer 2
        cc_all_h=tmcc_all(:,:,hind_l2);
        [maxcc,ind]=max(cc_all_h(:));
        [tmy,tmx]=ind2sub(size(cc_all_h),ind);
        if maxcc>ncc_thres_tsi32
            H_major(tmi)=Href_l2;
            HMV31_x_major(tmi)=HMV_x31_l2;
            HMV31_y_major(tmi)=HMV_y31_l2;
            HMV32_x_major(tmi)=HMV_x32_l2;
            HMV32_y_major(tmi)=HMV_y32_l2;
            MV_x_major(tmi)=tmx-Conn_W(tmi)-NextFrameMVSearchWinSize;
            MV_y_major(tmi)=tmy-Conn_H(tmi)-NextFrameMVSearchWinSize;
            continue;
        end
        % For high altittude cloud
        cc_all_h=tmcc_all(:,:,hind_h);
        [maxcc,ind]=max(cc_all_h(:));
        [tmy,tmx]=ind2sub(size(cc_all_h),ind);
        if maxcc>ncc_thres_tsi32
            H_major(tmi)=Href_h;
            HMV31_x_major(tmi)=HMV_x31_h;
            HMV31_y_major(tmi)=HMV_y31_h;
            HMV32_x_major(tmi)=HMV_x32_h;
            HMV32_y_major(tmi)=HMV_y32_h;
            MV_x_major(tmi)=tmx-Conn_W(tmi)-NextFrameMVSearchWinSize;
            MV_y_major(tmi)=tmy-Conn_H(tmi)-NextFrameMVSearchWinSize;
            continue;
        end
    end
    
    Href_l1_ind=find(H_major==Href_l1);
    Href_l2_ind=find(H_major==Href_l2);
    Href_h_ind=find(H_major==Href_h);
    lcount=0;hcount=0;MV_x_ref_l=0;MV_y_ref_l=0;MV_x_ref_h=0;MV_y_ref_h=0;
    [MVx_l1_class,MVy_l1_class,MV_l1_classcount,MV_l1_nclass]=...
        findConnMVClass(MV_x_major,MV_y_major,Href_l1_ind,Conn_cldcount);
    [MVx_l2_class,MVy_l2_class,MV_l2_classcount,MV_l2_nclass]=...
        findConnMVClass(MV_x_major,MV_y_major,Href_l2_ind,Conn_cldcount);
    
    [MVx_H_class,MVy_H_class,MV_H_classcount,MV_H_nclass]=...
        findConnMVClass(MV_x_major,MV_y_major,Href_h_ind,Conn_cldcount);
    
    %     for tmi=1:length(Href_l_ind)
    %         tmind=Href_l_ind(tmi);
    %         if MV_x_major(tmind)==0 && MV_y_major(tmind)
    %             continue;
    %         end
    %         MV_x_ref_l=MV_x_ref_l+Conn_cldcount(tmind)*MV_x_major(tmind);
    %         MV_y_ref_l=MV_y_ref_l+Conn_cldcount(tmind)*MV_y_major(tmind);
    %         lcount=lcount+Conn_cldcount(tmind);
    %     end
    %     for tmi=1:length(Href_h_ind)
    %         if MV_x_major(tmind)==0 && MV_y_major(tmind)
    %             continue;
    %         end
    %         tmind=Href_h_ind(tmi);
    %         MV_x_ref_h=MV_x_ref_h+Conn_cldcount(tmind)*MV_x_major(tmind);
    %         MV_y_ref_h=MV_y_ref_h+Conn_cldcount(tmind)*MV_y_major(tmind);
    %         hcount=hcount+Conn_cldcount(tmind);
    %     end
    %     MV_y_ref_l=MV_y_ref_l/lcount;
    %     MV_x_ref_l=MV_x_ref_l/lcount;
    %     MV_y_ref_h=MV_y_ref_h/hcount;
    %     MV_x_ref_h=MV_x_ref_h/hcount;
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Show low cloud (H_refl) field results on TSI3(left) and TSI2(right)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %         %connind=H_major==Href_l&MV_x_major~=0&MV_y_major~=0;
    %         connind=H_major==Href_l1|H_major==Href_l2;
    %         figure();
    %         subplot(1,2,1);
    %         imshow(imma3);
    %         hold on;
    %         for tmi=1:nConn
    %             if connind(tmi)==0
    %                 continue;
    %             end
    %             minsx=Conn_sx(tmi); minsy=Conn_sy(tmi);
    %             recH=Conn_H(tmi); recW=Conn_W(tmi);
    %             if H_major(tmi)==Href_l1
    %                 rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
    %             elseif H_major(tmi)==Href_l2
    %                 rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','y');
    %             end
    %             quiver(minsx+round(recW/2),minsy+round(recH/2),HMV31_x_major(tmi),HMV31_y_major(tmi));
    %             quiver(minsx+round(recW/2),minsy+round(recH/2),HMV32_x_major(tmi),HMV32_y_major(tmi));
    %             text(minsx+round(recW/2),minsy+round(recH/2),['\color{red} ' int2str(H_major(tmi))],'FontSize',18);
    %         end
    %         hold off;
    %     %     subplot(1,3,1);
    %     %     imshow(imma1);
    %     %     hold on;
    %     %     for tmi=1:nConn
    %     %         if MAXCC_hcld(tmi)==0
    %     %             continue;
    %     %         end
    %     %         minsx=Conn_sx(tmi)+HMV_hcld_x31(tmi); minsy=Conn_sy(tmi)+HMV_hcld_y31(tmi);
    %     %         recH=Conn_H(tmi); recW=Conn_W(tmi);
    %     %         rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
    %     %         %quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi),HMV_y(tmi));
    %     %     end
    %     %     hold off;
    %         subplot(1,2,2);
    %         imshow(imma2);
    %         hold on;
    %         for tmi=1:nConn
    %             if connind(tmi)==0
    %                 continue;
    %             end
    %             minsx=Conn_sx(tmi)+HMV32_x_major(tmi); minsy=Conn_sy(tmi)+HMV32_y_major(tmi);
    %             recH=Conn_H(tmi); recW=Conn_W(tmi);
    %             rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
    %             quiver(minsx+round(recW/2),minsy+round(recH/2),MV_x_major(tmi),MV_y_major(tmi));
    %         end
    %         hold off;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Show High cloud (H_refl) field results on TSI3(left) and TSI2(right)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %     connind=H_major==Href_h&MV_x_major~=0&MV_y_major~=0;
    %     figure();
    %     subplot(1,3,2);
    %     imshow(imma3);
    %     hold on;
    %     for tmi=1:nConn
    %         if connind(tmi)==0
    %             continue;
    %         end
    %         minsx=Conn_sx(tmi); minsy=Conn_sy(tmi);
    %         recH=Conn_H(tmi); recW=Conn_W(tmi);
    %         rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
    %         quiver(minsx+round(recW/2),minsy+round(recH/2),HMV31_x_major(tmi),HMV31_y_major(tmi));
    %         quiver(minsx+round(recW/2),minsy+round(recH/2),HMV32_x_major(tmi),HMV32_y_major(tmi));
    %         text(minsx+round(recW/2),minsy+round(recH/2),['\color{red} ' int2str(H_major(tmi))],'FontSize',18);
    %     end
    %     hold off;
    %     subplot(1,3,1);
    %     imshow(imma1);
    %     hold on;
    %     for tmi=1:nConn
    %         if MAXCC_hcld(tmi)==0
    %             continue;
    %         end
    %         minsx=Conn_sx(tmi)+HMV_hcld_x31(tmi); minsy=Conn_sy(tmi)+HMV_hcld_y31(tmi);
    %         recH=Conn_H(tmi); recW=Conn_W(tmi);
    %         rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
    %         %quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi),HMV_y(tmi));
    %     end
    %     hold off;
    %     subplot(1,3,3);
    %     imshow(imma2);
    %     hold on;
    %     for tmi=1:nConn
    %         if connind(tmi)==0
    %             continue;
    %         end
    %         minsx=Conn_sx(tmi)+HMV32_x_major(tmi); minsy=Conn_sy(tmi)+HMV32_y_major(tmi);
    %         recH=Conn_H(tmi); recW=Conn_W(tmi);
    %         rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
    %         quiver(minsx+round(recW/2),minsy+round(recH/2),MV_x_major(tmi),MV_y_major(tmi));
    %     end
    %     hold off;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    mv_checkrange_x=[MVx_l1_class;MVx_l2_class;MVx_H_class];
    mv_checkrange_y=[MVy_l1_class;MVy_l2_class;MVy_H_class];
    mv_checkrange=[mv_checkrange_x mv_checkrange_y];
    mv_checkrange_zeroindma=(mv_checkrange_x==0)&(mv_checkrange_x==0);
    
    
    tml1=length(MVx_l1_class);  tml2=length(MVx_l2_class); tml3=length(MVx_H_class);
    H_vec=mv_checkrange_x;
    H_vec(1:tml1)=Href_l1;   H_vec(tml1+1:tml1+tml2)=Href_l2;   H_vec(tml1+tml2+1:tml1+tml2+tml3)=Href_h;
    cldcount_vec=[MV_l1_classcount;MV_l2_classcount;MV_H_classcount];
    
    
    %--------------------------------------------------------------------------------%
    % 3. Multi-layer cloud extraction
    %       1. Current Multi-layer results based on extracted H
    %       2. TSI3 cloud mask/cloud field MV similarity check
    %       3. TSI1,TSI2 cloud mask MV similarity check
    %       4. History cloud field with motion vectors and verified(if exists)
    %       5. Treat as noise or not-trust area following its neighbor cloud field
    %--------------------------------------------------------------------------------%
    % 3.1   Current Multi-layer results
    % H_major
    H_multi=H_major;
    MV_x_multi=MV_x_major;  MV_y_multi=MV_y_major;
    
    
    imma1_1faf=tmp_GetImgMatFromDataDir(tmdv_1faf,TSI1,dataDir);
    imma2_1faf=tmp_GetImgMatFromDataDir(tmdv_1faf,TSI2,dataDir);
    imma3_1faf=tmp_GetImgMatFromDataDir(tmdv_1faf,TSI3,dataDir);
    imma1_1faf=TO_histeq_TSISky(imma1_1faf,ref_sky_hist);
    imma2_1faf=TO_histeq_TSISky(imma2_1faf,ref_sky_hist);
    imma1_1faf_lum=TO_rgb2lum(imma1_1faf);
    imma2_1faf_lum=TO_rgb2lum(imma2_1faf);
    imma3_1faf_lum=TO_rgb2lum(imma3_1faf);
    
    
    % 3.2   TSI3 cloud mask MV
    for tmi=1:nConn
        if H_multi(tmi)~=0 && (MV_x_multi(tmi)~=0 || MV_y_multi(tmi)~=0)
            continue;
        end
        if H_major(tmi)==1100
            tmi
        end
%         if tmi==6
%             tmi
%         end
        recH=Conn_H(tmi); recW=Conn_W(tmi);
        uy31=Conn_sy(tmi);           ux31=Conn_sx(tmi);
        cc_vals=ncc_mvpick(imma3_lum,imma3_1faf_lum,[uy31 ux31],[recH recW],NextFrameMVSearchWinSize,mv_checkrange);
        cc_vals(cc_vals<ncc_cloudfield_trust)=0;    % if ncc of cl    imma1_1faf=tmp_GetImgMatFromDataDir(tmdv_1faf,TSI1,dataDir);
        [maxcc,maxind]=max(cc_vals);
        if maxcc==0
            % no match found here still keep 0 as invalid of multilayer classfication
            % Test Reference H and MV
            cc_vals1=ncc_mvpick(imma3_lum,imma3_1faf_lum,[uy31 ux31],[recH recW],NextFrameMVSearchWinSize,[MVx_Ref MVy_Ref]);
            cc_vals1(cc_vals1<ncc_cloudfield_trust)=0;
            if max(cc_vals1)==0
                % Search result based on reference H([MVx_Ref MVy_Ref]) is none. No need to search
                % or mark it as potential cloud field for height extraction
                continue;
            end
            [tmH,tmHi]=tmp_mvcc2H(cc_vals1,H_Ref,MV_classc_Ref,zeros(length(H_Ref),1),ncc_cloudfield_trust);
            if tmHi~=0
                MV_x_multi(tmi)=MVx_Ref(tmHi);
                MV_y_multi(tmi)=MVy_Ref(tmHi);
                H_multi(tmi)=tmH;
            end
        else
            % use matched results for current found
            [tmH,tmHi]=tmp_mvcc2H(cc_vals,H_vec,cldcount_vec,mv_checkrange_zeroindma,ncc_cloudfield_trust);
            %             if tmHi~=maxind
            %             tmHi
            %         end
            if tmHi==0
                cc_vals1=ncc_mvpick(imma3_lum,imma3_1faf_lum,[uy31 ux31],[recH recW],NextFrameMVSearchWinSize,[MVx_Ref MVy_Ref]);
                cc_vals1(cc_vals1<ncc_cloudfield_trust)=0;
                [tmH,tmHi]=tmp_mvcc2H(cc_vals1,H_Ref,MV_classc_Ref,zeros(length(H_Ref),1),ncc_cloudfield_trust);
                if tmHi~=0
                    MV_x_multi(tmi)=MVx_Ref(tmHi);
                    MV_y_multi(tmi)=MVy_Ref(tmHi);
                    H_multi(tmi)=tmH;
                end
            else
                MV_x_multi(tmi)=mv_checkrange(tmHi,1);
                MV_y_multi(tmi)=mv_checkrange(tmHi,2);
                H_multi(tmi)=tmH;
            end
            
        end
    end
    [tmHMV_x,tmHMV_y]=TO_H2HMV(H_multi,TSI3);
    HMV31_x_multi=tmHMV_x(:,1); HMV32_x_multi=tmHMV_x(:,2);
    HMV31_y_multi=tmHMV_y(:,1); HMV32_y_multi=tmHMV_y(:,2);
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Show low cloud (H_refl) field results on TSI3(left) and TSI2(right)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %connind=H_major==Href_l&MV_x_major~=0&MV_y_major~=0;
    %connind=H_multi==Href_l1|H_multi==Href_l2;
    connind=(MV_x_multi~=0|MV_y_multi~=0);
    figure();
    subplot(1,2,1);
    imshow(imma3);
    hold on;
    for tmi=1:nConn
        if connind(tmi)==0
            continue;
        end
        minsx=Conn_sx(tmi); minsy=Conn_sy(tmi);
        recH=Conn_H(tmi); recW=Conn_W(tmi);
        if H_multi(tmi)==Href_l1
            rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
        elseif H_multi(tmi)==Href_l2
            rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','y');
        end
        quiver(minsx+round(recW/2),minsy+round(recH/2),HMV31_x_multi(tmi),HMV31_y_multi(tmi));
        quiver(minsx+round(recW/2),minsy+round(recH/2),HMV32_x_multi(tmi),HMV32_y_multi(tmi));
        text(minsx+round(recW/2),minsy+round(recH/2),['\color{red} ' int2str(H_multi(tmi))],'FontSize',18);
    end
    hold off;
    %     subplot(1,3,1);
    %     imshow(imma1);
    %     hold on;
    %     for tmi=1:nConn
    %         if MAXCC_hcld(tmi)==0
    %             continue;
    %         end
    %         minsx=Conn_sx(tmi)+HMV_hcld_x31(tmi); minsy=Conn_sy(tmi)+HMV_hcld_y31(tmi);
    %         recH=Conn_H(tmi); recW=Conn_W(tmi);
    %         rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
    %         %quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi),HMV_y(tmi));
    %     end
    %     hold off;
    subplot(1,2,2);
    imshow(imma2);
    hold on;
    for tmi=1:nConn
        if connind(tmi)==0
            continue;
        end
        minsx=Conn_sx(tmi)+HMV32_x_multi(tmi); minsy=Conn_sy(tmi)+HMV32_y_multi(tmi);
        recH=Conn_H(tmi); recW=Conn_W(tmi);
        rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
        quiver(minsx+round(recW/2),minsy+round(recH/2),MV_x_multi(tmi),MV_y_multi(tmi));
    end
    hold off;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % 3.3 TSI1,TSI2 cloud mask MV similarity check
    imma1_cldmask_tsi3=false(UNDISTH,UNDISTW);
    imma2_cldmask_tsi3=false(UNDISTH,UNDISTW);
    imma3_cldmask_tsi3=false(UNDISTH,UNDISTW);
    for tmi=1:nConn
        if H_multi(tmi)==0
            continue;
        end
        recH=Conn_H(tmi); recW=Conn_W(tmi);
        uy31=Conn_sy(tmi);           ux31=Conn_sx(tmi);
        uy11=round(uy31+HMV31_y_multi(tmi));
        ux11=round(ux31+HMV31_x_multi(tmi));
        uy21=round(uy31+HMV32_y_multi(tmi));
        ux21=round(ux31+HMV32_x_multi(tmi));
        imma1_cldmask_tsi3=TO_SetImgVal_Pad([uy11 ux11],true(recH,recW),imma1_cldmask_tsi3);
        imma2_cldmask_tsi3=TO_SetImgVal_Pad([uy21 ux21],true(recH,recW),imma2_cldmask_tsi3);
        imma3_cldmask_tsi3=TO_SetImgVal_Pad([uy31 ux31],true(recH,recW),imma3_cldmask_tsi3);
    end
    %     figure();
    %     subplot(2,3,1);imshow(imma1);
    %     subplot(2,3,2);imshow(imma2);
    %     subplot(2,3,3);imshow(imma3);
    %     subplot(2,3,4);imshow(imma1_cldmask_tsi3);
    %     subplot(2,3,5);imshow(imma2_cldmask_tsi3);
    %     subplot(2,3,6);imshow(imma3_cldmask_tsi3);
    
    
    
    [ConnLabelma_1,nConn1]=bwlabel(imma1_cldma_n&~imma1_cldmask_tsi3);
    [ConnLabelma_1,nConn1]=bwlabel_adjust(ConnLabelma_1,ConnMinSize,ConnMaxSize);
    [ConnLabelma_1,nConn1]=bwlabel_adjust(ConnLabelma_1,ConnMinSize,ConnMaxSize);
    [ConnLabelma_2,nConn2]=bwlabel(imma2_cldma_n&~imma2_cldmask_tsi3);
    [ConnLabelma_2,nConn2]=bwlabel_adjust(ConnLabelma_2,ConnMinSize,ConnMaxSize);
    [ConnLabelma_2,nConn2]=bwlabel_adjust(ConnLabelma_2,ConnMinSize,ConnMaxSize);
    
    H_multi_1=zeros(nConn1,1);  Conn_cldcount1=zeros(nConn1,1);
    Conn_sx1=zeros(nConn1,1); Conn_sy1=zeros(nConn1,1);
    Conn_H1=zeros(nConn1,1); Conn_W1=zeros(nConn1,1);
    MV_x_multi_1=zeros(nConn1,1); MV_y_multi_1=zeros(nConn1,1);
    
    H_multi_2=zeros(nConn2,1);  Conn_cldcount2=zeros(nConn2,1);
    Conn_sx2=zeros(nConn2,1); Conn_sy2=zeros(nConn2,1);
    Conn_H2=zeros(nConn2,1); Conn_W2=zeros(nConn2,1);
    MV_x_multi_2=zeros(nConn2,1); MV_y_multi_2=zeros(nConn2,1);
    
    for tmi=1:nConn1
        if H_multi_1(tmi)~=0
            continue;
        end
        tmind=find(ConnLabelma_1==tmi);
        Conn_cldcount1(tmi)=length(tmind);
        [y,x]=ind2sub([UNDISTH,UNDISTW],tmind);
        minsy=min(y);
        minsx=min(x);
        recH=max(y)-min(y)+1;
        recW=max(x)-min(x)+1;
        Conn_sx1(tmi)=minsx;Conn_sy1(tmi)=minsy;
        Conn_H1(tmi)=recH;Conn_W1(tmi)=recW;
        uy11=Conn_sy1(tmi);           ux11=Conn_sx1(tmi);
        cc_vals=ncc_mvpick(imma1_lum,imma1_1faf_lum,[uy11 ux11],[recH recW],NextFrameMVSearchWinSize,mv_checkrange);
        cc_vals(cc_vals<ncc_cloudfield_trust)=0;    % if ncc of cloud field is smaller than threshold just ignore it
        [maxcc,maxind]=max(cc_vals);
        if maxcc==0
            % no match found here still keep 0 as invalid of multilayer classfication
            % Test Reference H and MV
            cc_vals1=ncc_mvpick(imma1_lum,imma1_1faf_lum,[uy11 ux11],[recH recW],NextFrameMVSearchWinSize,[MVx_Ref MVy_Ref]);
            cc_vals1(cc_vals1<ncc_cloudfield_trust)=0;
            if max(cc_vals1)==0
                % Search result based on reference H([MVx_Ref MVy_Ref]) is none. No need to search
                % or mark it as potential cloud field for height extraction
                continue;
            end
            [tmH,tmHi]=tmp_mvcc2H(cc_vals1,H_Ref,MV_classc_Ref,zeros(length(H_Ref),1),ncc_cloudfield_trust);
            if tmHi~=0
                MV_x_multi_1(tmi)=MVx_Ref(tmHi);
                MV_y_multi_1(tmi)=MVy_Ref(tmHi);
                H_multi_1(tmi)=tmH;
            end
        else
            % use matched results for current found
            [tmH,tmHi]=tmp_mvcc2H(cc_vals,H_vec,cldcount_vec,mv_checkrange_zeroindma,ncc_cloudfield_trust);
            %             if tmHi~=maxind
            %             tmHi
            %         end
            if tmHi==0
                cc_vals1=ncc_mvpick(imma1_lum,imma1_1faf_lum,[uy11 ux11],[recH recW],NextFrameMVSearchWinSize,[MVx_Ref MVy_Ref]);
                cc_vals1(cc_vals1<ncc_cloudfield_trust)=0;
                [tmH,tmHi]=tmp_mvcc2H(cc_vals1,H_Ref,MV_classc_Ref,zeros(length(H_Ref),1),ncc_cloudfield_trust);
                if tmHi~=0
                    MV_x_multi_1(tmi)=MVx_Ref(tmHi);
                    MV_y_multi_1(tmi)=MVy_Ref(tmHi);
                    H_multi_1(tmi)=tmH;
                end
            else
                MV_x_multi_1(tmi)=mv_checkrange(tmHi,1);
                MV_y_multi_1(tmi)=mv_checkrange(tmHi,2);
                H_multi_1(tmi)=tmH;
            end
        end
    end
    
    
    for tmi=1:nConn2
        if H_multi_2(tmi)~=0
            continue;
        end
        tmind=find(ConnLabelma_2==tmi);
        Conn_cldcount2(tmi)=length(tmind);
        [y,x]=ind2sub([UNDISTH,UNDISTW],tmind);
        minsy=min(y);
        minsx=min(x);
        recH=max(y)-min(y)+1;
        recW=max(x)-min(x)+1;
        Conn_sx2(tmi)=minsx;Conn_sy2(tmi)=minsy;
        Conn_H2(tmi)=recH;Conn_W2(tmi)=recW;
        uy21=Conn_sy2(tmi);           ux21=Conn_sx2(tmi);
        cc_vals=ncc_mvpick(imma2_lum,imma2_1faf_lum,[uy21 ux21],[recH recW],NextFrameMVSearchWinSize,mv_checkrange);
        cc_vals(cc_vals<ncc_cloudfield_trust)=0;    % if ncc of cloud field is smaller than threshold just ignore it
        [maxcc,maxind]=max(cc_vals);
        if maxcc==0
            % no match found here still keep 0 as invalid of multilayer classfication
            % Test Reference H and MV
            cc_vals1=ncc_mvpick(imma2_lum,imma2_1faf_lum,[uy21 ux21],[recH recW],NextFrameMVSearchWinSize,[MVx_Ref MVy_Ref]);
            cc_vals1(cc_vals1<ncc_cloudfield_trust)=0;
            if max(cc_vals1)==0
                % Search result based on reference H([MVx_Ref MVy_Ref]) is none. No need to search
                % or mark it as potential cloud field for height extraction
                continue;
            end
            [tmH,tmHi]=tmp_mvcc2H(cc_vals1,H_Ref,MV_classc_Ref,zeros(length(H_Ref),1),ncc_cloudfield_trust);
            if tmHi~=0
                MV_x_multi_2(tmi)=MVx_Ref(tmHi);
                MV_y_multi_2(tmi)=MVy_Ref(tmHi);
                H_multi_2(tmi)=tmH;
            end
        else
            % use matched results for current found
            [tmH,tmHi]=tmp_mvcc2H(cc_vals,H_vec,cldcount_vec,mv_checkrange_zeroindma,ncc_cloudfield_trust);
            %             if tmHi~=maxind
            %             tmHi
            %         end
            if tmHi==0
                cc_vals1=ncc_mvpick(imma2_lum,imma2_1faf_lum,[uy21 ux21],[recH recW],NextFrameMVSearchWinSize,[MVx_Ref MVy_Ref]);
                cc_vals1(cc_vals1<ncc_cloudfield_trust)=0;
                [tmH,tmHi]=tmp_mvcc2H(cc_vals1,H_Ref,MV_classc_Ref,zeros(length(H_Ref),1),ncc_cloudfield_trust);
                if tmHi~=0
                    MV_x_multi_2(tmi)=MVx_Ref(tmHi);
                    MV_y_multi_2(tmi)=MVy_Ref(tmHi);
                    H_multi_2(tmi)=tmH;
                end
            else
                MV_x_multi_2(tmi)=mv_checkrange(tmHi,1);
                MV_y_multi_2(tmi)=mv_checkrange(tmHi,2);
                H_multi_2(tmi)=tmH;
            end
        end
    end
    
    [tmHMV_x,tmHMV_y]=TO_H2HMV(H_multi_1,TSI3);
    HMV31_x_multi1=tmHMV_x(:,1); HMV32_x_multi1=tmHMV_x(:,2);
    HMV31_y_multi1=tmHMV_y(:,1); HMV32_y_multi1=tmHMV_y(:,2);
    
    [tmHMV_x,tmHMV_y]=TO_H2HMV(H_multi_2,TSI3);
    HMV31_x_multi2=tmHMV_x(:,1); HMV32_x_multi2=tmHMV_x(:,2);
    HMV31_y_multi2=tmHMV_y(:,1); HMV32_y_multi2=tmHMV_y(:,2);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Show mv range search results of TSI1 and TSI2. Motion vector that has no motion are not
    % plotted out
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    figure();
    imshow(imma1);
    hold on;
    for tmi=1:nConn1
        if MV_x_multi_1(tmi)==0 && MV_y_multi_1(tmi)==0
            continue;
        end
        ux11=Conn_sx1(tmi);  uy11=Conn_sy1(tmi);
        recH=Conn_H1(tmi);  recW=Conn_W1(tmi);
        if H_multi_1(tmi)<=LOWCLDHTHRES
            rectangle('Position',[ux11 uy11 recW recH],'LineStyle','--','EdgeColor','r');
            text(ux11+round(recW/2),uy11+round(recH/2),['\color{red} ' int2str(H_multi_1(tmi))],'FontSize',18);
        else
            rectangle('Position',[ux11 uy11 recW recH],'LineStyle','--','EdgeColor','y');
            text(ux11+round(recW/2),uy11+round(recH/2),['\color{yellow} ' int2str(H_multi_1(tmi))],'FontSize',18);
        end
    end
    hold off;
    
    figure();
    imshow(imma2);
    hold on;
    for tmi=1:nConn2
        if MV_x_multi_2(tmi)==0 && MV_y_multi_2(tmi)==0
            continue;
        end
        ux21=Conn_sx2(tmi);  uy21=Conn_sy2(tmi);
        recH=Conn_H2(tmi);  recW=Conn_W2(tmi);
        if H_multi_2(tmi)<=LOWCLDHTHRES
            rectangle('Position',[ux21 uy21 recW recH],'LineStyle','--','EdgeColor','r');
            text(ux21+round(recW/2),uy21+round(recH/2),['\color{red} ' int2str(H_multi_2(tmi))],'FontSize',18);
        else
            rectangle('Position',[ux21 uy21 recW recH],'LineStyle','--','EdgeColor','y');
            text(ux21+round(recW/2),uy21+round(recH/2),['\color{yellow} ' int2str(H_multi_2(tmi))],'FontSize',18);
        end
    end
    hold off;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    
    
    
    % Generate Multi-layer cloud results based on the TSI1, TSI2, TSI3 cloud field
    H_multi_all1=zeros(UNDISTH,UNDISTW);
    H_multi_all2=zeros(UNDISTH,UNDISTW);
    H_multi_all3=zeros(UNDISTH,UNDISTW);
    
    for tmi=1:nConn
        if H_multi(tmi)==0
            continue;
        end
        recH=Conn_H(tmi); recW=Conn_W(tmi);
        uy31=Conn_sy(tmi);           ux31=Conn_sx(tmi);
        uy11=round(uy31+HMV31_y_multi(tmi));
        ux11=round(ux31+HMV31_x_multi(tmi));
        uy21=round(uy31+HMV32_y_multi(tmi));
        ux21=round(ux31+HMV32_x_multi(tmi));
        % tsi3
        tmHma=TO_GetImgVal_Pad([uy31 ux31],[recH recW],H_multi_all3,NextFrameMVSearchWinSize);
        tmcldma=logical(TO_GetImgVal_Pad([uy31 ux31],[recH recW],imma3_cldma_n,NextFrameMVSearchWinSize));
        if ~isempty(find(tmcldma,1))
            tmHma(tmcldma)=H_multi(tmi);
        end
        H_multi_all3=TO_SetImgVal_Pad([uy31 ux31],tmHma,H_multi_all3);
        % tsi2
        tmHma=TO_GetImgVal_Pad([uy21 ux21],[recH recW],H_multi_all2,NextFrameMVSearchWinSize);
        tmcldma=logical(TO_GetImgVal_Pad([uy21 ux21],[recH recW],imma2_cldma_n,NextFrameMVSearchWinSize));
        if ~isempty(find(tmcldma,1))
            tmHma(tmcldma)=H_multi(tmi);
        end
        H_multi_all2=TO_SetImgVal_Pad([uy21 ux21],tmHma,H_multi_all2);
        % tsi1
        tmHma=TO_GetImgVal_Pad([uy11 ux11],[recH recW],H_multi_all1,NextFrameMVSearchWinSize);
        tmcldma=logical(TO_GetImgVal_Pad([uy11 ux11],[recH recW],imma1_cldma_n,NextFrameMVSearchWinSize));
        if ~isempty(find(tmcldma,1))
            tmHma(tmcldma)=H_multi(tmi);
        end
        H_multi_all1=TO_SetImgVal_Pad([uy11 ux11],tmHma,H_multi_all1);
    end
    
    for tmi=1:nConn1
        if H_multi_1(tmi)==0
            continue;
        end
        if MV_x_multi_1(tmi)==0&&MV_y_multi_1(tmi)==0
            continue;
        end
        recH=Conn_H1(tmi); recW=Conn_W1(tmi);
        uy11=Conn_sy1(tmi);           ux11=Conn_sx1(tmi);
        uy31=round(uy11-HMV31_y_multi1(tmi));
        ux31=round(ux11-HMV31_x_multi1(tmi));
        uy21=round(uy31+HMV32_y_multi1(tmi));
        ux21=round(ux31+HMV32_x_multi1(tmi));
        
        % tsi3
        tmHma=TO_GetImgVal_Pad([uy31 ux31],[recH recW],H_multi_all3,NextFrameMVSearchWinSize);
        tmcldma=logical(TO_GetImgVal_Pad([uy31 ux31],[recH recW],imma3_cldma_n,NextFrameMVSearchWinSize));
        if ~isempty(find(tmcldma,1))
            %tmHma(tmcldma)=H_multi_1(tmi);
            tmvals=tmHma(tmcldma);
            tmvals(tmvals==0|tmvals>H_multi_1(tmi))=H_multi_1(tmi);
            tmHma(tmcldma)=tmvals;
            
            H_multi_all3=TO_SetImgVal_Pad([uy31 ux31],tmHma,H_multi_all3);
        end
        % tsi2
        tmHma=TO_GetImgVal_Pad([uy21 ux21],[recH recW],H_multi_all2,NextFrameMVSearchWinSize);
        tmcldma=logical(TO_GetImgVal_Pad([uy21 ux21],[recH recW],imma2_cldma_n,NextFrameMVSearchWinSize));
        if ~isempty(find(tmcldma,1))
            %tmHma(tmcldma)=H_multi_1(tmi);
            tmvals=tmHma(tmcldma);
            tmvals(tmvals==0|tmvals>H_multi_1(tmi))=H_multi_1(tmi);
            tmHma(tmcldma)=tmvals;
            H_multi_all2=TO_SetImgVal_Pad([uy21 ux21],tmHma,H_multi_all2);
        end
        
        % tsi1
        tmHma=TO_GetImgVal_Pad([uy11 ux11],[recH recW],H_multi_all1,NextFrameMVSearchWinSize);
        tmcldma=logical(TO_GetImgVal_Pad([uy11 ux11],[recH recW],imma1_cldma_n,NextFrameMVSearchWinSize));
        if ~isempty(find(tmcldma,1))
            %tmHma(tmcldma)=H_multi_1(tmi);
            tmvals=tmHma(tmcldma);
            tmvals(tmvals==0|tmvals>H_multi_1(tmi))=H_multi_1(tmi);
            tmHma(tmcldma)=tmvals;
            H_multi_all1=TO_SetImgVal_Pad([uy11 ux11],tmHma,H_multi_all1);
        end
    end
    
    for tmi=1:nConn2
        if H_multi_2(tmi)==0
            continue;
        end
        if MV_x_multi_2(tmi)==0&&MV_y_multi_2(tmi)==0
            continue;
        end
        recH=Conn_H2(tmi); recW=Conn_W2(tmi);
        uy21=Conn_sy2(tmi);           ux21=Conn_sx2(tmi);
        uy31=round(uy21-HMV32_y_multi2(tmi));
        ux31=round(ux21-HMV32_x_multi2(tmi));
        uy11=round(uy31+HMV31_y_multi2(tmi));
        ux11=round(ux31+HMV31_x_multi2(tmi));
        
        % tsi3
        tmHma=TO_GetImgVal_Pad([uy31 ux31],[recH recW],H_multi_all3,NextFrameMVSearchWinSize);
        tmcldma=logical(TO_GetImgVal_Pad([uy31 ux31],[recH recW],imma3_cldma_n,NextFrameMVSearchWinSize));
        if ~isempty(find(tmcldma,1))
            %tmHma(tmcldma)=H_multi_2(tmi);
            tmvals=tmHma(tmcldma);
            tmvals(tmvals==0|tmvals>H_multi_2(tmi))=H_multi_2(tmi);
            tmHma(tmcldma)=tmvals;
            H_multi_all3=TO_SetImgVal_Pad([uy31 ux31],tmHma,H_multi_all3);
        end
        
        % tsi2
        tmHma=TO_GetImgVal_Pad([uy21 ux21],[recH recW],H_multi_all2,NextFrameMVSearchWinSize);
        tmcldma=logical(TO_GetImgVal_Pad([uy21 ux21],[recH recW],imma2_cldma_n,NextFrameMVSearchWinSize));
        if ~isempty(find(tmcldma,1))
            %tmHma(tmcldma)=H_multi_2(tmi);
            tmvals=tmHma(tmcldma);
            tmvals(tmvals==0|tmvals>H_multi_2(tmi))=H_multi_2(tmi);
            tmHma(tmcldma)=tmvals;
            H_multi_all2=TO_SetImgVal_Pad([uy21 ux21],tmHma,H_multi_all2);
        end
        
        % tsi1
        tmHma=TO_GetImgVal_Pad([uy11 ux11],[recH recW],H_multi_all1,NextFrameMVSearchWinSize);
        tmcldma=logical(TO_GetImgVal_Pad([uy11 ux11],[recH recW],imma1_cldma_n,NextFrameMVSearchWinSize));
        if ~isempty(find(tmcldma,1))
            %tmHma(tmcldma)=H_multi_2(tmi);
            tmvals=tmHma(tmcldma);
            tmvals(tmvals==0|tmvals>H_multi_2(tmi))=H_multi_2(tmi);
            tmHma(tmcldma)=tmvals;
            H_multi_all1=TO_SetImgVal_Pad([uy11 ux11],tmHma,H_multi_all1);
        end
        
    end
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Show mv range search results of TSI1 and TSI2. Motion vector that has no motion are not
    % plotted out
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    figure();
    subplot(2,3,1); imshow(imma1);
    subplot(2,3,2); imshow(imma3);
    subplot(2,3,3); imshow(imma2);
    subplot(2,3,4); imshow(H_multi_all1==Href_l1);
    subplot(2,3,5); imshow(H_multi_all3==Href_l1);
    subplot(2,3,6); imshow(H_multi_all2==Href_l1);
    figure();
    subplot(2,3,1); imshow(imma1);
    subplot(2,3,2); imshow(imma3);
    subplot(2,3,3); imshow(imma2);
    subplot(2,3,4); imshow(H_multi_all1==Href_l2);
    subplot(2,3,5); imshow(H_multi_all3==Href_l2);
    subplot(2,3,6); imshow(H_multi_all2==Href_l2);
    hold off;
    
    
    
    
    
    
    
    
    
    %--------------------------------------------------------------------------------%
    % 4. Image stiching
    %   Condt 1: Use multi-layer results for image shifting
    %   Condt 2: Fill image following the order of height
    %       1. Fill high cloud field
    %       2. Fill low cloud field
    %       3. Fill previous cloud based on motion
    %--------------------------------------------------------------------------------%
    imma3_fill_indma=imma3_lum==0;
    imma3_fill_indma_3d=TO_3DMaskfrom2D(imma3_fill_indma);
    imma3_fill=imma3;
    
    imma1_cldma_l1=H_multi_all1==Href_l1;
    imma2_cldma_l1=H_multi_all2==Href_l1;
    imma3_cldma_l1=H_multi_all3==Href_l1;
    
    imma1_cldma_l2=H_multi_all1==Href_l2;
    imma2_cldma_l2=H_multi_all2==Href_l2;
    imma3_cldma_l2=H_multi_all3==Href_l2;
    
    imma1_cldma_h=H_multi_all1==Href_h;
    imma2_cldma_h=H_multi_all2==Href_h;
    imma3_cldma_h=H_multi_all3==Href_h;
    
    
    %   Fill the sky
    skyfillfpath='skyfill.bmp';
    skyfillma=imread(skyfillfpath);
    imma3_fill(TO_3DMaskfrom2D(imma3_fill_indma))=skyfillma(TO_3DMaskfrom2D(imma3_fill_indma));
    %     figure();
    %     imshow(imma3_fill);
    
    
    % high cloud
    imma3_fill1=imma3;
    [tmHMV_x,tmHMV_y]=TO_H2HMV(Href_h,TSI3);
    tmHMV_x31_h=tmHMV_x(1); tmHMV_y31_h=tmHMV_y(1);
    tmHMV_x32_h=tmHMV_x(2); tmHMV_y32_h=tmHMV_y(2);
    imma1_s=imma1;
    imma2_s=imma2;
    imma1_s(TO_3DMaskfrom2D(~imma1_cldma_h))=0;
    imma2_s(TO_3DMaskfrom2D(~imma2_cldma_h))=0;
    imma1_s=TO_imshift(imma1_s,[-tmHMV_y31_h -tmHMV_x31_h]);
    imma2_s=TO_imshift(imma2_s,[-tmHMV_y32_h -tmHMV_x32_h]);
    tminvalidma1=TO_rgb2lum(imma1_s)==0; tminvalidma2=TO_rgb2lum(imma2_s)==0;
    imma1_s(TO_3DMaskfrom2D(tminvalidma1))=imma2_s(TO_3DMaskfrom2D(tminvalidma1));
    imma2_s(TO_3DMaskfrom2D(tminvalidma2))=imma1_s(TO_3DMaskfrom2D(tminvalidma2));
    imma3_fill1(imma3_fill_indma_3d)=round((single(imma1_s(imma3_fill_indma_3d))+single(imma2_s(imma3_fill_indma_3d)))/2);
    tminvalidma1=TO_rgb2lum(imma3_fill1)==0;
    imma3_fill1(TO_3DMaskfrom2D(tminvalidma1))=imma3_fill(TO_3DMaskfrom2D(tminvalidma1));
    imma3_fill=uint8(imma3_fill1);
    %figure();imshow(imma3_fill);
    
    
    
    
    % low cloud
    imma3_fill1=imma3;
    [tmHMV_x,tmHMV_y]=TO_H2HMV(Href_l2,TSI3);
    tmHMV_x31_l=tmHMV_x(1); tmHMV_y31_l=tmHMV_y(1);
    tmHMV_x32_l=tmHMV_x(2); tmHMV_y32_l=tmHMV_y(2);
    imma1_s=imma1;
    imma2_s=imma2;
    imma1_s(TO_3DMaskfrom2D(~imma1_cldma_l2))=0;
    imma2_s(TO_3DMaskfrom2D(~imma2_cldma_l2))=0;
    imma1_s=TO_imshift(imma1_s,[-tmHMV_y31_l -tmHMV_x31_l]);
    imma2_s=TO_imshift(imma2_s,[-tmHMV_y32_l -tmHMV_x32_l]);
    tminvalidma1=TO_rgb2lum(imma1_s)==0; tminvalidma2=TO_rgb2lum(imma2_s)==0;
    imma1_s(TO_3DMaskfrom2D(tminvalidma1))=imma2_s(TO_3DMaskfrom2D(tminvalidma1));
    imma2_s(TO_3DMaskfrom2D(tminvalidma2))=imma1_s(TO_3DMaskfrom2D(tminvalidma2));
    imma3_fill1(imma3_fill_indma_3d)=round((single(imma1_s(imma3_fill_indma_3d))+single(imma2_s(imma3_fill_indma_3d)))/2);
    tminvalidma1=TO_rgb2lum(imma3_fill1)==0;
    imma3_fill1(TO_3DMaskfrom2D(tminvalidma1))=imma3_fill(TO_3DMaskfrom2D(tminvalidma1));
    imma3_fill=uint8(imma3_fill1);
    %figure();imshow(imma3_fill);
    
    
    imma3_fill1=imma3;
    [tmHMV_x,tmHMV_y]=TO_H2HMV(Href_l1,TSI3);
    tmHMV_x31_l=tmHMV_x(1); tmHMV_y31_l=tmHMV_y(1);
    tmHMV_x32_l=tmHMV_x(2); tmHMV_y32_l=tmHMV_y(2);
    imma1_s=imma1;
    imma2_s=imma2;
    imma1_s(TO_3DMaskfrom2D(~imma1_cldma_l1))=0;
    imma2_s(TO_3DMaskfrom2D(~imma2_cldma_l1))=0;
    imma1_s=TO_imshift(imma1_s,[-tmHMV_y31_l -tmHMV_x31_l]);
    imma2_s=TO_imshift(imma2_s,[-tmHMV_y32_l -tmHMV_x32_l]);
    tminvalidma1=TO_rgb2lum(imma1_s)==0; tminvalidma2=TO_rgb2lum(imma2_s)==0;
    imma1_s(TO_3DMaskfrom2D(tminvalidma1))=imma2_s(TO_3DMaskfrom2D(tminvalidma1));
    imma2_s(TO_3DMaskfrom2D(tminvalidma2))=imma1_s(TO_3DMaskfrom2D(tminvalidma2));
    imma3_fill1(imma3_fill_indma_3d)=round((single(imma1_s(imma3_fill_indma_3d))+single(imma2_s(imma3_fill_indma_3d)))/2);
    tminvalidma1=TO_rgb2lum(imma3_fill1)==0;
    imma3_fill1(TO_3DMaskfrom2D(tminvalidma1))=imma3_fill(TO_3DMaskfrom2D(tminvalidma1));
    imma3_fill=uint8(imma3_fill1);
    
    figure();
    subplot(2,3,1); imshow(imma1);
    subplot(2,3,2); imshow(imma3);
    subplot(2,3,3); imshow(imma2);
    subplot(2,3,4); imshow(uint8(imma1_s));
    subplot(2,3,5); imshow(imma3_fill);
    subplot(2,3,6); imshow(uint8(imma2_s));
    
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Show two filling matrix of imma3 for debug
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    figure();
    subplot(2,2,1);imshow(imma3);
    subplot(2,2,2);imshow(uint8(imma3_fill_h));
    subplot(2,2,3);imshow(uint8(imma1_s_h));
    subplot(2,2,4);imshow(uint8(imma2_s_h));
    figure();
    subplot(2,2,1);imshow(imma3);
    subplot(2,2,2);imshow(uint8(imma3_fill_l));
    subplot(2,2,3);imshow(uint8(imma1_s_l));
    subplot(2,2,4);imshow(uint8(imma2_s_l));
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    
end
