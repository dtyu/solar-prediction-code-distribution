%   VERSION 0.0.1 Fix the bug of getting 0 as reference when use min H
%   VERSION 0.0.2 Use logger,configfpath
function [HRef,MVRef]=GetLayersInfo_realtime(realtimedv,HRefDir,configfpath,logger)
run(configfpath);
loggerID=TO_getloggerID();

HRef=DefaultHeightReference;
MVRef=DefaultMVReference;
TIMETHRES=HistoryLayerTimeThres;   % last one hour
HRefDir=FormatDirName(HRefDir);
if(~exist(HRefDir,'dir'))
    logger.error(loggerID,'No HRefDir exists!');
    return;
end

%bnltsiskyimageC3.a1.20140311.143022.jpg.20140311143022.cldtrk.layers.mat
TSIRefMatExpression='bnltsiskyimageC[123]\.a[1]\.\d{8}\.\d{6}\.jpg\.\d{14}\.cldtrk\.layers.mat$';


realtimedn=datenum(realtimedv);
startdn=realtimedn-TIMETHRES;
enddn=realtimedn+TIMETHRES;
allfiles=dir(HRefDir);
[nfiles,~]=size(allfiles);
leastdiff=TIMETHRES;
leastdn=realtimedn-leastdiff;
leastf='';
alldns=zeros(nfiles,1);

for i=1:nfiles 
    filename=allfiles(i).name;
    ind=regexp(filename,TSIRefMatExpression,'match');
    if isempty(ind)
        continue;
    end
    tmdv=GetDVFromImg(filename);
    tmdn=datenum(tmdv);
    if (tmdn<startdn || tmdn>enddn) % ideal case will not exceed enddn since realtimedn is always latest dn
        continue; 
    end
    alldns(i)=tmdn;
end

alldns=alldns(alldns~=0);

if isempty(alldns)
    dispstr=sprintf('No Reference file is available at %s use default H = %d, MV = [%d %d]',datestr(realtimedv),HRef,MVRef(1),MVRef(2));
    logger.debug(loggerID,dispstr);
    return;
end

N = length(alldns);
HRef_sum=0; HRef_n=0;
MVx_sum=0;  MVy_sum=0; 
for i=1:N
    tmdn=alldns(i);
    tmdv=datevec(tmdn);
    filename=GetImgFromDV_BNL(tmdv,TSI3);
    filename=[filename(1:end-4) '.cldtrk.layers.mat'];
    inputf=sprintf('%s%s',HRefDir,filename);
    t=load(inputf);
    C_l=t.C_l;
    if C_l(1,1)==0 || C_l(2,1)==0
        [HRef,ind]=max(C_l(:,1));   % One of the layer is empty
    else
        [HRef,ind]=min(C_l(:,1));   % Two layers
    end
    if HRef==0  % no available reference height extracted in this case
        continue;
    end
    HRef_n=HRef_n+1;
    HRef_sum=HRef_sum+HRef;
    MVx_sum=MVx_sum+C_l(ind,2);
    MVy_sum=MVy_sum+C_l(ind,3);
    
end

HRef=round(HRef_sum/HRef_n);
MVRef=[round(MVx_sum/HRef_n) round(MVy_sum/HRef_n)];



% for i=1:nfiles 
%     filename=allfiles(i).name;
%     ind=regexp(filename,TSIRefMatExpression,'match');
%     if isempty(ind)
%         continue;
%     end
%     tmdv=GetDVFromImg(filename);
%     tmdn=datenum(tmdv);
%     if (tmdn<startdn || abs(tmdn-realtimedn)>leastdiff)
%         continue; 
%     end
%     absf=sprintf('%s%s',HRefDir,filename);
%     leastdiff=abs(tmdn-realtimedn);
%     leastf=absf;
%     leastdn=tmdn;
% end
% 
% 
% if leastdiff>=TIMETHRES
%     disp(['No Reference file is available for time = ' datestr(realtimedv)]);
%     return;
% end
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %   Current settings for finding HRef
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% t=load(leastf);
% C_l=t.C_l;
% if C_l(1,1)==0 || C_l(2,1)==0
%     [HRef,ind]=max(C_l(:,1));   % One of the layer is empty
% else
%     [HRef,ind]=min(C_l(:,1));   % Two layers
% end
% HRef=round(HRef);
% MVRef=[C_l(ind,2) C_l(ind,3)];
% MVRef=round(MVRef);
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


end