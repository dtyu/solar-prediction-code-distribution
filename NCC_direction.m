function [Cof H_ran mv_x mv_y]=NCC_direction(template,A,template_startp,site1,site2)
mtsi_startup;
if strcmpi(site1,TSI1)
    loc1=location1;
elseif strcmpi(site1,TSI2)
    loc1=location2;
elseif strcmpi(site1,TSI3)
    loc1=location3;
end
if strcmpi(site2,TSI1)
    loc2=location1;
elseif strcmpi(site2,TSI2)
    loc2=location2;
elseif strcmpi(site2,TSI3)
    loc2=location3;
end
% mv_x=motionvector(1);
% mv_y=motionvector(2);

H_ran=1000:100:5000;
ux=template_startp(2);
uy=template_startp(1);
H_l=length(H_ran);

x=single(template(:));
validind=(x~=0);
x=x(validind);
x_a=mean(x);
x_d=std(x);
calX=x-x_a;

[tH,tW]=size(template);
[aH,aW]=size(A);
extH=250;
extW=250;
A_ext=zeros(aH+2*extH,aW+2*extW);
A_ext(extH+1:extH+aH,extW+1:extW+aW)=A(:,:);
Cof=zeros(H_l,1);
tmN=tH*tW;
y_d_ma=zeros(H_l,1);
mv_x=zeros(H_l,1);
mv_y=zeros(H_l,1);

for i=1:H_l
    rz=H_ran(i);
    [az ze]=TO_UNDISTCoor2AZ(ux,uy);
    [rx,ry,rz]=AZH2SpaceCoor(az,ze,rz);    
    [rx_new ry_new rz_new]=TO_RelativeCoor2RelativeCoor(rx,ry,rz,loc1,loc2);
    [az_new ze_new]=TO_RelativeCoor2AZ(rx_new,ry_new,rz_new);
    [ux_new uy_new]=TO_AZ2UNDISTCoor(az_new,ze_new);
    ux_new=round(ux_new);
    uy_new=round(uy_new);
    ux_end=round(ux_new+tW-1);
    uy_end=round(uy_new+tH-1);
    if ux_new>=1 && uy_new>=1 && ux_end<=UNDISTW && uy_end<=UNDISTH
        mv_x(i)=ux_new-ux;
        mv_y(i)=uy_new-uy;
        y=single(A(uy_new:uy_end,ux_new:ux_end));
        y=y(:);
        y=y(validind);
        y_a=mean(y);
        y_d=std(y);
        calY=y-y_a;
        c=sum(calX.*calY);
        Cof(i)=c;
        y_d_ma(i)=y_d;
    else 
        mv_x(i)=ux_new-ux;
        mv_y(i)=uy_new-uy;
        ux_ext=ux_new+extW;
        uy_ext=uy_new+extH;
        ux_ext_end=ux_ext+tW-1;
        uy_ext_end=uy_ext+tH-1;
        y=single(A_ext(uy_ext:uy_ext_end,ux_ext:ux_ext_end));
        y=y(:);
        y=y(validind);
        y_a=mean(y);
        y_d=std(y);
        calY=y-y_a;
        c=sum(calX.*calY);
        Cof(i)=c;
        y_d_ma(i)=y_d;
    end
    
end
Cof=Cof./y_d_ma./x_d/tmN;

end