% mtsi_HPlot
%   This function is NOT a part of mtsi pipeline. It is designed for testing of mtsipipe2.m tracking
%   results. Basically it uses the tracking mat files to generate height distribution and wind speed
%   on images. It is crucial for DEBUG of mtsipipe2
%   
% INPUT:
%       startdv                 --- Datevec, controls the start time
%       enddv                   --- Datevec, controls the end time
%       mode                    --- String, for parsing running environment
%       HRefDir                 --- Directory, containing mtsipipe2 regulate module results
%       plotOutDir              --- Directory, output for plots of heights/MV/single timestamps.
%       configfpath             --- String, mtsi_startup filepath
% OUTPUT:
%       plotOutDir/*.png        --- MAT files, output file
%
% PREREQUISITE:
%       mtsipipe2 ---  Get layers information and tracking results
% VERSION: 1.0      2013/12/04
%
% USAGE:
%   mtsi_HPlot([2013 05 14 0 0 0],[2013 05 15 0 0 0],'visible','./HistRef_recheck','./HistRef_HPlot_days','mtsi_startup');
%
function mtsi_HPlot(startdv,enddv,mode,HRefDir,plotOutDir,configfpath)
run(configfpath);
bPlotVisible=modepaser(mode,'visible');
bPlot=modepaser(mode,'plot');
if bPlotVisible
    set(0,'DefaultFigureVisible', 'on');
else
    set(0,'DefaultFigureVisible', 'off');
end

HRefDir=FormatDirName(HRefDir);
plotOutDir=FormatDirName(plotOutDir);
if ~exist(plotOutDir,'dir')
    mkdir(plotOutDir);
end

%   1. Generate days datenumbers
startdn=datenum(startdv);
enddn=datenum(enddv);
odstartdv=startdv;
odenddv=enddv;
odstartdv(4:6)=0;
odenddv(4:6)=0;
odstartdn=datenum(odstartdv);
odenddn=datenum(odenddv);
ONEDAY_DN=datenum([0 0 1 0 0 0]);
%TENSECS_DN=datenum([0 0 0 0 10 0]);
oddns=odstartdn:ONEDAY_DN:odenddn;
ndays=length(oddns);
ONEMIN_DN=datenum([0 0 0 0 1 0]);
FIVEMIN_DN=datenum([0 0 0 0 5 0]);
daytime_dn=datenum([0 0 0 14 0 0]);
dayend_dn=datenum([0 0 0 20 0 0]);

nframes_lookup=4;

%   2. Plot for each
for iday=1:ndays;
    tmstartdn=oddns(iday)+daytime_dn;
    tmenddn=oddns(iday)+dayend_dn;
    tmdn=tmstartdn;
    dns=[];
    i=0;
    while(tmdn<tmenddn)
        tmdn=tmstartdn+i*FIVEMIN_DN;
        %tmdn=tmstartdn+i*TENSECS_DN;
        dns=[dns;tmdn];
        i=i+1;
    end
    oddns_n=length(dns);
    
    REC_N=round((tmenddn-tmstartdn)./TENSECS_DN);
    Hs=zeros(REC_N,3);
    Hs_all=[];  Index_all=[];
    mvx_all=[]; mvy_all=[];
    for i=1:oddns_n-1
        tmdn=dns(i);
        tmdv=datevec(tmdn);
%         if (tmdn>=datenum([2013 05 14 16 09 0]))
%             i
%         else
%             continue;
%         end
        tmf=GetImgFromDV_BNL(tmdv,TSI3);
        cldtrkmatoutf_reg=sprintf('%s%s.cldtrkreg.mat',HRefDir,tmf(1:end-4));
        cldtrkmatoutf_2=sprintf('%s%s.cldtrk2.mat',HRefDir,tmf(1:end-4));
        if ~exist(cldtrkmatoutf_reg,'file')
            continue;
        end
        indexi=round((tmdn-tmstartdn)/TENSECS_DN)+1;
        [HRef MVRef]=tmp_ChooseHFromHRegfiles(HRefDir,tmdv,nframes_lookup);
        tml=length(HRef);
        %         for hi=1:tml
        %             if 0==HRef(hi)
        %                 continue;
        %             end
        %             Hs(indexi,hi)=HRef(hi);
        %         end
        
        for hi=1:tml
            if 0==HRef(hi)
                continue;
            end
            if HRef(hi)<5000
                Hs(indexi,1)=HRef(hi);
            elseif HRef(hi)>=5000 && HRef(hi)<10000
                Hs(indexi,2)=HRef(hi);
            else
                Hs(indexi,3)=HRef(hi);
            end
            Hs_all=[Hs_all HRef(hi)];
            Index_all=[Index_all indexi];
            mvx_all=[mvx_all MVRef(hi,1)];
            mvy_all=[mvy_all MVRef(hi,1)];
            %Hs(indexi,1)=HRef(hi);
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Show cloud field recheck results
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        if bPlot
            dataDir='/data/workdata/mtsi_stich/';
            
            imma1_ori=tmp_GetImgMatFromDataDir(tmdv,TSI1,dataDir);
            imma2_ori=tmp_GetImgMatFromDataDir(tmdv,TSI2,dataDir);
            imma3=tmp_GetImgMatFromDataDir(tmdv,TSI3,dataDir);
            Red1 = imma3(:, :, 1);
            Green1 = imma3(:, :, 2);
            Blue1 = imma3(:, :, 3);
            HnRed1=imhist(Red1);
            HnGreen1=imhist(Green1);
            HnBlue1=imhist(Blue1);
            HnRed1(1)=0;HnGreen1(1)=0;HnBlue1(1)=0;     % 0 means not valid
            ref_sky_hist=[HnRed1 HnGreen1 HnBlue1];
            
            imma1=TO_histeq_TSISky(imma1_ori,ref_sky_hist);
            imma2=TO_histeq_TSISky(imma2_ori,ref_sky_hist);
            
            t=load(cldtrkmatoutf_2);
            H=t.H;
            Conn_sx=t.Conn_sx;    Conn_sy=t.Conn_sy;
            Conn_H=t.Conn_H;      Conn_W=t.Conn_W;
            HMV_x=t.HMV_x; HMV_y=t.HMV_y;
            MAXCC=t.MAXCC;
            nConn=t.nCF;
            
            t=load(cldtrkmatoutf_reg);
            H_Reg=t.H_Reg;  Conn_Reg=t.Conn_Reg;    CldC_Reg=t.CldC_Reg;
            MV_x_Reg=t.MV_x_Reg;    MV_y_Reg=t.MV_y_Reg;
            HMV_x_Reg=t.HMV_x_Reg;    HMV_y_Reg=t.HMV_y_Reg;
            RegH=t.RegH;    RegHMV_x=t.RegHMV_x;    RegHMV_y=t.RegHMV_y;
            RegMV_x=t.RegMV_x;  RegMV_y=t.RegMV_y;
            tmoutDir=sprintf('HistRef_HPlot_each/');
            if ~exist(tmoutDir,'dir');
                mkdir(tmoutDir);
            end
            f1=figure();
            subplot(1,3,2);
            imshow(imma3);
            hold on;
            bShowText1=false;   bShowText2=false;
            for tmi=1:nConn
                %         if MAXCC(tmi)==0
                %             continue;
                %         end
                minsx=Conn_sx(tmi); minsy=Conn_sy(tmi);
                recH=Conn_H(tmi); recW=Conn_W(tmi);
                if RegH(tmi)==0
                    rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','c');
                elseif RegH(tmi)==H_Reg(1)
                    rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
                    quiver(minsx+round(recW/2),minsy+round(recH/2),RegHMV_x(tmi,1),RegHMV_y(tmi,1));
                    quiver(minsx+round(recW/2),minsy+round(recH/2),RegHMV_x(tmi,2),RegHMV_y(tmi,2));
                    if bShowText1==false
                        text(minsx+round(recW/2),minsy+round(recH/2),['\color{red} ' int2str(RegH(tmi))],'FontSize',18);
                        bShowText1=true;
                    end
                elseif RegH(tmi)==H_Reg(2)
                    rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','y');
                    quiver(minsx+round(recW/2),minsy+round(recH/2),RegHMV_x(tmi,1),RegHMV_y(tmi,1));
                    quiver(minsx+round(recW/2),minsy+round(recH/2),RegHMV_x(tmi,2),RegHMV_y(tmi,2));
                    if bShowText2==false
                        text(minsx+round(recW/2),minsy+round(recH/2),['\color{yellow} ' int2str(RegH(tmi))],'FontSize',18);
                        bShowText2=true;
                    end
                end
                %rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
                %quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi,1),HMV_y(tmi,1));
                %quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi,2),HMV_y(tmi,2));
                
            end
            hold off;
            subplot(1,3,3);
            imshow(imma2);
            hold on;
            for tmi=1:nConn
                if RegH(tmi)==0
                    continue;
                end
                minsx=Conn_sx(tmi)+RegHMV_x(tmi,1); minsy=Conn_sy(tmi)+RegHMV_y(tmi,1);
                recH=Conn_H(tmi); recW=Conn_W(tmi);
                if RegH(tmi)==H_Reg(1)
                    rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
                else
                    rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','y');
                end
                %rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
                %quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi),HMV_y(tmi));
            end
            hold off;
            subplot(1,3,1);
            imshow(imma1);
            hold on;
            for tmi=1:nConn
                if RegH(tmi)==0
                    continue;
                end
                minsx=Conn_sx(tmi)+RegHMV_x(tmi,2); minsy=Conn_sy(tmi)+RegHMV_y(tmi,2);
                recH=Conn_H(tmi); recW=Conn_W(tmi);
                if RegH(tmi)==H_Reg(1)
                    rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
                else
                    rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','y');
                end
                %rectangle('Position',[minsx minsy recW recH],'LineStyle','--','EdgeColor','r');
                %quiver(minsx+round(recW/2),minsy+round(recH/2),HMV_x(tmi),HMV_y(tmi));
            end
            hold off;
            tmfilename=GetImgFromDV_BNL(tmdv,TSI1);
            tmoutf=sprintf('%s%s.jpg',tmoutDir,tmfilename(1:end-4));
            print(f1,tmoutf,'-djpeg');
        end
    end
    
    ind1=Hs_all<5000&Hs_all>0;
    ind2=Hs_all>=5000&Hs_all<10000;
    ind3=Hs_all>=10000;
    if ~isempty(find(Hs,1))
        filename=GetImgFromDV_BNL(datevec(tmstartdn),TSI3);
        
        % Plot of Height
        outputf=sprintf('%s%s_H.png',plotOutDir,filename(1:end-4));
        f1=figure();
        xmin=1;    xmax=REC_N+1;
        axis([xmin xmax 500 15000]);
        titStr=sprintf(' Height Distribution of %s',datestr(tmstartdn,'yyyy-mm-dd'));
        legStr={'<5000','5000~10000','>10000'};
        timelabel=14:20;
        xStr='EST(UTC/GMT-5)';
        grid on;
        hold on;
        xlabel(xStr);
        Hs(Hs==0)=nan;
        plot(Index_all(ind1),Hs_all(ind1),'xr');
        plot(Index_all(ind2),Hs_all(ind2),'.b');
        plot(Index_all(ind3),Hs_all(ind3),'*y');
        set(gca,'XTick',1:360:REC_N+1);
        set(gca,'XTickLabel',timelabel);
        title(titStr);
        legend(legStr,'location','North','orientation','horizontal');
        hold off;
        print(f1,outputf,'-dpng');
        
        
        % Plot of motion vectors
        outputf=sprintf('%s%s_MV.png',plotOutDir,filename(1:end-4));
        f2=figure();
        xmin=1;    xmax=REC_N+1;
        axis([xmin xmax 500 15000]);
        NX=xmax-xmin+1; NY=15000-500+1;
        MVRatio=NY/NX;
        ploty=round(-mvy_all*MVRatio);
        plotx=mvy_all;
        titStr=sprintf(' Wind Speed Distribution of %s',datestr(tmstartdn,'yyyy-mm-dd'));
        legStr={'Wind Speed Vector'};
        timelabel=14:20;
        xStr='EST(UTC/GMT-5)';
        grid on;
        hold on;
        xlabel(xStr);
        quiver(Index_all,Hs_all,plotx,ploty,0.5);
        set(gca,'XTick',1:360:REC_N+1);
        set(gca,'XTickLabel',timelabel);
        title(titStr);
        legend(legStr,'location','North','orientation','horizontal');
        hold off;
        print(f2,outputf,'-dpng');
        
    end
    
end





end