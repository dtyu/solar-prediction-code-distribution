%   Temp version of get image from data directory. The function is to get mat file and its image
%   matrix when given filename or datevector
%   INPUT:
%       imgObj              --- Datevector or String(filepath)
%       site(Optional)      --- String, tsi1,tsi2,tsi3
%       dataDir(Optional)   --- String, date directory
%                               default is '/data/workdata/mtsi_stich/'
%   OUTPUT:
%       imma                ----   Matrix, image matrix
%       matf                ----    Absolute path of mat file
%   VERSION 1.0     2013-07-29
function [imma,matf]=tmp_GetImgMatFromDataDir(imgObj,site,dataDir)

matf=tmp_IsImgMatInDataDir(imgObj,site,dataDir);

if ~isempty(matf)
    t=load(matf);
    imma=t.imma;
else
    bmpf=tmp_IsImgBMPInDataDir(imgObj,site,dataDir);
    assert(~isempty(bmpf),'Input Image Mat File Doesn''t Exist!!');
    imma=imread(bmpf);
    matf=bmpf;
end


end