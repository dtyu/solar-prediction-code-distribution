/*
 * ===============================================================
 * MplusM.c  Takes two matrices of doubles and returns their sum.
 * Example for illustrating how to handle N-dimensional arrays in a MEX-file. 
 * NOTE: MATLAB uses 1-based indexing, C uses 0-based indexing.
 * ===============================================================
 */

#include "mex.h"
#include "matrix.h"
#include <math.h>
void matrixstd(double *imma, double *imma_std, double *imma_std_invalid, int M, int N, int stdwidth){
	int i,j=0;
	int tmi,tmj;
	double tmsum,tmsumsqr,tmavg;
	int n=(2*stdwidth+1)*(2*stdwidth+1);
	
	for(i=0+stdwidth; i<M-stdwidth; i++){
		for(j=0+stdwidth; j<N-stdwidth; j++){
			tmsum=0;	tmsumsqr=0;
			imma_std_invalid[i*N+j]=0;
			for (tmi=i-stdwidth;tmi<=i+stdwidth;tmi++){
				for (tmj=j-stdwidth;tmj<=j+stdwidth;tmj++){
					if (0==imma_std_invalid[i*N+j] && 0==imma[tmi*N+tmj]){
						imma_std_invalid[i*N+j]=1;
					}
					tmsum+=imma[tmi*N+tmj];
				}
			}			
			if (0==tmsum){
				imma_std[i*N+j]=0;
				/*
				char c[80];
				sprintf(c,"i=%d,j=%d,imma_std_invalid=%f,imma=%f",i,j,imma_std_invalid[i*N+j],imma[i*N+j]);
				mexWarnMsgTxt(c);*/
				continue;
			}
			
			tmavg=tmsum/n;
			for (tmi=i-stdwidth;tmi<=i+stdwidth;tmi++){
				for (tmj=j-stdwidth;tmj<=j+stdwidth;tmj++){
					tmsumsqr+=(imma[tmi*N+tmj]-tmavg)*(imma[tmi*N+tmj]-tmavg);
				}
			}
			imma_std[i*N+j]=sqrt(tmsumsqr/n);
          }
     }
	return;
}



void matrixstd1(double *imma, double *imma_std, int M, int N, int stdwidth){
	int i,j=0;
	int tmi,tmj;
	double tmsum,tmsumsqr,tmavg;
	int n=(2*stdwidth+1)*(2*stdwidth+1);
	for(i=0+stdwidth; i<M-stdwidth; i++){
		for(j=0+stdwidth; j<N-stdwidth; j++){
			tmsum=0;	tmsumsqr=0;
			/*imma_std_invalid[i*N+j]=0;*/
			for (tmi=i-stdwidth;tmi<=i+stdwidth;tmi++){
				for (tmj=j-stdwidth;tmj<=j+stdwidth;tmj++){
					/*if (imma[tmi*N+tmj]==0&&imma_std_invalid[i*N+j]==0){
						imma_std_invalid[i*N+j]=1;
					}*/
					tmsum+=imma[tmi*N+tmj];
				}
			}
			tmavg=tmsum/n;
			

			for (tmi=i-stdwidth;tmi<=i+stdwidth;tmi++){
				for (tmj=j-stdwidth;tmj<=j+stdwidth;tmj++){
					tmsumsqr+=(imma[tmi*N+tmj]-tmavg)*(imma[tmi*N+tmj]-tmavg);
				}
			}
			imma_std[i*N+j]=sqrt(tmsumsqr/n);
          }
     }
	return;
}


/*****************************************************************
* INPUT:
*	imma1 		---	[M,N], double
*	stdwidth	---	standard deviation of [y-std y+std, x-std x+std]
* OUTPUT:
*	imma1_std	---	[M,N] double
******************************************************************/

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
 	 /* Declare variables. */ 
	double *imma, *stdwidthp, *imma_std_invalid, *imma_std;
	size_t M,N,N1,stdwidth;
  
  /* Check for proper number of input and output arguments. */    
    if (nrhs != 2) {
        mexErrMsgTxt("Two input arguments required.");
    } 
    if (nlhs > 2) {
        mexErrMsgTxt("Too many output arguments.");
    }

  /* Check data type of input argument. */
  /*  if (!(mxIsDouble(prhs[0]))) {
        mexErrMsgTxt("Input array must be of type double.");
    }*/

  /* Create a pointer to every input matrix. */
    imma  = mxGetPr(prhs[0]);
    stdwidthp  = mxGetPr(prhs[1]);
  	stdwidth=stdwidthp[0];

   /* Get the dimensions of the input matrices. */
    M = mxGetM(prhs[0]);
    N = mxGetN(prhs[0]);

  /* Set the output pointer to the output matrix. */
    plhs[0] = mxCreateDoubleMatrix(M,N, mxREAL);
  	plhs[1] = mxCreateDoubleMatrix(M,N, mxREAL);

  /* Create a C pointer to a copy of the output matrix. */
    imma_std  = mxGetPr(plhs[0]);
	imma_std_invalid  = mxGetPr(plhs[1]);
	matrixstd(imma,imma_std,imma_std_invalid,M,N,stdwidth);	
}
