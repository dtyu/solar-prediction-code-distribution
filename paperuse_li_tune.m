%paperuse_li_tune
modelsDir='models_li/';
modelsDir=FormatDirName(modelsDir);
if ~exist(modelsDir,'dir')
    mkdir(modelsDir);
end

cran=-10:10;
epran=0.01:0.02:0.6;
%eran=0.01:0.03:0.7;

% cran=1:6;
% nuran=0.2:0.05:0.6;
% eran=0.01:0.05:0.2;

bServer=true;
bMulti=true;
if bServer && bMulti
    nMatlabinst=16;
    severtype=64;
elseif  ~bServer && bMulti
    nMatlabinst=4;
    severtype=64;
end


poly_paraperm=GenPermutation(cran,epran);
[nSub,nDim]=size(poly_paraperm);
opt1='-s 3 -t 0';
nfolds=5;
BPS_N=25;

models_li=cell(BPS_N,5);
models_rbf=cell(BPS_N,5);
models_rbr=cell(BPS_N,1);
models_a=cell(BPS_N,1);
predmatf=sprintf('sftmp.mat');
t1=load(predmatf);
od_r_n=t1.od_r_n;   od_g_n=t1.od_g_n;   od_b_n=t1.od_b_n;
od_prer_n=t1.od_prer_n; od_preg_n=t1.od_preg_n; od_preb_n=t1.od_preb_n;
od_rbr_n=t1.od_rbr_n;
od_prerbr_n=t1.od_prerbr_n;
od_cf=t1.od_cf;
od_rad_norm=t1.od_rad_norm;
od_prerad_norm=t1.od_prerad_norm;
showstarti=t1.showstarti;
showendi=t1.showendi;
timelabel=t1.timelabel;
NinHour=t1.NinHour;

mae_sub=zeros(nSub,1);
rmse_sub=zeros(nSub,1);

bpsmae=zeros(BPS_N,nSub);  bpsrmse=zeros(BPS_N,nSub);
for i=1:BPS_N
%     if i~=BPS_N
%         continue;
%     end
    %outputf=sprintf('%ssfmodel_bps%d.png',outputDir,i);
    r_m=od_r_n(showstarti:showendi,i,3);
    g_m=od_g_n(showstarti:showendi,i,3);
    b_m=od_b_n(showstarti:showendi,i,3);
    r_min=od_r_n(showstarti:showendi,i,1);
    g_min=od_g_n(showstarti:showendi,i,1);
    b_min=od_b_n(showstarti:showendi,i,1);
    r_max=od_r_n(showstarti:showendi,i,2);
    g_max=od_g_n(showstarti:showendi,i,2);
    b_max=od_b_n(showstarti:showendi,i,2);
    prer=od_prer_n(showstarti:showendi,i);
    preg=od_preg_n(showstarti:showendi,i);
    preb=od_preb_n(showstarti:showendi,i);
    prerad=od_prerad_norm(showstarti:showendi,i);
    rbr=od_rbr_n(showstarti:showendi,i);
    prerbr=od_prerbr_n(showstarti:showendi,i);
    cldf=od_cf(showstarti:showendi);
    fit_y=od_rad_norm(showstarti:showendi,i);
    fit_x=[ones(size(r_m)) r_m g_m b_m prer preg preb prerbr rbr cldf prerad];
    a=fit_x\fit_y;  % y=a1+a2*x1+a3*x2+a4*x3+a5*x4+a6*x6
    pre_y=fit_x*a;
    pre_y(pre_y>1)=1;
    pre_y(pre_y<0)=0;
    xtrain_norm=[r_m r_min r_max g_m g_min g_max b_m b_min b_max prer preg preb prerbr rbr cldf prerad];
    ytrain_norm=fit_y;
    [xtrain,ytrain,xtest,ytest]=cvGenFolds(xtrain_norm,ytrain_norm,nfolds);
    inmat=sprintf('%slii%d.mat',modelsDir,i);
    save(inmat);
    outmat=sprintf('%slio%d.mat',modelsDir,i);
    
    if bMulti
        %paperuse_li_tune_single(inmat,outmat);
        cmmd=sprintf('paperuse_li_tune_single(''%s'',''%s'');',inmat,outmat);
        subprocess_matlab(cmmd,nMatlabinst,severtype);
        disp(cmmd);
    else
        paperuse_li_tune_single(inmat,outmat);
    end
    %     for si=1:nSub
    %         tmparaset=poly_paraperm(si,:);
    %         c=tmparaset(1);
    %         nu=tmparaset(2);
    %         e=tmparaset(3);
    %         modelfile=sprintf('%sli_%d_%f_%f.mat',modelsDir,c,nu,e);
    %         svmoptions_li=sprintf('%s -c %d -e %f -n %f',opt1,2^c,e,nu);
    %         MAE_li=zeros(nfolds,1);
    %         RMSE_li=zeros(nfolds,1);
    %         for fi=1:nfolds
    %             model_li=svmtrain(ytrain{fi},xtrain{fi},svmoptions_li);
    %             [predicted_label, accuracy, y1]=svmpredict(ytest{fi},xtest{fi},model_li);
    %             y1(y1>1)=1; y1(y1<0)=0;
    %             MAE_li(fi)=mean(abs(y1-ytest{fi}));
    %             RMSE_li(fi)=sqrt(mean((y1-ytest{fi}).^2));
    %         end
    %         MAE_ref=mean(abs(pre_y-fit_y));
    %         RMSE_ref=sqrt(mean((pre_y-fit_y).^2));
    %         %if mean(MAE_li(:))<MAE_ref && mean(RMSE_li(:))<RMSE_ref
    %         bpsmae(i,si)=mean(MAE_li(:));
    %         bpsrmse(i,si)=mean(RMSE_li(:));
    %     end
end
    

   


