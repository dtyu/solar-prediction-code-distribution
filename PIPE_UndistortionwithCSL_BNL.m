% PIPE_Undistortion: Main Entrance of the pipeline of preprocessing. It
% needs to ClearSkyLibrary to do shadow band masking and holding arm
% masking.
%   IN:
%       startv  --   start date vector(2010,10,0,x,x,x)
%       endv    --   end date vector(2010,10,0,x,x,x)
%       DataDirectory   --  Directory where you put input data
%           data/sbmask
%           data/hamask
%       oriimgDir       --  Original Images(already flipped)
%       undistimgDir    --  Undistortion images
%       UndistortionParameters.mat(in)  --  related parameters
%       sbmaskDir(in)   --  'sbmask/' which contains all sb masks related
%                           to tsdn items
%       hamaskDir(in)   --  'hamask/' which contains all ha masks related
%                           to tsdn items
%   OUT:
%       undistimgDir/*     --  all the undistorted images
%
%   VERSION 1.1     2012-04-01
%   1.  Add new input freq(in seconds)
%   VERSION 1.1     2013-01-29
%       Add the BuildClearSkyLib into the code
%       Remove freq in undistortion code
%   VERSION 2.0     2013-04-22
%       Test of new GetAZ code
%   VERSION 3.0     2013-06-04      ADD SITE AS INPUT FOR TSI1, TSI2, TSI3

function PIPE_UndistortionwithCSL_BNL(startv,endv,DataDirectory,oriimgDir,undistimgDir,site)
%load('UndistortionParameters.mat');     % load ALL the related parameters
% load('undist2ori.mat');             % matrix that contains all undist->ori mapping
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% STARTUP to get all possible parameter settings
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
mtsi_startup_mini;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%load(tsi1undistmat);        % tsi1undistmat='tsi1_ori2undist.mat';
DataDirectory=FormatDirName(DataDirectory);
if site==TSI1
    lon=location1.longitude;
    lat=location1.latitude;
    alt=location1.altitude;
    azshift=AzumithShift1;
    zeshift=ZenithShift1;
    t=load(tsi1undistmat);
    sbmaskDir=sprintf('%ssbmask1/',DataDirectory);
    hamaskDir=sprintf('%shamask1/',DataDirectory);
    sbazmatf='sbazimuth_tsi1.mat';
    haazmatf='haazimuth_tsi1.mat';
elseif site == TSI2
    lon=location2.longitude;
    lat=location2.latitude;
    alt=location2.altitude;
    azshift=AzumithShift2;
    zeshift=ZenithShift2;
    t=load(tsi2undistmat);
    sbmaskDir=sprintf('%ssbmask2/',DataDirectory);
    hamaskDir=sprintf('%shamask2/',DataDirectory);
    sbazmatf='sbazimuth_tsi2.mat';
    haazmatf='haazimuth_tsi2.mat';
elseif site == TSI3
    lon=location3.longitude;
    lat=location3.latitude;
    alt=location3.altitude;
    azshift=AzumithShift3;
    zeshift=ZenithShift3;
    t=load(tsi3undistmat);
    sbmaskDir=sprintf('%ssbmask3/',DataDirectory);
    hamaskDir=sprintf('%shamask3/',DataDirectory);
    sbazmatf='sbazimuth_tsi3.mat';
    haazmatf='haazimuth_tsi3.mat';
end
u2oma=t.u2oma;
undist2orima=u2oma;
%sbazmatf='sbazimuth.mat';
%haazmatf='haazimuth.mat';

% DataDirectory
% if(DataDirectory(end)~='/')
%     DataDirectory=sprintf('%s%s',DataDirectory,'/');
% end
DataDirectory=FormatDirName(DataDirectory);
%   Data/input
%   Data/output
%   Data/sbmask
%   Data/hamask
oriimgDir=FormatDirName(oriimgDir);
inputimgDir=oriimgDir;
outputimgDir=sprintf('%soutput/',DataDirectory);
sbmaskDir=GetAbspathFromFilename(sbmaskDir);
hamaskDir=GetAbspathFromFilename(hamaskDir);
inputimgDir=FormatDirName(inputimgDir);
undistoutDir=undistimgDir;
undistoutDir=FormatDirName(undistoutDir);

if ~exist(outputimgDir,'dir')
    mkdir(outputimgDir);
end
if ~exist(hamaskDir,'dir')
    mkdir(hamaskDir);
end
if ~exist(sbmaskDir,'dir')
    mkdir(sbmaskDir);
end
if ~exist(undistoutDir,'dir')
    mkdir(undistoutDir);
end


%   Build Clear Sky Lib for SB masks
BuildClearSkyLib_wBMPMask(inputimgDir,hamaskDir,sbmaskDir,site);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   This code is to generate available avail MAT of SB and HA masks
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Re-gen the sb and ha azimuth list
GenAvailAzMat_SBHA(hamaskDir,sbmaskDir,haazmatf,sbazmatf,site);

sbazimuth=load(sbazmatf);                % load all sbmask timenumbers and azumith
haazimuth=load(haazmatf);                % load all hamask timenumbers and azumith



%strcmpi(PRO_MODE,'REMOVE')==1
%   Set start time vector and end time vector
starttimevec=startv;
% starttimevec(4)=0;
% starttimevec(5)=0;
% starttimevec(6)=0;
endtimevec=endv;
% endtimevec(4)=0;
% endtimevec(5)=0;
% endtimevec(6)=0;

%   Basic parameters and prerequisites checking
% hamaskDir='hamask/';
% sbmaskDir='sbmask/';
% disp(SITE);
if(exist(inputimgDir,'dir')==0)
    disp('input data file does not exist!');
end
if(exist(sbmaskDir,'dir')==0)
    disp('WARNING: sbmask does not exist');
end
if(exist(hamaskDir,'dir')==0)
    disp('WARNING: hamask does not exist');
end
if(exist('UndistortionParameters.mat','file')==0)
    disp('ERROR: UndistortionParameters.mat doesnot exist!');
end
if(exist('undist2ori.mat','file')==0)
    disp('ERROR: undist2ori.mat doesnot exist!');
end


if ~exist(undistoutDir,'dir')
    mkdir(undistoutDir);
end

holdma=zeros(ORIH,ORIW);
shadowma=zeros(ORIH,ORIW);
totalmaskma=zeros(ORIH,ORIW);
totalmaskma=logical(totalmaskma);
tmimgma=zeros(ORIH,ORIW);
tmimgma=uint8(tmimgma);
unimg=zeros(UNDISTH,UNDISTW,3);
unimg=uint8(unimg);
AZThres=4/180*pi;  % Inner Control parameter to consider within Azimuth Threshold masks only.
DNThres=datenum([0 0 0 0 2 0]);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% FreqTn=datenum([0 0 0 0 0 Freq]);
% % tmstep=datenum([0,0,0,0,0,30]);       %tmstep -- time step of consecutive images.
% tmstep=FreqTn;
% %which is 30s by default Here can be changed to 1s for BNL
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% tmvec=starttimevec;
% nfiles=0;
% while(tmvec~=endtimevec)
%     nfiles=nfiles+1;
%     tmvec=tmvec+tmstep;
% end
tmdn=datenum(starttimevec);
startdn=datenum(starttimevec);
enddn=datenum(endtimevec);
% testdn=datenum([2010 10 26 00 15 00]);
iter_i=1;       % for timestep increment
nfiles=1;

% imgfilename=allfiles(3).name;
% tmfiledn=GetDVFromImg(imgfilename);
% if(tmfiledn>=enddn)
%     disp('NO match in startvec to endvec range');
%     return;
% else
%     %     startdn=tmfiledn;
% end
% tmvec=GetDVFromImg(imgfilename);
tmvec=starttimevec;


sun=INNER_GETAandZ(tmvec,lat,lon,alt);

tmimgma=[];

UNDISTWndist=UNDISTW;
UNDISTHndist=UNDISTH;

%   Add avail tmdns for huge dataset
oriimgdirname=GetFilenameFromAbsPath(oriimgDir);
allfiles=dir(inputimgDir);
[NFILES,~]=size(allfiles);
nfiles=NFILES;
avail_f=sprintf('avail_%s.mat',oriimgdirname);
if ~exist(avail_f,'file')
    tmdns=zeros(nfiles,1);
    for i =1:nfiles
        filename=allfiles(i).name;
        tmdv=GetDVFromImg(filename);
        if tmdv==0
            continue;
        end
        tmdn=datenum(tmdv);
        tmdns(i)=tmdn;
    end
    save(avail_f,'tmdns');
end
t=load(avail_f);
tmdns=t.tmdns;

predn=0;
pred_ran=datenum([0 0 0 0 5 0]);
for i=1:NFILES  %remove first two directory name
    if allfiles(i).isdir==1
        continue;
    end
    tmfiledn=tmdns(i);
%     %   DEBUG ONLY
%     if tmfiledn==datenum([2012 09 24 15 05 52])
%         tmfiledn
%     else
%         continue;
%     end


    if(tmfiledn<startdn||tmfiledn>enddn)
        %   Not in the range of requested time
        continue;
    end
    %    %   ADD THIS AS TEST!!!!!
    %    %   For DEBUG OF RANGE   
    %     if tmfiledn-predn<=pred_ran
    %         continue;
    %     end
    %     predn=tmfiledn;
    tmvec=datevec(tmfiledn);
    imgfilename=allfiles(i).name;
    %disp(imgfilename);
    %tmvec=GetDVFromImg(imgfilename);
    %tmfiledn=datenum(tmvec);
    %     disp('Debug: input valid');
    inputfilep=sprintf('%s%s',inputimgDir,imgfilename);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %   Output file skipping(uncomment to skip)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    outp=sprintf('%s%s.bmp',undistoutDir,imgfilename(1:end-4));
    %     if exist(outp,'file')~=0
    %         continue;
    %     end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    oriimg=imread(inputfilep);
    %     time.year    =  tmvec(1);
    %     time.month   =  tmvec(2);
    %     time.day     =  tmvec(3);
    %     time.hour    =  tmvec(4);
    %     time.min    =   tmvec(5);
    %     time.sec    =   tmvec(6);
    %     time.UTC    =   0;
    %     sun = sun_position(time, location);
    sun=INNER_GETAandZ(tmvec,lat,lon,alt);
    az=sun.azimuth/180*pi+azshift/180*pi;
    ze=sun.zenith/180*pi+zeshift/180*pi;
%     az=sun.azimuth/180*pi
% %     ze=sun.zenith/180*pi;
%     haindexi=GetAZIndex(haazimuth.azs,haazimuth.zes,az,ze);
%     [diffdn,diffi]=min(abs(sbazimuth.tsdn-tmfiledn));
%     if diffdn<DNThres  % trust -2~2 minites data before getting az match
%         sbindexi=diffi; 
%     else
%         sbindexi=GetAZIndex_wThres(sbazimuth.azs,sbazimuth.zes,az,ze,AZThres);
%     end
    haindexi=GetAZIndex(haazimuth.azs,haazimuth.zes,az,ze);
    sbindexi=GetAZIndex_wThres(sbazimuth.tsdn,sbazimuth.azs,sbazimuth.zes,tmfiledn,az,ze);
%     dispstr=sprintf('mask: %s,az= %f, ori:%s,az = %f',...
%         datestr(sbazimuth.tsdn(sbindexi),'[yyyy mm dd HH MM SS]'),...
%         sbazimuth.azs(sbindexi),datestr(tmfiledn,'[yyyy mm dd HH MM SS]'),az);
%     disp(dispstr);
    if(sbindexi==-1)
        disp(datestr(tmvec));
        continue;
    end
    
    %     disp('Debug: sbmask and hamask valid');
    hatmdv=datevec(haazimuth.tsdn(haindexi));
    sbtmdv=datevec(sbazimuth.tsdn(sbindexi));
%     az1=sbazimuth.azs(sbindexi);
%     ze1=sbazimuth.zes(sbindexi);
    %         ii=indexi+2;    %   indexi=i+2 to avoid directory
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% %%%%%%%%%%%
    hamaskfilename=GetImgFromDV_BNL(hatmdv,site);
    hamaskfilename=sprintf('%s%s',hamaskfilename,'.mat');
    sbmaskfilename=GetImgFromDV_BNL(sbtmdv,site);
    sbmaskfilename=sprintf('%s%s',sbmaskfilename,'.mat');
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    hamaskf=sprintf('%s%s',hamaskDir,hamaskfilename);
    sbmaskf=sprintf('%s%s',sbmaskDir,sbmaskfilename);
    s=load(hamaskf, '-mat', 'hama');
    holdma=s.hama;  %   tmma is the final name of holding arm
    s=load(sbmaskf, '-mat', 'sbma');
    shadowma=s.sbma;
    totalmaskma=holdma&shadowma;
    totalmaskma=~totalmaskma;
    %     for j=1:3
    %         tmimgma=oriimg(:,:,j);
    %         tmimgma(totalmaskma)=0;
    %         oriimg(:,:,j)=tmimgma(:,:);
    %     end
    oriimg=INNER_MASK(oriimg,totalmaskma);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %     [ix,iy]=CalImageCoorFrom_ALL(az,ze);
    %     ix=round(ix);iy=round(iy);
    %     if(iy<480&&ix<480&&ix>0&&iy>0)
    %         oriimg(iy,ix,1)=255;
    %         oriimg(iy,ix,2)=255;
    %         oriimg(iy,ix,3)=255;
    %         oriimg(iy-1,ix,1)=255;
    %         oriimg(iy-1,ix,2)=255;
    %         oriimg(iy-1,ix,3)=255;
    %         oriimg(iy+1,ix,1)=255;
    %         oriimg(iy+1,ix,2)=255;
    %         oriimg(iy+1,ix,3)=255;
    %         oriimg(iy,ix-1,1)=255;
    %         oriimg(iy,ix-1,2)=255;
    %         oriimg(iy,ix-1,3)=255;
    %         oriimg(iy,ix+1,1)=255;
    %         oriimg(iy,ix+1,2)=255;
    %         oriimg(iy,ix+1,3)=255;
    %     end
    
    %     [ix,iy]=CalImageCoorFrom_ALL(az1,ze1);
    %     ix=round(ix);iy=round(iy);
    %     if(iy<480&&ix<480&&ix>0&&iy>0)
    %         oriimg(iy,ix,1)=255;
    %         oriimg(iy,ix,2)=0;
    %         oriimg(iy,ix,3)=0;
    %         oriimg(iy-1,ix,1)=255;
    %         oriimg(iy-1,ix,2)=0;
    %         oriimg(iy-1,ix,3)=0;
    %         oriimg(iy+1,ix,1)=255;
    %         oriimg(iy+1,ix,2)=0;
    %         oriimg(iy+1,ix,3)=0;
    %         oriimg(iy,ix-1,1)=255;
    %         oriimg(iy,ix-1,2)=0;
    %         oriimg(iy,ix-1,3)=0;
    %         oriimg(iy,ix+1,1)=255;
    %         oriimg(iy,ix+1,2)=0;
    %         oriimg(iy,ix+1,3)=0;
    %     end
    
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Undistortion Mapping here
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    unimg=uint8(INNER_Undist(oriimg,undist2orima,UNDISTWndist,UNDISTHndist));
    %     for Xu=1:UNDISTW
    %         for Yu=1:UNDISTH
    %             %                 if(Xu==240&&Yu==240)
    %             %                     Xu
    %             %                 end
    %             yo=round(orim(Yu,Xu,1));
    %             xo=round(orim(Yu,Xu,2));
    %             if(xo==0&&yo==0)
    %                 continue;
    %                 %                     unimg(Yu,Xu,1)=0;%black
    %                 %                     unimg(Yu,Xu,2)=0;
    %                 %                     unimg(Yu,Xu,3)=0;
    %             else
    %                 unimg(Yu,Xu,1)=oriimg(yo,xo,1);
    %                 unimg(Yu,Xu,2)=oriimg(yo,xo,2);
    %                 unimg(Yu,Xu,3)=oriimg(yo,xo,3);
    %             end
    %         end
    %     end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Output images. BMP files are generated here
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    outp=sprintf('%s%s.bmp',undistoutDir,imgfilename(1:end-4));
    imwrite(unimg,outp,'bmp');
end
end
