% BuildClearSkyLib:
%   If we have clear sky timestamps to build a library. Then read it and
%   Generate all the mask files and put them together into sbmask and
%   hamask files.
%   INPUT:
%       DataDir         --  Contains the original images
%       HAMaskDir      --  Contains the HA output mask files
%       SBMaskDir      --  Contains the SB output mask files
%       SBMaskOutDir    -- Direcotry, contains output sbmasks
%       TestDir(test)       --  test the mask images
%       RemovalParameters.mat(in)   --  all the readin thresholds
%       UndistortionParameters.mat(in)--Original Image information
%       cls_ts.mat(in)              --  all clear sky timestamp (OBSOLETE)
%       sbmask_pixelrange.bmp       --  bmp file (black -> invalid white -> valid)
%                                       pixels in line regression cannot
%                                       pick points out of white area
%   OUTPUT:
%       HAMaskDir/*    -- All the mask files(.mat) will be put here
%       SBMaskDir/*    -- All the mask files(.mat) will be put here
%       TestDir/*    -- Test result for itself. Fore later manually pick
%
%   Version: 1.0    2012/3/30
%   Version: 1.1    2013/01/24
%       Use SBMask only for testing of July data of 2012
%       Make it as newest version of BNL C1 data for later usage

function BuildClearSkyLib_singlefile(inputf,HAMaskDir,SBMaskOutDir)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Pre-set parameters for edge detection
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if ~exist(inputf,'file')
    dispstr=sprintf('%s doesn''t exist!',inputf);
    disp(dispstr);
    return;
end
filename=GetFilenameFromAbsPath(inputf);
fileinfo=dir(inputf);
if fileinfo.bytes<1000
    dispstr=sprintf('file size is %d, skip it',fileinfo.bytes);
    disp(dispstr);
    return;
end
outfma=sprintf('%s%s.mat',SBMaskOutDir,filename);
if(exist(outfma,'file')~=0)
    disp('output file exists,skip');
    return;
end

%   if not exist define as default
if(exist('HAMaskDir','var')==0)
    HAMaskDir='hamask/';
end
if(exist('SBMaskOutDir','var')==0)
    SBMaskOutDir='sbmaskout/';
end

if(exist('sbmask_ranf','var')==0)
    sbmask_ranf='sbmask_pixelrange.bmp';
    sbmaskranma=imread(sbmask_ranf);
    sbmaskranmask=rgb2gray(sbmaskranma);
    tmindice=(sbmaskranmask>150);
    %     sbmaskranmask(tmindice)=255;
    %     sbmaskranmask(~tmindice)=0;
    sbmaskranmask=tmindice;
end

if(HAMaskDir(end)~='/')
    HAMaskDir=sprintf('%s%s',HAMaskDir,'/');
end
if(SBMaskOutDir(end)~='/')
    SBMaskOutDir=sprintf('%s%s',SBMaskOutDir,'/');
end


load('reflines.mat');
% load('csl_ts.mat');
load('UndistortionParameters.mat');     % TSI1
load('RemovalParameters.mat');  % Preprocessing done for HA and SB mask

ORI_WIDTH=Wi;
ORI_HEIGHT=Hi;
rcentrep.x=ORI_WIDTH/2+CenShiftX;     %real original center(rotating center)
rcentrep.y=ORI_HEIGHT/2+CenShiftY;

midpos=zeros(1,2);
midd=zeros(1,2);

SDIST_START2END=200;        %   Generated search array length, which is
SunSpotRange=20;            %   Sun range around sunpositon which will not
FillRadius=230;              %   Fill the Sun range(SPX-ran:SPX+ran)
UpperLine_DistMovement=43;       %  Upperline is to control the upper end of
%  the shadow band

%   generate any edge detection
%   distributed along the azumith line.
BoundSearchRange=   35;    % from x-30 ->   x+30Edge
BoundaryThres=      0;    % drop range exceeds 30 then treat as boundary
BrightThres=        150;   % if lines points larger than threshold no boundary searche
SearchPixelLen=   180;     % The search pixel number along the mid line
LineShift=  10;      %   Edge move N pixels out to get tolerant line
AngleDist=5/180*pi;        %   Upper bound and lower bound should have less than this threshold angle dfference to middline
TwoLineDist= 40;    %    minimum dist between two edge lines.

inputmatrix=zeros(Hi,Wi);
inputmatrix=uint8(inputmatrix);
greyimg=zeros(Hi,Wi);
greyimg=uint8(greyimg);
greyma=zeros(Hi,Wi);
greyma=uint8(greyma);
gma=zeros(Hi,Wi);
gma=uint8(gma);

maskindexma=zeros(Hi,Wi);

for xr=1:Wi
    for yr=1:Hi
        if(sqrt((xr-rcentrep.x)^2+(yr-rcentrep.y)^2)<=FillRadius)
            maskindexma(yr,xr)=1;
        end
    end
end

saz=pi;
eaz=3*pi/2;

if(exist('TestDir','var')==0)
    TestDir='test/';
end
if(TestDir(end)~='/')
    TestDir=sprintf('%s%s',TestDir,'/');
end
testfilldir='testfill_specialtest_20120409/';        %   test fill is to see SB filling result

if ~exist(testfilldir,'dir')
    mkdir(testfilldir);
end
if ~exist(HAMaskDir,'dir')
    mkdir(HAMaskDir);
end
if ~exist(SBMaskOutDir,'dir')
    mkdir(SBMaskOutDir);
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

ref_tmdv=tmdv;
datevector=ref_tmdv;
time.year    =  datevector(1);
time.month   =  datevector(2);
time.day     =  datevector(3);
time.hour    =  datevector(4);
time.min    =   datevector(5);
time.sec    =   datevector(6);
time.UTC    =   0;
sun = sun_position(time, location);
ref_az=sun.azimuth/180*pi+AzumithShift/180*pi;
ref_ze=sun.zenith/180*pi+ZenithShift/180*pi;
ref_as=as;
ref_bs=bs;



tmdv=GetDVFromImg(filename);
%     datevector=datevec(tsdn(i));
datevector=tmdv;
time.year    =  datevector(1);
time.month   =  datevector(2);
time.day     =  datevector(3);
time.hour    =  datevector(4);
time.min    =   datevector(5);
time.sec    =   datevector(6);
time.UTC    =   0;
sun = sun_position(time, location);
az=sun.azimuth/180*pi;
ze=sun.zenith/180*pi;
%     if(az<saz||az>eaz)
%         continue;
%     end

%     azs(i)=az;
%     zes(i)=ze;

%     filename=GetImgFromDV_TWPC1(datevector);
%     inputf=sprintf('%s%s',DataDir,filename);


outputf=sprintf('%s%s',testfilldir,filename);
%     if(exist(outputf,'file')~=0)
%         return;
%     end
img1=imread(inputf);    %original image readin
f=img1(:,:,3);
%     f1=img1(:,:,1);
%     f2=img1(:,:,2);
%     index1=((f2-f)>=MaxDiffRan)&((f2-f1)>0)&f2<CloudThres;  %SB and HA index1 calculation is the same
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   HA Mask
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     hama=uint8(zeros(Hi,Wi));
%     index=((f>=BLUETHRES_HA)&~index1)|~areamask;   %Holding arm
%     hama(index)=1;
%     %     outm=zeros(Hi,Wi);
%     %     outp=sprintf('%s%s',TestDir,filename);
%     %     outm(index)=255;
%     %     imwrite(outm,outp,'jpg');
%     hamaskf=sprintf('%s%s.mat',HAMaskDir,filename);
%     save(hamaskf,'hama');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   SB Mask
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
sbma=edge(f,'sobel');
%     sbedgema=sbma;
sbedgema=sbma&sbmaskranmask;
%     sbmaskf=sprintf('%s%s.mat',SBMaskDir,filename);
%     save(sbmaskf,'sbma');
%     sbma=uint8(zeros(Hi,Wi));
%     index=((f>=BLUETHRES_SB)&~index1);   %SB Extraction
%     sbma(index)=1;
%     %     outm=zeros(Hi,Wi);
%     %     outp=sprintf('%s%s',TestDir,filename);
%     %     outm(index)=255;
%     %     imwrite(outm,outp,'jpg');
%     sbmaskf=sprintf('%s%s.mat',SBMaskDir,filename);
%     save(sbmaskf,'sbma');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   SB Edge test -- single
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[x,y]=CalImageCoorFrom_ALL(az,ze);
az=az+AzumithShift/180*pi;
ze=ze+ZenithShift/180*pi;
azdiff=az-ref_az;
if(azdiff==pi/2)
    newref_as=1./ref_as;
else
    if isempty(find((ref_as.*tan(azdiff)-1)==0, 1)) ==0
        disp('pi/2 appears, do not consider this situation');
        return;
    end
    newref_as=(ref_as+tan(azdiff))./(1-ref_as.*tan(azdiff));
end

aangle=az-pi/2;
if(abs(double(aangle)-double(pi/2))<0.001)
    % x=x1
    a_midline=-9999;      % stands for infinite
    b_midline=-9999;
    
else
    a_midline=tan(aangle);
    b_midline=y-a_midline*x;
end
midpos=[rcentrep.x,rcentrep.y];
[tmpx,tmpy]=GetEndPoint(midpos(1),midpos(2),...
    a_midline,b_midline,SDIST_START2END);
%   Start point has the same vector direction with sun position then
%   we can get right end point
if(sign(x-midpos(1))==sign(tmpx(1)-midpos(1)))
    midd(1)=tmpx(1);
    midd(2)=tmpy(1);
else
    midd(1)=tmpx(2);
    midd(2)=tmpy(2);
end

if(x-midpos(1)>0)
    [midd(1),tmpp]=max(tmpx);
    midd(2)=tmpy(1,tmpp);
else
    [midd(1),tmpp]=min(tmpx);
    midd(2)=tmpy(1,tmpp);
end
clear tmpx tmpy;
inputmatrix=sbma;
greyimg(:,:)=inputmatrix(:,:);
[SXArray,SYArray]=GetSearchArray(a_midline,b_midline,...
    midpos(1,:),midd(1,:),SearchPixelLen,SDIST_START2END);
%   Rebuild SXArray and SYArray to fit the edge
%     [SXArray,SYArray]=ReGenArrayOnBoundary(SXArray,SYArray,x,y,SunSpotRange);
[B_UpArray,B_DownArray]=FindBoundary(SXArray,SYArray,BoundSearchRange,...
    BoundaryThres,BrightThres,a_midline,b_midline,greyimg);
LPUpArray=     [B_UpArray(B_UpArray(:,1)>0) B_UpArray(B_UpArray(:,2)>0,2)];
LPDownArray=   [B_DownArray(B_DownArray(:,1)>0) B_DownArray(B_DownArray(:,2)>0,2)];
lpdist=LPUpArray(:,1).*a_midline-LPUpArray(:,2)+b_midline;
id=abs(lpdist-mean(lpdist))<=2*std(lpdist);
tmpx = LPUpArray(id,1);
tmpy = LPUpArray(id,2);
if(isempty(tmpx)~=0||isempty(tmpy)~=0)
    disp('No boundary found, skip this TS');
    return;
end
if length(tmpx)<=20||length(tmpx)<=20
    disp('boundary is not defined since points are less than 20');
    return;
end
LPUpArray=[tmpx,tmpy];
lpdist=LPDownArray(:,1).*a_midline-LPDownArray(:,2)+b_midline;
id=abs(lpdist-mean(lpdist))<=2*std(lpdist);
tmpx = LPDownArray(id,1);
tmpy = LPDownArray(id,2);
LPDirect=LPDirection(az);
LPDownArray=[tmpx,tmpy];
if(isempty(tmpx)~=0||isempty(tmpy)~=0)
    disp('No boundary found, skip this TS');
    return;
end
if length(tmpx)<=20||length(tmpx)<=20
    disp('boundary is not defined since points are less than 20');
    return;
end

[as bs]=FindLinearApprox(LPUpArray,LPDownArray,LPDirect);
LinRegreDropThres=  8;
clear tmpx tmpy;
[tmpx,tmpy]=GetNewLineArrays(as(1),bs(1),LPUpArray(:,1),LPUpArray(:,2),...
    LinRegreDropThres,LPDirect);
[tmpx1,tmpy1]=GetNewLineArrays(as(2),bs(2),LPDownArray(:,1),...
    LPDownArray(:,2),LinRegreDropThres,LPDirect);
if(isempty(tmpx1)~=0||isempty(tmpy1)~=0)
    disp('No boundary found, skip this TS');
    return;
end
[asnew bsnew]=FindLinearApprox([tmpx,tmpy],[tmpx1,tmpy1],LPDirect);
as=asnew;
bs=bsnew;
%     save('reflines.mat','LPDirect','as','bs','tmdv');


%   filter points that are not regular with AngleDist
%     midgrad=a_midline;
midgrad=newref_as;
if(LPDirect=='x')
    %Ax-y+B=0
    linegrad=as;    %-A/B
    tangenval=abs(linegrad-midgrad)./abs(1+linegrad.*midgrad);
    angledi=atan(tangenval)-AngleDist;
    normindex=find(angledi<=0);
    nofnorm=length(normindex);
    if(nofnorm==2)
        %             return;
    elseif nofnorm ==1
        disp(' nofnorm ==1');
        return;
        %   Choose the normal one,the other one use its gradient
        %             if(normindex(1)==1)
        %                 norma=as(1);
        %                 normb=bs(1);
        %                 downarr_b=tmpy1-norma.*tmpx1;
        %                 paradist=abs(downarr_b-normb)./sqrt(norma^2+1);    % abs(B/sqrt(1+A^2))
        %                 meandown_b=mean(downarr_b(paradist>TwoLineDist));  % valid mean\
        %                 if(isnan(meandown_b))
        %                     meandown_b=40       % test for nan
        %                 end
        %                 as(2)=norma;
        %                 bs(2)=meandown_b;
        %             else
        %                 norma=as(2);
        %                 normb=bs(2);
        %                 uparr_b=tmpy-norma.*tmpx;
        %                 paradist=abs(uparr_b-normb)./sqrt(norma^2+1);    % abs(B/sqrt(1+A^2))
        %                 meandown_b=mean(uparr_b(paradist>TwoLineDist));  % valid mean
        %                 if(isnan(meandown_b))
        %                     meandown_b=40       % test for nan
        %                 end
        %                 as(1)=norma;
        %                 bs(1)=meandown_b;
        %             end
    elseif nofnorm ==0
        disp(' nofnorm ==0');
        return;
        %   Choose middle line, two lines both use its gradient
        %             norma=a_midline;
        %             normb=b_midline;
        %             uparr_b=mean(tmpy-norma.*tmpx);
        %             downarr_b=mean(tmpy1-norma.*tmpx1);     % need to be tuned
        %             as(:)=norma;
        %             bs(1)=uparr_b;
        %             bs(2)=downarr_b;
    else
        disp('Wrong dimension of a and b to form two lines!');
        return;
    end
else
    %Ay-x+B=0
    linegrad=1./as;    %1/A
    tangenval=abs(linegrad-midgrad)./abs(1+linegrad.*midgrad);
    angledi=atan(tangenval)-AngleDist;
    normindex=find(angledi<=0);
    nofnorm=length(normindex);
    if(nofnorm==2)
        %             return;
    elseif nofnorm ==1
        disp(' nofnorm ==1');
        %   Choose the normal one,the other one use its gradient
        return;
        %             if(normindex(1)==1)
        %                 norma=as(1);    %   A
        %                 normb=bs(1);    %   B
        %                 downarr_b=tmpx1-norma.*tmpy1;   %   B = x-Ay
        %                 paradist=abs(downarr_b-normb)./sqrt(norma^2+1);    % abs(B/sqrt(1+A^2))
        %                 meandown_b=mean(downarr_b(paradist>TwoLineDist));  % valid mean\
        %                 if(isnan(meandown_b))
        %                     meandown_b=40       % test for nan
        %                 end
        %                 as(2)=norma;
        %                 bs(2)=meandown_b;
        %             else
        %                 norma=as(2);
        %                 normb=bs(2);
        %                 uparr_b=tmpx-norma.*tmpy;   %   B = x-Ay
        %                 paradist=abs(uparr_b-normb)./sqrt(norma^2+1);    % abs(B/sqrt(1+A^2))
        %                 meandown_b=mean(uparr_b(paradist>TwoLineDist));  % valid mean
        %                 if(isnan(meandown_b))
        %                     meandown_b=40       % test for nan
        %                 end
        %                 as(1)=norma;
        %                 bs(1)=meandown_b;
        %             end
    elseif nofnorm ==0
        disp(' nofnorm ==0');
        %   Choose middle line, two lines both use its gradient
        return;
        %             norma=a_midline;
        %             normb=b_midline;
        %             uparr_b=mean(tmpy-norma.*tmpx);
        %             downarr_b=mean(tmpy1-norma.*tmpx1);     % need to be tuned
        %             as(:)=1/norma; % A=1/a
        %             bs(1)=-uparr_b/norma;  % B= -b/a
        %             bs(2)=-uparr_b/norma; % B= -b/a
    else
        disp('Wrong dimension of a and b to form two lines!');
        return;
    end
end


if(LPDirect=='x')
    %Ax-y+B=0
    if(sign(as(1)*x+bs(1)-y)<0)
        bs(1)=bs(1)-LineShift;
    else
        bs(1)=bs(1)+LineShift;
    end
else
    %Ay-x+B=0
    if(sign(as(1)*y+bs(1)-x)<0)
        bs(1)=bs(1)-LineShift;
    else
        bs(1)=bs(1)+LineShift;
    end
end
if(LPDirect=='x')
    %Ax-y+B=0
    if(sign(as(2)*x+bs(2)-y)<0)
        bs(2)=bs(2)-LineShift;
    else
        bs(2)=bs(2)+LineShift;
    end
else
    %Ay-x+B=0
    if(sign(as(2)*y+bs(2)-x)<0)
        bs(2)=bs(2)-LineShift;
    else
        bs(2)=bs(2)+LineShift;
    end
end
%function [Xarr,Yarr]=GenFillPoints(SPx,SPy,FillRadius)
greyimg=rgb2gray(img1);
gma(:,:)=sbma(:,:);
%         Xarr=zeros((2*FillRadius)^2,1);
%         Yarr=zeros((2*FillRadius)^2,1);
%         ci=1;
SPx=round(x);
SPy=round(y);
cenl_a=-1/a_midline;
cenl_b=-rcentrep.x*cenl_a+rcentrep.y;
cenl_sign=sign(x*cenl_a-y+cenl_b);
uppl_a=cenl_a;
uppl_b=cenl_b+cenl_sign*sqrt(1+cenl_a^2)*UpperLine_DistMovement;

for xr=1:Wi
    for yr=1:Hi
        if(maskindexma(yr,xr)==0)
            gma(yr,xr)=1;
            continue;
        end
        %   a. If beyond upper line then fill 1
        if(sign(uppl_a*xr-yr+uppl_b)~=cenl_sign)
            %                 greyimg(yr,xr)=255;
            gma(yr,xr)=1;
            continue;
        end
        %   b. Should be inside of two edge lines otherwise fill 1
        if(LPDirect=='x')
            if(sign(as(1)*xr-yr+bs(1))==sign(as(2)*xr-yr+bs(2)))
                %                     greyimg(yr,xr)=255;
                gma(yr,xr)=1;
                continue;
            end
        else
            if(sign(as(1)*yr-xr+bs(1))==sign(as(2)*yr-xr+bs(2)))
                %                     greyimg(yr,xr)=255;
                gma(yr,xr)=1;
                continue;
            end
        end
        % c. should be the same side with SP to central Line
        %    otherwise continue without filling
        %             if(sign(cenl_a*xr-yr+cenl_b)~=cenl_sign)
        %                 gma(yr,xr)=1;
        %                 continue;
        %             end
        % d. Should be in Fill Circle
        %    otherwise continue without filling
        %             if(sqrt((xr-x)^2+(yr-y)^2)>FillRadius)
        %                 gma(yr,xr)=1;
        %                 continue;
        %             end
        %             greyimg(yr,xr)=0;   %all condition satisfied
        gma(yr,xr)=0;
    end
end
greyimg(gma>0)=255;
greyimg(gma==0)=0;

%     outfma=sprintf('%s%s.mat',SBMaskOutDir,filename);
%     outputf=sprintf('%s%s',testfilldir,filename);
%     imwrite(greyimg,outputf,'jpg');
%             outputma(ii,:,:)=gma;

%     save(maoutf,'gma');
clear SXArray SYArray B_UpArray B_DownArray LPUpArray LPDownArray aa id tmpx tmpy tmpx1 tmpy1 greyma greyimg inputimg;
sbma(:,:)=gma(:,:);
outfma=sprintf('%s%s.mat',SBMaskOutDir,filename);
save(outfma,'sbma');



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Test for result put the result to TestDir
%   Uncomment to use
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %     outputimf=sprintf('%s%s',TestDir,filename);
% %     totalmaskma=~(gma&hama);
%     totalmaskma=~(gma);
% %     img2=img1;
%     tmcolor=zeros(Hi,Wi);
%     for j=1:3
% %         img2(totalmaskma,j)=img1(totalmaskma,j);
%         tmcolor(:,:)=img1(:,:,j);
%         tmcolor(totalmaskma)=0;
%         tmcolor(sbedgema)=255;
%         img1(:,:,j)=tmcolor;
%     end
%     outputf=sprintf('%s%s',testfilldir,filename);
%     imwrite(img1,outputf,'jpg');
% %     outputf=sprintf('%s%s.mat',outputDir,filename);
end
