function [inputparas,outputparas]=getfuncdef(fhandle)

funcname=func2str(fhandle);
funcfile=[funcname '.m'];
assert(~exist(funcfile,'file'),'fhandle file cannot be found using default matlab searching path');
funccont=fileread(funcfile);
funcdefformat=['\s*function \S*={0,1}\s*' funcname '\(\S*\)\s*'];


% function [t1,t2]=tt()
funcdefformat_noinput='function \S+=\w+\(\)\s*$';     % function [t1,t2]=tt()
funcdefformat_nooutput='function \w+\(\S+\)\s*$';     % function tt(...)
funcdefformat_singleoutput='function \w+=\w+(\w,\w+)';


end