%   This function is a module of cloud tracking algorithm
%   INPUT:
%       HRef        --- 2x1 Int, [HRef(1) HRef(2)] reference layer
%       MVRef       --- 2x2 Int, [mv_y1 mv_x1;mv_y2 mv_x2] reference layer
%       CFMaskMa    --- [UNDISTH,UNDISTW] Int, CF lables matrix
%       tmdv        --- Datevector, current frame
%       dataDir     --- String, directory of TSIs
%       outmatf     --- String, output mat file, given as input
%   VERSION 1.0     2013/10/07
%   VERSION 2.0     2014/04/05 Add logger.
%   VERSION 2.0.1   2014/04/18  Fix the bug of reading bad tmp MAT file
%
function CldTrack_2(HRef,MVRef,CFMaskMa,tmdv,dataDir,outmatf,configfpath,bMulticore)
run(configfpath);
%mtsi_startup_mini;
servertype=64;
maxmatlabinst=24;
WAITN=60*10;    % wait for only ten minutes or 600s
logger = log4m.getLogger('./log/CldTrack_2.log');
loggerID=datestr(tmdv,'yyyy-mm-dd HH:MM:SS');
logger.setLogLevel(logger.DEBUG);
logger.setCommandWindowLevel(logger.DEBUG);


if isempty(find(HRef~=0,1))
    bFullSearch=true;
else
    bFullSearch=false;
    if HRef(2)~=0
        assert(HRef(1)<HRef(2),'The HRef should be ascendent!');
    end
end

tmvals=CFMaskMa(:);
nCF=max(tmvals);  % from 1 ~ maxval
nConn=nCF;
ConnLabelma=CFMaskMa;


MV_x=cell(nConn,1); MV_y=cell(nConn,1);
HMV_x=cell(nConn,2); HMV_y=cell(nConn,2);
H=cell(nConn,1);     MAXCC=cell(nConn,1);
TrustMVCounts=cell(nConn,1);
Conn_H=zeros(nConn,1); Conn_W=zeros(nConn,1);
Conn_sx=zeros(nConn,1);Conn_sy=zeros(nConn,1);
Conn_cldcount=zeros(nConn,1);
if bFullSearch==false
    if HRef(1)~=0
        HRan1=TO_GenHSearchRange(HRef(1));
    else
        HRan1=[];
    end
    if HRef(2)~=0
        HRan2=TO_GenHSearchRange(HRef(2));
    else
        HRan2=[];
    end
    
    Hsran_quick=[HRan1 HRan2];
    Hsran_quick=unique(Hsran_quick);    % make sure there is no collapse
    %Hsran_quick=Hsran_quick(Hsran_quick>=1000);
else
    Hsran_quick=H_sran;
end

if ~bMulticore
    for tmi=1:nCF
        disp(tmi);
        tmind=find(ConnLabelma==tmi);
        Conn_cldcount(tmi)=length(tmind);
        [y,x]=ind2sub([UNDISTH,UNDISTW],tmind);
        minsy=min(y);
        minsx=min(x);
        recH=max(y)-min(y)+1;
        recW=max(x)-min(x)+1;
        Conn_sx(tmi)=minsx;Conn_sy(tmi)=minsy;
        Conn_H(tmi)=recH;Conn_W(tmi)=recW;
        tmblkmask=false(UNDISTH,UNDISTW);
        tmblkmask(minsy:minsy+recH-1,minsx:minsx+recW-1)=true;
        [tmH,MV,HMV,cc_all,maxcc,HCredCounts,bCloseNeighbor]=nccsim_pair_3f(...
            tmdv,TENSECS_DN,[minsy minsx],[recH recW],NextFrameMVSearchWinSize,...
            dataDir,Hsran_quick,ncc_thres,tmblkmask,CLDFRACTHRES,configfpath);
        %     if bFullSearch==false && maxcc==0
        %         [tmH,MV,HMV,cc_all,maxcc]=nccsim(...
        %             tmdns,[minsy minsx],[recH recW],NextFrameMVSearchWinSize,...
        %             dataDir,H_sran,ncc_thres,tmblkmask,CLDFRACTHRES,configfpath);
        %     end
        if (0==maxcc(1))
            continue;
        end
        ind=Hsran_quick<5000;
        HLowCredCounts=HCredCounts(ind);
        
        if true==bCloseNeighbor && isempty(find(HLowCredCounts~=0,1))
            disp('This case is skipped since it is close to the neighbor and search low altitude is INVALID');
            %continue;
        end
        
        H{tmi}=tmH;
        MV_y{tmi}=MV(:,1);    MV_x{tmi}=MV(:,2);
        TrustMVCounts{tmi}=HCredCounts;
        %     HMV_y{tmi,1}=HMV(1,1); HMV_x{tmi,1}=HMV(1,2);
        %     HMV_y{tmi,2}=HMV(2,1); HMV_x{tmi,2}=HMV(2,2);
        MAXCC{tmi}=maxcc;
    end
else
    %disp('[WARNING]: multicore switch is on.');
    logger.debug(loggerID,'[WARNING]: multicore switch is on.');
    % 1. Call nccsim function for each CF
    for tmi=1:nCF
        tmind=find(ConnLabelma==tmi);
        Conn_cldcount(tmi)=length(tmind);
        [y,x]=ind2sub([UNDISTH,UNDISTW],tmind);
        minsy=min(y);
        minsx=min(x);
        recH=max(y)-min(y)+1;
        recW=max(x)-min(x)+1;
        Conn_sx(tmi)=minsx;Conn_sy(tmi)=minsy;
        Conn_H(tmi)=recH;Conn_W(tmi)=recW;
        cenx=(max(x)+min(x))/2;
        ceny=(max(x)+min(x))/2;
        d2center=sqrt((cenx-rcentrep3.x)^2+(ceny-rcentrep3.y)^2);
        if (d2center>=200)
            % not trusted area
            logger.debug(loggerID,['[WARNING]: CF ' int2str(tmi) ' is too far way from centre. Skip this.']);
            continue;
        end
        tmblkmask=false(UNDISTH,UNDISTW);
        tmblkmask(minsy:minsy+recH-1,minsx:minsx+recW-1)=true;
        inputmatf=[outmatf(1:end-4) '_i' int2str(tmi) '.mat'];
        outputmatf=[outmatf(1:end-4) '_o' int2str(tmi) '.mat'];
        TSI3StartPt=[minsy minsx];
        nextframe_dn=TENSECS_DN;
        TSI3RecSize=[recH recW];
        MVWinSize=NextFrameMVSearchWinSize;
        HSearchRange=Hsran_quick;
        NCCMiniThres=ncc_thres;
        TSI3BlkMask=tmblkmask;
        MVRef=MVRef;
        save(inputmatf,'tmdv','nextframe_dn','TSI3StartPt','TSI3RecSize','MVWinSize','MVRef','dataDir',...
            'HSearchRange','NCCMiniThres','TSI3BlkMask','CLDFRACTHRES','configfpath');
        cmmd=sprintf('nccsim_pair_3f_multi(''%s'',''%s'');',inputmatf,outputmatf);
        if (~exist(outputmatf,'file'))
            %disp(cmmd);
            logger.debug(loggerID,cmmd);
            %nccsim_pair_3f_multi(inputmatf,outputmatf);
            subprocess_matlab(cmmd,maxmatlabinst,servertype);
            pause(0.5);
        else
            logger.debug(loggerID,sprintf('cf = %d is skipped.',tmi));
        end
        
    end
    
    % 2. Aggregate all CF results
    sleepcount=WAITN;
    for tmi=1:nCF
        tmind=find(ConnLabelma==tmi);
        [y,x]=ind2sub([UNDISTH,UNDISTW],tmind);
        cenx=(max(x)+min(x))/2;
        ceny=(max(x)+min(x))/2;
        d2center=sqrt((cenx-rcentrep3.x)^2+(ceny-rcentrep3.y)^2);
        if (d2center>=200)
            continue;
        end
        inputmatf=[outmatf(1:end-4) '_i' int2str(tmi) '.mat'];
        outputmatf=[outmatf(1:end-4) '_o' int2str(tmi) '.mat'];
        while (~exist(outputmatf,'file') && sleepcount>=0)
            pause(1);
            sleepcount=sleepcount-1;
        end
        if exist(outputmatf,'file')
            try
                tmt=load(outputmatf);
                % 'H','MV','HMV','cc_all','maxcc','HCredCounts','bCloseNeighbor');
                if(~isempty(tmt.H))
                    H{tmi}=tmt.H;   MV_y{tmi}=tmt.MV(:,1);  MV_x{tmi}=tmt.MV(:,2);
                    TrustMVCounts{tmi}=tmt.HCredCounts;
                    MAXCC{tmi}=tmt.maxcc;
                end
            catch err
                disp('[ERROR]: Cannot load file. Try to load it again..');
                pause(0.5);
                tmt=load(outputmatf);
                % 'H','MV','HMV','cc_all','maxcc','HCredCounts','bCloseNeighbor');
                if(~isempty(tmt.H))
                    H{tmi}=tmt.H;   MV_y{tmi}=tmt.MV(:,1);  MV_x{tmi}=tmt.MV(:,2);
                    TrustMVCounts{tmi}=tmt.HCredCounts;
                    MAXCC{tmi}=tmt.maxcc;
                end
            end
        else
            logger.debug(loggerID,sprintf('MAX WAIT TIME = %d has expired. cf = %d will be choosen as default value.',WAITN,tmi));
            %dispstr=sprintf('MAX Wait time %ds has passed. Choose default null values!',WAITN);
            %disp(dispstr);
        end
    end
    
    
    % 3. Remove temp in and out mat files
    for tmi=1:nCF
        inputmatf=[outmatf(1:end-4) '_i' int2str(tmi) '.mat'];
        outputmatf=[outmatf(1:end-4) '_o' int2str(tmi) '.mat'];
        if (exist(inputmatf,'file')&&exist(outputmatf,'file'))
            delete(inputmatf);
            delete(outputmatf);
        end
    end
    
end
save(outmatf,'H','MV_y','MV_x','TrustMVCounts','HMV_x','HMV_y','MAXCC','CFMaskMa','nCF','Conn_sx','Conn_sy','Conn_H','Conn_W');
%save('cldtrk2_test.mat','H','MV_y','MV_x','HMV_x','HMV_y','MAXCC','CFMaskMa','nCF','Conn_sx','Conn_sy','Conn_H','Conn_W');


end