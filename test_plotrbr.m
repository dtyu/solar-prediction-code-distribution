function [rbr rbr_norm]=test_plotrbr(imma)
r=imma(:,:,1);
b=imma(:,:,3);
rbr=double(r)./double(b);
invalidind=isnan(rbr);
rbr(invalidind)=0;
validrbr=rbr(rbr~=inf);
maxr=max(validrbr);
minr=min(validrbr);
rbr(rbr==inf)=maxr;
rbr_norm=(rbr-minr)./(maxr-minr);

end