function VectExtraction(coor,DegreeThres)
%   coor= Nx2 matrix
[m,n]=size(coor);
v=zeros(m,1);

for i=1:m-1
    x=coor(i,1);
    y=coor(i,2);
    x1=coor(i+1,1);
    y1=coor(i+1,2);
    v(i)=(x-x1)/(y-y1);
end

avg_v=mean(v);
avg_a=atan(avg_v);


for i=1:m-1
    x=coor(i,1);
    y=coor(i,2);
    x1=coor(i+1,1);
    y1=coor(i+1,2);
    v(i)=(x-x1)/(y-y1);
end

end