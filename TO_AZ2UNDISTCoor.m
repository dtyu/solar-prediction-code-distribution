function [undistx,undisty]=TO_AZ2UNDISTCoor(az,ze)
mtsi_startup;
undistcen_x=UNDISTW/2;
undistcen_y=UNDISTH/2;
% r/UNDISTR=tan(ze)/tan(va)
tan_va=tan(ViewAngle_r/2);
r=tan(ze)*UNDISTR/tan_va;
rx=r*sin(az);
ry=r*cos(az);

% rx=undistx-undistcen_x;
% ry=undistcen_y-undisty;
undistx=rx+undistcen_x;
undisty=undistcen_y-ry;

end