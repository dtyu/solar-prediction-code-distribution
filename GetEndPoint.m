    function [x,y]=GetEndPoint(x0,y0,a,b,d)
        %ax+b=y
%         A=(1+a^2);
%         B=-2*x0+2*a*(b-y0);
%         C=x0^2+(b-y0)^2-d^2;
%         x=[(-B+sqrt(B^2-4*A*C))/(2*A),(-B-sqrt(B^2-4*A*C))/(2*A)];
        paras=d/sqrt(1+a^2);
        x=[x0+paras, x0-paras];
        y=x*a+b;
    end