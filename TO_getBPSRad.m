% This function will call extract radiation and put them into each mat file
function radvals =TO_getBPSRad(startdv,enddv)
startdn=datenum(startdv);
enddn=datenum(enddv);
ONESEC_DN=datenum([0 0 0 0 0 1]);
NRECS=enddn-startdn/ONESEC_DN;
%BPSRADTIMEFORMAT='yyyy-mm-dd_HH-MM-SS';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% InsertPrediction()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% REPLACE INTO %s values (%d,%s,%s);";
TBName='BPS_SEC';
MySQLServerURL='solar-db.bnl.gov';
username='realtime_matlab';
password='realtime_matlab';
DBName='SolarFarm';
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% select least dist
if (0~=mysql('status'))
    mysql('open',MySQLServerURL,username,password);
    mysql(['use ' DBName]);
end
selectcmmd_bps='select SP2A_H_Avg,BPS_N,time_stamp from %s where time_stamp >= %d and time_stamp < %d;';
BPSN=25;
dbtable=TBName;
radvals=zeros(NRECS,BPSN);

ts_gmt=int32(TO_dn2unix(startdn));
start_est=int32(ts_gmt-5*3600);
ts_gmt=int32(TO_dn2unix(enddn));
end_est=int32(ts_gmt-5*3600);
cmmd=sprintf(selectcmmd_bps,dbtable,start_est,end_est);
[rad,stn,ts]=mysql(cmmd);
dns=TO_unix2dn(ts+3600*5);
ind=round((dns-startdn)./ONESEC_DN)+1;
l = length(rad);
for i = 1:l
    radvals(ind(i),stn(i))=rad(i);
end


mysql('close');



end