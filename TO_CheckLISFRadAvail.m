%   This function is to generate LISF radiation availability mat file. 
%   VERSION 1.0         Currently DEBUG version
%   USAGE:
%       [LISFRADavail,LISFRADavail_days,LISFRAD_all,LISFRAD_days]=TO_CheckLISFRadAvail('/home/mikegrup/D/workspace/SolarFarmDataProcessing/BPS_SP2A_H_Avg_MIN_days_dat/');
%       See mtsipipe_GenDataset.m for details
%       



function [LISFRADavail,LISFRADavail_days,LISFRAD_all,LISFRAD_days]=TO_CheckLISFRadAvail(sfradDir,startdv,enddv)
mode=0;
indexi=strfind(sfradDir,'MIN');
if ~isempty(indexi)
    mode=1; % MINUTE
end
indexi=strfind(sfradDir,'SEC');
if ~isempty(indexi)
    mode=2; % SECOND
end
assert(mode==1,'Current version support only minute data!');

lisfpbs_startup;

%% minute file
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if 1==nargin
    startdv=[2013 04 01 0 0 0];         % Before this date, the Radiation file seems to be GMT/UTC. From this date, timestamp is EST.
    enddv=[2015 08 01 0 0 0];
end
startdn=datenum(startdv);
enddn=datenum(enddv);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

ONEDAY_DN=datenum([0 0 1 0 0 0]);
ONEDAY=60*24;                       % minutes in a day
ONEMINDN=datenum([0 0 0 0 1 0]);
tmdn=startdn;
NDAYS=1;
while (tmdn< enddn)
    NDAYS=NDAYS+1;
    tmdn=startdn+(NDAYS-1)*ONEDAY_DN;
end

% 1. Check available days
LISFRADavail_days=zeros(NDAYS,1);
for iday= 1:NDAYS
    bexist=true;
    tmdn=startdn+(iday-1)*ONEDAY_DN;
    tmdv=datevec(tmdn);
    for i=1:NPBS
        bpssubDir=sprintf('%sBPS_%d',sfradDir,i);
        bpssubDir=FormatDirName(bpssubDir);
        filenamestr=datestr(tmdv,BPSDATTIMEFORMAT);
        inputf=sprintf('%s%s.dat',bpssubDir,filenamestr);
        if ~exist(inputf,'file')
            bexist=false;
            break;
        end
    end
    if bexist
        LISFRADavail_days(iday)=tmdn;
    end
end

LISFRADavail_days=LISFRADavail_days(LISFRADavail_days~=0);
LISFRADavail_days=sort(LISFRADavail_days,'ascend');
NDAYS=length(LISFRADavail_days);
assert(NDAYS~=0,'The LISF datadir is empty!');
% 2. Read into LISFRAD_days
LISFRAD_days=zeros(ONEDAY,NDAYS,NPBS);
%LISFRADavail=zeros(ONEDAY*NDAYS);
LISFRADavail=[];
LISFRAD_all=zeros(ONEDAY*NDAYS,NPBS);
for iday= 1:NDAYS
    tmdn=LISFRADavail_days(iday);
    tmdv=datevec(tmdn);
    if tmdn<datenum([2013 03 22 0 0 0])
        continue;
    end
    od_ts1=repmat(tmdn,ONEDAY,1);
    od_ts1=od_ts1+(0:ONEDAY-1)'.*ONEMINDN;
    bDisplay=false;
    tmcounts=zeros(NPBS,1);
    for i=1:NPBS
        od_rad=zeros(ONEDAY,1);
        od_ts=zeros(ONEDAY,1);
        bpssubDir=sprintf('%sBPS_%d',sfradDir,i);
        bpssubDir=FormatDirName(bpssubDir);
        filenamestr=datestr(tmdv,BPSDATTIMEFORMAT);
        inputf=sprintf('%s%s.dat',bpssubDir,filenamestr);
        fid = fopen(inputf);
        data = textscan(fid,'%d\t%f','HeaderLines',1,'CollectOutput',1);
        data1 = data{1}; data2 = data{2};
        fclose(fid);
        if length(data1)==ONEDAY
            od_rad(:)=data2(:);
%             od_ts(:)= unixts2datenum(data1);
%             if ~isempty(find(od_ts1~=od_ts,1))
%                 od_ts1;
%             end

            tmcounts(i)=ONEDAY;
        else
            bDisplay=true;
            tmdns=unixts2datenum(data1);
            tminds=round((tmdns-tmdn)./ONEMINDN)+1;
            tmcounts(i)=length(tminds);
            od_rad(tminds)=data2(:);
        end
        LISFRAD_days(:,iday,i)=od_rad(:);
        starti=1+(iday-1)*ONEDAY;
        endi=iday*ONEDAY;
        LISFRAD_all(starti:endi,i)=od_rad(:);
    end
    if bDisplay
        disp(tmdv);
        disp(tmcounts');
    end
    
    LISFRADavail(starti:endi)=od_ts1(:);
end




% LISFRAD_days=zeros(ONEDAY,days,25);
%     tmdv=datevec(tmdn);
%     disp(tmdv);
%     od_rad=zeros(ONEDAY,NPBS);
%     od_ts=zeros(ONEDAY,NPBS);
%     sfradDir=FormatDirName(sfradDir);
%     for i=1:NPBS
%         bpssubDir=sprintf('%sBPS_%d',sfradDir,i);
%         bpssubDir=FormatDirName(bpssubDir);
%         filenamestr=datestr(tmdv,BPSDATTIMEFORMAT);
%         inputf=sprintf('%s%s.dat',bpssubDir,filenamestr);
%         fid = fopen(inputf);
%         data = textscan(fid,'%d\t%f','HeaderLines',1,'CollectOutput',1);
%         data1 = data{1}; data2 = data{2};
%         fclose(fid);
%         if length(data1)==ONEDAY
%             od_rad(:,i)=data2(:);
%             od_ts(:,i)= unixts2datenum(data1);
%         else
%             tmdns=unixts2datenum(data1);
%             tminds=round((tmdns-tmdn)./ONEMINDN)+1;
%             od_rad(tminds,i)=data2(:);
%         end
%     end
%     
%     tmdn=tmdn+ONEDAY_DN;
% end








end