function rbrma=GetRBR(imma)
r1=double(imma(:,:,1));
g1=double(imma(:,:,2));
b1=double(imma(:,:,3));

rbrma=r1./b1;


end