function [az,ze]=sun_position_new(tmdns,location)
l=length(tmdns);
az=zeros(l,1);  ze=zeros(l,1);
tmdvs=datevec(tmdns);
for i=1:l
    time.year    =  tmdvs(i,1);
    time.month   =  tmdvs(i,2);
    time.day     =  tmdvs(i,3);
    time.hour    =  tmdvs(i,4);
    time.min    =   tmdvs(i,5);
    time.sec    =   tmdvs(i,6);
    sun = sun_position(time, location);
    az(i)=sun.azimuth;
    ze(i)=sun.zenith;
end


end