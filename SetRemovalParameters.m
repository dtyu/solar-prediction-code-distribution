% SetRemovalParameters:
%   It will provide all pre-set parameters such as thresholds to control
%   the mask files and save them into an output file
%   Version: 1.0    2012/3/30   
function SetRemovalParameters
load('UndistortionParameters.mat');
CloudThres=170;        % if blue > CloudThres treat as have-cloud 
MinALLThres=90;        % 
MaxDiffRan=15;         % Blue - Green Should be larger than this
BLUETHRES_SB=140;         % Basic Blue Threshold to consider as clear-sky(SB)
BLUETHRES_HA=120;         % Basic Blue Threshold to consider as clear-sky(SB)


harectwid=50;
haupheight=250;
hadownheight=65;
ORI_WIDTH=Wi;
ORI_HEIGHT=Hi;
rcentrep.x=ORI_WIDTH/2+CenShiftX;     %real original center(rotating center)
rcentrep.y=ORI_HEIGHT/2+CenShiftY;

haupleft.x=rcentrep.x-harectwid/2;
haupleft.y=rcentrep.y-haupheight;
hadownleft.x=rcentrep.x-harectwid/2;
hadownleft.y=rcentrep.y+hadownheight;
haupright.x=rcentrep.x+harectwid/2;
haupright.y=rcentrep.y-haupheight;
hadownright.x=rcentrep.x+harectwid/2;
hadownright.y=rcentrep.y+hadownheight;

% Generate Mask Matrx
areamask=zeros(Hi,Wi);
for li=1:Hi
    for lj=1:Wi
        if li>haupleft.y&&li<hadownleft.y&&lj>haupleft.x&&lj<haupright.x
            areamask(li,lj)=1;
            continue;
        end
    end
end


% Save the result to output file
outputf='RemovalParameters.mat';
save(outputf,'CloudThres','MaxDiffRan','BLUETHRES_HA','BLUETHRES_SB','areamask','MinALLThres');

end