function H_all=test_HRange2MVMatrix(tmhmatf,H_range)
t=load(tmhmatf);
H_x=t.H_x;
H_y=t.H_y;

Hl=length(H_range);
H_all=zeros(size(H_x));
for i=1:Hl
    H=H_range(i);
    tmph=abs(H_x-H);
    if isempty(find(tmph<100,1))
        continue;
    end
    [~,tmx]=ind2sub(size(tmph),find(tmph==min(min(tmph))));
    x=round(mean(tmx));
    %mv_x=x-241;
    tmph=abs(H_y-H);
    [tmy,~]=ind2sub(size(tmph),find(tmph==min(min(tmph))));
    y=round(mean(tmy));
    %mv_y=y-241;
    H_all(y,x)=H;
end


end