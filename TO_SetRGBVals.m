function imma=TO_SetRGBVals(imma_ref,imma,startp,recSize)

y=startp(1);
x=startp(2);
recH=recSize(1);
recW=recSize(2);

% Rr=imma_ref(:,:,1);
% Gr=imma_ref(:,:,2);
% Br=imma_ref(:,:,3);
% 
% imma_set=imma;
% R=imma(:,:,1);
% G=imma(:,:,2);
% B=imma(:,:,3);
% 
% R(y:y+recH-1,x:x+recW-1)=Rr(y:y+recH-1,x:x+recW-1);
% G(y:y+recH-1,x:x+recW-1)=Gr(y:y+recH-1,x:x+recW-1);
% B(y:y+recH-1,x:x+recW-1)=Br(y:y+recH-1,x:x+recW-1);
% 
% imma_set(:,:,1)=R;
% imma_set(:,:,2)=G;
% imma_set(:,:,3)=B;



imma(y:y+recH-1,x:x+recW-1,1)=imma_ref(y:y+recH-1,x:x+recW-1,1);
imma(y:y+recH-1,x:x+recW-1,2)=imma_ref(y:y+recH-1,x:x+recW-1,2);
imma(y:y+recH-1,x:x+recW-1,3)=imma_ref(y:y+recH-1,x:x+recW-1,3);



end