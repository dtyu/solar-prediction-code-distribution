function test_immean(imma,n)
r=double(imma(:,:,1));
g=double(imma(:,:,2));
b=double(imma(:,:,3));
imma_lum=TO_rgb2lum(imma);
invalidma=(imma_lum<10);
r(invalidma)=nan;
g(invalidma)=nan;
b(invalidma)=nan;
invalidma_3d=logical(TO_3DMaskfrom2D(invalidma));
h = fspecial('average', n);
r1=round(filter2(h,r));
g1=round(filter2(h,g));
b1=round(filter2(h,b));
imma1(:,:,1)=r1;
imma1(:,:,2)=g1;
imma1(:,:,3)=b1;
imma1=uint8(imma1);
imma(invalidma_3d)=imma1(invalidma_3d);


figure();
subplot(1,2,1);
imshow(imma);
subplot(1,2,2);
imshow(imma1);


end