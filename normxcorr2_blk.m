% VERSION 2.0   2013/11/07
%   1. if template or A BLACK then cc = 0
%   2. if template std = 0 then cc = 0
%   3. if template is larger than A then cc=0;
%   3. if NO BLACK, then call default normxcorr2. Otherwise call c-version normxcorr2_c
function cc=normxcorr2_blk(template,A)

% Cont. 1: Either template or A is empty
if isempty(find(template~=0,1)) || isempty(find(A~=0,1)) 
    disp('[WARNING]: TEMPLATE OR A IS EMPTY. CC IS SET TO BE 0');
    cc=0;
    return;
end


% Cont. 2: Either template or A is empty
if 0==std(template(:))
    disp('[WARNING]: TEMPLATE STD IS 0. CC IS SET TO BE 0');
    cc=0;
    return;
end


% Cont. 3: template is bigger than A
if size(template,1)>size(A,1) || size(template,2)>size(A,2)
    disp('[WARNING]: TEMPLATE SHOULD NOT BE LARGER THAN A. CC IS SET TO BE 0');
    cc=0;
    return;
end


% Condt. 4: template and A are not empty. Use the default normxcorr
if ~isempty(find(template==0,1))
    bBlack_t=true;
else
    bBlack_t=false;
end
if ~isempty(find(A==0,1))
    bBlack_A=true;
else
    bBlack_A=false;
end

if ~bBlack_A && ~bBlack_t
    cc=normxcorr2(template,A);
else
    % c version implementation of normxcorr2_blk
    %save('crash.mat','template','A');
    cc=normxcorr2_c(uint8(template),uint8(A));
    %cc=normxcorr2(template,A);
end
cc(isnan(cc))=0;    % set NaN as 0 indicating invalid


end