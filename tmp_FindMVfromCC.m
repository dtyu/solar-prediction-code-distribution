function [MV_vec,maxcc,cc_vals]=tmp_FindMVfromCC(cc_ma,tempSize,ASize,H_range,H)
[Hc,Wc,H_l]=size(cc_ma);
SearchWinSizeH=round((ASize(1)-tempSize(1))/2);
SearchWinSizeW=round((ASize(2)-tempSize(2))/2);
indexi=find(H_range==H,1);
assert(length(H_range)==H_l,'cc matrix should have the same dimension of H range!');
assert(SearchWinSizeH==SearchWinSizeW,'Search Windows(padding) is set to be the same!');
assert(~isempty(indexi),'H is not in H range!');
assert(Hc==ASize(1)+tempSize(1)-1&&Wc==ASize(2)+tempSize(2)-1,'cc matrix doesn''t match dimension of template and A!');

cc_vals=cc_ma(:,:,indexi);
[maxcc,ind]=max(cc_vals(:));
[tmy,tmx]=ind2sub([Hc,Wc],ind);
MV_vec=[tmy-tempSize(1)-SearchWinSizeH tmx-tempSize(2)-SearchWinSizeW];


end