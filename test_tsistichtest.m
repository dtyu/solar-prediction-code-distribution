function test_tsistichtest
startup;
tsiDirs={'./undist_20130612/tsi1_mat/','./undist_20130612/tsi2_mat/','./undist_20130612/tsi3_mat/'};
otsiDirs={'./otsi1/','./otsi2/','./otsi3/'};
nTSI=3;
tsi1Dir=tsiDirs{1};
tsi2Dir=tsiDirs{2};
tsi3Dir=tsiDirs{3};
otsi1Dir=otsiDirs{1};
otsi2Dir=otsiDirs{2};
otsi3Dir=otsiDirs{3};
mkdir(otsi1Dir);
mkdir(otsi2Dir);
mkdir(otsi3Dir);
alltsi1=dir(tsi1Dir);
[nfiles1,~]=size(alltsi1);

imma_avgf='./imma_avgf.mat';
t=load(imma_avgf);
imma1_avg=t.imma1_avg_2r;
imma2_avg=t.imma2_avg_2r;
imma3_avg=t.imma3_avg_2r;

for i =1:nfiles1
    filename=alltsi1(i).name;
    f1=sprintf('%s%s',tsi1Dir,filename);
    tmdv=GetDVFromImg(filename);
    if tmdv==0
        continue;
    end
    tmdn=datenum(tmdv);
    if tmdn~=datenum([2013 06 09 17 0 0])
        continue;
    end
    filename2=GetImgFromDV_BNL(tmdv,TSI2);
    filename3=GetImgFromDV_BNL(tmdv,TSI3);
    f2=sprintf('%s%s.mat',tsi2Dir,filename2(1:end-4));
    f3=sprintf('%s%s.mat',tsi3Dir,filename3(1:end-4));
    try
        t1=load(f1,'imma');
        imma1=t1.imma;
        t1=load(f2,'imma');
        imma2=t1.imma;
        t1=load(f3,'imma');
        imma3=t1.imma;
    catch
        continue;
    end
    [ma1,cldind1]=test_GenCldMask_2r(imma1,imma1_avg);
    [ma2,cldind2]=test_GenCldMask_2r(imma2,imma2_avg);
    [ma3,cldind3]=test_GenCldMask_2r(imma3,imma3_avg);
    %   1. Block Dividing
    [bstarty,bstartx,bendy,bendx,nBlocky,nBlockx,blockvals]=DivideBlocks(...
        cldind3,[InnerBlockSize InnerBlockSize]);
    
    %   2. Block classfication(1 = cloud ,0 = sky)
    blkclass=zeros(nBlocky,nBlockx);
    for byi=1:nBlocky
        for bxi=1:nBlockx
            if isempty(find(blockvals(byi,bxi,:,:)==1,1))
                blkclass(byi,bxi)=0;
            else
                blkclass(byi,bxi)=1;
            end
        end
    end
    
    %   3. Cld Block match
    H_ALL=500:200:3000; % test only
    H_l=length(H_ALL);
    H_corrvals=zeros(H_l,1);
    H_ma=zeros(nBlocky,nBlockx);
    Cof_ma=zeros(nBlocky,nBlockx);
    for byi=1:nBlocky
        for bxi=1:nBlockx
            if blkclass(byi,bxi)~=1
                continue;
            end
            %             if bxi<35 || byi<30
            %                 continue;
            %             end
            for Hi =1:H_l
                H=H_ALL(Hi);
                blkstarty=bstarty(byi,bxi);
                blkstartx=bstartx(byi,bxi);
                blkendy=bendy(byi,bxi);
                blkendx=bendx(byi,bxi);
                
                ltpoint3=round([blkstarty blkstartx]);  %lefttop
                rbpoint3=round([blkendy blkendx]);      %right bottom
                [u_x,u_y]=TO_Undist2Undist(ltpoint3(2),ltpoint3(1),H,TSI3);
                ltpoint1=round([u_y(1) u_x(1)]);
                ltpoint2=round([u_y(2) u_x(2)]);
                [u_x,u_y]=TO_Undist2Undist(rbpoint3(2),rbpoint3(1),H,TSI3);
                rbpoint1=round([u_y(1) u_x(1)]);
                rbpoint2=round([u_y(2) u_x(2)]);
                % plot for debug
                %             figure();
                %             subplot(1,3,1);
                %             imshow(imma1);
                %             hold on;
                %             rectangle('Position',[ltpoint1(2) ltpoint1(1) rbpoint1(2)-ltpoint1(2)+1 rbpoint1(1)-ltpoint1(1)+1],...
                %                 'LineStyle','--','EdgeColor','r');
                %             subplot(1,3,2);
                %             imshow(imma3);
                %             hold on;
                %             rectangle('Position',[ltpoint3(2) ltpoint3(1) rbpoint3(2)-ltpoint3(2)+1 rbpoint3(1)-ltpoint3(1)+1],...
                %                 'LineStyle','--','EdgeColor','r');
                %             subplot(1,3,3);
                %             imshow(imma2);
                %             hold on;
                %             rectangle('Position',[ltpoint2(2) ltpoint2(1) rbpoint2(2)-ltpoint2(2)+1 rbpoint2(1)-ltpoint2(1)+1],...
                %                 'LineStyle','--','EdgeColor','r');
                %             hold off;
                if ltpoint2(1)<1 || ltpoint2(2)<1 || rbpoint2(1)>UNDISTH || rbpoint2(2)>UNDISTW ||...
                    ltpoint3(1)<1 || ltpoint3(2)<1 || rbpoint3(1)>UNDISTH || rbpoint3(2)>UNDISTW
                    continue;
                end
                imma2_blk=imma2(ltpoint2(1):rbpoint2(1),ltpoint2(2):rbpoint2(2));
                imma3_blk=imma3(ltpoint3(1):rbpoint3(1),ltpoint3(2):rbpoint3(2));
                if isempty(find(imma2_blk~=0,1))
                    continue;   % not valid block
                end
                tmma=size(imma2_blk)>=size(imma3_blk);
                if find(tmma==0,1)
                    R=normxcorr2(imma2_blk,imma3_blk);
                else
                    try
                        R=normxcorr2(imma3_blk,imma2_blk);
                    catch
                        byi,bxi
                    end
                end
                [Cof,imax]=max(R(:));
                %[ypeak, xpeak] = ind2sub(size(R),imax(1));
                H_corrvals(Hi)=Cof;
            end
            [tmval,maxi]=max(H_corrvals);
            H_ma(byi,bxi)=H_ALL(maxi);
            Cof_ma(byi,bxi)=tmval;
        end
    end
    H_ma;
end


end