function [a,b]=FindLinearApprox(uparray,downarray,LPDirect)
if(LPDirect=='x')
    p1=polyfit(uparray(:,1),uparray(:,2),1);
    p2=polyfit(downarray(:,1),downarray(:,2),1);
else
    p1=polyfit(uparray(:,2),uparray(:,1),1);
    p2=polyfit(downarray(:,2),downarray(:,1),1);
end
a=[p1(1);p2(1)];
b=[p1(2);p2(2)];
end